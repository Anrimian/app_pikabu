package com.emirinay.linkstest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String PROFILE_LINK = "http://pikabu.ru/profile/moderator";
    private static final String STORY_LINK = "http://pikabu.ru/story/neudobno_poluchilos_3997798#comment_61034043";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tabanim_toolbar);
        setSupportActionBar(toolbar);
        HtmlTextView textView = (HtmlTextView) findViewById(R.id.textView);
        ArrayList<String> links = new ArrayList<>();
        links.add("http://pikabu.ru/story/dayosh_bumazhnuyu_kameru_v_kazhdyiy_dom_3217774");
        links.add("http://pikabu.ru/story/igra_kak_v_realnoy_zhizni_4049812#comment_62228337");
        links.add("http://pikabu.ru/story/batareyki_aaaa_4039720#comments");
        links.add("http://pikabu.ru/story/batareyki_aaaa_4039720#comment_61993055");
        links.add("http://pikabu.ru/story/postrazoblachenie_4121884#comment_63927579");
        links.add("http://pikabu.ru/story/astrolog_iz_indii_cherez_sud_potreboval_zakryit_bitvu_yekstrasensov_4132990#comments");
        links.add("http://pikabu.ru/community/business/new");
        links.add(STORY_LINK);
        links.add(PROFILE_LINK);
        textView.setHtmlText(getHtmlText(links));
        //textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private String getHtmlText(ArrayList<String> links) {
        StringBuilder builder = new StringBuilder();
        for (String link : links) {
            builder.append("<p>");
            builder.append("<a href=");
            builder.append(link);
            builder.append(">");
            builder.append(link);
            builder.append("</a>");
            builder.append("</p>");
        }
        return builder.toString();
    }
}
