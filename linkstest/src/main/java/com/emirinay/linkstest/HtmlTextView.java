package com.emirinay.linkstest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.Touch;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

/**
 * Created on 21.09.2015.
 */
public class HtmlTextView extends TextView {

    private boolean linkHit;

    private LinkOnClickListener listener;

    public HtmlTextView(Context context) {
        super(context);
    }

    public HtmlTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HtmlTextView(
            Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        linkHit = false;
        super.onTouchEvent(event);
        return linkHit;
    }

    public void setHtmlText(String html) {
        if (html == null) {
            html = "";
        }
        CharSequence sequence = trimTrailingWhitespace(Html.fromHtml(html, null, null));//TODO set default image getter
        if (sequence.length() != 0) {
            SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
            URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
            for(URLSpan span : urls) {
                makeLinkClickable(strBuilder, span); //TODO links one href tag
            }
            setText(strBuilder);
        }
        setLinksClickable(true);
        setMovementMethod(LocalLinkMovementMethod.getInstance());
    }

    private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        strBuilder.removeSpan(span);
        ClickableSpan clickable = new URLSpanNoUnderline(span.getURL());
        strBuilder.setSpan(clickable, start, end, flags);
    }

    @Override
    public void setPressed(boolean pressed) {
        super.setPressed(pressed);
        invalidate();
    }

    private CharSequence trimTrailingWhitespace(CharSequence source) {
        if (source == null)
            return "";
        int i = source.length();

        while (--i >= 0) {
            if (!Character.isWhitespace(source.charAt(i))) {
                break;
            }
        }
        return source.subSequence(0, i + 1);
    }

    public void setLinkOnClickListener(LinkOnClickListener listener) {
        this.listener = listener;
    }

    public interface LinkOnClickListener {

        void onLinkClick(View view, String url);
    }

    private class URLSpanNoUnderline extends URLSpan {

        public URLSpanNoUnderline(String url) {
            super(url);
        }

        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                listener.onLinkClick(view, getURL());
            } else {
                super.onClick(view);
            }
        }
    }

    public static class LocalLinkMovementMethod extends LinkMovementMethod {

        private static LocalLinkMovementMethod sInstance;

        public static LocalLinkMovementMethod getInstance() {
            if (sInstance == null) {
                sInstance = new LocalLinkMovementMethod();
            }
            return sInstance;
        }

        @Override
        public boolean onTouchEvent(@NonNull TextView widget,
                                    @NonNull Spannable buffer, @NonNull MotionEvent event) {
            int action = event.getAction();

            if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_DOWN) {
                int x = (int) event.getX();
                int y = (int) event.getY();

                x -= widget.getTotalPaddingLeft();
                y -= widget.getTotalPaddingTop();

                x += widget.getScrollX();
                y += widget.getScrollY();

                Layout layout = widget.getLayout();
                int line = layout.getLineForVertical(y);
                int off = layout.getOffsetForHorizontal(line, x);

                ClickableSpan[] link = buffer.getSpans(
                        off, off, ClickableSpan.class);

                if (link.length != 0) {
                    if (action == MotionEvent.ACTION_UP) {
                        link[0].onClick(widget);
                        Selection.removeSelection(buffer);
                    } else {
                        Log.d(getClass().getSimpleName(), "setSelection");
                        Selection.setSelection(buffer,
                                buffer.getSpanStart(link[0]),
                                buffer.getSpanEnd(link[0]));
                    }

                    if (widget instanceof HtmlTextView){
                        ((HtmlTextView) widget).linkHit = true;
                    }
                    return true;
                } else {
                    Selection.removeSelection(buffer);
                    Touch.onTouchEvent(widget, buffer, event);
                    return false;
                }
            }
            return Touch.onTouchEvent(widget, buffer, event);
        }
    }
}
