package tests;


import com.emirinay.tools.TextUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pikabu.content.comments.blocks.CommentContentBlock;
import pikabu.content.comments.profile.ProfileComment;
import pikabu.content.comments.profile.UserComment;
import pikabu.content.comments.profile.parser.ProfileCommentParser;
import pikabu.content.comments.story.Comment;
import pikabu.content.comments.story.parser.xml.CommentXmlParser;
import pikabu.content.community.Community;
import pikabu.content.community.parser.CommunityParser;
import pikabu.content.profile.Profile;
import pikabu.content.profile.parser.ProfileParser;
import pikabu.content.story.Story;
import pikabu.content.story.StoryCommunityInfo;
import pikabu.content.story.parser.StoryParser;
import pikabu.data.Links;
import pikabu.main.sections.saved.stories.SaveCategory;
import pikabu.main.sections.saved.stories.parser.SaveStoriesParser;
import pikabu.user.info.UserInfo;
import pikabu.user.info.parser.UserInfoParser;
import pikabu.user.savedcategories.parser.SavedCategoriesParser;
import rx.Observable;

/**
 * Created on 25.01.2016.
 */


//@RunWith(RobolectricTestRunner.class)
//@Config(manifest=Config.NONE, sdk = 23)
public class ParserTest {

    @Test
    public void testSaveCategoryParserInUserSettings() {
        getDocumentObservable("http://pikabu.ru/editprofile.php?cmd=cats", true)
                .flatMap(SavedCategoriesParser::parseSavedCategories)
                .subscribe(categories -> {
                    displaySaveCategories(categories);
                }, throwable -> {
                    throwable.printStackTrace();
                    System.out.println("error");
                });
    }

    @Test
    public void testSaveCategoryParser() {
        getDocumentObservable("http://pikabu.ru/index.php?cmd=saved", true)
                .flatMap(SaveStoriesParser::getObservable)
                .subscribe(categories -> {
                    displaySaveCategories(categories);
                }, throwable -> {
                    throwable.printStackTrace();
                    System.out.println("error");
                });
    }

    private void displaySaveCategories(List<SaveCategory> categories) {
        for (SaveCategory category : categories) {
            System.out.println("name: " + category.getName());
            System.out.println("id: " + category.getId());
            System.out.println("stories count: " + category.getStoriesCount());
        }
    }

    @Test
    public void testUserInfoParser() {
        System.out.println("carmaF: " + TextUtils.getFloatNumbersFromString("carma: 4.5"));

        getDocumentObservable("http://pikabu.ru", true)
                .flatMap(UserInfoParser::getUserDataObservable)
                .subscribe(userInfo -> {
                    displayUserInfo(userInfo);
                }, throwable -> {
                    throwable.printStackTrace();
                    System.out.println("connection error");
                });
    }

    private void displayUserInfo(UserInfo userInfo) {
        System.out.println("userName: " + userInfo.getUserName());
        System.out.println("picture: " + userInfo.getPicture());
        System.out.println("carma: " + userInfo.getCarma());
        System.out.println("subs: " + userInfo.getSubsNum());
    }

    @Test
    public void testProfileCommentsParser() {//"http://pikabu.ru/freshitems.php";
        String link = "http://pikabu.ru/commented.php?cmd=saved_comments";

        System.out.println("Connecting...");
        getDocumentObservable(link, true)
                .flatMap(ProfileCommentParser::parseDocument)
                .subscribe(comments -> {
                    displayProfileComments(comments);
                    System.out.println("comments count: " + comments.size());
                }, throwable -> {
                    throwable.printStackTrace();
                    System.out.println("connection error");
                });
    }

    private void displayProfileComments(List<ProfileComment> comments) {
        for (ProfileComment comment : comments) {
            System.out.println("-----------------------");
            System.out.println("story title: " + comment.getStoryTitle());
            System.out.println("storyId: " + comment.getStoryId());
            System.out.println("comment-----------------");
            displayUserComment(comment.getComment());
            System.out.println("parent comment----------------");
            displayUserComment(comment.getParentComment());
        }
    }

    private void displayUserComment(UserComment comment) {
        if (comment != null) {
            System.out.println("author: " + comment.getAuthor());
            System.out.println("commentId: " + comment.getCommentId());
            System.out.println("rating: " + comment.getRating());
            System.out.println("time: " + comment.getTime());
            System.out.println("vote status: " + comment.getVoteStatus());
            System.out.println("blocks: " + comment.getContentBlocksList());
        }
    }


    @Test
    public void textCommunityParser() {
        String link = "http://pikabu.ru/community/true_story";
        try {
            System.out.println("Connecting...");
            Document doc = loadDocument(link, false);
            Community community = CommunityParser.getCommunityInstance(doc);
            displayCommunity(community);
            Assert.assertNotEquals(community, null);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("connection error");
        }
    }

    private void displayCommunity(Community community) {
        System.out.println("path name: " + community.getPathName());
        System.out.println("name: " + community.getName());
        System.out.println("backgroundImageLink: " + community.getBackgroundImageLink());
        System.out.println("headerImageLink: " + community.getHeaderImageLink());
        System.out.println("storyNumber: " + community.getStoryNumber());
        System.out.println("subsNumber: " + community.getSubsNumber());
        System.out.println("description: " + community.getDescription());
        System.out.println("rules description: " + community.getRulesDescription());
        System.out.println("owner: " + community.getCommunityOwner());

        System.out.println("moderators: " + community.getModerators());
        System.out.println("popular authors: " + community.getPopularAuthors());



    }

    @Test
    public void testProfileParser() {
        String link = "http://pikabu.ru/profile/Emirinay";
        try {
            System.out.println("Connecting...");
            Document doc = loadDocument(link, true);
            Profile profile = ProfileParser.getProfileInstance(doc);
            displayProfile(profile);
            Assert.assertNotEquals(profile, null);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("connection error");
        }
    }

    private void displayProfile(Profile profile) {
        System.out.println("name: " + profile.getName());
        System.out.println("approved info: " + profile.getApprovedInfo());
        System.out.println("picture link: " + profile.getPictureLink());
        System.out.println("register time: " + profile.getRegisterTime());
        System.out.println("user note: " + profile.getUserNote());
        System.out.println("comments count: " + profile.getCommentsCount());
        System.out.println("gender: " + profile.getGender());
        System.out.println("minuses: " + profile.getMinusesNumber());
        System.out.println("pluses: " + profile.getPlusesNumber());
        System.out.println("rating: " + profile.getRating());
        System.out.println("story count: " + profile.getStoryCount());
        System.out.println("story in hot: " + profile.getHotStoryCount());
        System.out.println("community: " + profile.getCommunityInfoList());
        System.out.println("awards: " + profile.getAwardsList());
        System.out.println("awards count: " + profile.getAwardsList().size());

    }

    @Test
    public void testHtmlStoryParser() {
        String link = "http://pikabu.ru/best";

        System.out.println("Connecting...");
        getDocumentObservable(link, false)
                .flatMap(StoryParser::parseDocument)
                .subscribe(stories -> {
                    displayStories(stories);
                    System.out.println("stories count: " + stories.size());
                    Assert.assertEquals(stories.size(), 20);
                }, throwable -> {
                    throwable.printStackTrace();
                    System.out.println("connection error");
                });
    }

    private void displayStories(List<Story> stories) {
        for (Story story : stories) {
            System.out.println("title: " + story.getTitle());
            System.out.println("link: " + story.getLink());
            StoryCommunityInfo communityInfo = story.getCommunityInfo();
            if (communityInfo != null) {
                System.out.println("community name: " + communityInfo.getName());
                System.out.println("community link: " + communityInfo.getPathName());
            }
        }
    }

    @Test
    public void testXmlCommentParser() {
        int storyId = 325140;// - old images 1331459;
        try {
            System.out.println("Connecting...");
            Response response = loadXmlComments(storyId);
            String xmlText = response.body().string();
            ArrayList<Comment> comments = CommentXmlParser.parseXmlComments(xmlText);
            Assert.assertNotEquals(comments, null);
            displayComments(comments);
            System.out.println("comments number: " + comments.size());
        } catch (IOException e) {
            System.out.println("Connection failed");
            e.printStackTrace();
        }
    }

    public static Response loadXmlComments(int storyId) throws IOException {
        String commentLink = Links.LINK_XML_COMM + storyId;
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(45, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder().url(commentLink).get().build();
        Response response = client.newCall(request).execute();
        System.out.println("replyComment response: " + response.toString());
        return response;
    }

    private Observable<Document> getDocumentObservable(String link, boolean login) {
        return Observable.create(subscriber -> {
           try {
               Document document = loadDocument(link, login);
               subscriber.onNext(document);
           } catch (IOException e) {
               subscriber.onError(e);
           }
        });
    }

    private Document loadDocument(String link, boolean login) throws IOException {
        OkHttpClient client;
        if (login) {
            TestHttpClient testHttpClient = TestHttpClient.getInstance();
            testHttpClient.login();
            client = testHttpClient.getOkHttpClient();
        } else {
            client = new OkHttpClient.Builder()
                    .connectTimeout(45, TimeUnit.SECONDS)
                    .readTimeout(45, TimeUnit.SECONDS)
                    .build();
        }
        Request request = new Request.Builder().url(link).build();
        Response response =  client.newCall(request).execute();
        return Jsoup.parse(response.body().string());
    }

    private void displayComments(ArrayList<Comment> comments) {
        for (Comment comment : comments) {
            System.out.println("-----------------------");
            System.out.println("getCommentId: " + comment.getCommentId());
            System.out.println("getParentCommentId: " + comment.getParentCommentId());
            System.out.println("getRating: " + comment.getRating());
            System.out.println("getAuthor: " + comment.getAuthor());
            System.out.println("getTime: " + comment.getTime());
            ArrayList<CommentContentBlock> blocks = comment.getContentBlocks();
            System.out.println("blocks: " + blocks);
        }
    }
}
