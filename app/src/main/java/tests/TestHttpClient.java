package tests;

import android.support.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pikabu.data.Links;

/**
 * Created on 24.08.2016.
 */
public class TestHttpClient {

    private static final long READ_TIMEOUT_TIME = 45;
    private static final long CONNECT_TIMEOUT_TIME = 45;

    private OkHttpClient okHttpClient;
    private MyCookieJar myCookieJar;

    private volatile static TestHttpClient client;

    private TestHttpClient() {
        myCookieJar = new MyCookieJar();
        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_TIME, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT_TIME, TimeUnit.SECONDS)
                .cookieJar(myCookieJar)
                .build();
    }

    public static TestHttpClient getInstance() {
        if (client == null) {
            synchronized (TestHttpClient.class) {
                if (client == null) {
                    client = new TestHttpClient();
                }
            }
        }
        return client;
    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public void saveCookies() {
    }

    public void clearCookies() {
        myCookieJar.clearCookies();
        saveCookies();
    }

    public void login() {
        try {
            RequestBody requestBody = new FormBody.Builder()
                    .add("mode", "login")
                    .add("username", "Emirinay@gmail.com")
                    .add("password", "pi14215")
                    .add("remember", "1")
                    .build();

            TestHttpClient myHttpClient = TestHttpClient.getInstance();
            String phpSessionId = getPhpSessionId();
            Headers headers = getHeaders(phpSessionId);
            Request request = new Request.Builder().url("http://pikabu.ru/ajax/ajax_login.php").headers(headers).post(requestBody).build();
            Response response = myHttpClient.getOkHttpClient().newCall(request).execute();
            if (response.code() != 200) {
                System.out.println("fail response: " + response.toString());
            }

            String responseBody = response.body().string();
            System.out.println("responseBody: " + responseBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getPhpSessionId() throws IOException {
        Cookie phpSessionCookie = myCookieJar.getCookie("PHPSESS");
        if (phpSessionCookie == null) {
            getFirstCookies();
        }
        return myCookieJar.getCookie("PHPSESS").value();
    }

    private void getFirstCookies() throws IOException {
        Request getHeadersRequest = new Request.Builder().url(Links.LINK_SITE).get().build();
        Response response = okHttpClient.newCall(getHeadersRequest).execute();
        if (response.code() != 200) {
            throw new IOException("fail to get php session id");
        }
    }

    private Headers getHeaders(@NotNull String phpSessionId) {
        return new Headers.Builder()
                .add("Referer", Links.LINK_SITE)
                .add("X-Csrf-Token", phpSessionId)
                .build();
    }

    /**
     * Created on 13.12.2016.
     */

    public static class MyCookieJar implements CookieJar {

        private Map<String, Cookie> cookieMap = new HashMap<>();

        @Override
        public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
            if (!url.host().equals(Links.PIKABU_HOST)) {
                return;
            }
            for (Cookie cookie : cookies) {
                cookieMap.put(cookie.name(), cookie);
            }
        }

        @Override
        public List<Cookie> loadForRequest(HttpUrl url) {
            if (!url.host().equals(Links.PIKABU_HOST)) {
                return new ArrayList<>();
            }
            //System.out.println("load cookies for: " + url);
            //displayCookies();
            return new ArrayList(cookieMap.values());
        }

        private void displayCookies() {
            for (Cookie cookie : cookieMap.values()) {
                System.out.println("cookie-----");
                System.out.println("name: " + cookie.name());
                System.out.println("value: " + cookie.value());
            }
        }

        @Nullable
        public Cookie getCookie(String name) {
            return cookieMap.get(name);
        }

        public List<Cookie> getCookies() {
            return new ArrayList(cookieMap.values());
        }

        public void clearCookies() {
            cookieMap.clear();
        }
    }
}
