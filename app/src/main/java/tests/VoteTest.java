package tests;

import org.junit.Test;

import pikabu.content.Vote;

/**
 * Created on 04.04.2016.
 */
public class VoteTest {

    @Test
    public void voteTest() {
        Vote[] oldVotes = {null, Vote.NOTHING, Vote.UP, Vote.DOWN};
        Vote[] newVotes = {null, Vote.NOTHING, Vote.UP, Vote.DOWN};
        for (Vote oldVote : oldVotes) {
            for (Vote newVote : newVotes) {
                System.out.println("oldVote: " + oldVote);
                System.out.println("newVote: " + newVote);
                System.out.println("result: " + Vote.mergeVote(oldVote, newVote));
                System.out.println("-------------");
            }
        }
    }
}
