package tests;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import pikabu.utils.network.UrlUtils;

/**
 * Created on 25.04.2016.
 */
public class TextTest {

    @Test
    public void testText() {
        String text = "this is a test for android and textView";
        System.out.println("text: " + text + ", size: " + text.length());
        int index = 7;

        char startLetter = text.charAt(index);
        System.out.println("index letter: " + startLetter);
        System.out.println("index: " + index);

        int startIndex = index;
        int endIndex = index;

        while (startIndex > 0) {
            int i = startIndex - 1;
            if (text.charAt(i) == ' ') {
                break;
            }
            startIndex--;
        }

        System.out.println("start index: " + startIndex);


        while (endIndex < text.length() - 1) {
            if (text.charAt(endIndex) == ' ') {
                break;
            }
            endIndex++;
        }

        System.out.println("end index: " + endIndex);
        System.out.println("selected word: \"" + text.substring(startIndex, endIndex) + "\"");
    }


    @Test
    public void testEqualsUrl() {
        String link1 = "http://pikabu.ru/story/proshu_pomoshchi_spb_propala_doch_druga_chast_1_4002031#comment_61153593";
        String link2 = "http://pikabu.ru/story/proshu_pomoshchi_spb_propala_doch_druga_chast_2_4002031#comment_61153592";
        boolean equals = UrlUtils.equalsStoryLinks(link1, link2);
        System.out.println("link1: " + link1);
        System.out.println("link2: " + link2);
        System.out.println("equals: " + equals);
    }

    @Test
    public void testStoryId() {
        String storyLink = "http://pikabu.ru/story/zhestoko_4673667?f=2#story-info";
        System.out.println("storyLink: " + storyLink);
        int storyId = UrlUtils.getStoryIdFromLink(storyLink);
        System.out.println("storyLink: " + storyLink);
        System.out.println("storyId: " + storyId);
    }

    @Test
    public void testTargetCommentLink() {
        String storyLink = "http://pikabu.ru/story/zhestoko_4673667?f=2#story-info";
        int targetCommentId = UrlUtils.selectTargetCommentId(storyLink);
        System.out.println("storyLink: " + storyLink);
        System.out.println("targetCommentId: " + targetCommentId);
    }

    @Test
    public void testCommunityPathName() {
        String[] links = {"http://pikabu.ru/community/business",
                "http://pikabu.ru/community/create/best",
                "http://pikabu.ru/community/create?page=2",
                "http://pikabu.ru/community/create/best?page=2",
                "m.pikabu.ru/community/create/best?page=3"};
        for (String link : links) {
            System.out.println("link: " + link);
            System.out.println("pathName: " + UrlUtils.getCommunityPathName(link));
            System.out.println("---------------");
        }
    }

    @Test
    public void primitiveValueMapTest() {
        String word = "test";
        Map<String, Integer> freq = new HashMap<>();
        Integer countObj = freq.get(word);
        int count = 0;
        if (countObj != null) {
            count = countObj;
        }
        System.out.println("count: " + count);
        count++;
        freq.put(word, count);
        System.out.println("count: " + freq.get(word));
    }
}
