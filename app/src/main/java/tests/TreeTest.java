package tests;

import com.emirinay.tools.treelist.Node;
import com.emirinay.tools.treelist.Tree;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created on 04.02.2016.
 */
public class TreeTest {

    @Test
    public void stressTestTree() {
        Tree tree = new Tree();
        Node prev = null;
        for (int i = 0; i < 700; i++) {
            Node node = tree.createNode("" + i, prev);
            prev = node;
        }

        tree.collapseNodes(tree.getVisibleNodeOnPosition(0));
        tree.expandNodes(tree.getVisibleNodeOnPosition(0));
    }


    @Test
    public void testTree() {
        Tree tree = new Tree(0);
        Node root1 = tree.createNode("Root 1", null);
        Node root2 = tree.createNode("Root 2", null);
        Node root3 = tree.createNode("Root 3", null);
        Node child11 = tree.createNode("child 11", root1);
        Node child12 = tree.createNode("child 12", root1);
        Node child13 = tree.createNode("child 13", root1);
        Node child31 = tree.createNode("child 31", root3);
        Node child111 = tree.createNode("child 111", child11);
        tree.collapseNodes(child11);

        Node child21 = tree.createNode("child 21", root2);
        Node child112 = tree.createNode("child 112", child11);
        Node child131 = tree.createNode("child 131", child13);
        Node child113 = tree.createNode("child 113", child11);
        Node child1111 = tree.createNode("child 1111", child111);
        Node child11111 = tree.createNode("child 11111", child1111);
        Node child111111 = tree.createNode("child 111111", child11111);
        tree.collapseNodes(child11);
        tree.collapseNodes(child1111);

        tree.expandNodes(root1);
        tree.collapseNodes(child11);
        tree.expandNodes(child11);
        tree.expandNodes(child131);

        tree.collapseNodes(root1);
        tree.collapseNodes(root2);
        tree.collapseNodes(root3);

        tree.expandNodes(root1);
        tree.expandNodes(root2);
        tree.expandNodes(root3);

        System.out.println("Node count: " + root1.getParent().getChildrenCount());
        System.out.println("Visible node count: " + tree.getVisibleNodeCount());
        displayTree(tree);
        displayTreeInfo(tree);
    }

    @Test
    public void testTree2() {
        Tree tree = new Tree();
        Node root1 = tree.createNode("Root 1", null);
        Node root2 = tree.createNode("Root 2", null);
        Node child11 = tree.createNode("child 11", root1);
        Node child111 = tree.createNode("child 12", child11);
        Node child1111 = tree.createNode("child 13", child111);
        Node child11111 = tree.createNode("child 111", child1111);

        tree.collapseNodes(child1111);
        tree.collapseNodes(child11);

        Node targetNode = child11111;
        if (targetNode.hasCollapsedParent()) {
                tree.expandBranch(targetNode);
        }

        System.out.println("Node count: " + root1.getParent().getChildrenCount());
        System.out.println("Visible node count: " + tree.getVisibleNodeCount());
        displayTree(tree);
        displayTreeInfo(tree);
    }

    private void displayTreeInfo(Tree tree) {
        for (int i = 0; i < tree.getVisibleNodeCount(); i++) {
            Node node = tree.getVisibleNodeOnPosition(i);
            System.out.println("-------------");
            System.out.println("Node: " + node.getNodeData());
            System.out.println("Data Level: " + node.getDataLevel());
            System.out.println("Index: " + node.getIndex());
            System.out.println("Index in list: " + i);
            System.out.println("Children count: " + node.getChildrenCount());
            System.out.println("Visible children count: " + node.getVisibleChildrenCount());
            System.out.println("hasCollapsedParent: " + node.hasCollapsedParent());
            assertEquals(node.getIndex(), i);
            assertEquals(node.hasCollapsedParent(), false);
        }
    }

    private void displayTree(Tree tree) {
        for (int i = 0; i < tree.getVisibleNodeCount(); i++) {
            Node node = tree.getVisibleNodeOnPosition(i);
            System.out.println(lines(node.getDataLevel()) + node.getNodeData()
                    + (node.isCollapsed() ? " (collapsed)" : ""));
        }
    }

    private String lines(int lines) {
        StringBuilder linesStr = new StringBuilder();
        for (int i = 0; i < lines; i++) {
            linesStr.append("| ");
        }
        return linesStr.toString();
    }
}
