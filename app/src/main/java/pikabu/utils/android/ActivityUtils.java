package pikabu.utils.android;

import android.app.Activity;
import android.content.Intent;

import com.emirinay.pikabuapp.R;

import pikabu.Preferences;

/**
 * Created on 24.01.2017.
 */

public class ActivityUtils {

    public static void setTheme(Activity activity) {
        String theme = Preferences.get().getString("theme", "light");
        int themeId = getThemeId(theme);
        activity.setTheme(themeId);

    }

    private static int getThemeId(String theme) {
        switch (theme) {
            case "dark" : return R.style.AppTheme_Dark;
            default: return R.style.AppTheme_Light;
        }
    }

    public static void recreateActivity(Activity activity) {
        Intent intent = activity.getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        activity.finish();
        activity.overridePendingTransition(0, 0);
        activity.startActivity(intent);
        activity.overridePendingTransition(0, 0);
    }
}
