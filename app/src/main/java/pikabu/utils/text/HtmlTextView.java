package pikabu.utils.text;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.method.Touch;
import android.text.style.ClickableSpan;
import android.text.style.QuoteSpan;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;

/**
 * Created on 21.09.2015.
 */
public class HtmlTextView extends TextView {

    private boolean linkHit;

    private boolean longTap = false;
    private boolean clickableLinks = true;

    private LinkOnClickListener linkOnClickListener;
    private LongTapListener longTapListener;

    private GestureDetector gestureDetector;

    public HtmlTextView(Context context) {
        this(context, null);
    }

    public HtmlTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HtmlTextView(
            Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setUp();
    }

    private void setUp() {
        gestureDetector = new GestureDetector(getContext(), new GestureListener());
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        linkHit = false;
        super.onTouchEvent(event);
        if (linkHit) {
            return true;
        }

        if (!longTap) {
            return false;
        } else {
            gestureDetector.onTouchEvent(event);
            return true;//true - error in on click, false - error in long click
        }
    }

    @Override
    public void setPressed(boolean pressed) {
        super.setPressed(pressed);
        invalidate();
    }

    public void setHtmlText(String text) {
        if (text == null) {
            text = "";
        }
        text = makeItalicQuote(text);

        CharSequence sequence = trimTrailingWhitespace(Html.fromHtml(text, null, null));//TODO set default image getter
        if (sequence.length() != 0) {
            SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
            URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
            for (URLSpan span : urls) {
                makeLinkClickable(strBuilder, span);
            }
            replaceQuoteSpans(strBuilder);
            setText(strBuilder);
        }

        setMovementMethod(LocalLinkMovementMethod.getInstance());
    }

    private void replaceQuoteSpans(Spannable spannable) {
        QuoteSpan[] quoteSpans = spannable.getSpans(0, spannable.length(), QuoteSpan.class);
        for (QuoteSpan quoteSpan : quoteSpans) {
            int start = spannable.getSpanStart(quoteSpan);
            int end = spannable.getSpanEnd(quoteSpan);
            int flags = spannable.getSpanFlags(quoteSpan);
            spannable.removeSpan(quoteSpan);

            int textColor = ContextCompat.getColor(getContext(), R.color.quote_text_color);
            int backgroundColor = ContextCompat.getColor(getContext(), R.color.quote_background_color);
            spannable.setSpan(new CustomQuoteSpan(textColor, backgroundColor, backgroundColor, 4, 0),
                    start,
                    end,
                    flags);
        }
    }

    private String makeItalicQuote(String text) {
        String result = text.replace("<blockquote>", "<i> <blockquote>");
        return result.replace("</blockquote>", "</blockquote> </i>");
    }

    private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        strBuilder.removeSpan(span);
        ClickableSpan clickable = new CustomURLSpan(span.getURL());
        strBuilder.setSpan(clickable, start, end, flags);
    }

    private CharSequence trimTrailingWhitespace(CharSequence source) {
        if (source == null)
            return "";
        int i = source.length();

        while (--i >= 0) {
            if (!Character.isWhitespace(source.charAt(i))) {
                break;
            }
        }
        return source.subSequence(0, i + 1);
    }

    public interface LinkOnClickListener {

        void onLinkClick(View view, String url);
    }

    public void setLinkOnClickListener(LinkOnClickListener listener) {
        if (listener != null) {
            clickableLinks = true;
        }
        this.linkOnClickListener = listener;
    }

    public interface LongTapListener {
        void onLongTap();
    }

    /**
     * if set this, textview lose the ability to respond of listview clicks
     *
     * @param longTapListener
     */
    public void setLongTapListener(LongTapListener longTapListener) {
        longTap = longTapListener != null;
        gestureDetector.setIsLongpressEnabled(longTap);
        this.longTapListener = longTapListener;
    }

    public boolean isLongTap() {
        return longTap;
    }

    public void setLongTap(boolean longTap) {
        this.longTap = longTap;
    }

    public void setClickableLinks(boolean clickableLinks) {
        this.clickableLinks = clickableLinks;
    }

    private class CustomURLSpan extends URLSpan {

        public CustomURLSpan(String url) {
            super(url);
        }

        @Override
        public void onClick(View view) {
            if (!clickableLinks) {
                return;
            }

            if (linkOnClickListener != null) {
                linkOnClickListener.onLinkClick(view, getURL());
            } else {
                super.onClick(view);
            }
        }
    }

    public static class LocalLinkMovementMethod extends LinkMovementMethod {

        private static LocalLinkMovementMethod sInstance;

        public static LocalLinkMovementMethod getInstance() {
            if (sInstance == null) {
                sInstance = new LocalLinkMovementMethod();
            }
            return sInstance;
        }

        @Override
        public boolean onTouchEvent(@NonNull TextView widget,
                                    @NonNull Spannable buffer, @NonNull MotionEvent event) {
            Log.d(getClass().getSimpleName(), "event: " + event);
            int action = event.getAction();

            if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_DOWN) {
                int x = (int) event.getX();
                int y = (int) event.getY();

                x -= widget.getTotalPaddingLeft();
                y -= widget.getTotalPaddingTop();

                x += widget.getScrollX();
                y += widget.getScrollY();

                Layout layout = widget.getLayout();
                int line = layout.getLineForVertical(y);
                int off = layout.getOffsetForHorizontal(line, x);

                ClickableSpan[] link = buffer.getSpans(
                        off, off, ClickableSpan.class);

                if (link.length != 0) {
                    if (action == MotionEvent.ACTION_UP) {
                        link[0].onClick(widget);
                        Log.d(getClass().getSimpleName(), "link up, click!");
                    } else {
                        Log.d(getClass().getSimpleName(), "link down");
                        Selection.setSelection(buffer,
                                buffer.getSpanStart(link[0]),
                                buffer.getSpanEnd(link[0]));
                    }

                    if (widget instanceof HtmlTextView) {
                        ((HtmlTextView) widget).linkHit = true;
                    }
                    return true;
                } else {
                    Selection.removeSelection(buffer);
                    Touch.onTouchEvent(widget, buffer, event);
                    return false;
                }
            }
            return Touch.onTouchEvent(widget, buffer, event);
        }
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public void onLongPress(MotionEvent event) {
            if (longTapListener != null) {
                longTapListener.onLongTap();
            }
        }
    }
}
