package pikabu.utils.text;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.emirinay.pikabuapp.R;

/**
 * Created on 18.04.2016.
 */
public class CopyTextActivity extends AppCompatActivity {
    public static final String TEXT = "text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.copy_text_view);

        setTitle(R.string.copy_text);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        HtmlTextView textView = (HtmlTextView) findViewById(R.id.textView);
        Intent intent = getIntent();
        String text = intent.getStringExtra(TEXT);
        textView.setHtmlText(text);
        textView.setClickableLinks(false);
        textView.setTextIsSelectable(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
