package pikabu.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;
import com.emirinay.tools.IoUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import pikabu.image.gif.loader.GifLoader;
import pikabu.image.gif.loader.GifResource;
import pikabu.image.glide.image.MyGlideImageLoader;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 06.05.2016.
 */
public class FileUtils {

    public static void loadAndStoreBitmap(String linkToSave, Context ctx) {
        MyGlideImageLoader.getBitmapObservable(linkToSave, ctx)
                .subscribeOn(AndroidSchedulers.mainThread())
                .flatMap(FileUtils::storeBitmap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(imageDirectory -> {
                    String message = ctx.getResources().getString(R.string.saved_in, imageDirectory);
                    Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
                }, throwable -> {
                    Toast.makeText(ctx, ((ExceptionWithInteger) throwable).getMessageId(), Toast.LENGTH_SHORT).show();
                });
    }

    private static Observable<String> storeBitmap(Bitmap bitmap) {
        return storeFile(getImageDirectory(),
                imageStorageDir -> File.createTempFile("pic", ".png", imageStorageDir),
                stream -> bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream));
    }

    public static void loadAndStoreGif(String gifLink, Context ctx) {
        GifLoader.getInstance().getGifResourceObservable(gifLink)
                .map(GifResource::getGifBytes)
                .flatMap(FileUtils::storeGif)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(imageDirectory -> {
                    String message = ctx.getResources().getString(R.string.saved_in, imageDirectory);
                    Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
                }, throwable -> {
                    Toast.makeText(ctx, ((ExceptionWithInteger) throwable).getMessageId(), Toast.LENGTH_SHORT).show();
                });
    }

    private static Observable<String> storeGif(byte[] bytes) {
        return storeFile(getGifImageDirectory(),
                imageStorageDir -> File.createTempFile("gif", ".gif", imageStorageDir),
                stream -> {
                    try {
                        stream.write(bytes);
                        return true;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                });
    }

    public static Observable<String> storeFile(String imageDirectory,
                                                FileCreator fileCreator,
                                                FileCompressor fileCompressor) {
        return Observable.create(subscriber -> {

            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                subscriber.onError(new ExceptionWithInteger(R.string.sd_card_not_mounted));
                return;
            }

            File imageStorageDir = new File(imageDirectory);
            if (!imageStorageDir.exists()){
                if (!imageStorageDir.mkdirs()){
                    subscriber.onError(new ExceptionWithInteger(R.string.error_creating_directory));
                    return;
                }
            }

            FileOutputStream fos = null;
            try {
                File pictureFile = fileCreator.createFile(imageStorageDir);
                fos = new FileOutputStream(pictureFile);
                boolean compressedResult = fileCompressor.compress(fos);
                if (!compressedResult) {
                    subscriber.onError(new Exception(new ExceptionWithInteger(R.string.error_write_image)));
                }
                subscriber.onNext(imageDirectory);
            } catch (FileNotFoundException e) {
                subscriber.onError(new ExceptionWithInteger(R.string.file_not_found));
            } catch (IOException e) {
                subscriber.onError(new ExceptionWithInteger(R.string.error_accessing_file));
            } catch (Exception e) {
                subscriber.onError(new ExceptionWithInteger(R.string.save_image_error));
            } finally {
                IoUtils.closeSilently(fos);
            }
        });
    }

    public interface FileCreator {
        File createFile(File imageStorageDir) throws IOException;
    }

    public interface FileCompressor {
        boolean compress(FileOutputStream stream);
    }

    private static String getImageDirectory() {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        StringBuilder sbImageDirectory = new StringBuilder(extStorageDirectory);
        sbImageDirectory.append("/Pikabu");
        sbImageDirectory.append("/Pictures");
        return sbImageDirectory.toString();
    }

    private static String getGifImageDirectory() {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        StringBuilder sbImageDirectory = new StringBuilder(extStorageDirectory);
        sbImageDirectory.append("/Pikabu");
        sbImageDirectory.append("/Pictures");
        sbImageDirectory.append("/Gif");
        return sbImageDirectory.toString();
    }
}
