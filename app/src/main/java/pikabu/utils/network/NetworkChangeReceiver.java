package pikabu.utils.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import pikabu.user.actions.DeferredTaskManager;

/**
 * Created on 20.11.2015.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (LoaderHelper.isNetworkConnected(context)) {
            Log.d(getClass().getSimpleName(), "Network Available Do operations");
            DeferredTaskManager.getInstance().runDeferredTasks();
        }

    }
}
