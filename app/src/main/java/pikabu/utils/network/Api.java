package pikabu.utils.network;

import android.util.Log;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Document;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pikabu.MyApplication;
import pikabu.content.Vote;
import pikabu.data.Links;
import rx.Observable;

public class Api {
    private static final String URL_LOGIN_SCRIPT = "http://pikabu.ru/ajax/ajax_login.php";
    private static final String URL_VOTE_SCRIPT = "http://pikabu.ru/ajax/dig.php";
    private static final String URL_SAVE_SCRIPT = "http://pikabu.ru/ajax/stories_actions.php";
    private static final String URL_VOTE_COMMENT_SCRIPT = "http://pikabu.ru/dig.php";
    private static final String URL_REPLY_COMMENT_SCRIPT = "http://pikabu.ru/ajax.php";

    private static final String ACTION_SAVE_STORY = "save_story+";
    private static final String ACTION_DELETE_SAVED_STORY = "save_story-";

	public static boolean login(String username, String password) throws IOException, JSONException {
        username = username.trim();

        RequestBody requestBody = new FormBody.Builder()
                .add("mode", "login")
                .add("username", username)
                .add("password", password)
                .add("remember", "1")
                .build();

        MyHttpClient myHttpClient = MyHttpClient.getInstance();
        String phpSessionId = myHttpClient.getPhpSessionId();
        Headers headers = getHeaders(phpSessionId);
        Request request = new Request.Builder().url(URL_LOGIN_SCRIPT).headers(headers).post(requestBody).build();
        Response response = myHttpClient.getOkHttpClient().newCall(request).execute();
        if (response.code() != 200) {
            return false;
        }

        String responseBody = response.body().string();
        Log.d("login", "login response: " + response.toString());
        Log.d("login", "login response body: " + responseBody);
        JSONObject jsonObject = new JSONObject(responseBody);
        if (jsonObject.getInt("logined") == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static Observable<Document> getLoginObservable(String username, String password){
        return Observable.create(subscriber -> {
            try {
                boolean result = login(username, password);
                if (result) {
                    MyHttpClient.getInstance().saveCookies();
                    Document doc = LoaderHelper.getDocument(Links.LINK_TO_SAVED_CATEGORIES);
                    subscriber.onNext(doc);
                } else {
                    subscriber.onError(new ExceptionWithInteger(R.string.wrong_name_or_password));
                }
            } catch (JSONException je) {
                je.printStackTrace();
                subscriber.onError(new ExceptionWithInteger(R.string.login_analyze_error));
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onError(new ExceptionWithInteger(R.string.login_connection_error));
            }
        });
    }

    public static final int ERROR_STATUS = Integer.MIN_VALUE;

    public static int voteStory(int storyId, Vote vote) throws IOException {
        try {
            Thread.sleep(2000);//server can ignore too often requests
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        RequestBody requestBody = new FormBody.Builder()
                .add("i", String.valueOf(storyId))
                .add("type", vote.toString())
                .build();

        MyHttpClient myHttpClient = MyHttpClient.getInstance();
        String phpSessionId = myHttpClient.getPhpSessionId();
        Headers headers = getHeaders(phpSessionId);
        Request request = new Request.Builder().url(URL_VOTE_SCRIPT).headers(headers).post(requestBody).build();
        Response response = myHttpClient.getOkHttpClient().newCall(request).execute();
        String responseBody = response.body().string();
        int status = ERROR_STATUS;
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            status = jsonObject.getInt("status");
            Log.d("api", "vote status: " + status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return status;
    }

    public static void saveStory(int storyId, int category) throws IOException  {
        RequestBody requestBody = new FormBody.Builder()
                .add("story_id", String.valueOf(storyId))
                .add("cat_id", String.valueOf(category))
                .add("action", ACTION_SAVE_STORY)
                .build();

        MyHttpClient myHttpClient = MyHttpClient.getInstance();
        String phpSessionId = myHttpClient.getPhpSessionId();
        Headers headers = getHeaders(phpSessionId);
        Request request = new Request.Builder().url(URL_SAVE_SCRIPT).headers(headers).post(requestBody).build();
        Response response = myHttpClient.getOkHttpClient().newCall(request).execute();
        Log.d("api", "save response: " + response.toString());
    }

    public static void deleteSavedStory(int storyId) throws IOException  {
        RequestBody requestBody = new FormBody.Builder()
                .add("story_id", String.valueOf(storyId))
                .add("action", ACTION_DELETE_SAVED_STORY)
                .build();

        MyHttpClient myHttpClient = MyHttpClient.getInstance();
        String phpSessionId = myHttpClient.getPhpSessionId();
        Headers headers = getHeaders(phpSessionId);
        Request request = new Request.Builder().url(URL_SAVE_SCRIPT).headers(headers).post(requestBody).build();
        Response response = myHttpClient.getOkHttpClient().newCall(request).execute();
        Log.d("api", "delete saved story response: " + response.toString());
    }

    public static Response loadXmlComments(int storyId) throws IOException {
        String commentLink = Links.LINK_XML_COMM + storyId;
        MyHttpClient myHttpClient = MyHttpClient.getInstance();
        Request request = new Request.Builder().url(commentLink).get().build();
        return myHttpClient.getOkHttpClient().newCall(request).execute();
    }

    public static Observable<String> getXmlCommentsObservable(int storyId) {
        return Observable.create(subscriber -> {
            try {
                Response response = loadXmlComments(storyId);
                if (response.code() == 403) {
                    subscriber.onError(new ExceptionWithInteger(R.string.wrong_story_id));
                } else {
                    subscriber.onNext(response.body().string());
                }
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onError(new ExceptionWithInteger(R.string.connection_error));
            }
        });
    }

    public static void voteComment(int storyId, int commentId, Vote vote) throws IOException {
        RequestBody requestBody = new FormBody.Builder()
                .add("type", "comm")
                .add("i", String.valueOf(commentId))
                .add("story", String.valueOf(storyId))
                .add("dir", vote == Vote.UP ? "1" : "0")
                .build();

        MyHttpClient myHttpClient = MyHttpClient.getInstance();
        String phpSessionId = myHttpClient.getPhpSessionId();
        Headers headers = getHeaders(phpSessionId);
        Request request = new Request.Builder().url(URL_VOTE_COMMENT_SCRIPT).headers(headers).post(requestBody).build();
        Response response = myHttpClient.getOkHttpClient().newCall(request).execute();

        Log.d("api", "vote comment response: " + response.toString());
        Log.d("api", "vote comment response body: " + response.body().string());
    }

    public static String replyComment(int storyId, String commentText, int parentCommentId) throws IOException, JSONException {
        Log.d("replyComment", "replyComment, storyId: " + storyId + ", parentCommentId: " + parentCommentId + ", commentText: " + commentText);
        RequestBody requestBody = new FormBody.Builder()
                .add("act", "addcom")
                .add("id", String.valueOf(storyId))
                .add("comment", commentText)
                .add("parentid", String.valueOf(parentCommentId))
                .add("include", "0")//what is it?
                .add("comment_images", "0")//what is it?
                .build();
        MyHttpClient myHttpClient = MyHttpClient.getInstance();
        String phpSessionId = myHttpClient.getPhpSessionId();
        Headers headers = getHeaders(phpSessionId);
        Request request = new Request.Builder().url(URL_REPLY_COMMENT_SCRIPT).headers(headers).post(requestBody).build();
        Response response = myHttpClient.getOkHttpClient().newCall(request).execute();

        String responseBody = response.body().string();
        Log.d("replyComment", "replyComment response: " + response.toString());
        Log.d("replyComment", "replyComment response body: " + responseBody);
        JSONObject jsonObject = new JSONObject(responseBody);
        Log.d("replyComment", "replyComment text: " + jsonObject.getString("text"));
        if (jsonObject.getString("type").equals("error")) {
            return jsonObject.getString("text");
        }
        return null;
    }

    public static Observable<Void> getReplyCommentObservable(int storyId, String commentText, int parentCommentId) {
        return Observable.create(subscriber -> {
            String message = null;
            try {
                message = Api.replyComment(storyId, commentText, parentCommentId);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                subscriber.onError(new Exception(MyApplication.getAppContext().getString(R.string.reply_comment_error)));
            }
            if (message == null) {
                subscriber.onNext(null);
            } else {
                subscriber.onError(new Exception(message));
            }
        });
    }

    private static Headers getHeaders(@NotNull String phpSessionId) {
        return new Headers.Builder()
                .add("Referer", Links.LINK_SITE)
                .add("X-Csrf-Token", phpSessionId)
                .build();
    }


}
