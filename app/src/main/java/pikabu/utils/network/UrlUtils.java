package pikabu.utils.network;

import android.support.annotation.NonNull;

/**
 * Created on 14.02.2016.
 */
public class UrlUtils {

    private static final String COMMENT_TAG = "#comment_";

    /**
     *
     * @param link link to story
     * @return comment id or 0
     */
    public static int selectTargetCommentId(String link) {
        int firstIndexOfCommentTag = link.indexOf(COMMENT_TAG);
        if (firstIndexOfCommentTag != -1) {
            int lastIndexOfCommentTag = firstIndexOfCommentTag + COMMENT_TAG.length();
            String commentId = link.substring(lastIndexOfCommentTag, link.length());
            try {
                return Integer.parseInt(commentId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public static boolean equalsStoryLinks(String link1, String link2) {
        link1 = getClearStoryLink(link1);
        link2 = getClearStoryLink(link2);
        return link1.equals(link2);
    }

    public static int getStoryIdFromLink(String link) {
        link = getClearStoryLink(link);
        int indexOfStartId = link.lastIndexOf("_") + 1;
        String storyId = link.substring(indexOfStartId, link.length());
        try {
            return Integer.parseInt(storyId);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private static final String COMMUNITY_PREFIX = "/community/";

    public static String getCommunityPathName(@NonNull String url) {
        int start = url.lastIndexOf(COMMUNITY_PREFIX) + COMMUNITY_PREFIX.length();
        String pathName = url.substring(start);
        int end = pathName.indexOf("/");
        if (end != -1)  {
            pathName = pathName.substring(0, end);
        }
        end = pathName.indexOf("?");
        if (end != -1) {
            pathName = pathName.substring(0, end);
        }
        return pathName;
    }

    private static final String STORY_LINK_TEMPLATE = "http://pikabu.ru/story/_";

    public static String createStoryLink(int storyId) {
        return STORY_LINK_TEMPLATE + storyId;
    }

    private static String getClearStoryLink(String link) {
        int indexArg = link.lastIndexOf("?");
        if (indexArg != -1) {
            link = link.substring(0, indexArg);
        }
        int indexSharp1 = link.lastIndexOf("#");
        if (indexSharp1 != -1) {
            link = link.substring(0, indexSharp1);
        }
        return link;
    }

    public static String getTargetCommentLink(int storyId, int commentId) {
        String storyLink = UrlUtils.createStoryLink(storyId);
        StringBuilder builder = new StringBuilder();
        builder.append(storyLink);
        builder.append(COMMENT_TAG);
        builder.append(commentId);
        return builder.toString();
    }
}
