package pikabu.utils.network;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cookie;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pikabu.data.Links;
import pikabu.utils.network.progress.LoadingProgressObservable;
import pikabu.utils.network.progress.ProgressNetworkInterceptor;

/**
 * Created on 23.10.2015.
 */
public class MyHttpClient {

    private static final long CONNECT_TIMEOUT_TIME = 45;
    private static final long READ_TIMEOUT_TIME = 60;

    private OkHttpClient okHttpClient;
    private PikabuCookieJar pikabuCookieJar;

    private volatile static MyHttpClient client;

    private MyHttpClient() {
        pikabuCookieJar = new PikabuCookieJar();
        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_TIME, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT_TIME, TimeUnit.SECONDS)
                .cookieJar(pikabuCookieJar)
                .addNetworkInterceptor(new ProgressNetworkInterceptor(LoadingProgressObservable.getInstance()))
                .build();
    }

    public static MyHttpClient getInstance() {
        if (client == null) {
            synchronized (MyHttpClient.class) {
                if (client == null) {
                    client = new MyHttpClient();
                }
            }
        }
        return client;
    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public void saveCookies() {
        pikabuCookieJar.saveCookies();
    }

    public void clearCookies() {
        pikabuCookieJar.clearCookies();
    }

    public Cookie getCookie(String name) {
        return pikabuCookieJar.getCookie(name);
    }

    String getPhpSessionId() throws IOException {
        Cookie phpSessionCookie = pikabuCookieJar.getCookie("PHPSESS");
        if (phpSessionCookie == null) {
            getFirstCookies();
        }
        return pikabuCookieJar.getCookie("PHPSESS").value();
    }

    private void getFirstCookies() throws IOException {
        Request getHeadersRequest = new Request.Builder().url(Links.LINK_SITE).get().build();
        Response response = okHttpClient.newCall(getHeadersRequest).execute();
        if (response.code() != 200) {
            throw new IOException("fail to get php session id");
        }
    }
}