package pikabu.utils.network;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import pikabu.data.DB;
import pikabu.data.Links;

/**
 * Created on 13.12.2016.
 */

public class PikabuCookieJar implements CookieJar {

    private Map<String, Cookie> cookieMap = new HashMap<>();

    public PikabuCookieJar() {
        for (Cookie cookie : DB.getInstance().loadCookies()) {
            cookieMap.put(cookie.name(), cookie);
        }
    }

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
        if (!url.host().equals(Links.PIKABU_HOST)) {
            return;
        }
        for (Cookie cookie : cookies) {
            cookieMap.put(cookie.name(), cookie);
        }
    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        if (!url.host().equals(Links.PIKABU_HOST)) {
            return new ArrayList<>();
        }
        return new ArrayList(cookieMap.values());
    }

    public void saveCookies() {
        DB.getInstance().saveCookies(new ArrayList<>(cookieMap.values()));
    }

    @Nullable
    public Cookie getCookie(String name) {
        return cookieMap.get(name);
    }

    public List<Cookie> getCookies() {
        return new ArrayList(cookieMap.values());
    }

    public void clearCookies() {
        cookieMap.clear();
        DB.getInstance().deleteCookies();
    }
}
