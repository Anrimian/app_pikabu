package pikabu.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;

public class LoaderHelper {

	public static Document getDocument(String link) {
		Document doc;
		try {
			Log.d("getDocument", "try to get doc " + link);
			MyHttpClient myHttpClient = MyHttpClient.getInstance();

			Request request = new Request.Builder().url(link).build();
			Response response = myHttpClient.getOkHttpClient().newCall(request).execute();
			
			doc = Jsoup.parse(response.body().string());
			Log.d("getDocument", "doc ");			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return doc;
	}

	public static Observable<Document> getDocumentObservable(String link) {
        return Observable.create(subscriber -> {
            Document doc = getDocument(link);
            if (doc != null) {
                subscriber.onNext(doc);
            } else {
                subscriber.onError(new ExceptionWithInteger(R.string.connection_error));
            }
        });

    }

	public static boolean isNetworkConnected(Context ctx) {
		if (ctx == null) {
			return false;
		}
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
	}
}
