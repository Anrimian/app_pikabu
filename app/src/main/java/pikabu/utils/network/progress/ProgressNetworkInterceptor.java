package pikabu.utils.network.progress;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created on 16.10.2016.
 */

public class ProgressNetworkInterceptor implements Interceptor {

    private LoadingObservableManager observableManager;

    public ProgressNetworkInterceptor(LoadingObservableManager observableManager) {
        this.observableManager = observableManager;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());
        String link = chain.request().url().toString();
        ProgressListener progressListener = new ProgressListenerImpl(link, observableManager);
        return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
    }
}
