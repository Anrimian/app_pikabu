package pikabu.utils.network.progress.observers;

import android.content.Context;
import android.util.AttributeSet;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import pikabu.utils.network.progress.LoadingObservableManager;
import pikabu.utils.network.progress.LoadingObserver;

/**
 * Created on 13.10.2016.
 */

public class ProgressBarLoadingObserver extends MaterialProgressBar implements LoadingObserver {

    private String link;

    public ProgressBarLoadingObserver(Context context) {
        super(context);
    }

    public ProgressBarLoadingObserver(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProgressBarLoadingObserver(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void registerProgressBar(String link, LoadingObservableManager observableManager) {
        this.link = link;
        observableManager.registerLoadingObserver(this);
    }

    @Override
    public void onLoaded(int progress) {
        setProgress(progress);
        setVisibility(VISIBLE);
    }

    @Override
    public void onCompleted() {
        setVisibility(GONE);
    }

    @Override
    public int getObserverId() {
        return hashCode();
    }

    @Override
    public String getLink() {
        return link;
    }
}
