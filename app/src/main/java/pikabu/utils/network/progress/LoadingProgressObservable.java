package pikabu.utils.network.progress;

/**
 * Created on 22.11.2016.
 */

public class LoadingProgressObservable {

    private static LoadingObservableManager instance = new LoadingObservableManager();

    public static LoadingObservableManager getInstance() {
        return instance;
    }
}
