package pikabu.utils.network.progress;

/**
 * Created on 13.10.2016.
 */

public interface LoadingObserver {
    void onLoaded(int progress);

    void onCompleted();

    int getObserverId();

    String getLink();
}
