package pikabu.utils.network.progress;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * Created on 16.10.2016.
 */

class ProgressListenerImpl implements ProgressListener {

    private final static int MESSAGE_FINISH = Integer.MAX_VALUE;
    private final static int MESSAGE_START = Integer.MIN_VALUE;

    private LoadingObservableManager observableManager;
    private LoadingObservable loadingObservable;
    private String link;

    private int progress;

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_START: {
                    observableManager.registerLoadingObservable(link, loadingObservable);
                    break;
                }
                case MESSAGE_FINISH: {
                    loadingObservable.onCompleted();
                    observableManager.unregisterLoadingObservable(link);
                    break;
                }
                default: {
                    loadingObservable.onLoaded(msg.what);
                }
            }
        }
    };

    ProgressListenerImpl(String link, LoadingObservableManager observableManager) {
        this.link = link;
        this.observableManager = observableManager;
        loadingObservable = new LoadingObservable();
        handler.sendEmptyMessage(MESSAGE_START);
    }

    @Override
    public void update(long bytesRead, long contentLength) {
        int progress = (int) ((100 * bytesRead) / contentLength);
        if (this.progress < progress) {
            this.progress = progress;
            handler.sendEmptyMessage(progress);
        }
    }

    @Override
    public void finish() {
        handler.sendEmptyMessage(MESSAGE_FINISH);
    }
}
