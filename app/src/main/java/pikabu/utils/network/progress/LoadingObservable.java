package pikabu.utils.network.progress;

import com.emirinay.tools.observer.WeakObservable;

import java.lang.ref.WeakReference;

/**
 * Created on 12.10.2016.
 */

class LoadingObservable extends WeakObservable<LoadingObserver>{

    private int currentProgress;
    private boolean done = false;

    @Override
    public void registerObserver(LoadingObserver observer) {
        super.registerObserver(observer);
        if (done) {
            observer.onCompleted();
        } else {
            observer.onLoaded(currentProgress);
        }
    }

    @Override
    public void unregisterObserver(LoadingObserver observer) {
        super.unregisterObserver(observer);
    }

    void onLoaded(int progress) {
        currentProgress = progress;
        for (WeakReference<LoadingObserver> observerRef : mObservers) {
            LoadingObserver observer = observerRef.get();
            if (observer != null) {
                observer.onLoaded(progress);
            } else {
                mObservers.remove(observerRef);
            }
        }
    }

    void onCompleted() {
        done = true;
        currentProgress = 0;
        for (WeakReference<LoadingObserver> observerRef : mObservers) {
            LoadingObserver observer = observerRef.get();
            if (observer != null) {
                observer.onCompleted();
            } else {
                mObservers.remove(observerRef);
            }
        }
    }
}
