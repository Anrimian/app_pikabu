package pikabu.utils.network.progress;

/**
 * Created on 16.10.2016.
 */

public interface ProgressListener {
    void update(long bytesRead, long contentLength);

    void finish();
}
