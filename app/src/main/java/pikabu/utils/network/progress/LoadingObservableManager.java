package pikabu.utils.network.progress;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created on 10.10.2016.
 */

public class LoadingObservableManager {

    private Map<String, LoadingObservable> observableMap = new HashMap();

    private Map<Integer, WeakReference<LoadingObserver>> observersIdMap = new ConcurrentHashMap<>();

    void registerLoadingObservable(String link, LoadingObservable observable) {
        observableMap.put(link, observable);
        for (WeakReference<LoadingObserver> observerRef : observersIdMap.values()) {
            LoadingObserver observer = observerRef.get();
            if (observer == null) {
                observersIdMap.values().remove(observerRef);
            } else if (link.equals(observer.getLink())) {
                observable.registerObserver(observer);
            }
        }
    }

    void unregisterLoadingObservable(String link) {
        observableMap.remove(link);
    }

    public void registerLoadingObserver(LoadingObserver observer) {
        String link = observer.getLink();
        int id = observer.getObserverId();
        WeakReference<LoadingObserver> previousObserverRef = observersIdMap.get(id);
        if (previousObserverRef != null) {
            removeObserver(previousObserverRef);
        }

        WeakReference<LoadingObserver> observerRef = new WeakReference(observer);
        observersIdMap.put(id, observerRef);

        LoadingObservable observable = observableMap.get(link);
        if (observable != null) {
            observable.registerObserver(observer);
        } else {
            observer.onCompleted();
        }
    }

    private void removeObserver(WeakReference<LoadingObserver> observerRef) {
        LoadingObserver observer = observerRef.get();
        if (observer != null) {
            LoadingObservable observable = observableMap.get(observer.getLink());
            if (observable != null) {
                observable.unregisterObserver(observer);
            }
        }
        observersIdMap.values().remove(observerRef);
    }
}
