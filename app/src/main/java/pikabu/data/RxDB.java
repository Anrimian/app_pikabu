package pikabu.data;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import pikabu.AppExecutors;
import pikabu.content.story.Story;
import rx.Observable;
import rx.Subscriber;

/**
 * Created on 24.02.2016.
 */
public class RxDB {

    public static Observable<ArrayList<Story>> getStoryList(String category) {
        return makeObservable(getStoryListCallable(category))
                .subscribeOn(AppExecutors.DB_SCHEDULER);
    }

    private static Callable<ArrayList<Story>> getStoryListCallable(String category) {
        return () -> DB.getInstance().getStoryListFromCategory(category);
    }

    public static void addStoryListToCategory(String category, List<Story> stories) {
        Observable.create(subscriber -> DB.getInstance().addStoryListToCategory(category, stories))
                .subscribeOn(AppExecutors.DB_SCHEDULER)
                .subscribe();
    }

    public static void removeStoryListByCategory(String category) {
        Observable.create(subscriber -> DB.getInstance().deleteStoryListByCategory(category))
                .subscribeOn(AppExecutors.DB_SCHEDULER)
                .subscribe();
    }

    private static <T> Observable<T> makeObservable(final Callable<T> func) {
        return Observable.create(
                new Observable.OnSubscribe<T>() {
                    @Override
                    public void call(Subscriber<? super T> subscriber) {
                        try {
                            subscriber.onNext(func.call());
                        } catch (Exception e) {
                            subscriber.onError(e);
                        }
                    }
                });
    }
}
