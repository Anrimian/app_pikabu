package pikabu.data;

public class Links {
	public static final String PIKABU_HOST = "pikabu.ru";
    public static final String LINK_HOT = "http://pikabu.ru/hot";
    public static final String LINK_BEST = "http://pikabu.ru/best";
	public static final String LINK_NEW = "http://pikabu.ru/new";
    public static final String LINK_REG = "http://m.pikabu.ru/signup.php";
    public static final String LINK_SITE = "http://pikabu.ru";
    public static final String LINK_SEARCH = "http://pikabu.ru/search.php?";
	public static final String LINK_PROFILE = "http://pikabu.ru/profile";
	public static final String LINK_TO_SAVED_CATEGORIES = "http://pikabu.ru/editprofile.php?cmd=cats";
	public static final String LINK_SAVED_STORIES = "http://pikabu.ru/index.php?cmd=saved";
	public static final String LINK_VOTED_STORY_UP = "http://pikabu.ru/index.php?cmd=interested&page=";
	public static final String LINK_VOTED_STORY_DOWN = "http://pikabu.ru/index.php?cmd=not-interested&page=";
	public static final String LINK_XML_COMM = "http://pikabu.ru/generate_xml_comm.php?id=";
	public static final String LINK_COMMUNITY = "http://pikabu.ru/community/";
	public static final String LINK_DISPUTED = "http://pikabu.ru/disputed?page=";
	public static final String LINK_FEED = "http://pikabu.ru/subs";
	public static final String LINK_TO_SAVED_COMMENTS = "http://pikabu.ru/commented.php?cmd=saved_comments&page=";
	public static final String MESSAGES = "http://pikabu.ru/freshitems.php";
	public static final String COMMENTS = "http://pikabu.ru/commented.php";
}
