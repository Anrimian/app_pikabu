package pikabu.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Cookie;
import pikabu.content.Vote;
import pikabu.content.blocks.ContentBlock;
import pikabu.content.story.Story;
import pikabu.content.story.StoryCommunityInfo;
import pikabu.content.story.blocks.ContentType;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.main.sections.saved.stories.SaveCategory;
import pikabu.user.actions.Action;
import pikabu.user.actions.comments.VoteCommentAction;
import pikabu.user.actions.story.SaveStoryAction;
import pikabu.user.actions.story.VoteStoryAction;

public class DB {
	private static final String DB_NAME = "pikabu_database";
	private static final int DB_VERSION = 1;

	public static final String STORY_LIST = "story_list";
	public static final String STORY_TAG_LIST = "story_tag_list";
    public static final String COMMENT_LIST = "comments_list";
    public static final String COOKIES = "cookies";
    private static final String CONTENT_BLOCKS_LIST = "content_blocks_list";

    private static final String COOKIE_NAME = "name";
    private static final String COOKIE_VALUE = "value";

	public static final String COLUMN_ID = "_id";
    public static final String COLUMN_COMMENT_ID = "comment_id";
    public static final String COLUMN_PARENT_COMMENT_ID = "parent_comment_id";
    public static final String COLUMN_DATA_LEVEL = "data_level";
    public static final String COLUMN_AUTHOR_PICTURE = "author_picture";
    public static final String COLUMN_AUTHOR_GENDER = "author_gender";
	public static final String COLUMN_STORY_ID = "story_id";
	public static final String COLUMN_CATEGORY = "category";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_PREVIEW_TEXT = "preview_text";
	public static final String COLUMN_AUTHOR = "author";
	public static final String COLUMN_TIME = "time";
	public static final String COLUMN_RATING = "rating";
    public static final String COLUMN_VOTE_STATUS = "vote_status";
	public static final String COLUMN_MY = "my";
	public static final String COLUMN_BOOBS = "boobs";
	public static final String COLUMN_CONTENT_TYPE = "content_type";
	public static final String COLUMN_CONTENT_SECOND = "preview_image_link";
	public static final String COLUMN_PREVIEW_WIDTH = "preview_image_width";
	public static final String COLUMN_PREVIEW_HEIGHT = "preview_image_height";
	public static final String COLUMN_COMMENTS_NUMBER = "comments_number";
	public static final String COLUMN_CONTENT_SIZE = "content_size";
	public static final String COLUMN_CONTENT = "content";
	public static final String COLUMN_LINK = "story_link";
	public static final String COLUMN_TAG = "tag";
    public static final String COLUMN_SAVE_STATUS = "save_status";
    public static final String COMMUNITY_NAME = "community_name";
    public static final String COMMUNITY_PATH_NAME = "community_link";

    private static final String COOKIE_DOMAIN = "domain";
    private static final String COOKIE_MAX_AGE = "max_age";
    private static final String COOKIE_PATH = "path";
    private static final String COOKIE_SECURE = "secure";

    private static final String DEFERRED_VOTE_STORY_TASK_LIST = "deferred_vote_story_task_list";
    private static final String COLUMN_VOTE = "vote";

    private static final String DEFERRED_SAVE_STORY_TASK_LIST = "deferred_save_story_task_list";
    private static final String COLUMN_SAVE = "save";
    private static final String COLUMN_SAVE_STORY_CATEGORY = "save_category";

    private static final String DEFERRED_VOTE_COMMENT_TASK_LIST = "deferred_vote_comment_task_list";
    private static final String COOKIE_HTTP_ONLY = "http_only";
    private static final String STORIES_SAVED_CATEGORIES_LIST = "stories_saved_categories";
    private static final String COLUMN_CATEGORY_NAME = "category_name";
    private static final String COLUMN_CATEGORY_ID = "category_id";
    private static final String COLUMN_STORIES_COUNT = "stories_count";

    private static DB dbHelper = new DB();
    private DBOpenHelper dbOpenHelper;
    private SQLiteDatabase db;

	private DB() {
	}

    public static DB getInstance() {
        return dbHelper;
    }

    public void init(Context ctx) {
        if (!isOpen()) {
            dbOpenHelper = new DBOpenHelper(ctx, DB_NAME, null, DB_VERSION);
            db = dbOpenHelper.getWritableDatabase();
        }
    }

    private boolean isOpen() {
        return dbOpenHelper != null && db !=  null && db.isOpen();
    }

	public ArrayList<Story> getStoryListFromCategory(String category) {
		Cursor c = db.query(STORY_LIST, null, COLUMN_CATEGORY + " = ?", new String[] { category }, null, null, null);
        ArrayList<Story> storyList = new ArrayList<>();
        for (int i=0; i<c.getCount(); i++) {
            c.moveToPosition(i);

            Story story = new Story();
            story.setAuthor(c.getString(c.getColumnIndex(DB.COLUMN_AUTHOR)));
            story.setCommentsNumber(c.getInt(c.getColumnIndex(DB.COLUMN_COMMENTS_NUMBER)));
            story.setIsMyTag(c.getInt(c.getColumnIndex(DB.COLUMN_MY)) == 1);
            story.setIsStrawTag(c.getInt(c.getColumnIndex(DB.COLUMN_BOOBS)) == 1);
            story.setLink(c.getString(c.getColumnIndex(DB.COLUMN_LINK)));
            story.setPreviewText(c.getString(c.getColumnIndex(DB.COLUMN_PREVIEW_TEXT)));
            story.setRating(c.getInt(c.getColumnIndex(DB.COLUMN_RATING)));
            int voteStatusInt = c.getInt(c.getColumnIndex(DB.COLUMN_VOTE_STATUS));
            story.setVoteStatus(Vote.intToVote(voteStatusInt));
            story.setSaveStatus(c.getInt(c.getColumnIndex(DB.COLUMN_SAVE_STATUS)) == 1);
            story.setStoryId(c.getInt(c.getColumnIndex(DB.COLUMN_STORY_ID)));
            story.setTitle(c.getString(c.getColumnIndex(DB.COLUMN_TITLE)));
            story.setUnixTime(c.getInt(c.getColumnIndex(DB.COLUMN_TIME)));
            story.setTagList(getTagsFromStory(c.getInt(c.getColumnIndex(DB.COLUMN_STORY_ID)), category));
            story.setContentBlocks(getBlocksToStory(c.getInt(c.getColumnIndex(DB.COLUMN_STORY_ID)), category));

            String communityName = c.getString(c.getColumnIndex(DB.COMMUNITY_NAME));
            if (communityName != null && !communityName.isEmpty()) {
                String pathName = c.getString(c.getColumnIndex(DB.COMMUNITY_PATH_NAME));
                StoryCommunityInfo communityInfo = new StoryCommunityInfo(communityName, pathName);
                story.setCommunityInfo(communityInfo);
            }
            storyList.add(story);
        }
        c.close();
        return storyList;
	}

    public void addStoryListToCategory(String category, List<Story> storyList) {
		db.beginTransaction();
		try {
			for (Story story : storyList) {
                ContentValues cv = new ContentValues();
                cv.put(DB.COLUMN_AUTHOR, story.getAuthor());
                cv.put(DB.COLUMN_BOOBS, story.getIsStraw() ? 1 : 0);
                cv.put(DB.COLUMN_CATEGORY, category);
                cv.put(DB.COLUMN_COMMENTS_NUMBER, story.getCommentsNumber());
                cv.put(DB.COLUMN_LINK, story.getLink());
                cv.put(DB.COLUMN_MY, story.getIsMy() ? 1 : 0);
                cv.put(DB.COLUMN_PREVIEW_TEXT, story.getPreviewText());
                cv.put(DB.COLUMN_RATING, story.getRating());
                cv.put(DB.COLUMN_VOTE_STATUS, story.getVoteStatus().toInt());
                cv.put(DB.COLUMN_SAVE_STATUS, story.getSaveStatus() ? 1 : 0);
                cv.put(DB.COLUMN_STORY_ID, story.getStoryId());
                cv.put(DB.COLUMN_TIME, story.getUnixTime());
                cv.put(DB.COLUMN_TITLE, story.getTitle());
                StoryCommunityInfo communityInfo = story.getCommunityInfo();
                if (communityInfo != null) {
                    cv.put(DB.COMMUNITY_NAME, communityInfo.getName());
                    cv.put(DB.COMMUNITY_PATH_NAME, communityInfo.getPathName());
                }
				if (db.insert(STORY_LIST, null, cv) != -1) {
					addTagsToStory(story, category);
                    addContentBlocksToStory(story, category);
				}
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public void deleteStoryListByCategory(String category) {
		db.delete(STORY_LIST, COLUMN_CATEGORY + " = ?", new String[] { category });
		db.delete(STORY_TAG_LIST, COLUMN_CATEGORY + " = ?", new String[]{ category });
        db.delete(CONTENT_BLOCKS_LIST, COLUMN_CATEGORY + " = ?", new String[]{ category });

	}	

	public void addTagsToStory(Story story, String category) {
        for (String tag : story.getTagList()) {
            ContentValues cv = new ContentValues();
            cv.put(DB.COLUMN_TAG, tag);
            cv.put(DB.COLUMN_STORY_ID, story.getStoryId());
            cv.put(DB.COLUMN_CATEGORY, category);
            db.insert(STORY_TAG_LIST, null, cv);
        }
	}

	public ArrayList<String> getTagsFromStory(int storyId, String category) {
        Cursor c = db.query(STORY_TAG_LIST, new String[]{COLUMN_TAG}, COLUMN_CATEGORY + " = ? AND " + COLUMN_STORY_ID + " = ?", new String[]{category, Integer.toString(storyId)}, null, null, null);
        ArrayList<String> tagList = new ArrayList<>();
        if (c != null) {
            for (int tagNumber = 0; tagNumber < c.getCount(); tagNumber++) {
                c.moveToPosition(tagNumber);
                String tagText = c.getString(c.getColumnIndex(DB.COLUMN_TAG));
                tagList.add(tagText);

            }
            c.close();
        }
		return tagList;
	}

    public void addContentBlocksToStory(Story story, String category) {
        for (ContentBlock block : story.getContentBlocksList()) {
            ContentValues cv = block.toContentValues();
            cv.put(DB.COLUMN_CONTENT_TYPE, block.getContentType().toString());
            cv.put(DB.COLUMN_STORY_ID, story.getStoryId());
            cv.put(DB.COLUMN_CATEGORY, category);
            db.insert(CONTENT_BLOCKS_LIST, null, cv);
        }
    }

    private ArrayList<StoryContentBlock> getBlocksToStory(int storyId, String category) {
        Cursor c = db.query(CONTENT_BLOCKS_LIST, null, COLUMN_CATEGORY + " = ? AND " + COLUMN_STORY_ID + " = ?", new String[]{category, Integer.toString(storyId)}, null, null, null);
        ArrayList<StoryContentBlock> blocksList = new ArrayList<>();
        if (c != null) {
            for (int blockIndex = 0; blockIndex < c.getCount(); blockIndex++) {
                c.moveToPosition(blockIndex);
                String contentType = c.getString(c.getColumnIndex(DB.COLUMN_CONTENT_TYPE));
                String content = c.getString(c.getColumnIndex(DB.COLUMN_CONTENT));
                String contentSecond = c.getString(c.getColumnIndex(DB.COLUMN_CONTENT_SECOND));
                int width = c.getInt(c.getColumnIndex(DB.COLUMN_PREVIEW_WIDTH));
                int height = c.getInt(c.getColumnIndex(DB.COLUMN_PREVIEW_HEIGHT));
                int size = c.getInt(c.getColumnIndex(DB.COLUMN_CONTENT_SIZE));

                StoryContentBlock contentBlock = (StoryContentBlock) ContentType.createContentBlock(contentType,
                        content,
                        contentSecond,
                        width,
                        height,
                        size);
                blocksList.add(contentBlock);
            }
            c.close();
        }
        return blocksList;
    }

    /*public void addCommentsToStory(String storyLink, ArrayList<Comment> commentList){
        db.beginTransaction();
        try {
            for (Comment comment : commentList) {
                db.insert(COMMENT_LIST, null, comment.putCommentInContentValues(storyLink));
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public ArrayList<Comment> getCommentsList(String storyLink) {
        Cursor c = db.query(COMMENT_LIST, null, COLUMN_LINK + " = ?", new String[] { storyLink }, null, null, null);
        ArrayList<Comment> commentsList = new ArrayList<>();
        for (int i=0; i<c.getCount(); i++) {
            c.moveToPosition(i);
            Comment comment = Comment.getCommentFromCursor(c);
            commentsList.add(comment);
        }
        c.close();
        return commentsList;
    }

    public void removeComments(String storyLink) {
        db.delete(COMMENT_LIST, COLUMN_LINK + " = ?", new String[]{storyLink});
    }*/

    public void updateStoryVoteStatus(Story story) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_VOTE_STATUS, story.getVoteStatus().toInt());
        db.update(STORY_LIST, cv, COLUMN_STORY_ID + " = ?", new String[]{Integer.toString(story.getStoryId())});
    }

    public void updateStorySaveStatus(Story story) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_SAVE_STATUS, story.getSaveStatus() ? 1 : 0);
        db.update(STORY_LIST, cv, COLUMN_STORY_ID + " = ?", new String[]{Integer.toString(story.getStoryId())});
    }

    public void saveCookies(List<Cookie> cookies) {
        db.execSQL("delete from " + COOKIES);
        for (Cookie cookie : cookies) {
            ContentValues cv = new ContentValues();
            cv.put(COOKIE_NAME, cookie.name());
            cv.put(COOKIE_VALUE, cookie.value());
            cv.put(COOKIE_DOMAIN, cookie.domain());
            cv.put(COOKIE_MAX_AGE, cookie.expiresAt());
            cv.put(COOKIE_PATH, cookie.path());
            cv.put(COOKIE_SECURE, cookie.secure() ? 1 : 0);
            cv.put(COOKIE_HTTP_ONLY, cookie.httpOnly() ? 1 : 0);
            db.insert(COOKIES, null, cv);
        }
    }

    public List<Cookie> loadCookies() {
        List<Cookie> cookies = new ArrayList<>();
        Cursor c = db.query(COOKIES, null, null, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            String name = c.getString(c.getColumnIndex(COOKIE_NAME));
            String value = c.getString(c.getColumnIndex(COOKIE_VALUE));
            long expiresAt = c.getLong(c.getColumnIndex(COOKIE_MAX_AGE));
            String domain = c.getString(c.getColumnIndex(COOKIE_DOMAIN));
            String path = c.getString(c.getColumnIndex(COOKIE_PATH));
            boolean secure = c.getInt(c.getColumnIndex(COOKIE_SECURE)) == 1;
            boolean httpOnly = c.getInt(c.getColumnIndex(COOKIE_HTTP_ONLY)) == 1;

            Cookie.Builder builder = new Cookie.Builder()
                    .name(name)
                    .value(value)
                    .expiresAt(expiresAt)
                    .domain(domain)
                    .path(path)
                    .secure();
            if (secure) {
                builder.secure();
            }
            if (httpOnly) {
                builder.httpOnly();
            }
            cookies.add(builder.build());
        }
        c.close();
        return cookies;
    }

    public void deleteCookies() {
        db.execSQL("delete from " + COOKIES);
    }

    public void saveDeferredVoteStoryAction(VoteStoryAction voteAction) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_STORY_ID, voteAction.getVoteStoryId());
        cv.put(COLUMN_VOTE, voteAction.getVote().toInt());
        db.insert(DEFERRED_VOTE_STORY_TASK_LIST, null, cv);
    }

    public void removeDeferredVoteStoryAction(VoteStoryAction voteAction) {
        db.delete(DEFERRED_VOTE_STORY_TASK_LIST,
                COLUMN_STORY_ID + " = ? AND " + COLUMN_VOTE + " = ?",
                new String[]{String.valueOf(voteAction.getVoteStoryId()), String.valueOf(voteAction.getVote().toInt())});
    }

    public ArrayList<Action> getDeferredVoteStoryActionList() {
        ArrayList<Action> voteActionList = new ArrayList<>();
        Cursor c = db.query(DEFERRED_VOTE_STORY_TASK_LIST, null, null, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            int storyId = c.getInt(c.getColumnIndex(DB.COLUMN_STORY_ID));
            Vote vote  = Vote.intToVote(c.getInt(c.getColumnIndex(DB.COLUMN_VOTE)));
            Action voteAction = new VoteStoryAction(storyId, vote, null);
            voteActionList.add(voteAction);
        }
        c.close();
        return voteActionList;
    }

    public void saveDeferredSaveStoryAction(SaveStoryAction saveAction) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_STORY_ID, saveAction.getStoryId());
        cv.put(COLUMN_SAVE_STORY_CATEGORY, saveAction.getCategory());
        cv.put(COLUMN_SAVE, saveAction.isSave() ? 1 : 0);
        db.insert(DEFERRED_SAVE_STORY_TASK_LIST, null, cv);
    }

    public void removeDeferredSaveStoryAction(SaveStoryAction saveAction) {
        db.delete(DEFERRED_SAVE_STORY_TASK_LIST,
                COLUMN_STORY_ID + " = ? AND "
                        + COLUMN_SAVE + " = ? AND "
                        + COLUMN_SAVE_STORY_CATEGORY + " = ?",
                new String[]{String.valueOf(saveAction.getStoryId()),
                        String.valueOf(saveAction.isSave() ? 1 : 0),
                        String.valueOf(saveAction.getCategory())});
    }

    public ArrayList<Action> getDeferredSaveStoryActionList() {
        ArrayList<Action> saveActionList = new ArrayList<>();
        Cursor c = db.query(DEFERRED_SAVE_STORY_TASK_LIST, null, null, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            int storyId = c.getInt(c.getColumnIndex(DB.COLUMN_STORY_ID));
            int category = c.getInt(c.getColumnIndex(DB.COLUMN_SAVE_STORY_CATEGORY));
            boolean save  = c.getInt(c.getColumnIndex(DB.COLUMN_SAVE)) == 1;
            Action voteAction;
            if (save) {
                voteAction = new SaveStoryAction(storyId, category, null);
            } else {
                voteAction = new SaveStoryAction(storyId, null);
            }
            saveActionList.add(voteAction);
        }
        c.close();
        return saveActionList;
    }

    public void saveDeferredVoteCommentAction(VoteCommentAction voteAction) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_STORY_ID, voteAction.getStoryId());
        cv.put(COLUMN_COMMENT_ID, voteAction.getCommentId());
        cv.put(COLUMN_VOTE, voteAction.getVote().toInt());
        db.insert(DEFERRED_VOTE_COMMENT_TASK_LIST, null, cv);
    }

    public void removeDeferredVoteCommentAction(VoteCommentAction voteAction) {
        db.delete(DEFERRED_VOTE_COMMENT_TASK_LIST,
                        COLUMN_STORY_ID + " = ? AND " +
                        COLUMN_COMMENT_ID + " = ? AND " +
                        COLUMN_VOTE + " = ?",
                new String[]{
                        String.valueOf(voteAction.getStoryId()),
                        String.valueOf(voteAction.getCommentId()),
                        String.valueOf(voteAction.getVote().toInt())
                });
    }

    public ArrayList<Action> getDeferredVoteCommentActionList() {
        ArrayList<Action> voteActionList = new ArrayList<>();
        Cursor c = db.query(DEFERRED_VOTE_COMMENT_TASK_LIST, null, null, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            int storyId = c.getInt(c.getColumnIndex(DB.COLUMN_STORY_ID));
            int commentId = c.getInt(c.getColumnIndex(DB.COLUMN_COMMENT_ID));
            Vote vote  = Vote.intToVote(c.getInt(c.getColumnIndex(DB.COLUMN_VOTE)));
            Action voteAction = new VoteCommentAction(storyId, commentId, vote, null);
            voteActionList.add(voteAction);
        }
        c.close();
        return voteActionList;
    }

    public List<SaveCategory> getStorySaveCategories() {
        List<SaveCategory> categories = new ArrayList<>();
        Cursor c = db.query(STORIES_SAVED_CATEGORIES_LIST, null, null, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            int id = c.getInt(c.getColumnIndex(DB.COLUMN_CATEGORY_ID));
            String name = c.getString(c.getColumnIndex(DB.COLUMN_CATEGORY_NAME));
            int storiesCount = c.getInt(c.getColumnIndex(DB.COLUMN_STORIES_COUNT));
            SaveCategory saveCategory = new SaveCategory(id, name, storiesCount);
            categories.add(saveCategory);
        }
        c.close();
        return categories;
    }

    public void saveStorySaveCategories(List<SaveCategory> categories) {
        db.execSQL("delete from " + STORIES_SAVED_CATEGORIES_LIST);
        for (SaveCategory category : categories) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_CATEGORY_ID, category.getId());
            cv.put(COLUMN_CATEGORY_NAME, category.getName());
            cv.put(COLUMN_STORIES_COUNT, category.getStoriesCount());
            db.insert(STORIES_SAVED_CATEGORIES_LIST, null, cv);
        }
    }

    private class DBOpenHelper extends SQLiteOpenHelper {

		DBOpenHelper(Context context, String name, CursorFactory factory,
                            int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {			
			createStoryTable(db);
            createTagTable(db);
            createBlocksTable(db);
            createCommentsTable(db);
            createCookieTable(db);
            createDeferredVoteStoryTaskTable(db);
            createDeferredSaveStoryTaskTable(db);
            createDeferredVoteCommentTaskTable(db);
            createStorySavedCategoriesTable(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

        private void createStoryTable(SQLiteDatabase db) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table ");
            sb.append(STORY_LIST);
            sb.append("(");
            sb.append(COLUMN_ID);
            sb.append(" integer primary key autoincrement, ");
            sb.append(COLUMN_STORY_ID);
            sb.append(" integer, ");
            sb.append(COLUMN_CATEGORY);
            sb.append(" text, ");
            sb.append(COLUMN_TITLE);
            sb.append(" text, ");
            sb.append(COLUMN_PREVIEW_TEXT);
            sb.append(" text, ");
            sb.append(COLUMN_AUTHOR);
            sb.append(" text, ");
            sb.append(COLUMN_TIME);
            sb.append(" integer, ");
            sb.append(COLUMN_RATING);
            sb.append(" integer, ");
            sb.append(COLUMN_VOTE_STATUS);
            sb.append(" integer, ");
            sb.append(COLUMN_SAVE_STATUS);
            sb.append(" integer, ");
            sb.append(COLUMN_MY);
            sb.append(" integer, ");
            sb.append(COLUMN_BOOBS);
            sb.append(" integer, ");
            sb.append(COLUMN_COMMENTS_NUMBER);
            sb.append(" integer, ");
            sb.append(COLUMN_LINK);
            sb.append(" text, ");
            sb.append(COMMUNITY_NAME);
            sb.append(" text, ");
            sb.append(COMMUNITY_PATH_NAME);
            sb.append(" text, ");
            sb.append(" UNIQUE( ");
            sb.append(COLUMN_STORY_ID);
            sb.append(", ");
            sb.append(COLUMN_CATEGORY);
            sb.append(") ON CONFLICT IGNORE");
            sb.append(");");
            db.execSQL(sb.toString());
        }

        private void createTagTable(SQLiteDatabase db) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table ");
            sb.append(STORY_TAG_LIST);
            sb.append("(");
            sb.append(COLUMN_TAG);
            sb.append(" text, ");
            sb.append(COLUMN_STORY_ID);
            sb.append(" text, ");
            sb.append(COLUMN_CATEGORY);
            sb.append(" text");
            sb.append(");");
            db.execSQL(sb.toString());
        }

        private void createBlocksTable(SQLiteDatabase db) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table ");
            sb.append(CONTENT_BLOCKS_LIST);
            sb.append("(");
            sb.append(COLUMN_CONTENT_TYPE);
            sb.append(" text, ");
            sb.append(COLUMN_STORY_ID);
            sb.append(" text, ");
            sb.append(COLUMN_CATEGORY);
            sb.append(" text, ");
            sb.append(COLUMN_CONTENT);
            sb.append(" text, ");
            sb.append(COLUMN_CONTENT_SECOND);
            sb.append(" text, ");
            sb.append(COLUMN_PREVIEW_WIDTH);
            sb.append(" integer, ");
            sb.append(COLUMN_PREVIEW_HEIGHT);
            sb.append(" integer, ");
            sb.append(COLUMN_CONTENT_SIZE);
            sb.append(" integer");
            sb.append(");");
            db.execSQL(sb.toString());
        }

        private void createCommentsTable(SQLiteDatabase db) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table ");
            sb.append(COMMENT_LIST);
            sb.append("(");
            sb.append(COLUMN_ID);
            sb.append(" integer primary key autoincrement, ");
            sb.append(COLUMN_LINK);
            sb.append(" text, ");
            sb.append(COLUMN_COMMENT_ID);
            sb.append(" integer, ");
            sb.append(COLUMN_PARENT_COMMENT_ID);
            sb.append(" integer, ");
            sb.append(COLUMN_DATA_LEVEL);
            sb.append(" integer, ");
            sb.append(COLUMN_RATING);
            sb.append(" integer, ");
            sb.append(COLUMN_VOTE_STATUS);
            sb.append(" integer, ");
            sb.append(COLUMN_AUTHOR);
            sb.append(" text, ");
            sb.append(COLUMN_AUTHOR_PICTURE);
            sb.append(" text, ");
            sb.append(COLUMN_AUTHOR_GENDER);
            sb.append(" integer, ");
            sb.append(COLUMN_TIME);
            sb.append(" integer, ");
            sb.append(COLUMN_CONTENT);
            sb.append(" text, ");
            sb.append(" UNIQUE( ");
            sb.append(COLUMN_COMMENT_ID);
            sb.append(") ON CONFLICT IGNORE");
            sb.append(");");
            db.execSQL(sb.toString());
        }

        private void createCookieTable(SQLiteDatabase db) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table ");
            sb.append(COOKIES);
            sb.append("(");
            sb.append(COLUMN_ID);
            sb.append(" integer primary key autoincrement, ");
            sb.append(COOKIE_NAME);
            sb.append(" text, ");
            sb.append(COOKIE_VALUE);
            sb.append(" text, ");
            sb.append(COOKIE_DOMAIN);
            sb.append(" text, ");
            sb.append(COOKIE_MAX_AGE);
            sb.append(" integer, ");
            sb.append(COOKIE_PATH);
            sb.append(" text, ");
            sb.append(COOKIE_SECURE);
            sb.append(" integer, ");
            sb.append(COOKIE_HTTP_ONLY);
            sb.append(" integer, ");
            sb.append(" UNIQUE( ");
            sb.append(COOKIE_NAME);
            sb.append(") ON CONFLICT REPLACE");
            sb.append(");");
            db.execSQL(sb.toString());
        }

        private void createDeferredVoteStoryTaskTable(SQLiteDatabase db) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table ");
            sb.append(DEFERRED_VOTE_STORY_TASK_LIST);
            sb.append("(");
            sb.append(COLUMN_ID);
            sb.append(" integer primary key autoincrement, ");
            sb.append(COLUMN_STORY_ID);
            sb.append(" integer, ");
            sb.append(COLUMN_VOTE);
            sb.append(" integer");
            sb.append(");");
            db.execSQL(sb.toString());
        }

        private void createDeferredSaveStoryTaskTable(SQLiteDatabase db) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table ");
            sb.append(DEFERRED_SAVE_STORY_TASK_LIST);
            sb.append("(");
            sb.append(COLUMN_ID);
            sb.append(" integer primary key autoincrement, ");
            sb.append(COLUMN_STORY_ID);
            sb.append(" integer, ");
            sb.append(COLUMN_SAVE);
            sb.append(" integer, ");
            sb.append(COLUMN_SAVE_STORY_CATEGORY);
            sb.append(" integer");
            sb.append(");");
            db.execSQL(sb.toString());
        }

        private void createDeferredVoteCommentTaskTable(SQLiteDatabase db) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table ");
            sb.append(DEFERRED_VOTE_COMMENT_TASK_LIST);
            sb.append("(");
            sb.append(COLUMN_ID);
            sb.append(" integer primary key autoincrement, ");
            sb.append(COLUMN_STORY_ID);
            sb.append(" integer, ");
            sb.append(COLUMN_COMMENT_ID);
            sb.append(" integer, ");
            sb.append(COLUMN_VOTE);
            sb.append(" integer");
            sb.append(");");
            db.execSQL(sb.toString());
        }

        private void createStorySavedCategoriesTable(SQLiteDatabase db) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table ");
            sb.append(STORIES_SAVED_CATEGORIES_LIST);
            sb.append("(");
            sb.append(COLUMN_CATEGORY_NAME);
            sb.append(" text, ");
            sb.append(COLUMN_CATEGORY_ID);
            sb.append(" integer, ");
            sb.append(COLUMN_STORIES_COUNT);
            sb.append(" integer");
            sb.append(");");
            db.execSQL(sb.toString());
        }
    }
}