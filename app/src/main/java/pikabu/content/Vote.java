package pikabu.content;

/**
 * Created on 19.11.2015.
 */
public enum Vote {
    UP {
        public int toInt(){
            return 1;
        }

        public String toString() {
            return "+";
        }
    },
    DOWN {
        public int toInt() {
            return -1;
        }

        public String toString() {
            return "-";
        }
    },
    NOTHING;

    public int toInt() {
        return 0;
    }

    public static Vote intToVote(int voteInt) {
        switch (voteInt) {
            case 1 : return UP;
            case -1 : return DOWN;
            default: return NOTHING;
        }
    }

    public static Vote mergeVote(Vote oldVote, Vote newVote) {
        Vote result = null;
        if (newVote == null || newVote == Vote.NOTHING || oldVote == newVote) {
            result = oldVote;
        } else if (oldVote == null || oldVote == NOTHING) {
            result = newVote;
        }

        if (result == null) {
            result = NOTHING;
        }
        return result;

    }
}
