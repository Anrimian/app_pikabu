package pikabu.content;

public class ParserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5609682330649808209L;

	public ParserException() {		
	}

	public ParserException(String detailMessage) {
		super(detailMessage);		
	}

	public ParserException(Throwable throwable) {
		super(throwable);		
	}

	public ParserException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);		
	}

}
