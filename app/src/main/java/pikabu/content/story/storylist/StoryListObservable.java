package pikabu.content.story.storylist;

import android.database.Observable;

/**
 * Created on 12.07.2015.
 */
public class StoryListObservable extends Observable<StoryListObserver> {

    public void showState(ListState listState) {
        for(StoryListObserver observer : mObservers) {
            listState.showState(observer);
        }
    }

    public void updateList(boolean refresh){
        for (final StoryListObserver observer : mObservers) {
            observer.updateList(refresh);
        }
    }

    public void restoreListPosition() {
        for (final StoryListObserver observer : mObservers) {
            observer.restoreListPosition();
        }
    }
}
