package pikabu.content.story.storylist;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.ListPosition;
import com.emirinay.tools.fragment.lists.recyclerview.endlesslist.EndlessListCallback;
import com.emirinay.tools.fragment.lists.recyclerview.endlesslist.EndlessListScrollListener;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;

import pikabu.views.ProgressViewBinder;

/**
 * Created on 01.12.2016.
 */

public class StoryListView implements RecyclerViewInfo {

    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    private View emptyProgressView;
    private View progressStateView;
    private ProgressViewBinder currentProgressViewBinder;
    private ProgressViewBinder progressViewBinder;
    private ProgressViewBinder emptyListProgressViewBinder;

    private StoryListActions storyListActions;

    public StoryListView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        emptyProgressView = view.findViewById(R.id.empty_list_progress_state_view);
        emptyListProgressViewBinder = new ProgressViewBinder(emptyProgressView);
        progressStateView = View.inflate(getContext(), R.layout.progress_state_view, null);
        progressViewBinder = new ProgressViewBinder(progressStateView);
        View.OnClickListener clickListener = new ErrorOnClickListener();
        emptyListProgressViewBinder.setTryAgainButtonOnClickListener(clickListener);
        progressViewBinder.setTryAgainButtonOnClickListener(clickListener);

        swipeRefreshLayout = ((SwipeRefreshLayout) view.findViewById(R.id.ptr_layout));
        swipeRefreshLayout.setOnRefreshListener(new RefreshListener());
        swipeRefreshLayout.setColorSchemeResources(R.color.green);
    }

    public void bind(StoryListAdapter adapter, EndlessListCallback endlessListCallback, StoryListActions storyListActions) {
        this.storyListActions = storyListActions;
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new EndlessListScrollListener(linearLayoutManager, endlessListCallback));
        adapter.addFooter(progressStateView);
    }

    public void showProgress(boolean blockSwipeRefreshLayout, boolean refreshing) {
        currentProgressViewBinder.showProgress();
        if (blockSwipeRefreshLayout) {
            if (!swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setEnabled(false);
            }
        }
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(refreshing));
    }

    public void showError(int messageId) {
        enableSwipeRefreshLayout();
        currentProgressViewBinder.showMessage(messageId, true);
    }

    public void showEnd(int messageId) {
        enableSwipeRefreshLayout();
        currentProgressViewBinder.showMessage(messageId, false);
    }

    public void showWait() {
        enableSwipeRefreshLayout();
        currentProgressViewBinder.hideAll();
    }

    private void enableSwipeRefreshLayout() {
        swipeRefreshLayout.setEnabled(true);
        swipeRefreshLayout.setRefreshing(false);
    }

    void setUpProgressStateView(boolean showEmptyView) {
        if (showEmptyView) {
            progressStateView.setVisibility(View.INVISIBLE);
            emptyProgressView.setVisibility(View.VISIBLE);
            currentProgressViewBinder = emptyListProgressViewBinder;
        } else {
            emptyProgressView.setVisibility(View.INVISIBLE);
            progressStateView.setVisibility(View.VISIBLE);
            currentProgressViewBinder = progressViewBinder;
        }
    }

    private Context getContext() {
        return recyclerView.getContext();
    }

    public void restoreListPosition(ListPosition listPosition) {
        int index = listPosition.getIndex();
        int top = listPosition.getTop();
        linearLayoutManager.scrollToPositionWithOffset(index, top);
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public LinearLayoutManager getLinearLayoutManager() {
        return linearLayoutManager;
    }

    private class ErrorOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            storyListActions.onErrorClick();
        }
    }

    private class RefreshListener implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            storyListActions.onRefresh();
        }
    }
}
