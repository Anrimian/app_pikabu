package pikabu.content.story.storylist;

import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.ListPosition;
import com.emirinay.tools.fragment.lists.recyclerview.endlesslist.EndlessListCallback;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;

import pikabu.main.DrawerActivity;


/**
 * Created on 01.12.2016.
 */

public class StoryListController implements EndlessListCallback, StoryListActions, StoryListObserver, ArrowControllerInfo {

    private FragmentActivity activity;

    private StoryListAdapter adapter;
    private StoryListModel model;

    private StoryListView storyListView;

    public StoryListController(FragmentActivity activity, StoryListView storyListView) {
        this.activity = activity;
        this.storyListView = storyListView;
    }

    public void bindList(String link, StoryListModel model, StoryListAdapter adapter) {
        this.model = model;
        this.adapter = adapter;
        model.setLink(link);
        storyListView.bind(adapter, this, this);
        model.registerObserver(this);
    }

    public void unbind() {
        model.saveListValues();
        model.unregisterObserver(this);
    }

    @Override
    public void showProgressState() {
        storyListView.setUpProgressStateView(isListEmpty() && !isHeaderVisible());
        boolean blockSwipeRefreshLayout = isListEmpty() || isRefreshing();
        storyListView.showProgress(blockSwipeRefreshLayout, isRefreshing());
    }

    @Override
    public void showWaitState() {
        storyListView.setUpProgressStateView(isListEmpty() && !isHeaderVisible());
        storyListView.showWait();
    }

    @Override
    public void showErrorState() {
        storyListView.setUpProgressStateView(isListEmpty() && !isHeaderVisible());
        int messageId = model.getErrorMessageId();
        if (isRefreshing() && !isListEmpty()) {
            Toast.makeText(activity, messageId, Toast.LENGTH_SHORT).show();
            model.showState(ListState.WAIT);
            return;
        }
        storyListView.showError(messageId);
    }

    @Override
    public void showEndState() {
        int messageId = getEndMessageId();
        if (isListEmpty()) {
            messageId = getEmptyMessageId();
        }
        storyListView.setUpProgressStateView(isListEmpty() && !isHeaderVisible());
        storyListView.showEnd(messageId);
    }

    @Override
    public void updateList(boolean refresh) {
        adapter.notifyDataSetChanged();
        activity.invalidateOptionsMenu();
    }

    @Override
    public void restoreListPosition() {
        ListPosition listPosition = model.loadListPosition();
        if (listPosition != null) {
            storyListView.restoreListPosition(listPosition);
        }
    }

    protected boolean isHeaderVisible() {
        return false;
    }

    public boolean isListEmpty() {
        return adapter.getCount() == 0;
    }

    protected int getEmptyMessageId() {
        return R.string.no_story;
    }

    protected int getEndMessageId() {
        return R.string.end_of_story_list;
    }

    public boolean isRefreshing() {
        return model.isRefreshing();
    }

    @Override
    public boolean canLoad() {
        return model.getListState() == ListState.WAIT;
    }

    @Override
    public void loadNextData() {
        startLoading(false);
    }

    protected void startLoading(boolean refresh) {
        model.startLoading(refresh);
    }

    public void onErrorClick() {
        startLoading(false);
    }

    @Override
    public void onRefresh() {
        model.setRefreshing(true);
        startLoading(true);
    }

    @Override
    public boolean showArrow() {
        return !isListEmpty() && !((DrawerActivity) activity).isDrawerOpen();
    }

    public StoryListModel getModel() {
        return model;
    }
}
