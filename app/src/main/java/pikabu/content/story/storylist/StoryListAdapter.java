package pikabu.content.story.storylist;

import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.adapter.HeaderFooterRecyclerViewAdapter;

import java.util.ArrayList;

import pikabu.Preferences;
import pikabu.content.story.Story;
import pikabu.content.story.StoryViewBinder;
import pikabu.content.comments.story.commentslist.fullstory.FullStoryActivity;

public class StoryListAdapter extends HeaderFooterRecyclerViewAdapter<StoryListAdapter.StoryViewHolder> {

    private ArrayList<Story> dataList;
    private FragmentActivity fragmentActivity;

    public StoryListAdapter(ArrayList<Story> dataList, FragmentActivity fragmentActivity) {
        this.dataList = dataList;
        this.fragmentActivity = fragmentActivity;
    }

    @Override
    public StoryViewHolder createVH(ViewGroup viewGroup, int type) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.story_list_item, viewGroup, false);
        return new StoryViewHolder(view, fragmentActivity);
    }

	@Override
	public void bindVH(StoryViewHolder viewHolder, int position) {
        Story story = dataList.get(position);
        viewHolder.bindView(story);
	}

	@Override
	public int getCount() {
		return dataList.size();
	}

    public void setData(ArrayList<Story> dataList) {
        this.dataList = dataList;
    }

    public static class StoryViewHolder extends RecyclerView.ViewHolder {

        private StoryViewBinder storyViewBinder;

        private View cardView;

        public StoryViewHolder(View itemView, FragmentActivity fragmentActivity) {
            super(itemView);
            View storyView = itemView.findViewById(R.id.story_view);
            storyViewBinder = new StoryViewBinder(storyView, fragmentActivity);
            cardView = itemView.findViewById(R.id.story_card_view);
        }

        private void bindView(Story story) {
            SharedPreferences sp = Preferences.get();
            boolean showLongStory = !sp.getBoolean("hide_long_story", true);
            storyViewBinder.bindStory(story, showLongStory, false);
            cardView.setOnClickListener(v -> FullStoryActivity.start(itemView.getContext(), story));
        }
    }
}
