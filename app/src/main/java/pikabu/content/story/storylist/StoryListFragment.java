package pikabu.content.story.storylist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;


public class StoryListFragment extends UpArrowRecyclerViewFragmentV2 {
    public final static String STORY_LIST_CATEGORY = "story_list_category";
    public final static String STORY_LIST_LINK = "story_list_link";
    public final static String STORY_LIST_TITLE = "story_list_title";

    private StoryListModel model;
    private StoryListView storyListView;
    private StoryListController storyListController;

    /**
     * *
     *
     * @param category if null then fragment not save data
     * @param link     link to storylist
     * @return storyListFragment instance
     */
    public static StoryListFragment newInstance(String category, String link, int titleId) {
        StoryListFragment storyListFragment = new StoryListFragment();
        Bundle arguments = new Bundle();
        arguments.putString(STORY_LIST_CATEGORY, category);
        arguments.putString(STORY_LIST_LINK, link);
        arguments.putInt(STORY_LIST_TITLE, titleId);
        storyListFragment.setArguments(arguments);
        return storyListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int titleId = getArguments().getInt(STORY_LIST_TITLE);
        if (titleId != 0) {
            getActivity().setTitle(titleId);
        }
        storyListView = new StoryListView(view);
        model = StoryListModelContainer.getInstance(getStoryListCategory(), getModelTag());
        StoryListAdapter adapter = new StoryListAdapter(model.getStoryList(), getActivity());
        storyListController = new StoryListController(getActivity(), storyListView);
        storyListController.bindList(getStoryListLink(), model, adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        storyListController.unbind();
    }

    /*@Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }*/ //need?

    public String getStoryListCategory() {
        return getArguments().getString(STORY_LIST_CATEGORY);
    }

    public String getStoryListLink() {
        return getArguments().getString(STORY_LIST_LINK);
    }

    public String getModelTag() {
        return getStoryListLink();
    }

    /*protected void onLinkWasChanged() {
        StoryListModel newModel = StoryListModelContainer.getInstance(getStoryListCategory(), getStoryListLink(), getModelTag());
        if (newModel != model) {
            newModel.setAdditionalDataParser(getAdditionalDataParser());
            newModel.setAdditionalData(getAdditionalData());
            model.unregisterObserver(this);

            model = newModel;
            adapter.setData(model.getStoryList());
            adapter.notifyDataSetChanged();
            getActivity().invalidateOptionsMenu();
            model.registerObserver(this);
        }
    }*/ //change model

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return storyListController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return storyListView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return model;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return model;
    }

}
