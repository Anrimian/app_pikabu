package pikabu.content.story.storylist.tabs;

import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

import pikabu.main.DrawerActivity;

/**
 * Created on 27.11.2015.
 */
public class StoryTabPagerActivity extends DrawerActivity {

    public static final String TARGET_PAGE = "target_page";

    @Override
    protected Fragment initFragment() {
        Intent intent = getIntent();
        ArrayList<StoryListTabInfo> tabInfoList = (ArrayList<StoryListTabInfo>) intent.getSerializableExtra(StoryListTabInfo.TAB_INFO_LIST);
        int tabTitleId = intent.getIntExtra(StoryTabPagerFragment.TAB_PAGER_TITLE, 0);
        String savePositionTag = intent.getStringExtra(StoryTabPagerFragment.SAVE_POSITION_TAG);
        StoryTabPagerFragment fragment = StoryTabPagerFragment.newInstance(tabInfoList, tabTitleId, savePositionTag);

        int targetPosition = intent.getIntExtra(TARGET_PAGE, StoryTabPagerFragment.NO_POSITION);
        if (targetPosition != StoryTabPagerFragment.NO_POSITION) {
            fragment.selectPage(targetPosition);
            intent.putExtra(TARGET_PAGE, StoryTabPagerFragment.NO_POSITION);
        }
        return fragment;
    }
}
