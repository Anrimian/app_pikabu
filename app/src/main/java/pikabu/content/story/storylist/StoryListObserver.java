package pikabu.content.story.storylist;

/**
 * Created on 12.07.2015.
 */
public interface StoryListObserver {

    void showEndState();

    void showErrorState();

    void showWaitState();

    void showProgressState();

    void updateList(boolean refresh);

    void restoreListPosition();

}
