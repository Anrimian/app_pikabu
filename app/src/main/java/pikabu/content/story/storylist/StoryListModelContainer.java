package pikabu.content.story.storylist;

import android.support.v4.util.LruCache;

/**
 * Created on 14.07.2015.
 */
public class StoryListModelContainer {

    private final static int MODEL_CACHE_SIZE = 5;

    private static LruCache<String, StoryListModel> cache  = new LruCache(MODEL_CACHE_SIZE);

    public static StoryListModel getInstance(String category, String modelTag) {
        StoryListModel model = cache.get(modelTag);
        if (model == null) {
            model = new StoryListModel(category);
            cache.put(modelTag, model);
        }
        return model;
    }
}
