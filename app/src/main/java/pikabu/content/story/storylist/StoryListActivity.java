package pikabu.content.story.storylist;

import android.content.Intent;
import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;

/**
 * Created on 24.11.2015.
 */
public class StoryListActivity extends DrawerActivity {
    public static final String STORY_LIST_LINK = "story_list_link";
    public static final String STORY_LIST_CATEGORY = "story_list_category";
    public static final String STORY_LIST_TITLE_ID = "story_list_title_id";

    @Override
    protected Fragment initFragment() {
        Intent intent = getIntent();
        String link = intent.getStringExtra(STORY_LIST_LINK);
        String category = intent.getStringExtra(STORY_LIST_CATEGORY);
        int titleId = intent.getIntExtra(STORY_LIST_TITLE_ID, 0);
        return StoryListFragment.newInstance(category, link, titleId);
    }
}
