package pikabu.content.story.storylist;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import android.util.SparseArray;

import com.emirinay.tools.ExceptionWithInteger;
import com.emirinay.tools.fragment.lists.ListPosition;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;

import org.jetbrains.annotations.Nullable;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;

import pikabu.AppExecutors;
import pikabu.Preferences;
import pikabu.content.AdditionalDataParser;
import pikabu.content.Vote;
import pikabu.content.story.Story;
import pikabu.content.story.parser.StoryParser;
import pikabu.data.DB;
import pikabu.data.RxDB;
import pikabu.user.UserModel;
import pikabu.user.actions.UserActions;
import pikabu.user.actions.story.UserStoryActionsObserver;
import pikabu.utils.network.LoaderHelper;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public final class StoryListModel implements UserStoryActionsObserver, ListPositionModel, ArrowPositionModel {
	
	private final String SAVED_LIST_POSITION_INDEX = "saved_list_position_index_";
    private static final int DEFAULT_LIST_POSITION_INDEX = -1;
	
	private final String SAVED_LIST_POSITION_TOP = "saved_list_position_top_";
    private static final int DEFAULT_LIST_POSITION_TOP = 0;
	
	private final String SAVED_PAGE_NUMBER = "saved_page_number_";
    private static final int DEFAULT_PAGE_NUMBER = 1;
	private int pageNumber = DEFAULT_PAGE_NUMBER;	
	
	private String storyListCategory;
	private String storyListLink;
    private final String SAVE_TAG;

	private Observable additionalData;
	private AdditionalDataParser additionalDataParser;

    private SparseArray<Story> storyIdMap = new SparseArray<>();
	private ArrayList<Story> storyList = new ArrayList<>();

    private Subscription subscription;

	private StoryListObservable observable = new StoryListObservable();
	private ListState listState;
    private int errorMessageId;
    private boolean isRefreshing = false;

    private ListPosition arrowListPosition;
    private ListPosition listPosition;

	public StoryListModel(String category) {
        this.storyListCategory = category;
        SAVE_TAG = category + "_story_list_data";
		UserActions.getInstance().registerStoryActionsObserver(this);
    }

    public void setLink(String link) {
        String oldLink = storyListLink;
        if (!TextUtils.equals(oldLink, link)) {
            this.storyListLink = link;
            if (oldLink == null && saveData()) {
                loadListValues();
                loadStoriesFromDB();
            } else {
                deleteListData();
                updateList(false);
                startLoading(false);
            }
        } else {
            showState(ListState.WAIT);
        }
    }

	public void registerObserver(final StoryListObserver observer) {
        observable.registerObserver(observer);
        listState.showState(observer);
	}

	public void unregisterObserver(final StoryListObserver observer) {
		observable.unregisterObserver(observer);
	}

    public void deleteListData() {
        if (saveData()) {
            RxDB.removeStoryListByCategory(storyListCategory);
        }
        storyList.clear();
        storyIdMap.clear();
		pageNumber = DEFAULT_PAGE_NUMBER;
        listPosition = null;
        arrowListPosition = null;
    }

    public void updateList(boolean refresh) {
		observable.updateList(refresh);
	}

	public void startLoading(boolean refresh) {
		if (!TextUtils.isEmpty(storyListLink)) {
            cancelLoading();
			String link = storyListLink;
			if (!refresh) {
                link += pageNumber;
            }
            showState(ListState.PROGRESS);
		    startLoadingTask(link, refresh);
		}
	}

    public void cancelLoading() {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

	private void startLoadingTask(String link, boolean refresh) {
        subscription = LoaderHelper.getDocumentObservable(link)
                .subscribeOn(AppExecutors.STORY_LOADING_SCHEDULER)
                .doOnNext(this::parseAdditionalData)
                .doOnNext(UserModel.getInstance().getUserInfoModel()::updateUserInfo)
                .flatMap(StoryParser::parseDocument)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stories -> {
                    setListData(stories, refresh);
                    updateList(refresh);
                    isRefreshing = false;
                    if (stories.isEmpty()) {
                        showState(ListState.END);
                    } else if (pageNumber == DEFAULT_PAGE_NUMBER + 1) {
                        startLoading(false);
                    } else {
                        showState(ListState.WAIT);
                    }
                }, throwable -> {
                    errorMessageId = (((ExceptionWithInteger) throwable).getMessageId());
                    showState(ListState.ERROR);
                    isRefreshing = false;
                });
	}

    private void setListData(List<Story> stories, boolean refresh) {
        if (refresh && !storyList.isEmpty()) {
            deleteListData();
        }
        for (Story story : stories) {
            int storyId = story.getStoryId();
            if (storyIdMap.get(storyId) == null) {
                this.storyList.add(story);
                storyIdMap.put(storyId, story);
            }
        }
        if (saveData()) {
            RxDB.addStoryListToCategory(storyListCategory, stories);
        }
        pageNumber++;

    }

    private void loadStoriesFromDB() {
        showState(ListState.PROGRESS);
        RxDB.getStoryList(storyListCategory)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stories -> {
                    if (stories.isEmpty()) {
                        startLoading(false);
                    } else {
                        setStoryList(stories);
                        updateList(false);
                        observable.restoreListPosition();
                        showState(ListState.WAIT);
                    }
                });
    }

    private void setStoryList(ArrayList<Story> newStoryList) {
        storyList.addAll(newStoryList);
        for (Story story : newStoryList) {
            storyIdMap.put(story.getStoryId(), story);
        }
    }

    public void showState(ListState newState) {
        this.listState = newState;
        observable.showState(listState);
    }

    @Override
    public void storyWasVoted(int storyId, Vote vote) {
        Story story = findStoryById(storyId);
        if (story != null) {
            story.setVoteStatus(vote);
            story.setRating(story.getRating() + vote.toInt());
            if (saveData()) {
                DB.getInstance().updateStoryVoteStatus(story);
            }
            updateList(false);
        }
    }

    @Override
    public void storyWasSaved(int storyId, boolean saved) {
        Story story = findStoryById(storyId);
        if (story != null) {
            story.setSaveStatus(saved);
            if (saveData()) {
                DB.getInstance().updateStorySaveStatus(story);
            }
            updateList(false);
        }
    }

    public void loadListValues() {
		SharedPreferences sp = Preferences.get();
		int index = sp.getInt(SAVED_LIST_POSITION_INDEX + SAVE_TAG, DEFAULT_LIST_POSITION_INDEX);
		int top = sp.getInt(SAVED_LIST_POSITION_TOP + SAVE_TAG, DEFAULT_LIST_POSITION_TOP);
        if (index != DEFAULT_LIST_POSITION_INDEX) {
            listPosition = new ListPosition(index, top);
        }
		pageNumber = sp.getInt(SAVED_PAGE_NUMBER + SAVE_TAG, DEFAULT_PAGE_NUMBER);
	}
    
    public void saveListValues() {
        if (saveData()) {
            SharedPreferences sp = Preferences.get();
            Editor ed = sp.edit();
            if (listPosition != null) {
                ed.putInt(SAVED_LIST_POSITION_INDEX + SAVE_TAG, listPosition.getIndex());
                ed.putInt(SAVED_LIST_POSITION_TOP + SAVE_TAG, listPosition.getTop());
            }
            ed.putInt(SAVED_PAGE_NUMBER + SAVE_TAG, pageNumber);
            ed.apply();
        }
    }
    
	@Nullable
    private Story findStoryById(int storyId) {
        return storyIdMap.get(storyId);
	}

    public ArrayList<Story> getStoryList() {
        return storyList;
    }


    public boolean isRefreshing() {
        return isRefreshing;
    }

    public void setRefreshing(boolean isRefreshing) {
        this.isRefreshing = isRefreshing;
    }

    public int getErrorMessageId() {
        return errorMessageId;
    }

    private boolean saveData() {
        return storyListCategory != null;
    }

    public Observable getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(Observable additionalData) {
        this.additionalData = additionalData;
    }

    private void parseAdditionalData(Document document) {
        if (additionalDataParser != null) {
            this.additionalData = additionalDataParser.parseData(document);
        }
    }

    public void setAdditionalDataParser(AdditionalDataParser additionalDataParser) {
        this.additionalDataParser = additionalDataParser;
    }

    public ListPosition getArrowListPosition() {
        return arrowListPosition;
    }

    public void setArrowListPosition(ListPosition arrowListPosition) {
        this.arrowListPosition = arrowListPosition;
    }

    public ListState getListState() {
        return listState;
    }

    @Override
    public void saveListPosition(ListPosition listPosition) {
        this.listPosition = listPosition;
    }

    @Override
    public ListPosition loadListPosition() {
        return listPosition;
    }
}
