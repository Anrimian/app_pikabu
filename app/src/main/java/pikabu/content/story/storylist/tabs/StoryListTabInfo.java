package pikabu.content.story.storylist.tabs;

import java.io.Serializable;

/**
 * Created on 25.11.2015.
 */
public class StoryListTabInfo implements Serializable {
    public transient static final String TAB_INFO_LIST = "tab_info_list";

    private int titleId;

    private String link;
    private String category;

    private boolean hasFilters;

    public StoryListTabInfo(String category, String link, int titleId) {
        this(category, link, titleId, false);
    }

    public StoryListTabInfo(String category, String link, int titleId, boolean hasFilters) {
        this.category = category;
        this.link = link;
        this.titleId = titleId;
        this.hasFilters = hasFilters;
    }

    public String getLink() {
        return link;
    }

    public int getTitleId() {
        return titleId;
    }

    public String getCategory() {
        return category;
    }

    public boolean isHasFilters() {
        return hasFilters;
    }
}
