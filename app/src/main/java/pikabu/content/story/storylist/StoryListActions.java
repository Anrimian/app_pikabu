package pikabu.content.story.storylist;

/**
 * Created on 18.11.2016.
 */

public interface StoryListActions {

    void onErrorClick();

    void onRefresh();
}
