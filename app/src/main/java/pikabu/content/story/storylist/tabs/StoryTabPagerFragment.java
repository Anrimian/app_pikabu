package pikabu.content.story.storylist.tabs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.adapter.ViewPagerAdapter;
import com.emirinay.tools.views.CustomViewPager;

import java.util.ArrayList;

import pikabu.Preferences;
import pikabu.content.story.storylist.StoryListFragment;

/**
 * Created on 27.11.2015.
 */
public class StoryTabPagerFragment extends Fragment {
    protected static final String TAB_INFO_LIST = "tab_info_list";

    public static final String TAB_PAGER_TITLE = "story_list_title";

    public static final String SAVE_POSITION_TAG = "save_position_tag";

    private CustomViewPager viewPager;

    public static final int NO_POSITION = -1;
    private int pagerPosition = NO_POSITION;

    public static StoryTabPagerFragment newInstance(ArrayList<StoryListTabInfo> storyCategories,
                                                    int titleId) {
        return newInstance(storyCategories, titleId, null);
    }

    public static StoryTabPagerFragment newInstance(ArrayList<StoryListTabInfo> storyCategories,
                                                    int titleId,
                                                    String savePositionTag) {
        StoryTabPagerFragment fragment = new StoryTabPagerFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(TAB_INFO_LIST, storyCategories);
        arguments.putInt(TAB_PAGER_TITLE, titleId);
        arguments.putString(SAVE_POSITION_TAG, savePositionTag);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (pagerPosition == NO_POSITION) {
            if (needSavePosition()) {
                SharedPreferences sp = Preferences.get();
                pagerPosition = sp.getInt(getSavePositionTag(), 0);
            } else {
                pagerPosition = 0;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        int titleId = getArguments().getInt(TAB_PAGER_TITLE);
        if (titleId != 0) {
            getActivity().setTitle(titleId);
        }

        View view = inflater.inflate(R.layout.tab_pager, null);
        viewPager = (CustomViewPager) view.findViewById(R.id.tabanim_viewpager);
        viewPager.setPagingEnabled(Preferences.get().getBoolean("enable_swipe", false));
        setupViewPager(viewPager);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(new OnTabSelectedListener(viewPager));
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        for(StoryListTabInfo tabInfo: getTabInfoList()) {
            Fragment fragment = (StoryListFragment.newInstance(tabInfo.getCategory(), tabInfo.getLink(), 0));
            pagerAdapter.addFragment(fragment, getString(tabInfo.getTitleId()));
        }
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(pagerAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (needSavePosition()) {
            selectPage(pagerPosition);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (needSavePosition()) {
            SharedPreferences sp = Preferences.get();
            sp.edit().putInt(getSavePositionTag(), pagerPosition).apply();
        }
    }

    public void selectPage(final int position) {
        pagerPosition = position;
        if (viewPager != null) {
            viewPager.post(() -> viewPager.setCurrentItem(position));
        }
    }

    private ArrayList<StoryListTabInfo> getTabInfoList() {
        return (ArrayList<StoryListTabInfo>) getArguments().getSerializable(TAB_INFO_LIST);
    }

    private boolean needSavePosition() {
        return getSavePositionTag() != null;
    }

    private String getSavePositionTag() {
        return getArguments().getString(SAVE_POSITION_TAG);
    }

    private class OnTabSelectedListener extends TabLayout.ViewPagerOnTabSelectedListener {

        public OnTabSelectedListener(ViewPager viewPager) {
            super(viewPager);
        }

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            super.onTabSelected(tab);
            if (needSavePosition()) {
                pagerPosition = tab.getPosition();
            }
        }
    }
}
