package pikabu.content.story.storylist;

/**
 * Created on 01.12.2016.
 */

public enum ListState {
    END {
        @Override
        public void showState(StoryListObserver observable) {
            observable.showEndState();
        }
    },
    ERROR {
        @Override
        public void showState(StoryListObserver observable) {
            observable.showErrorState();
        }
    },
    PROGRESS {
        @Override
        public void showState(StoryListObserver observable) {
            observable.showProgressState();
        }
    },
    WAIT {
        @Override
        public void showState(StoryListObserver observable) {
            observable.showWaitState();
        }

    };

    public void showState(StoryListObserver observer) {

    }
}
