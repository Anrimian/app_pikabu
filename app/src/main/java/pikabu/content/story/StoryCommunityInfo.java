package pikabu.content.story;

import java.io.Serializable;

/**
 * Created on 19.06.2016.
 */
public class StoryCommunityInfo implements Serializable {
    private String name;
    private String pathName;

    public StoryCommunityInfo(String name, String pathName) {
        this.name = name;
        this.pathName = pathName;
    }

    public String getName() {
        return name;
    }

    public String getPathName() {
        return pathName;
    }
}
