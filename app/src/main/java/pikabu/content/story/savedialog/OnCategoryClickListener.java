package pikabu.content.story.savedialog;

import pikabu.main.sections.saved.stories.SaveCategory;

/**
 * Created on 18.12.2016.
 */

public interface OnCategoryClickListener {
    void onSelected(SaveCategory saveCategory);
}
