package pikabu.content.story.savedialog;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;

import java.util.List;

import pikabu.main.sections.saved.stories.SaveCategory;

/**
 * Created on 18.12.2016.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.SaveCategoryViewHolder> {

    private OnCategoryClickListener onCategoryClickListener;
    private List<SaveCategory> categories;

    public ListAdapter(List<SaveCategory> categories, OnCategoryClickListener onCategoryClickListener) {
        this.categories = categories;
        this.onCategoryClickListener = onCategoryClickListener;
    }

    @Override
    public SaveCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_view, parent, false);
        return new SaveCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SaveCategoryViewHolder holder, int position) {
        SaveCategory saveCategory = categories.get(position);
        holder.bind(saveCategory);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setCategories(List<SaveCategory> categories) {
        this.categories = categories;
    }

    public void setData(List<SaveCategory> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    class SaveCategoryViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;

        private SaveCategory saveCategory;

        public SaveCategoryViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> onCategoryClickListener.onSelected(saveCategory));
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        public void bind(SaveCategory saveCategory) {
            this.saveCategory = saveCategory;
            tvName.setText(saveCategory.getName());
        }
    }
}
