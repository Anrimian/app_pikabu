package pikabu.content.story.savedialog;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;

/**
 * Created on 15.12.2016.
 */

public class SaveStoryView {

    private ViewActions viewActions;

    private ImageView btnRefresh;
    private RecyclerView recyclerView;
    private TextView errorView;

    public SaveStoryView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.select_category);
        btnRefresh = (ImageView) view.findViewById(R.id.refresh);
        View clickable_button = view.findViewById(R.id.clickable_button);
        clickable_button.setOnClickListener(v -> viewActions.startLoading());
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        errorView = (TextView) view.findViewById(R.id.tv_error);
    }

    public void bind(ListAdapter adapter, ViewActions viewActions) {
        this.viewActions = viewActions;
        recyclerView.setAdapter(adapter);
    }

    public void onComplete() {
        stopButtonAnimation();
        recyclerView.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
    }


    public void showError(int errorMessageId) {
        stopButtonAnimation();
        recyclerView.setVisibility(View.INVISIBLE);
        errorView.setVisibility(View.VISIBLE);
        errorView.setText(errorMessageId);
    }

    public void showProgress() {
        startButtonAnimation();
        recyclerView.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
    }

    private void startButtonAnimation() {
        if (btnRefresh != null) {
            Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_refresh);
            anim.setRepeatCount(Animation.INFINITE);
            btnRefresh.setEnabled(false);
            btnRefresh.startAnimation(anim);
        }
    }

    private void stopButtonAnimation() {
        if (btnRefresh != null) {
            btnRefresh.setEnabled(true);
            btnRefresh.clearAnimation();
        }
    }

    private Context getContext() {
        return btnRefresh.getContext();
    }
}
