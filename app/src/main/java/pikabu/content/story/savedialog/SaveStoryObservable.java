package pikabu.content.story.savedialog;

import android.database.Observable;

import java.util.List;

import pikabu.main.sections.saved.stories.SaveCategory;

/**
 * Created on 12.07.2015.
 */
public class SaveStoryObservable extends Observable<SaveStoryObserver> {

    public void showState(State state) {
        for(SaveStoryObserver observer : mObservers) {
            state.showState(observer);
        }
    }

    public void updateList(List<SaveCategory> categories){
        for (final SaveStoryObserver observer : mObservers) {
            observer.updateList(categories);
        }
    }
}
