package pikabu.content.story.savedialog;

import com.emirinay.pikabuapp.R;

import java.util.List;

import pikabu.content.ParserException;
import pikabu.data.Links;
import pikabu.main.sections.saved.stories.SaveCategory;
import pikabu.user.UserModel;
import pikabu.user.savedcategories.SavedCategoriesModel;
import pikabu.user.savedcategories.SavedCategoriesObserver;
import pikabu.user.savedcategories.parser.SavedCategoriesParser;
import pikabu.utils.network.LoaderHelper;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 15.12.2016.
 */

public class SaveStoryModel implements SavedCategoriesObserver {

    private State state = State.COMPLETE;

    private SaveStoryObservable observable = new SaveStoryObservable();

    private SavedCategoriesModel savedCategoriesModel;

    private List<SaveCategory> categories;
    private SaveCategory selectedCategory;
    private int errorMessageId;

    public SaveStoryModel() {
        savedCategoriesModel = UserModel.getInstance().getSavedCategoriesModel();
        savedCategoriesModel.registerObserver(this);
    }

    public void registerObserver(SaveStoryObserver observer) {
        state.showState(observer);
        observable.registerObserver(observer);
    }

    public void unregisterObserver(SaveStoryObserver observer) {
        observable.unregisterObserver(observer);
    }

    @Override
    public void onUpdateSavedCategories() {
        categories = savedCategoriesModel.getCategories();
        observable.updateList(categories);
    }

    public void updateSaveCategories() {
        showState(State.PROGRESS);
        LoaderHelper.getDocumentObservable(Links.LINK_TO_SAVED_CATEGORIES)
                .subscribeOn(Schedulers.newThread())
                .flatMap(SavedCategoriesParser::parseSavedCategories)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(categories -> {
                    savedCategoriesModel.setCategories(categories);
                    observable.updateList(categories);
                    showState(State.COMPLETE);
                }, throwable -> {
                    throwable.printStackTrace();
                    if (throwable instanceof ParserException) {
                        errorMessageId = R.string.parser_error;
                    } else {
                        errorMessageId = R.string.connection_error;
                    }
                    showState(State.ERROR);
                });
    }

    public void showState(State newState) {
        state = newState;
        observable.showState(state);
    }

    public List<SaveCategory> getCategories() {
        return categories;
    }

    public SaveCategory getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(SaveCategory selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public int getErrorMessageId() {
        return errorMessageId;
    }
}
