package pikabu.content.story.savedialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created on 18.12.2016.
 */

public class SaveStoryModelContainer extends Fragment {

    private static final String SAVE_STORY_MODEL_TAG = "save_story_model_tag";

    private SaveStoryModel model;

    public static SaveStoryModel getModel(Context ctx) {
        FragmentManager fm = ((FragmentActivity) ctx).getSupportFragmentManager();
        SaveStoryModelContainer container = (SaveStoryModelContainer) fm
                .findFragmentByTag(SAVE_STORY_MODEL_TAG);
        if (container == null) {
            container = new SaveStoryModelContainer();
            fm.beginTransaction().add(container, SAVE_STORY_MODEL_TAG).commit();
        }
        if (container.model == null) {
            container.model = new SaveStoryModel();
        }
        return container.model;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}