package pikabu.content.story.savedialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;

import pikabu.main.sections.saved.stories.SaveCategory;
import pikabu.user.UserModel;
import pikabu.user.actions.UserActions;

/**
 * Created on 15.12.2016.
 */

public class SaveStoryDialogFragment extends DialogFragment implements DialogResultActions {
    private static final String STORY_ID = "story_id";

    public static void start(FragmentActivity act, int storyId) {
        if (UserModel.getInstance().isLogin()) {
            SaveStoryDialogFragment dialogFragment = new SaveStoryDialogFragment();
            Bundle args = new Bundle();
            args.putInt(STORY_ID, storyId);
            dialogFragment.setArguments(args);
            dialogFragment.show(act.getSupportFragmentManager(), "save_dialog");
        } else {
            Toast.makeText(act, R.string.must_login_for_action, Toast.LENGTH_SHORT).show();
        }
    }

    private SaveStoryModel saveStoryModel;
    private SaveStoryView saveStoryView;
    private SaveStoryController saveStoryController;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(getContext(), R.layout.select_save_category_dialog, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        saveStoryView = new SaveStoryView(view);
        saveStoryModel = SaveStoryModelContainer.getModel(getContext());
        saveStoryController = new SaveStoryController(saveStoryView, saveStoryModel, getActivity());
        saveStoryController.bind(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        saveStoryController.unbind();
    }


    @Override
    public void complete() {
        SaveCategory saveCategory = saveStoryModel.getSelectedCategory();
        if (saveCategory == null) {
            Toast.makeText(getContext(), R.string.select_category, Toast.LENGTH_SHORT).show();
            return;
        }
        UserActions.getInstance().saveStory(getArguments().getInt(STORY_ID), saveCategory.getId());
        dismiss();
    }

    @Override
    public void cancel() {
        dismiss();
    }
}
