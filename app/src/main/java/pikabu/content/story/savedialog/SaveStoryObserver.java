package pikabu.content.story.savedialog;

import java.util.List;

import pikabu.main.sections.saved.stories.SaveCategory;

/**
 * Created on 12.07.2015.
 */
public interface SaveStoryObserver {

    void onComplete();

    void onError();

    void onProgress();

    void updateList(List<SaveCategory> categories);
}
