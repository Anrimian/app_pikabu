package pikabu.content.story.savedialog;

/**
 * Created on 01.12.2016.
 */

public enum State {
    COMPLETE {
        @Override
        public void showState(SaveStoryObserver observable) {
            observable.onComplete();
        }
    },
    ERROR {
        @Override
        public void showState(SaveStoryObserver observable) {
            observable.onError();
        }
    },
    PROGRESS {
        @Override
        public void showState(SaveStoryObserver observable) {
            observable.onProgress();
        }
    };

    public void showState(SaveStoryObserver observer) {

    }
}
