package pikabu.content.story.savedialog;

/**
 * Created on 19.12.2016.
 */

public interface DialogResultActions {
    void complete();

    void cancel();
}
