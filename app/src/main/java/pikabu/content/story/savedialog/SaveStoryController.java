package pikabu.content.story.savedialog;

import android.app.Activity;
import android.widget.Toast;

import java.util.List;

import pikabu.main.sections.saved.stories.SaveCategory;

/**
 * Created on 15.12.2016.
 */

public class SaveStoryController implements SaveStoryObserver, OnCategoryClickListener, ViewActions {

    private Activity activity;

    private ListAdapter adapter;
    private SaveStoryView saveStoryView;
    private SaveStoryModel saveStoryModel;

    private DialogResultActions dialogActions;

    public SaveStoryController(SaveStoryView saveStoryView, SaveStoryModel saveStoryModel, Activity activity) {
        this.saveStoryView = saveStoryView;
        this.saveStoryModel = saveStoryModel;
        this.activity = activity;
        adapter = new ListAdapter(saveStoryModel.getCategories(), this);
    }

    public void bind(DialogResultActions dialogActions) {
        this.dialogActions = dialogActions;
        saveStoryModel.registerObserver(this);
        saveStoryView.bind(adapter, this);
    }

    public void unbind() {
        saveStoryModel.unregisterObserver(this);
    }

    @Override
    public void onComplete() {
        saveStoryView.onComplete();
    }

    @Override
    public void onError() {
        int messageId = saveStoryModel.getErrorMessageId();
        if (!isListEmpty()) {
            Toast.makeText(activity, messageId, Toast.LENGTH_SHORT).show();
            saveStoryModel.showState(State.COMPLETE);
            return;
        }
        saveStoryView.showError(messageId);
    }

    @Override
    public void onProgress() {
        saveStoryView.showProgress();
    }

    @Override
    public void updateList(List<SaveCategory> categories) {
        adapter.setData(categories);
    }

    @Override
    public void onSelected(SaveCategory saveCategory) {
        saveStoryModel.setSelectedCategory(saveCategory);
        dialogActions.complete();
    }

    @Override
    public void startLoading() {
        saveStoryModel.updateSaveCategories();
    }

    public boolean isListEmpty() {
        return adapter.getItemCount() == 0;
    }
}
