package pikabu.content.story.parser.blocks;

import com.emirinay.tools.TextUtils;

import org.jsoup.nodes.Element;

import pikabu.content.ParserException;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.content.story.blocks.VideoContentBlock;
import pikabu.content.story.blocks.parser.StoryContentBlockParser;
import pikabu.image.ImageSize;

/**
 * Created on 04.12.2015.
 */
public class VideoStoryContentBlockParser extends StoryContentBlockParser {

    protected Element element;

    public VideoStoryContentBlockParser(Element element) {
        this.element = element;
    }

    @Override
    public StoryContentBlock parseContent() throws NumberFormatException, ParserException {
        return new VideoContentBlock(selectVideoLink(),
                selectPreviewImageLink(),
                selectImageSize());
    }

    private String selectVideoLink() throws ParserException {
        String link = element.select(".b-video").attr("data-url");
        if (link.isEmpty()) {
            throw new ParserException("video link is empty");
        }
        return link;
    }

    private String selectPreviewImageLink() {
        try {
            String previewImageLinkSource = element.select(".b-video__preview.b-video__preview_show_yes").attr("style");
            return TextUtils.getStringBetween(previewImageLinkSource, "background-image: url(", ");");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private ImageSize selectImageSize() {
        ImageSize imageSize = new ImageSize();
        imageSize.setPreviewWidth(480);
        imageSize.setPreviewHeight(360);
        return imageSize;
    }
}
