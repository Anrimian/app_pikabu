package pikabu.content.story.parser;

import com.emirinay.tools.TextUtils;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import pikabu.content.ParserException;
import pikabu.content.Vote;
import pikabu.content.story.Story;
import pikabu.content.story.StoryCommunityInfo;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.content.story.blocks.parser.StoryContentBlockParser;
import pikabu.content.story.parser.blocks.ImageStoryContentBlockParser;
import pikabu.content.story.parser.blocks.SingleGifStoryContentBlockParser;
import pikabu.content.story.parser.blocks.SingleImageStoryContentBlockParser;
import pikabu.content.story.parser.blocks.SingleTextStoryContentBlockParser;
import pikabu.content.story.parser.blocks.SingleVideoStoryContentBlockParser;
import pikabu.content.story.parser.blocks.TextStoryContentBlockParser;
import pikabu.content.story.parser.blocks.VideoStoryContentBlockParser;
import rx.Observable;

public class StoryParser {

    public static Observable<List<Story>> parseDocument(Document doc) {
        return getElements(doc).filter(StoryParser::isStory)
                .flatMap(StoryParser::parseElement)
                .toList();
    }

    private static Observable<Element> getElements(Document doc) {
        return Observable.from(selectElements(doc));
    }

    private static Observable<Story> parseElement(Element element) {
        return Observable.create(subscriber -> {
            StoryParser parser = new StoryParser(element);
            Story story = parser.parseElement();
            if (story != null) {
                subscriber.onNext(story);
            }
            subscriber.onCompleted();
        });
    }

    private static Elements selectElements(Document doc) {
        return doc.select(".story");
    }

    private static boolean isStory(Element element) {
        String author = element.select(".story__author").attr("href");
        if (author.equals("http://pikabu.ru/profile/ads")) {
            return false;
        }

        if (!element.select(".story__sponsor").isEmpty()) {
            return false;
        }

        if (!element.select(".story__gag").isEmpty()) {
            return false;
        }

        String adText = element.select(".story__header-additional-wrapper").attr("href");
        if (adText.equals("http://pikabu.ru/html.php?id=ad")) {
            return false;
        }
        return true;
    }

    private Element elStory;

    private StoryParser(Element elStory) {
        this.elStory = elStory;
    }

	private Story parseElement() {
		Story story = new Story();
		try {			
			story.setTitle(selectTitle());
			story.setAuthor(selectAuthor());
			story.setCommentsNumber(selectCommentsNumber());
			story.setPreviewText(selectPreviewText());
			story.setLink(selectLink());
			story.setRating(selectRating());
			story.setVoteStatus(selectVoteStatus());
			story.setSaveStatus(selectSaveStatus());
			story.setIsMyTag(selectMyTag());
			story.setIsStrawTag(selectStrawTag());
			story.setStoryId(selectStoryId());
			story.setUnixTime(selectTime());
			story.setCommunityInfo(selectCommunityInfo());
			story.setTagList(selectTagList());
            story.setContentBlocks(selectContentBlocks());
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}		
		return story;
	}

	private StoryCommunityInfo selectCommunityInfo() {
		Element element = elStory
				.select(".story__header-additional-wrapper")
				.first()
				.getElementsByAttributeValueContaining("href", "/community/").first();
		if (element == null) {
			return null;
		}
        String name = element.text();
        String link = element.attr("href");
        if (name.isEmpty() || link.isEmpty()) {
            return null;
        } else {
            link = TextUtils.getUrlLastSegment(link);
            return new StoryCommunityInfo(name, link);
        }
	}

	private ArrayList<StoryContentBlock> selectContentBlocks() {
        ArrayList<StoryContentBlock> contentBlocks = new ArrayList<>();
        if (!elStory.select(".b-story-blocks__wrapper").isEmpty()) {
            contentBlocks.addAll(selectMultipleBlocks());
        } else {
			StoryContentBlock contentBlock = selectSingleBlock();
			if (contentBlock != null) {
				contentBlocks.add(contentBlock);
			}
        }
        return contentBlocks;
    }

    private StoryContentBlock selectSingleBlock() {
        try {
            return selectSingleBlockParser().parseContent();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private StoryContentBlockParser selectSingleBlockParser() throws ParserException {
        String blockType = elStory.select(".story__wrapper").toString();
        if (blockType.contains("b-gifx")) {
            return new SingleGifStoryContentBlockParser(elStory);
        }
        if (blockType.contains("b-video")) {
            return new SingleVideoStoryContentBlockParser(elStory);
        }
        if (blockType.contains("b-story__content b-story__content_type_media")) {
            return new SingleImageStoryContentBlockParser(elStory);
        }
        if (blockType.contains("b-story__content b-story__content_type_text") || blockType.contains("text_story_div")) {
            return new SingleTextStoryContentBlockParser(elStory);
        }
        throw new ParserException("can not select single block parser");
    }

    private ArrayList<StoryContentBlock> selectMultipleBlocks() {
        ArrayList<StoryContentBlock> contentBlocks = new ArrayList<>();
        try {
            for (Element elBlock : elStory.select(".b-story-block")) {
                StoryContentBlockParser blockParser = selectMultipleBlockParser(elBlock);
				StoryContentBlock contentBlock = blockParser.parseContent();
                contentBlocks.add(contentBlock);
            }
        } catch (Exception e) {
            e.printStackTrace();
            contentBlocks.clear();
        }
        return contentBlocks;
    }

    private StoryContentBlockParser selectMultipleBlockParser(Element elBlock) throws ParserException {
        if (!elBlock.select(".b-story-block_type_image").isEmpty()) {
            if (!elBlock.select(".b-gifx").isEmpty()) {
                return new SingleGifStoryContentBlockParser(elBlock);
            } else {
                return new ImageStoryContentBlockParser(elBlock);
            }
        }
        if (!elBlock.select(".b-story-block_type_text").isEmpty()) {
            return new TextStoryContentBlockParser(elBlock);
        }
        if (!elBlock.select(".b-story-block_type_video").isEmpty()) {
            return new VideoStoryContentBlockParser(elBlock);
        }
        throw new ParserException("can not selecr multiple block parser");
    }

    private Vote selectVoteStatus() {
	    String htmlVoteStatus = elStory.select(".story__rating-block").attr("data-vote");
		if (htmlVoteStatus.equals("1")) {
		    return Vote.UP;
		}
		if (htmlVoteStatus.equals("-1")) {
		    return Vote.DOWN;
		}		    
        return Vote.NOTHING;
	}

    private boolean selectSaveStatus() {
        String htmlVoteStatus = elStory.select(".story__save.i-sprite--comments__save_active").toString();
        return !htmlVoteStatus.isEmpty();
    }

	private int selectStoryId() throws NumberFormatException, ParserException {
		String storyIdStr = elStory.select(".story").attr("data-story-id");
		int storyId = Integer.parseInt(storyIdStr);
		if (storyId <= 0) {
			throw new ParserException("invalid storyId: " + storyIdStr);
		}
		return storyId;
	}

	private String selectTitle() throws ParserException {
		String title = elStory.select(".story__title-link").text();
		if (title.isEmpty()) {
			throw new ParserException("title is empty, element: " + elStory);
		}		
		return title;
	}

	private ArrayList<String> selectTagList() {
		Elements tags = elStory.select(".story__tag");
		ArrayList<String> tagList = new ArrayList<>();
		for (Element tag : tags) {
			String stTag = tag.text();
			if (!stTag.isEmpty()) {
				tagList.add(stTag);
			}
		}
		return tagList;

	}

	private String selectPreviewText() {
        return elStory.select(".story__description").text();
	}

	private String selectAuthor() throws ParserException {
		String author = elStory.select(".story__author").text();
		if (author.isEmpty() || (author.contains("html.php"))) {
			throw new ParserException("can not get author name");
		}		
		return author;

	}

	private int selectCommentsNumber() throws NumberFormatException, ParserException {
		String commentsNumberStr = elStory.select(".story__comments-count.story__to-comments").text();
		if (commentsNumberStr.isEmpty()) {
			throw new ParserException("comment number string is empty");
		}
		return TextUtils.getNumbersFromString(commentsNumberStr);
	}

	private long selectTime() throws NumberFormatException, ParserException, ParseException {
		String timeStr = elStory.select(".story__date").attr("title");
		long unixTime = Long.parseLong(timeStr);
		if (unixTime <= 0) {
            throw new ParserException("invalid unix time: " + unixTime);
        }
		return unixTime;
	}

	private int selectRating() {
        Element element = elStory.select(".story__rating-count").first();
		String ratingStr = element.text();

		if (ratingStr.isEmpty()) {
            if (!element.select(".i-sprite--inline-block.i-sprite--feed__rating-trash").toString().isEmpty()) {
                return Story.DELETED;
            }
            if (!element.select(".i-sprite--inline-block.i-sprite--feed__rating-lock").toString().isEmpty()) {
                return Story.HIDE_RATING;
            }
        }
		try {
		    return Integer.parseInt(ratingStr);
		} catch(NumberFormatException e) {
            e.printStackTrace();
			return Story.NO_RATING;
		}

	}

	private boolean selectMyTag() {
		String my = elStory.select(".story__authors").toString();
		boolean isMy = false;
		if (!my.isEmpty()) {
            isMy = true;
        }
		return isMy;
	}

	private boolean selectStrawTag() {
		String straw = elStory.select(".story__straw").toString();
		boolean isStraw = false;
		if (!straw.isEmpty()) {
            isStraw = true;
        }
		return isStraw;
	}

	private String selectLink() throws ParserException {
		String link = elStory.select(".story__title-link").attr("href");
		if (link.isEmpty()) {
			throw new ParserException("link is empty");
		}
		return link;

	}
}
