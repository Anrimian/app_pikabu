package pikabu.content.story.parser.blocks;

import org.jsoup.nodes.Element;

import pikabu.content.ParserException;
import pikabu.content.story.blocks.ImageContentBlock;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.content.story.blocks.parser.StoryContentBlockParser;
import pikabu.image.ImageSize;

/**
 * Created on 04.12.2015.
 */
public class ImageStoryContentBlockParser extends StoryContentBlockParser {

    protected Element element;

    public ImageStoryContentBlockParser(Element element) {
        this.element = element;
    }

    @Override
    public StoryContentBlock parseContent() throws NumberFormatException, ParserException {
        return new ImageContentBlock(selectImageLink(),
                selectBigImageLink(),
                selectImageSize());
    }

    private String selectImageLink() throws ParserException {
        String previewImageLink = element.select("img").attr("src");
        if (previewImageLink.isEmpty()) {
            previewImageLink = element.select("img").attr("data-src");
            if (previewImageLink.isEmpty()) {
                throw new ParserException("previewImageLink is empty");
            }
        }
        return previewImageLink;
    }

    private ImageSize selectImageSize() {
        ImageSize imageSize = new ImageSize();
        int height = selectImageHeight();
        if (height != 0) {
            imageSize.setPreviewHeight(height);
        }
        return imageSize;
    }

    private int selectImageHeight() {
        String heightStr = element.select("img").attr("data-height");
        try {
            int height = Integer.parseInt(heightStr);
            if (height > 0) {
                return height;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private String selectBigImageLink() {
        String bigImageLink = element.select("img").attr("data-large-image");
        if (bigImageLink.isEmpty()) {
            return null;
        }
        return bigImageLink;
    }
}
