package pikabu.content.story.parser.blocks;

import org.jsoup.nodes.Element;

import pikabu.content.ParserException;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.content.story.blocks.VideoContentBlock;
import pikabu.content.story.blocks.parser.StoryContentBlockParser;
import pikabu.image.ImageSize;

/**
 * Created on 01.12.2015.
 */
public class SingleVideoStoryContentBlockParser extends StoryContentBlockParser {

    protected Element element;

    public SingleVideoStoryContentBlockParser(Element element) {
        this.element = element;
    }

    @Override
    public StoryContentBlock parseContent() throws NumberFormatException, ParserException {
        return new VideoContentBlock(selectVideoLink(),
                selectPreviewImageLink(),
                selectImageSize());
    }

    private String selectVideoLink() throws ParserException {
        String link = element.select(".b-video").attr("data-url");
        if (link.isEmpty()) {
            throw new ParserException("video link is empty");
        }
        return link;
    }

    private String selectPreviewImageLink() {
        return element.select(".sv_img.showvideo ").attr("preview");
    }

    private ImageSize selectImageSize() {
        ImageSize imageSize = new ImageSize();
        imageSize.setPreviewWidth(480);
        imageSize.setPreviewHeight(360);
        return imageSize;
    }
}
