package pikabu.content.story.parser.blocks;

import org.jsoup.nodes.Element;

import pikabu.content.ParserException;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.content.story.blocks.TextContentBlock;
import pikabu.content.story.blocks.parser.StoryContentBlockParser;

/**
 * Created on 04.12.2015.
 */
public class TextStoryContentBlockParser extends StoryContentBlockParser {

    protected Element element;

    public TextStoryContentBlockParser(Element element) {
        this.element = element;
    }

    @Override
    public StoryContentBlock parseContent() throws NumberFormatException, ParserException {
        return new TextContentBlock(selectText());
    }

    private String selectText() {
        return element.select(".b-story-block__content").toString();
    }
}
