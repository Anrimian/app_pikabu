package pikabu.content.story.parser.blocks;

import org.jsoup.nodes.Element;

import pikabu.content.ParserException;
import pikabu.content.story.blocks.GifContentBlock;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.content.story.blocks.parser.StoryContentBlockParser;
import pikabu.image.ImageSize;

/**
 * Created on 01.12.2015.
 */
public class SingleGifStoryContentBlockParser extends StoryContentBlockParser {

    protected Element element;

    public SingleGifStoryContentBlockParser(Element element) {
        this.element = element;
    }

    @Override
    public StoryContentBlock parseContent() throws NumberFormatException, ParserException {
        return new GifContentBlock(selectLink(), selectImageSize());
    }

    private String selectLink() throws ParserException {
        String link = element.select(".b-gifx__player").attr("data-src");
        if (link.isEmpty()) {
            throw new ParserException("gif link is empty");
        }
        return link;
    }

    private ImageSize selectImageSize()  {
        ImageSize imageSize = new ImageSize();

        int height = selectImageHeight();
        if (height != 0) {
            imageSize.setPreviewHeight(height);
        }

        int width = selectImageWidth();
        if (width != 0) {
            imageSize.setPreviewWidth(width);
        }
        return imageSize;
    }

    private int selectImageHeight() {
        String heightStr = element.select(".b-gifx").attr("data-height");
        try {
            int height = Integer.parseInt(heightStr);
            if (height > 0) {
                return height;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int selectImageWidth() {
        String widthStr = element.select(".b-gifx").attr("data-width");
        try {
            int width = Integer.parseInt(widthStr);
            if (width > 0) {
                return width;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
