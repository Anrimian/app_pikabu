package pikabu.content.story.parser.blocks;

import org.jsoup.nodes.Element;

import pikabu.content.ParserException;
import pikabu.content.story.blocks.ImageContentBlock;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.content.story.blocks.parser.StoryContentBlockParser;
import pikabu.image.ImageSize;

/**
 * Created on 01.12.2015.
 */
public class SingleImageStoryContentBlockParser extends StoryContentBlockParser {

    protected Element element;

    public SingleImageStoryContentBlockParser(Element element) {
        this.element = element;
    }

    @Override
    public StoryContentBlock parseContent() throws NumberFormatException, ParserException {
        return new ImageContentBlock(selectImageLink(),
                selectBigImageLink(),
                selectImageSize());
    }

    private String selectImageLink() throws ParserException {
        String previewImageLink = element.select(".b-story__content.b-story__content_type_media a img").attr("src");
        if (previewImageLink.isEmpty()) {
            throw new ParserException("image link is empty");
        }
        return previewImageLink;
    }

    private ImageSize selectImageSize()  {
        ImageSize imageSize = new ImageSize();

        int height = selectImageHeight();
        if (height != 0) {
            imageSize.setPreviewHeight(height);
        }

        int width = selectImageWidth();
        if (width != 0) {
            imageSize.setPreviewWidth(width);
        }
        return imageSize;
    }

    private int selectImageHeight() {
        String heightStr = element.select(".b-story__content.b-story__content_type_media a img").attr("height");
        try {
            int height = Integer.parseInt(heightStr);
            if (height > 0) {
                return height;
            }
        } catch (NumberFormatException e) {
            return 0;
        }
        return 0;
    }

    private int selectImageWidth() {
        String widthStr = element.select(".b-story__content.b-story__content_type_media a img").attr("width");
        try {
            int width = Integer.parseInt(widthStr);
            if (width > 0) {
                return width;
            }
        } catch (NumberFormatException e) {
            return 0;
        }
        return 0;
    }

    private String selectBigImageLink() {
        String bigImageLink = element.select(".b-story__content.b-story__content_type_media a img").attr("data-large-image");
        if (bigImageLink.isEmpty()) {
            return null;
        }
        return bigImageLink;
    }
}
