package pikabu.content.story.parser.blocks;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import pikabu.content.ParserException;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.content.story.blocks.TextContentBlock;
import pikabu.content.story.blocks.parser.StoryContentBlockParser;

/**
 * Created on 01.12.2015.
 */
public class SingleTextStoryContentBlockParser extends StoryContentBlockParser {

    protected Element element;

    public SingleTextStoryContentBlockParser(Element element) {
        this.element = element;
    }

    @Override
    public StoryContentBlock parseContent() throws NumberFormatException, ParserException {
        return new TextContentBlock(selectText());
    }

    private String selectText() throws ParserException {
        Elements elements = element.select(".b-story__content.b-story__content_type_text");

        Elements elementsToRemove = elements.select(".b-story__show-all.b-story__show-all_legacy");
        elementsToRemove.remove();

        String text = elements.toString();
        if (text.isEmpty()) {
            text = element.select(".text_story_div").toString();
        }
        if (text.isEmpty()) {
            throw new ParserException("text is empty");
        }
        return text;
    }
}
