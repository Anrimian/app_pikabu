package pikabu.content.story;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.PopupMenu;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.AndroidUtils;
import com.emirinay.tools.time.FormatTimeHelper;

import org.apmem.tools.layouts.FlowLayout;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import pikabu.Preferences;
import pikabu.content.PikabuLinkOnClickListener;
import pikabu.content.Vote;
import pikabu.content.blocks.ContentBlocksViewHolder;
import pikabu.content.community.CommunityOnClickListener;
import pikabu.content.community.view.CommunityActivity;
import pikabu.content.profile.ProfileOnClickListener;
import pikabu.content.profile.view.ProfileActivity;
import pikabu.content.story.blocks.ContentBlockHeightCalculator;
import pikabu.content.story.blocks.StoryBindArgs;
import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.content.story.savedialog.SaveStoryDialogFragment;
import pikabu.image.LongStoryCallback;
import pikabu.main.sections.search.SearchActivity;
import pikabu.main.sections.settings.SelectSaveCategoryDialogPreference;
import pikabu.user.UserModel;
import pikabu.user.actions.UserActions;
import pikabu.utils.text.CopyTextActivity;
import pikabu.utils.text.HtmlTextView;

/**
 * Created on 22.01.2016.
 */
public class StoryViewBinder {

    private View btnActions;
    private View tvLongStory;
    private View longStoryBorder;
    private View inView;

    private TextView tvCommunity;
    private TextView tvTitle;
    private TextView tvRating;
    private TextView tvAuthor;
    private TextView tvCommentsNumber;
    private TextView tvTime;
    private HtmlTextView tvPreviewText;

    private ImageView btnVoteUp;
    private ImageView btnVoteDown;
    private ImageView btnSave;
    private ImageView ivRatingIcon;

    private FlowLayout vgTagContainer;

    private ContentBlocksViewHolder blocksViewHolder;

    private Story story;

    private FragmentActivity fragmentActivity;

    public StoryViewBinder(View view, FragmentActivity fragmentActivity) {
        this.fragmentActivity = fragmentActivity;
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        btnActions = view.findViewById(R.id.story_actions);
        tvRating = (TextView) view.findViewById(R.id.tv_rating);
        btnVoteUp = (ImageView) view.findViewById(R.id.btn_story_vote_up);
        btnVoteDown = (ImageView) view.findViewById(R.id.btn_story_vote_down);
        btnSave = (ImageView) view.findViewById(R.id.iv_save_story);
        tvAuthor = (TextView) view.findViewById(R.id.tvStoryListAuthor);
        tvCommentsNumber = (TextView) view.findViewById(R.id.tv_comments_number);
        tvTime = (TextView) view.findViewById(R.id.tvStoryListTime);
        tvPreviewText = (HtmlTextView) view.findViewById(R.id.tvStoryPreviewText);
        vgTagContainer = (FlowLayout) view.findViewById(R.id.flStoryTagList);
        tvLongStory = view.findViewById(R.id.tv_long_story);
        longStoryBorder = view.findViewById(R.id.long_story_border);
        ivRatingIcon = (ImageView) view.findViewById(R.id.iv_rating_icon);
        tvCommunity = (TextView) view.findViewById(R.id.tv_community);
        inView = view.findViewById(R.id.in);

        ViewGroup vgContentView = (ViewGroup) view.findViewById(R.id.content_view);
        blocksViewHolder = new ContentBlocksViewHolder(vgContentView);
    }

    public void bindStory(@NotNull Story story, boolean showFullContent, boolean longClickableBlock) {
        this.story = story;
        registerStoryActions();
        showTitle();
        showRating();
        showCommunity();
        showVoteStatus();
        showSaveStatus();
        showAuthor();
        showCommentsNumber();
        showTime();
        showPreviewText(longClickableBlock);
        showTagList();
        showContentView(showFullContent, longClickableBlock);

    }

    public void updateVoteStatus(Story story) {
        this.story = story;
        showVoteStatus();
        showRating();
    }

    public void updateSaveStatus(Story story) {
        this.story = story;
        showSaveStatus();
    }

    public void updateCommentsNumber(int commentsCount) {
        if (story != null) {
            story.setCommentsNumber(commentsCount);
            showCommentsNumber();
        }
    }

    private void showVoteStatus() {
        Vote vote = story.getVoteStatus();
        boolean voteUp = vote == Vote.UP;
        boolean voteDown = vote == Vote.DOWN;

        btnVoteUp.setSelected(voteUp);
        btnVoteUp.setEnabled(!voteUp);

        btnVoteDown.setSelected(voteDown);
        btnVoteDown.setEnabled(!voteDown);

        btnVoteUp.setOnClickListener(v -> voteStory(Vote.UP));
        btnVoteDown.setOnClickListener(v -> voteStory(Vote.DOWN));
    }

    private void showSaveStatus() {
        boolean isSaved = story.getSaveStatus();
        btnSave.setSelected(isSaved);
        btnSave.setOnClickListener(new SaveStoryOnClickListener());
    }

    private void showTitle() {
        String title = story.getTitle();
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(title);
        if (story.getIsStraw()) {
            ImageSpan imagespan = new ImageSpan(getContext(), R.drawable.straw, ImageSpan.ALIGN_BASELINE);
            strBuilder.append("   ");
            strBuilder.setSpan(imagespan, strBuilder.length() - 2, strBuilder.length() - 1, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        }
        if (story.getIsMy()) {
            ImageSpan imagespan = new ImageSpan(getContext(), R.drawable.my_vector, ImageSpan.ALIGN_BOTTOM);
            strBuilder.append("      ");
            strBuilder.setSpan(imagespan, strBuilder.length() - 5, strBuilder.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        tvTitle.setText(strBuilder);
    }

    private void registerStoryActions() {
        btnActions.setOnClickListener(new StoryActionsOnClickListener());
    }

    private void showRating() {
        int rating = story.getRating();
        if (rating == Story.NO_RATING) {
            tvRating.setVisibility(View.GONE);
            ivRatingIcon.setVisibility(View.GONE);
            return;
        }
        if (rating == Story.HIDE_RATING || rating == Story.DELETED ) {
            int imageId = R.drawable.clock;
            int contentDescriptionId = R.string.hide_story_rating;
            if (rating == Story.DELETED) {
                imageId = R.drawable.delete;
                contentDescriptionId = R.string.story_was_deleted;
            }
            tvRating.setVisibility(View.GONE);
            ivRatingIcon.setImageResource(imageId);
            ivRatingIcon.setContentDescription(getContext().getString(contentDescriptionId));
            ivRatingIcon.setVisibility(View.VISIBLE);
        } else {
            ivRatingIcon.setVisibility(View.GONE);
            tvRating.setText(String.valueOf(rating));
            tvRating.setVisibility(View.VISIBLE);
        }
    }

    private void showAuthor() {
        String author = story.getAuthor();
        tvAuthor.setText(author);
        tvAuthor.setOnClickListener(new ProfileOnClickListener(getContext(), author));
    }

    private void showCommentsNumber() {
        int commentsNumber = story.getCommentsNumber();
        tvCommentsNumber.setText(String.valueOf(commentsNumber));
    }

    private void showTime() {
        long unixTime = story.getUnixTime();
        StringBuilder builder = new StringBuilder();
        builder.append(" ");
        builder.append(new FormatTimeHelper(getContext()).formatTime(unixTime));
        builder.append(" ");
        tvTime.setText(builder.toString());
    }

    private void showCommunity() {
        StoryCommunityInfo communityInfo = story.getCommunityInfo();
        if (communityInfo == null) {
            inView.setVisibility(View.GONE);
            tvCommunity.setVisibility(View.GONE);
        } else {
            inView.setVisibility(View.VISIBLE);
            tvCommunity.setVisibility(View.VISIBLE);
            tvCommunity.setText(communityInfo.getName());
            tvCommunity.setOnClickListener(new CommunityOnClickListener(communityInfo.getPathName()));
        }
    }

    private void showPreviewText(boolean longClickable) {
        String text = story.getPreviewText();
        if (!text.isEmpty()) {
            tvPreviewText.setVisibility(View.VISIBLE);
            tvPreviewText.setLinkOnClickListener(new PikabuLinkOnClickListener());
            if (longClickable) {
                tvPreviewText.setLongTapListener(() -> {
                    Intent intent = new Intent(getContext(), CopyTextActivity.class);
                    intent.putExtra(CopyTextActivity.TEXT, text);
                    getContext().startActivity(intent);
                });
            }
            tvPreviewText.setHtmlText(text);
        } else
            tvPreviewText.setVisibility(View.GONE);
    }

    private void showTagList() {
        ArrayList<String> tagList = story.getTagList();
        for (int i = 0; i < tagList.size(); i++) {
            View tagView = vgTagContainer.getChildAt(i);
            if (tagView == null) {
                tagView = View.inflate(getContext(), R.layout.tag_view, null);
                vgTagContainer.addView(tagView);
            }
            tagView.setVisibility(View.VISIBLE);

            String tagText = tagList.get(i);
            TextView textView = (TextView) tagView.findViewById(R.id.tv_tag);
            textView.setText(tagText);
            tagView.setOnClickListener(new TagOnClickListener(tagText));
        }

        for (int i = tagList.size(); i < vgTagContainer.getChildCount(); i++) {
            vgTagContainer.getChildAt(i).setVisibility(View.GONE);
        }
    }

    private void showContentView(boolean showFullContent, boolean longClickable) {
        ArrayList<StoryContentBlock> blocks = story.getContentBlocksList();

        tvLongStory.setVisibility(View.GONE);
        longStoryBorder.setVisibility(View.GONE);

        LongStoryCallback longStoryCallback = () -> {
            tvLongStory.setVisibility(View.VISIBLE);
            longStoryBorder.setVisibility(View.VISIBLE);
        };

        ContentBlockHeightCalculator calculator = new ContentBlockHeightCalculator(blocks);
        if (!showFullContent) {
            blocks = calculator.trimBlocks();
        }

        StoryBindArgs bindArgs = new StoryBindArgs(showFullContent, longClickable, longStoryCallback);
        blocksViewHolder.bindContentBlocks(blocks, bindArgs);

        if (calculator.isTrimmed()) {
            longStoryCallback.showLongStoryPicture();
        }
    }

    private Context getContext() {
        return tvAuthor.getContext();
    }

    private class SaveStoryOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (UserModel.getInstance().isLogin()) {
                boolean save = !story.getSaveStatus();
                if (save) {
                    if (Preferences.get().getBoolean("hide_save_story_dialog", true)) {
                        int categoryId = Preferences.get().getInt(SelectSaveCategoryDialogPreference.DEFAULT_SAVE_CATEGORY, 0);
                        UserActions.getInstance().saveStory(story.getStoryId(), categoryId);
                    } else {
                        SaveStoryDialogFragment.start(fragmentActivity, story.getStoryId());
                    }
                } else {
                    UserActions.getInstance().deleteStory(story.getStoryId());
                }
            } else {
                Toast.makeText(getContext(), R.string.must_login_for_action, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void voteStory(Vote vote) {
        if (UserModel.getInstance().isLogin()) {
            if (isTooLateForVote()) {
                Toast.makeText(getContext(), R.string.too_late_for_vote, Toast.LENGTH_SHORT).show();
            } else {
                UserActions.getInstance().voteStory(story.getStoryId(), vote);
            }
        } else {
            Toast.makeText(getContext(), R.string.must_login_for_action, Toast.LENGTH_SHORT).show();
        }
    }

    private static final long STORY_VOTE_TIME = 604800;

    private boolean isTooLateForVote() {
        long time = story.getUnixTime();
        long currentTime = System.currentTimeMillis();
        long seconds = (currentTime - time * 1000L) / 1000;
        return seconds > STORY_VOTE_TIME && story.getRating() < 250;
    }

    class StoryActionsOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.setOnMenuItemClickListener(new StoryActionsMenuOnClickListener());
            popup.inflate(R.menu.story_actions_menu);
            if (story.getCommunityInfo() != null) {
                popup.inflate(R.menu.community_actions_menu);
            }
            popup.show();
        }

        class StoryActionsMenuOnClickListener implements PopupMenu.OnMenuItemClickListener {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.story_action_share:
                        AndroidUtils.shareText(story.getLink(), getContext());
                        return true;
                    case R.id.story_action_copy_link:
                        String storyLink = story.getLink();
                        AndroidUtils.copyText(storyLink, getContext());
                        return true;
                    case R.id.go_to_profile:
                        Intent intent = new Intent(getContext(), ProfileActivity.class);
                        intent.putExtra(ProfileActivity.PROFILE_NAME, story.getAuthor());
                        getContext().startActivity(intent);
                        return true;
                    case R.id.go_to_community:
                        StoryCommunityInfo communityInfo = story.getCommunityInfo();
                        CommunityActivity.start(getContext(), communityInfo.getPathName());
                        return true;
                    default:
                        return false;
                }
            }
        }
    }

    class TagOnClickListener implements View.OnClickListener {
        private String tagText;

        public TagOnClickListener(String tagText) {
            this.tagText = tagText;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.setOnMenuItemClickListener(new TagMenuClickListener());
            popup.inflate(R.menu.tag_actions_menu);
            popup.show();
        }

        class TagMenuClickListener implements PopupMenu.OnMenuItemClickListener {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.show_story_with_tag:
                        Intent intent = new Intent(getContext(), SearchActivity.class);
                        intent.putExtra(SearchActivity.START_TAG, tagText);
                        getContext().startActivity(intent);
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}
