package pikabu.content.story.blocks;

import android.content.Context;

import pikabu.content.blocks.BindArgs;
import pikabu.content.blocks.BlockViewHolder;

/**
 * Created on 19.10.2016.
 */

public abstract class StoryContentBlockViewHolder<T extends StoryContentBlock> extends BlockViewHolder<StoryContentBlock> {

    public StoryContentBlockViewHolder(Context ctx) {
        super(ctx);
    }

    @Override
    public void bindView(StoryContentBlock block, BindArgs bindArgs) {
        bindView((T) block, (StoryBindArgs) bindArgs);
    }

    public abstract void bindView(T block, StoryBindArgs bindArgs);
}
