package pikabu.content.story.blocks;

import pikabu.content.blocks.BindArgs;
import pikabu.image.LongStoryCallback;

/**
 * Created on 14.08.2016.
 */
public class StoryBindArgs implements BindArgs {

    private boolean showFullContent;
    private boolean longClickable;
    private LongStoryCallback longStoryCallback;

    public StoryBindArgs(boolean showFullContent, boolean longClickable, LongStoryCallback longStoryCallback) {
        this.showFullContent = showFullContent;
        this.longClickable = longClickable;
        this.longStoryCallback = longStoryCallback;
    }

    public boolean isShowFullContent() {
        return showFullContent;
    }

    public boolean isLongClickable() {
        return longClickable;
    }

    public LongStoryCallback getLongStoryCallback() {
        return longStoryCallback;
    }
}
