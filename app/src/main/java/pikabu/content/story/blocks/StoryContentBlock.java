package pikabu.content.story.blocks;

import pikabu.content.blocks.ContentBlock;

/**
 * Created on 14.08.2016.
 */
public abstract class StoryContentBlock extends ContentBlock {
    public static final int BLOCK_MAX_HEIGHT = 2000;
    public static final int TRIMMED_BLOCK_HEIGHT = 900;
    public static final int DEFAULT_MAX_HEIGHT = Integer.MAX_VALUE;

    public abstract int getBlockHeight();

    public void setMaxBlockHeight(int maxHeight) {

    }
}
