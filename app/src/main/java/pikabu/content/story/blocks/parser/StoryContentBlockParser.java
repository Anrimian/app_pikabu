package pikabu.content.story.blocks.parser;

import pikabu.content.ParserException;
import pikabu.content.story.blocks.StoryContentBlock;

/**
 * Created on 01.12.2015.
 */
public abstract class StoryContentBlockParser {

    public abstract StoryContentBlock parseContent() throws NumberFormatException, ParserException;
}
