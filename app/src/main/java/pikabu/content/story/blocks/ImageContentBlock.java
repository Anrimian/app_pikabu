package pikabu.content.story.blocks;

import android.content.ContentValues;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.emirinay.pikabuapp.R;

import pikabu.content.blocks.BlockViewHolder;
import pikabu.data.DB;
import pikabu.image.ImageSize;
import pikabu.image.glide.image.MyGlideImageLoader;
import pikabu.image.glide.image.MyImageViewTarget;
import pikabu.image.glide.progress.GlideProgressBarLoadingObserver;
import pikabu.image.zoom.ZoomImageOnClickListener;

/**
 * Created on 25.07.2016.
 */
public class ImageContentBlock extends StoryContentBlock {

    private String imageLink;
    private String bigImageLink;

    private ImageSize imageSize;

    public ImageContentBlock(String imageLink, String bigImageLink, ImageSize imageSize) {
        this.imageLink = imageLink;
        this.bigImageLink = bigImageLink;
        this.imageSize = imageSize;
    }

    @Override
    public BlockViewHolder createBlockViewHolder(Context ctx) {
        return new ImageContentBlockViewHolder(ctx);
    }

    @Override
    public int getBlockHeight() {
        return imageSize.getPreviewHeight();
    }

    @Override
    public ContentType getContentType() {
        return ContentType.IMAGE;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DB.COLUMN_CONTENT, imageLink);
        cv.put(DB.COLUMN_CONTENT_SECOND, bigImageLink);
        cv.put(DB.COLUMN_PREVIEW_WIDTH, imageSize.getPreviewWidth());
        cv.put(DB.COLUMN_PREVIEW_HEIGHT, imageSize.getPreviewHeight());
        return cv;
    }

    private static class ImageContentBlockViewHolder extends StoryContentBlockViewHolder<ImageContentBlock> {

        private View view;

        private GlideProgressBarLoadingObserver progressBar;
        private ImageView imageView;

        private MyImageViewTarget myImageViewTarget;

        ImageContentBlockViewHolder(Context ctx) {
            super(ctx);
            view = View.inflate(ctx, R.layout.block_image, null);
            progressBar = (GlideProgressBarLoadingObserver) view.findViewById(R.id.progress_bar);
            imageView = (ImageView) view.findViewById(R.id.image_view);
        }

        @Override
        public void bindView(ImageContentBlock block, StoryBindArgs bindArgs) {
            progressBar.registerProgressBar(block.imageLink);
            ZoomImageOnClickListener zoomOnClickListener = new ZoomImageOnClickListener(view.getContext(),
                    block.imageLink, block.bigImageLink);
            imageView.setOnClickListener(zoomOnClickListener);
            myImageViewTarget = MyGlideImageLoader.displayImage(block.imageLink,
                    imageView,
                    block.imageSize,
                    bindArgs.isShowFullContent(),
                    bindArgs.getLongStoryCallback());
        }

        @Override
        public void clearView() {
            super.clearView();
            if (myImageViewTarget != null) {
                Glide.clear(myImageViewTarget);
                myImageViewTarget = null;
            }
        }

        @Override
        public View getView() {
            return view;
        }
    }
}
