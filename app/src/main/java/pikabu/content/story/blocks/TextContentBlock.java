package pikabu.content.story.blocks;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.emirinay.pikabuapp.R;

import pikabu.content.PikabuLinkOnClickListener;
import pikabu.content.blocks.BlockViewHolder;
import pikabu.data.DB;
import pikabu.utils.text.CopyTextActivity;
import pikabu.utils.text.HtmlTextView;

/**
 * Created on 25.07.2016.
 */
public class TextContentBlock extends StoryContentBlock {

    private String text;

    private int maxHeight = DEFAULT_MAX_HEIGHT;

    public TextContentBlock(String text) {
        this.text = text;
    }

    @Override
    public BlockViewHolder createBlockViewHolder(Context ctx) {
        return new TextStoryContentBlockViewHolder(ctx);
    }

    @Override
    public int getBlockHeight() {
        return text.length();
    }

    @Override
    public void setMaxBlockHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

    @Override
    public ContentType getContentType() {
        return ContentType.TEXT;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DB.COLUMN_CONTENT, text);
        return cv;
    }

    private static class TextStoryContentBlockViewHolder extends StoryContentBlockViewHolder<TextContentBlock> {

        private View view;

        private HtmlTextView textView;

        public TextStoryContentBlockViewHolder(Context ctx) {
            super(ctx);
            view = View.inflate(ctx, R.layout.block_text, null);
            textView = (HtmlTextView) view.findViewById(R.id.text_view);
            textView.setLinkOnClickListener(new PikabuLinkOnClickListener());
        }

        @Override
        public void bindView(TextContentBlock block, StoryBindArgs bindArgs) {
            Context context = view.getContext();
            String text = block.text;
            if (bindArgs.isLongClickable()) {
                textView.setLongTapListener(() -> {
                    Intent intent = new Intent(context, CopyTextActivity.class);
                    intent.putExtra(CopyTextActivity.TEXT, text);
                    context.startActivity(intent);
                });
            }
            String finalText = text;
            if (text.length() > block.maxHeight) {
                finalText = text.subSequence(0, block.maxHeight).toString();
            }
            textView.setHtmlText(finalText);
        }

        @Override
        public View getView() {
            return view;
        }
    }
}
