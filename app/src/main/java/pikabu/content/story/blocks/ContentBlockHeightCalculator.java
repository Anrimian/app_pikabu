package pikabu.content.story.blocks;

import java.util.ArrayList;

/**
 * Created on 23.07.2016.
 */
public class ContentBlockHeightCalculator {

    private ArrayList<StoryContentBlock> blocks;

    private boolean trimmed = false;

    public ContentBlockHeightCalculator(ArrayList<StoryContentBlock> blocks) {
        this.blocks = blocks;
        resetMaxHeight();
    }

    public ArrayList<StoryContentBlock> trimBlocks() {
        ArrayList<StoryContentBlock> result = blocks;
        if (isTooLongBlocks()) {
            result = getTrimmedBlocks();
            trimmed = true;
        }
        return result;
    }

    private void resetMaxHeight() {
        for(StoryContentBlock contentBlock : blocks) {
            contentBlock.setMaxBlockHeight(StoryContentBlock.DEFAULT_MAX_HEIGHT);
        }
    }

    private ArrayList<StoryContentBlock> getTrimmedBlocks() {
        ArrayList<StoryContentBlock> result = new ArrayList<>();
        int blocksHeight = 0;
        for(StoryContentBlock contentBlock : blocks) {
            result.add(contentBlock);
            int height = contentBlock.getBlockHeight();
            blocksHeight += height;
            if (blocksHeight > StoryContentBlock.TRIMMED_BLOCK_HEIGHT) {
                int lastBlockHeight = height - (blocksHeight - StoryContentBlock.TRIMMED_BLOCK_HEIGHT);
                trimLastBlock(contentBlock, lastBlockHeight);
                break;
            }
        }
        return result;
    }

    private void trimLastBlock(StoryContentBlock contentBlock, int lastBlockHeight) {
        contentBlock.setMaxBlockHeight(lastBlockHeight);
    }

    private boolean isTooLongBlocks() {
        int blocksHeight = 0;
        for(StoryContentBlock contentBlock : blocks) {
            blocksHeight += contentBlock.getBlockHeight();
            if (blocksHeight > StoryContentBlock.BLOCK_MAX_HEIGHT) {
                return true;
            }
        }
        return false;
    }

    public boolean isTrimmed() {
        return trimmed;
    }
}
