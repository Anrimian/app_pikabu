package pikabu.content.story.blocks;

import pikabu.content.blocks.ContentBlock;
import pikabu.image.ImageSize;

/**
 * Created on 01.12.2015.
 */
public enum ContentType {
    TEXT,
    IMAGE,
    VIDEO,
    GIF;

    public static ContentBlock createContentBlock(String contentType,
                                                  String content,
                                                  String contentSecond,
                                                  int width,
                                                  int height,
                                                  int size) {
        if (contentType.equals(IMAGE.toString())) {
            return new ImageContentBlock(content, contentSecond, new ImageSize(width, height));
        }
        if (contentType.equals(VIDEO.toString())) {
            return new VideoContentBlock(content, contentSecond, new ImageSize(width, height));
        }
        if (contentType.equals(GIF.toString())) {
            return new GifContentBlock(content, new ImageSize(width, height));
        }
        return new TextContentBlock(content);
    }
}
