package pikabu.content.story.blocks;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.emirinay.pikabuapp.R;
import com.emirinay.tools.TextUtils;

import pikabu.content.blocks.BlockViewHolder;
import pikabu.data.DB;
import pikabu.image.ImageSize;
import pikabu.image.glide.image.MyGlideImageLoader;
import pikabu.image.glide.image.MyImageViewTarget;
import pikabu.image.glide.progress.GlideProgressBarLoadingObserver;

/**
 * Created on 25.07.2016.
 */
public class VideoContentBlock extends StoryContentBlock {

    private String videoLink;
    private String previewImageLink;

    private ImageSize imageSize;

    public VideoContentBlock(String videoLink, String previewImageLink, ImageSize imageSize) {
        this.videoLink = videoLink;
        this.previewImageLink = previewImageLink;
        this.imageSize = imageSize;
    }

    @Override
    public BlockViewHolder createBlockViewHolder(Context ctx) {
        return new VideoContentBlockViewHolder(ctx);
    }

    @Override
    public int getBlockHeight() {
        return imageSize.getPreviewHeight();
    }

    @Override
    public ContentType getContentType() {
        return ContentType.VIDEO;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DB.COLUMN_CONTENT, videoLink);
        cv.put(DB.COLUMN_CONTENT_SECOND, previewImageLink);
        cv.put(DB.COLUMN_PREVIEW_WIDTH, imageSize.getPreviewWidth());
        cv.put(DB.COLUMN_PREVIEW_HEIGHT, imageSize.getPreviewHeight());
        return cv;
    }

    private static class VideoContentBlockViewHolder extends StoryContentBlockViewHolder<VideoContentBlock> {

        private View view;

        private GlideProgressBarLoadingObserver progressBar;
        private ImageView imageView;
        private View selectionView;

        private MyImageViewTarget myImageViewTarget;

        VideoContentBlockViewHolder(Context ctx) {
            super(ctx);
            view = View.inflate(ctx, R.layout.block_video, null);
            progressBar = (GlideProgressBarLoadingObserver) view.findViewById(R.id.progress_bar);
            imageView = (ImageView) view.findViewById(R.id.image_view);
            selectionView = view.findViewById(R.id.clickable_view);
        }

        @Override
        public void bindView(VideoContentBlock block, StoryBindArgs bindArgs) {
            String previewImageLink = TextUtils.getYoutubeVideoImage(block.videoLink);
            if (previewImageLink == null) {
                previewImageLink = block.previewImageLink;
            }
            progressBar.registerProgressBar(previewImageLink);
            myImageViewTarget = MyGlideImageLoader.displayImage(previewImageLink, imageView, block.imageSize);
            Context ctx = view.getContext();
            selectionView.setOnClickListener(view -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(block.videoLink));
                ctx.startActivity(browserIntent);
            });
        }

        @Override
        public void clearView() {
            super.clearView();
            if (myImageViewTarget != null) {
                Glide.clear(myImageViewTarget);
                myImageViewTarget = null;
            }
        }

        @Override
        public View getView() {
            return view;
        }
    }
}
