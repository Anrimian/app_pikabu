package pikabu.content.story.blocks;

import android.content.ContentValues;
import android.content.Context;
import android.view.View;

import com.emirinay.pikabuapp.R;

import pikabu.content.blocks.BlockViewHolder;
import pikabu.data.DB;
import pikabu.image.ImageSize;
import pikabu.image.gif.GifController;
import pikabu.image.gif.GifViewHolderImpl;

/**
 * Created on 25.07.2016.
 */
public class GifContentBlock extends StoryContentBlock {

    private String link;

    private ImageSize imageSize;

    public GifContentBlock(String link, ImageSize imageSize) {
        this.link = link;
        this.imageSize = imageSize;
    }

    @Override
    public BlockViewHolder createBlockViewHolder(Context ctx) {
        return new GifContentBlockViewHolder(ctx);
    }

    @Override
    public int getBlockHeight() {
        return imageSize.getPreviewHeight();
    }

    @Override
    public ContentType getContentType() {
        return ContentType.GIF;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DB.COLUMN_CONTENT, link);
        cv.put(DB.COLUMN_PREVIEW_WIDTH, imageSize.getPreviewWidth());
        cv.put(DB.COLUMN_PREVIEW_HEIGHT, imageSize.getPreviewHeight());
        return cv;
    }

    private static class GifContentBlockViewHolder extends StoryContentBlockViewHolder<GifContentBlock> {

        private View view;

        private GifViewHolderImpl gifViewHolder;

        private GifController gifController;

        GifContentBlockViewHolder(Context ctx) {
            super(ctx);
            view = View.inflate(ctx, R.layout.block_gif, null);
            gifViewHolder = new GifViewHolderImpl(view);
            gifController = new GifController(gifViewHolder);
        }

        @Override
        public void bindView(GifContentBlock block, StoryBindArgs bindArgs) {
            gifController.bindGif(block.link, block.imageSize);
        }

        @Override
        public void clearView() {
            super.clearView();
            gifController.clear();
        }

        @Override
        public View getView() {
            return view;
        }
    }
}
