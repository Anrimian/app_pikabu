package pikabu.content.story;

import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;

import pikabu.content.Vote;
import pikabu.content.story.blocks.StoryContentBlock;

public class Story implements Serializable {
	
	public static final transient int NO_RATING = Integer.MIN_VALUE;
	public static final transient int HIDE_RATING = Integer.MIN_VALUE + 1;
	public static final transient int DELETED = Integer.MIN_VALUE + 2;
	
	private static final long serialVersionUID = -5280962624396565435L;

	private String title;
	private String author;
	private String previewText;
	private String link;

	private int commentsNumber;
	private int rating;
	private int storyId;

    private Vote voteStatus;

    private boolean saveStatus;
    private boolean isMy;
    private boolean isStraw;

	private long unixTime;

	private StoryCommunityInfo communityInfo;

	private ArrayList<String> tagList;
    private ArrayList<StoryContentBlock> contentBlocksList;

    public ArrayList<StoryContentBlock> getContentBlocksList() {
        return contentBlocksList;
    }

    public void setContentBlocks(ArrayList<StoryContentBlock> contentBlocksList) {
        this.contentBlocksList = contentBlocksList;
    }

    public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getCommentsNumber() {
		return commentsNumber;
	}

	public void setCommentsNumber(int commentsNumber) {
		this.commentsNumber = commentsNumber;
	}

	public String getPreviewText() {
		return previewText;
	}

	public void setPreviewText(String previewText) {
		this.previewText = previewText;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int newRating) {
		if (rating != NO_RATING && rating != HIDE_RATING && rating != DELETED) {
			rating = newRating;
		}
	}

	public boolean getIsMy() {
		return isMy;
	}

	public void setIsMyTag(boolean isMy) {
		this.isMy = isMy;
	}

	public boolean getIsStraw() {
		return isStraw;
	}

	public void setIsStrawTag(boolean isStraw) {
		this.isStraw = isStraw;
	}

	public int getStoryId() {
		return storyId;
	}

	public void setStoryId(int storyId) {
		this.storyId = storyId;
	}

	public long getUnixTime() {
		return unixTime;
	}

	public void setUnixTime(long unixTime) {
		this.unixTime = unixTime;
	}

	public ArrayList<String> getTagList() {
		return tagList;
	}

	public void setTagList(ArrayList<String> tagList) {
		this.tagList = tagList;
	}

    public Vote getVoteStatus() {
        return voteStatus;
    }

    public void setVoteStatus(Vote voteStatus) {
		this.voteStatus = Vote.mergeVote(this.voteStatus, voteStatus);
    }

    public boolean getSaveStatus() {
        return saveStatus;
    }

    public void setSaveStatus(boolean saveStatus) {
        this.saveStatus = saveStatus;
    }

    @Nullable
    public StoryCommunityInfo getCommunityInfo() {
        return communityInfo;
    }

    public void setCommunityInfo(StoryCommunityInfo communityInfo) {
        this.communityInfo = communityInfo;
    }

    @Override
	public boolean equals(Object o) {
		return getClass() == o.getClass() && this.storyId == ((Story) o).getStoryId();
	}
}
