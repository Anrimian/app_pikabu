package pikabu.content.comments.blocks;

import android.content.Context;
import android.view.View;

import com.emirinay.pikabuapp.R;

import pikabu.content.blocks.BlockViewHolder;
import pikabu.content.story.blocks.ContentType;
import pikabu.image.ImageSize;
import pikabu.image.gif.GifController;
import pikabu.image.gif.GifViewHolderImpl;

/**
 * Created on 01.03.2016.
 */
public class GifContentBlock extends CommentContentBlock {
    private String link;

    private ImageSize imageSize;
    private int size;

    public GifContentBlock(String link) {
        this(link, DEFAULT_IMAGE_HEIGHT, DEFAULT_IMAGE_WIDTH, 0);
    }

    public GifContentBlock(String link, int height, int width, int size) {
        this.size = size;
        this.link = link;
        imageSize = new ImageSize();
        imageSize.setPreviewWidth(width);
        imageSize.setPreviewHeight(height);
    }

    @Override
    public BlockViewHolder createBlockViewHolder(Context ctx) {
        return new GifContentBlockViewHolder(ctx);
    }

    @Override
    public ContentType getContentType() {
        return ContentType.GIF;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("link = ");
        sb.append(link);
        sb.append(", height = ");
        sb.append(imageSize.getPreviewHeight());
        sb.append(", width = ");
        sb.append(imageSize.getPreviewWidth());
        sb.append(", size = ");
        sb.append(size);
        return sb.toString();
    }

    private static class GifContentBlockViewHolder extends CommentContentBlockViewHolder<GifContentBlock> {

        private View view;

        private GifViewHolderImpl gifViewHolder;

        private GifController gifController;

        GifContentBlockViewHolder(Context ctx) {
            super(ctx);
            view = View.inflate(ctx, R.layout.comment_block_gif, null);
            gifViewHolder = new GifViewHolderImpl(view);
            gifController = new GifController(gifViewHolder);
        }

        @Override
        public void bindView(GifContentBlock block, CommentBindArgs bindArgs) {
            gifController.bindGif(block.link, block.imageSize);
        }

        @Override
        public void clearView() {
            super.clearView();
            gifController.clear();
        }

        @Override
        public View getView() {
            return view;
        }
    }
}
