package pikabu.content.comments.blocks;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.emirinay.pikabuapp.R;

import pikabu.content.blocks.BlockViewHolder;
import pikabu.content.story.blocks.ContentType;
import pikabu.image.ImageSize;
import pikabu.image.glide.image.MyGlideImageLoader;
import pikabu.image.glide.image.MyImageViewTarget;
import pikabu.image.glide.progress.GlideProgressBarLoadingObserver;
import pikabu.image.zoom.ZoomImageOnClickListener;

/**
 * Created on 29.02.2016.
 */
public class ImageContentBlock extends CommentContentBlock {
    private String imageLink;
    private String bigImageLink;

    private ImageSize imageSize = new ImageSize();

    public ImageContentBlock(String imageLink, String bigImageLink) {
        this(imageLink, bigImageLink, DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);
    }

    public ImageContentBlock(String imageLink, String bigImageLink, int width, int height) {
        this.imageLink = imageLink;
        this.bigImageLink = bigImageLink;
        imageSize.setPreviewWidth(width);
        imageSize.setPreviewHeight(height);
    }

    @Override
    public BlockViewHolder createBlockViewHolder(Context ctx) {
        return new ImageContentBlockViewHolder(ctx);
    }

    @Override
    public ContentType getContentType() {
        return ContentType.IMAGE;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("imageLink: ");
        sb.append(imageLink);
        sb.append(", bigImageLink: ");
        sb.append(bigImageLink);
        sb.append(", width: ");
        sb.append(imageSize.getPreviewWidth());
        sb.append(", height: ");
        sb.append(imageSize.getPreviewHeight());
        return sb.toString();
    }

    private static class ImageContentBlockViewHolder extends CommentContentBlockViewHolder<ImageContentBlock> {

        private View view;

        private GlideProgressBarLoadingObserver progressBar;
        private ImageView imageView;

        private MyImageViewTarget myImageViewTarget;

        ImageContentBlockViewHolder(Context ctx) {
            super(ctx);
            view = View.inflate(ctx, R.layout.comment_block_image, null);
            progressBar = (GlideProgressBarLoadingObserver) view.findViewById(R.id.progress_bar);
            imageView = (ImageView) view.findViewById(R.id.image_view);
        }

        @Override
        public void bindView(ImageContentBlock block, CommentBindArgs bindArgs) {
            progressBar.registerProgressBar(block.imageLink);

            ZoomImageOnClickListener zoomOnClickListener = new ZoomImageOnClickListener(
                    view.getContext(), block.imageLink, block.bigImageLink);
            imageView.setOnClickListener(zoomOnClickListener);
            myImageViewTarget = MyGlideImageLoader.displayImage(block.imageLink, imageView, block.imageSize, true, null);
        }

        @Override
        public void clearView() {
            super.clearView();
            if (myImageViewTarget != null) {
                Glide.clear(myImageViewTarget);
                myImageViewTarget = null;
            }
        }

        @Override
        public View getView() {
            return view;
        }
    }
}
