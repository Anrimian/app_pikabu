package pikabu.content.comments.blocks;

import pikabu.content.blocks.BindArgs;
import pikabu.content.comments.story.CommentLinkOnClickListener;

/**
 * Created on 15.08.2016.
 */
public class CommentBindArgs implements BindArgs {

    private CommentLinkOnClickListener linkOnClickListener;

    public CommentBindArgs(CommentLinkOnClickListener linkOnClickListener) {
        this.linkOnClickListener = linkOnClickListener;
    }

    public CommentLinkOnClickListener getLinkOnClickListener() {
        return linkOnClickListener;
    }
}
