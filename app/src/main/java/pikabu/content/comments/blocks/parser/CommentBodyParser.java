package pikabu.content.comments.blocks.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import pikabu.content.ParserException;
import pikabu.content.comments.blocks.CommentContentBlock;
import pikabu.content.comments.blocks.GifContentBlock;
import pikabu.content.comments.blocks.ImageContentBlock;
import pikabu.content.comments.blocks.TextContentBlock;

/**
 * Created on 09.03.2016.
 */
public class CommentBodyParser {

    public ArrayList<CommentContentBlock> parseCommentBody(String commentBody) throws ParserException {
        Document doc = Jsoup.parse(commentBody);
        ArrayList<CommentContentBlock> contentBlocks = new ArrayList<>();
        selectMediaElements(doc, contentBlocks);
        if (!doc.text().isEmpty()) {
            TextContentBlock textBlock = new TextContentBlock(doc.toString());
            contentBlocks.add(0, textBlock);
        }
        return contentBlocks;
    }

    private void selectMediaElements(Document doc, ArrayList<CommentContentBlock> contentBlocks) throws ParserException {
        Elements mediaElements = doc.select(".b-p.b-p_type_image");
        for (Element element : mediaElements) {
            CommentContentBlock contentBlock = selectMediaElement(element);
            contentBlocks.add(contentBlock);
            element.remove();
        }
        if (mediaElements.isEmpty()) {
            selectOldMediaElements(doc, contentBlocks);
        }
    }

    protected CommentContentBlock selectMediaElement(Element element) throws ParserException {
        String imageLink = selectImageLink(element);
        if (imageLink.contains("gif")) {
            return selectGifBlock(element, imageLink);
        } else {
            return selectImageBlock(element, imageLink);
        }
    }

    protected String selectImageLink(Element element) throws ParserException {
        String imageLink = element.select("img").attr("src");
        if (imageLink == null || imageLink.isEmpty()) {
            throw new ParserException("fail parse comment image link from: " + element);
        }
        return imageLink;
    }

    protected CommentContentBlock selectGifBlock(Element element, String imageLink) throws ParserException{
        int width = selectWidth(element);
        int height = selectHeight(element);
        int gifSize = selectGifSize(element);
        return new GifContentBlock(imageLink, height, width, gifSize);
    }

    private CommentContentBlock selectImageBlock(Element element, String imageLink) {
        String bigImageLink = element.select("img").attr("data-large-image");
        int width = selectWidth(element);
        int height = selectHeight(element);
        return new ImageContentBlock(imageLink, bigImageLink, width, height);
    }

    private int selectWidth(Element element) {
        try {
            String width = element.select("img").attr("data-width");
            return Integer.parseInt(width);
        } catch (NumberFormatException e) {
            return CommentContentBlock.DEFAULT_IMAGE_WIDTH;
        }
    }

    private int selectHeight(Element element) {
        try {
            String height = element.select("img").attr("data-height");
            return Integer.parseInt(height);
        } catch (NumberFormatException e) {
            return CommentContentBlock.DEFAULT_IMAGE_HEIGHT;
        }
    }

    private int selectGifSize(Element element) {
        try {
            String height = element.select("img").attr("data-size-gif");
            return Integer.parseInt(height);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private void selectOldMediaElements(Document doc, ArrayList<CommentContentBlock> contentBlocks) {
        Elements mediaElements = doc.select(".c_img");
        mediaElements.addAll(doc.select("a img"));
        for (Element element : mediaElements) {
            Element parent = element.parent();
            if (parent != null) {
                CommentContentBlock contentBlock = selectOldMediaElement(parent);
                contentBlocks.add(contentBlock);
                element.remove();
            }
        }
    }

    private CommentContentBlock selectOldMediaElement(Element element) {
        String imageLink = element.select("img").attr("src");
        if (imageLink.contains(".gif")) {
            return new GifContentBlock(imageLink);
        } else {
            String bigImageLink = element.attr("href");
            return new ImageContentBlock(imageLink, bigImageLink);
        }
    }
}

