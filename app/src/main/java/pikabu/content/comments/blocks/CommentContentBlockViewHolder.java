package pikabu.content.comments.blocks;

import android.content.Context;

import pikabu.content.blocks.BindArgs;
import pikabu.content.blocks.BlockViewHolder;

/**
 * Created on 19.10.2016.
 */

public abstract class CommentContentBlockViewHolder <T extends CommentContentBlock> extends BlockViewHolder<CommentContentBlock> {

    public CommentContentBlockViewHolder(Context ctx) {
        super(ctx);
    }

    @Override
    public void bindView(CommentContentBlock block, BindArgs bindArgs) {
        bindView((T) block, (CommentBindArgs) bindArgs);
    }

    public abstract void bindView(T block, CommentBindArgs bindArgs);
}
