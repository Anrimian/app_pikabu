package pikabu.content.comments.blocks;

import android.content.Context;
import android.view.View;

import com.emirinay.pikabuapp.R;

import pikabu.content.PikabuLinkOnClickListener;
import pikabu.content.blocks.BlockViewHolder;
import pikabu.content.story.blocks.ContentType;
import pikabu.utils.text.HtmlTextView;

/**
 * Created on 29.02.2016.
 */
public class TextContentBlock extends CommentContentBlock {

    private String text;

    public TextContentBlock(String text) {
        this.text = text;
    }

    @Override
    public BlockViewHolder createBlockViewHolder(Context ctx) {
        return new TextContentBlockViewHolder(ctx);
    }

    @Override
    public ContentType getContentType() {
        return ContentType.TEXT;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "text: " + text;
    }

    private static class TextContentBlockViewHolder extends CommentContentBlockViewHolder<TextContentBlock> {

        private View view;

        private HtmlTextView htmlTextView;

        TextContentBlockViewHolder(Context ctx) {
            super(ctx);
            view = View.inflate(ctx, R.layout.comment_block_text, null);
            htmlTextView = (HtmlTextView) view.findViewById(R.id.text_view);
        }

        @Override
        public void bindView(TextContentBlock block, CommentBindArgs bindArgs) {
            if (bindArgs != null) {
                htmlTextView.setLinkOnClickListener(bindArgs.getLinkOnClickListener());
            } else {
                htmlTextView.setLinkOnClickListener(new PikabuLinkOnClickListener());
            }
            htmlTextView.setHtmlText(block.text);
        }

        @Override
        public View getView() {
            return view;
        }
    }
}
