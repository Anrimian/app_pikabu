package pikabu.content.comments.blocks;

import pikabu.content.blocks.ContentBlock;

/**
 * Created on 29.02.2016.
 */
public abstract class CommentContentBlock extends ContentBlock {
    public static final int DEFAULT_IMAGE_WIDTH = 400;
    public static final int DEFAULT_IMAGE_HEIGHT = 300;
}
