package pikabu.content.comments.story.reply;

import pikabu.AppExecutors;
import pikabu.utils.network.Api;
import rx.android.schedulers.AndroidSchedulers;


/**
 * Created on 01.06.2016.
 */
public class ReplyModel {

    private String replyCommentText;

    private int storyId;
    private int parentCommentId;

    private ReplyState replyState = ReplyState.WAIT;
    private ReplyStateObservable stateObservable;

    private String errorMessage;

    public ReplyModel(int storyId, int parentCommentId) {
        this.storyId = storyId;
        this.parentCommentId = parentCommentId;
        stateObservable = new ReplyStateObservable();
    }

    public void replyComment(String replyCommentText) {
        replyState = ReplyState.PROGRESS;
        replyState.showState(stateObservable);

        Api.getReplyCommentObservable(storyId, replyCommentText, parentCommentId)
                .subscribeOn(AppExecutors.COMMENTS_SCHEDULER)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    replyState = ReplyState.COMPLETE;
                    replyState.showState(stateObservable);
                }, throwable -> {
                    errorMessage = throwable.getMessage();
                    replyState = ReplyState.ERROR;
                    replyState.showState(stateObservable);

                    replyState = ReplyState.WAIT;
                    replyState.showState(stateObservable);
                });
    }

    public void registerObserver(ReplyStateObserver observer) {
        stateObservable.registerObserver(observer);
    }

    public void unregisterObserver(ReplyStateObserver observer) {
        stateObservable.unregisterObserver(observer);
    }

    public String getReplyCommentText() {
        return replyCommentText;
    }

    public void setReplyCommentText(String replyCommentText) {
        this.replyCommentText = replyCommentText;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private enum ReplyState {
        PROGRESS {
            public void showState(ReplyStateObservable stateObservable) {
                stateObservable.onProgress();
            }
        },
        ERROR {
            public void showState(ReplyStateObservable stateObservable) {
                stateObservable.onError();
            }
        },
        COMPLETE {
            public void showState(ReplyStateObservable stateObservable) {
                stateObservable.onCompleted();
            }
        },
        WAIT;

        public void showState(ReplyStateObservable stateObservable) {

        }
    }


}
