package pikabu.content.comments.story.commentslist;

import android.support.v4.util.LruCache;

/**
 * Created on 14.07.2015.
 */
public class CommentsListModelContainer {

    private static final int MODEL_CACHE_SIZE = 5;

    private static LruCache<Integer, CommentsListModel> cache = new LruCache<>(MODEL_CACHE_SIZE);

    public static CommentsListModel getInstance(int storyId) {
        CommentsListModel model = cache.get(storyId);
        if (model == null) {
            model = new CommentsListModel(storyId);
            cache.put(storyId, model);
        }
        return model;
    }
}
