package pikabu.content.comments.story;

import android.content.Context;

import pikabu.content.PikabuLinkOnClickListener;
import pikabu.content.comments.story.commentslist.CommentsTreeActions;
import pikabu.utils.network.UrlUtils;

/**
 * Created on 16.02.2016.
 */
public class CommentLinkOnClickListener extends PikabuLinkOnClickListener {
    private int currentStoryId;

    private CommentsTreeActions commentsTreeActions;

    public CommentLinkOnClickListener(int currentStoryId, CommentsTreeActions commentsTreeActions) {
        this.currentStoryId = currentStoryId;
        this.commentsTreeActions = commentsTreeActions;
    }

    @Override
    protected void goToStory(Context ctx, String url) {
        int storyId = UrlUtils.getStoryIdFromLink(url);
        if (storyId == currentStoryId) {
            int targetCommentId = UrlUtils.selectTargetCommentId(url);
            if (targetCommentId != 0) {
                commentsTreeActions.goToComment(targetCommentId);
                return;
            }
        }
        super.goToStory(ctx, url);
    }
}
