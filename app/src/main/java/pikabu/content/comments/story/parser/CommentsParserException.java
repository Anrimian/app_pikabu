package pikabu.content.comments.story.parser;

public class CommentsParserException extends Exception {

	/**
	 * 
	 */
	public CommentsParserException() {		
	}

	public CommentsParserException(String detailMessage) {
		super(detailMessage);		
	}

	public CommentsParserException(Throwable throwable) {
		super(throwable);		
	}

	public CommentsParserException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);		
	}
}
