package pikabu.content.comments.story.commentslist;

import android.database.Observable;

public class CommentsActionObservable extends Observable<CommentsActionObserver> {

	public void showState(ListState listState) {
		for (final CommentsActionObserver observer : mObservers) {
			listState.showState(observer);
		}
	}

	public void updateList() {
		for (final CommentsActionObserver observer : mObservers) {
			observer.updateList();
		}
	}
}
