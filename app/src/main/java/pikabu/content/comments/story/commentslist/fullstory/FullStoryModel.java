package pikabu.content.comments.story.commentslist.fullstory;

import android.os.AsyncTask;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;

import org.jsoup.nodes.Document;

import pikabu.content.Vote;
import pikabu.content.story.Story;
import pikabu.content.story.parser.StoryParser;
import pikabu.user.UserModel;
import pikabu.user.actions.UserActions;
import pikabu.user.actions.story.UserStoryActionsObserver;
import pikabu.utils.network.LoaderHelper;
import pikabu.utils.network.UrlUtils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 06.10.2015.
 */
public class FullStoryModel implements UserStoryActionsObserver {

    private StoryLoadingObservable observable;

    private String storyLink;

    private Story story;

    private int errorMessageId;

    private LoadingState loadingState;

    public FullStoryModel(int storyId, Story story) {
        this.story = story;
        this.storyLink = UrlUtils.createStoryLink(storyId);
        observable = new StoryLoadingObservable();
        UserActions.getInstance().registerStoryActionsObserver(this);
        if (story == null) {
            loadStory();
        } else {
            loadingState = LoadingState.COMPLETE;
        }
    }

    public void loadStory() {
        if (getStory() != null || loadingState == LoadingState.PROGRESS) {
            return;
        }
        loadingState = LoadingState.PROGRESS;
        loadingState.showState(observable);

        LoaderHelper.getDocumentObservable(storyLink)
                .subscribeOn(Schedulers.from(AsyncTask.SERIAL_EXECUTOR))
                .doOnNext(document -> UserModel.getInstance().getUserInfoModel().updateUserInfo(document))
                .flatMap(FullStoryModel::getOneStoryObservable)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(story -> {
                    this.story = story;
                    loadingState = LoadingState.COMPLETE;
                    loadingState.showState(observable);
                }, throwable -> {
                    errorMessageId = ((ExceptionWithInteger) throwable).getMessageId();
                    loadingState = LoadingState.ERROR;
                    loadingState.showState(observable);
                });
    }

    public void registerObserver(StoryLoadingObserver observer) {
        observable.registerObserver(observer);
        loadingState.showState(observable);
    }

    public void unregisterObserver(StoryLoadingObserver observer) {
        observable.unregisterObserver(observer);
    }

    /**
     * only use in FullStoryModelContainer
     * @param story
     */
    void setStory(Story story) {
        this.story = story;
    }

    public Story getStory() {
        return story;
    }

    public int getErrorMessageId() {
        return errorMessageId;
    }

    public LoadingState getLoadingState() {
        return loadingState;
    }

    private static Observable<Story> getOneStoryObservable(Document document) {
        return StoryParser.parseDocument(document).flatMap(stories -> Observable.create(subscriber -> {
            if (stories.isEmpty()) {
                subscriber.onError(new ExceptionWithInteger(R.string.parser_error));
            } else {
                subscriber.onNext(stories.get(0));
            }
        }));
    }

    @Override
    public void storyWasVoted(final int storyId, final Vote vote) {
        if (story == null) {
            return;
        }
        if (story.getStoryId() == storyId) {
            story.setVoteStatus(vote);
            story.setRating(story.getRating() + vote.toInt());
            observable.updateVoteStatus();
        }
    }

    @Override
    public void storyWasSaved(final int storyId, final boolean saved) {
        if (story == null) {
            return;
        }
        if (story.getStoryId() == storyId) {
            story.setSaveStatus(saved);
            observable.updateSaveStatus();
        }
    }

    enum LoadingState {
        PROGRESS {
            @Override
            void showState(StoryLoadingObservable observable) {
                observable.onProgress();
            }
        },
        ERROR {
            @Override
            void showState(StoryLoadingObservable observable) {
                observable.onError();
            }
        },
        COMPLETE {
            @Override
            void showState(StoryLoadingObservable observable) {
                observable.onCompleted();
            }
        };

        void showState(StoryLoadingObservable observable) {

        }
    }
}
