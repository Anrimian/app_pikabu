package pikabu.content.comments.story.commentslist.fullstory;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.TextUtils;

import pikabu.content.comments.story.commentslist.CommentsListFragment;
import pikabu.content.story.Story;
import pikabu.data.Links;
import pikabu.main.DrawerActivity;
import pikabu.utils.network.UrlUtils;

/**
 * Created on 27.08.2015.
 */
public class FullStoryActivity extends DrawerActivity {
    public static final String STORY_LINK = "story_link";
    public static final String STORY = "story";

    public static void start(Context ctx, Story story) {
        Intent intent = new Intent(ctx, FullStoryActivity.class);
        intent.putExtra(FullStoryActivity.STORY, story);
        ctx.startActivity(intent);
    }

    @Override
    protected Fragment initFragment() {
        Intent intent = getIntent();
        Fragment storyPagerFragment;
        Story story = (Story) intent.getSerializableExtra(STORY);
        if (story != null) {
            storyPagerFragment = CommentsListFragment.newInstance(story);
        } else {
            String link = intent.getStringExtra(STORY_LINK);
            if (link == null) {
                String lastSegment = TextUtils.getUrlLastSegment(intent.getData().toString());
                link = Links.LINK_SITE + "/story/" + lastSegment;
            }
            int targetCommentId = UrlUtils.selectTargetCommentId(link);
            int storyId = UrlUtils.getStoryIdFromLink(link);
            if (storyId == 0) {
                Toast.makeText(this, R.string.can_not_get_story_id, Toast.LENGTH_LONG).show();
                finish();
                return null;
            }
            storyPagerFragment = CommentsListFragment.newInstance(storyId, targetCommentId);
        }
        return storyPagerFragment;
    }
}
