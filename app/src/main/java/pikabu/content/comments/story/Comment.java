package pikabu.content.comments.story;

import java.io.Serializable;
import java.util.ArrayList;

import pikabu.content.comments.blocks.CommentContentBlock;

public class Comment implements Serializable{

	public static final int HIDE_RATING = Integer.MIN_VALUE;

	private int commentId;
	private int parentCommentId;
	private int rating;
	
	private long time;
	
	private String author;
	
	private ArrayList<CommentContentBlock> blocks;
	
	public Comment(int commentId,
				   int parentCommentId,
				   int rating,
				   String author,
				   long time,
				   ArrayList<CommentContentBlock> blocks) {
		this.commentId = commentId;
		this.parentCommentId = parentCommentId;
		this.rating = rating;
		this.time = time;
		this.author = author;
		this.blocks = blocks;
	}

	public int getCommentId() {
		return commentId;
	}

	public int getParentCommentId() {
		return parentCommentId;
	}

	public int getRating() {
		return rating;
	}

	public long getTime() {
		return time;
	}

	public String getAuthor() {
		return author;
	}

	public void setRating(int newRating) {
		if (rating != HIDE_RATING) {
			rating = newRating;
		}
	}

	public ArrayList<CommentContentBlock> getContentBlocks() {
		return blocks;
	}
}
