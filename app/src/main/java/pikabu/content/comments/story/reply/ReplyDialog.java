package pikabu.content.comments.story.reply;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;

import pikabu.user.UserModel;

/**
 * Created on 07.06.2016.
 */
public class ReplyDialog extends DialogFragment implements ReplyStateObserver {
    private static final String STORY_ID = "story_id";
    private static final String PARENT_COMMENT_ID = "parent_comment_id";

    public static void start(FragmentActivity act, int storyId, int parentCommentId) {
        if (UserModel.getInstance().isLogin()) {
            ReplyDialog replyDialog = new ReplyDialog();
            Bundle args = new Bundle();
            args.putInt(STORY_ID, storyId);
            args.putInt(PARENT_COMMENT_ID, parentCommentId);
            replyDialog.setArguments(args);
            replyDialog.show(act.getSupportFragmentManager(), "replyDialog");
        } else {
            Toast.makeText(act, R.string.must_login_for_action, Toast.LENGTH_SHORT).show();
        }
    }

    private int storyId;
    private int parentCommentId;

    private EditText etReplyComment;
    private ProgressBar progressBar;
    private Button btnOk;
    private Button btnCancel;

    private ReplyModel replyModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        storyId = getArguments().getInt(STORY_ID);
        parentCommentId = getArguments().getInt(PARENT_COMMENT_ID);

        if (storyId == 0) {
            Toast.makeText(getActivity(), R.string.can_not_get_story_id, Toast.LENGTH_LONG).show();
            dismiss();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.reply_comment);
        View view = inflater.inflate(R.layout.reply_comment_dialog_view, container);
        etReplyComment = (EditText) view.findViewById(R.id.edit_text);
        etReplyComment.addTextChangedListener(new ReplyTextWatcher());
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.INVISIBLE);

        btnOk = (Button) view.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(btn -> replyComment());

        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(btn -> dismiss());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        replyModel = ReplyModelCache.getReplyModel(storyId, parentCommentId);
        etReplyComment.setText(replyModel.getReplyCommentText());
        etReplyComment.requestFocus();
        replyModel.registerObserver(this);
        super.onViewCreated(view, savedInstanceState);
    }

    private InputMethodManager inputMethodManager;

    @Override
    public void onResume() {
        super.onResume();
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            inputMethodManager.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        replyModel.unregisterObserver(this);
    }

    @Override
    public void onCompleted() {
        showProgress(false);
        ReplyModelCache.removeReplyModel(storyId, parentCommentId);
        Toast.makeText(getActivity(), R.string.reply_comment_success, Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void onProgress() {
        showProgress(true);
    }

    @Override
    public void onError() {
        showProgress(false);
        Toast.makeText(getActivity(), replyModel.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    private void replyComment() {
        String text = etReplyComment.getText().toString();
        if (text.isEmpty()) {
            Toast.makeText(getActivity(), R.string.empty_comment_error, Toast.LENGTH_SHORT).show();
            return;
        }
        replyModel.replyComment(text);
    }

    private void showProgress(boolean show) {
        boolean enabled = !show;
        btnOk.setEnabled(enabled);
        btnCancel.setEnabled(enabled);
        etReplyComment.setEnabled(enabled);

        progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    class ReplyTextWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence text, int start, int before, int count) {
            String htmlText = text.toString();
            replyModel.setReplyCommentText(htmlText);
        }
    }
}
