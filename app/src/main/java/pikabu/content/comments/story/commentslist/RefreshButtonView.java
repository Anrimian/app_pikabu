package pikabu.content.comments.story.commentslist;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.emirinay.pikabuapp.R;

/**
 * Created on 22.12.2016.
 */

public class RefreshButtonView {

    @Nullable
    private ImageView btnRefresh;

    public void setView(View view, boolean refresh) {
        btnRefresh = (ImageView) view.findViewById(R.id.refresh);
        if (refresh) {
            startButtonAnimation();
        }
    }

    public void startButtonAnimation() {
        if (btnRefresh != null) {
            Log.d("refresh", "startButtonAnimation");
            Animation anim = AnimationUtils.loadAnimation(btnRefresh.getContext(), R.anim.rotate_refresh);
            anim.setRepeatCount(Animation.INFINITE);
            btnRefresh.setEnabled(false);
            btnRefresh.startAnimation(anim);
        }
    }

    public void stopButtonAnimation() {
        if (btnRefresh != null) {
            Log.d("refresh", "stopButtonAnimation");
            btnRefresh.setEnabled(true);
            btnRefresh.clearAnimation();
        }
    }
}
