package pikabu.content.comments.story.commentslist;

/**
 * Created on 22.12.2016.
 */

public interface ViewActions {

    void onRefresh();

    void onErrorClick();
}
