package pikabu.content.comments.story.commentslist.fullstory;

import android.database.Observable;

public class StoryLoadingObservable extends Observable<StoryLoadingObserver> {

	public void onProgress() {
		for (final StoryLoadingObserver observer : mObservers) {
			observer.onProgress();
		}
	}

	public void onError() {
		for (final StoryLoadingObserver observer : mObservers) {
			observer.onError();
		}
	}	

	public void onCompleted(){
		for (final StoryLoadingObserver observer : mObservers) {
			observer.onCompleted();
		}
	}

	public void updateSaveStatus(){
		for (final StoryLoadingObserver observer : mObservers) {
			observer.updateSaveStatus();
		}
	}

	public void updateVoteStatus(){
		for (final StoryLoadingObserver observer : mObservers) {
			observer.updateVoteStatus();
		}
	}
}
