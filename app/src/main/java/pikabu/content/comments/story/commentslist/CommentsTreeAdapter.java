package pikabu.content.comments.story.commentslist;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.adapter.HeaderFooterRecyclerViewAdapter;
import com.emirinay.tools.treelist.Node;
import com.emirinay.tools.treelist.Tree;
import com.emirinay.tools.views.FRelativeLayout;

import pikabu.content.Vote;
import pikabu.content.comments.story.Comment;
import pikabu.content.comments.story.StoryCommentViewHolder;

/**
 * Created on 06.09.2016.
 */
public class CommentsTreeAdapter extends HeaderFooterRecyclerViewAdapter {

    private Tree tree;

    private CommentsTreeInfo commentsTreeInfo;

    private CommentsTreeActions commentsTreeActions;

    private SparseArray<Vote> voteList;

    private int animationTargetId = 0;

    public CommentsTreeAdapter(Tree tree,
                               SparseArray<Vote> voteList,
                               CommentsTreeInfo commentsTreeInfo,
                               CommentsTreeActions commentsTreeActions) {
        this.tree = tree;
        this.voteList = voteList;
        this.commentsTreeInfo = commentsTreeInfo;
        this.commentsTreeActions = commentsTreeActions;
    }

    @Override
    public int getCount() {
        return tree.getVisibleNodeCount();
    }

    @Override
    public RecyclerView.ViewHolder createVH(ViewGroup viewGroup, int type) {
        View view = View.inflate(viewGroup.getContext(), R.layout.story_comment_view, null);
        return new CommentViewHolder(view);
    }

    @Override
    public void bindVH(RecyclerView.ViewHolder viewHolder, int position) {
        Node node = tree.getVisibleNodeOnPosition(position);
        ((CommentViewHolder) viewHolder).bindView(node);

    }

    public void setAnimationTargetId(int animationTargetId) {
        this.animationTargetId = animationTargetId;
    }

    private class CommentViewHolder extends RecyclerView.ViewHolder {

        private FRelativeLayout clickableContainer;

        private StoryCommentViewHolder binder;

        public CommentViewHolder(View itemView) {
            super(itemView);
            clickableContainer = (FRelativeLayout) itemView.findViewById(R.id.clickable_container);
            binder = new StoryCommentViewHolder(itemView);
        }

        public void bindView(Node node) {
            View colorView = itemView.findViewById(R.id.data_level_color_view);
            showDataLevel(itemView.getContext(), colorView, node.getDataLevel());

            Comment comment = (Comment) node.getNodeData();
            binder.bindCommentInTree(comment, node, commentsTreeInfo, commentsTreeActions);
            binder.showVoteStatus(voteList.get(comment.getCommentId()));

            if (animationTargetId == comment.getCommentId()) {
                animationTargetId = 0;
                animateTargetView();
            } else {
                cancelTargetViewAnimation();
            }
        }

        private ObjectAnimator targetAnimator;

        public void animateTargetView() {
            targetAnimator = ObjectAnimator.ofFloat(clickableContainer, "alpha", 0.4f, 1f);
            targetAnimator.setDuration(500);
            targetAnimator.start();
        }

        public void cancelTargetViewAnimation() {
            if (targetAnimator != null) {
                targetAnimator.end();
                targetAnimator.cancel();
            }
        }

        private static final int MAX_DATA_LEVEL = 8;

        public void showDataLevel(Context ctx, View colorView, int dataLevel) {
            int leftMargin = 0;
            if (dataLevel == 0) {
                colorView.setVisibility(View.GONE);
            } else {
                dataLevel %= MAX_DATA_LEVEL;
                int leftMarginBase = ctx.getResources().getDimensionPixelSize(R.dimen.comment_tree_left_margin);
                int color;
                if (dataLevel == 0) {
                    color = ContextCompat.getColor(ctx, R.color.comment_level_8_color);
                } else {
                    color = selectTreeItemColor(dataLevel, ctx);
                    leftMargin = dataLevel * leftMarginBase;
                }
                colorView.setBackgroundColor(color);
                colorView.setVisibility(View.VISIBLE);
            }


            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) colorView.getLayoutParams();
            params.leftMargin = dp2px(ctx, leftMargin);
            colorView.setLayoutParams(params);
        }

        private int selectTreeItemColor(int dataLevel, Context ctx) {
            int[] colorArray = ctx.getResources().getIntArray(R.array.comment_tree_colors);
            return colorArray[dataLevel - 1];
        }

        private int dp2px(Context ctx, int dp) {
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, ctx.getResources().getDisplayMetrics());
        }
    }

}
