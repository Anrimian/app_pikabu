package pikabu.content.comments.story.commentslist.fullstory;

import android.support.v4.util.LruCache;

import pikabu.content.story.Story;

/**
 * Created on 06.10.2015.
 */
public class FullStoryModelContainer {

    private static final int MODEL_CACHE_SIZE = 5;

    private static LruCache<Integer, FullStoryModel> cache = new LruCache<>(MODEL_CACHE_SIZE);

    public static FullStoryModel getInstance(int storyId, Story story) {
        FullStoryModel model = cache.get(storyId);
        if (model == null) {
            model = new FullStoryModel(storyId, story);
            cache.put(storyId, model);
        } else {
            if (story != null) {
                model.setStory(story);
            }
        }
        return model;
    }
}
