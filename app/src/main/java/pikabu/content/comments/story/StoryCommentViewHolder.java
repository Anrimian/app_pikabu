package pikabu.content.comments.story;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.AndroidUtils;
import com.emirinay.tools.time.FormatTimeHelper;
import com.emirinay.tools.treelist.Node;
import com.emirinay.tools.views.FRelativeLayout;

import java.util.ArrayList;

import pikabu.content.Vote;
import pikabu.content.blocks.ContentBlocksViewHolder;
import pikabu.content.comments.blocks.CommentBindArgs;
import pikabu.content.comments.blocks.CommentContentBlock;
import pikabu.content.comments.blocks.TextContentBlock;
import pikabu.content.comments.story.commentslist.CommentsTreeActions;
import pikabu.content.comments.story.commentslist.CommentsTreeInfo;
import pikabu.content.comments.story.commentslist.NavigationArrowInfo;
import pikabu.content.profile.ProfileOnClickListener;
import pikabu.content.profile.view.ProfileActivity;
import pikabu.content.story.blocks.ContentType;
import pikabu.user.UserModel;
import pikabu.user.actions.UserActions;
import pikabu.utils.text.CopyTextActivity;
import pikabu.utils.network.UrlUtils;

/**
 * Created on 22.01.2016.
 */
public class StoryCommentViewHolder {

    private TextView tvAuthorName;
    private TextView tvRating;
    private TextView tvTime;
    private TextView tvCommentNumber;

    private ImageView btnGoToParent;
    private ImageView btnGoBack;

    private View ivHideRating;
    private View btnVoteUp;
    private View btnVoteDown;
    private View btnCommentActions;
    private View ivCommentsNumberIcon;

    private FRelativeLayout clickableContainer;

    private ContentBlocksViewHolder contentBlocksViewHolder;

    private Comment comment;
    private Node commentNode;
    private NavigationArrowInfo navigationArrowInfo;

    private CommentsTreeInfo commentsTreeInfo;

    private CommentsTreeActions commentsTreeActions;

    public StoryCommentViewHolder(View view) {
        tvAuthorName = (TextView) view.findViewById(R.id.tv_comment_author_name);
        tvRating = (TextView) view.findViewById(R.id.tv_comment_rating);
        tvTime = (TextView) view.findViewById(R.id.tv_comment_time);
        btnVoteUp = view.findViewById(R.id.btn_comment_vote_up);
        btnVoteDown = view.findViewById(R.id.btn_comment_vote_down);
        btnCommentActions = view.findViewById(R.id.btn_comment_actions_menu);
        ivHideRating = view.findViewById(R.id.iv_clock);
        ivHideRating.setOnClickListener(new HideRatingOnClickListener());
        tvCommentNumber = (TextView) view.findViewById(R.id.tv_comments_number);
        btnGoToParent = (ImageView) view.findViewById(R.id.btn_go_to_parent);
        btnGoBack = (ImageView) view.findViewById(R.id.btn_go_back);
        ivCommentsNumberIcon = view.findViewById(R.id.iv_comments_icon);
        clickableContainer = (FRelativeLayout) view.findViewById(R.id.clickable_container);

        ViewGroup contentContainer = (ViewGroup) view.findViewById(R.id.comment_content_container);
        contentBlocksViewHolder = new ContentBlocksViewHolder(contentContainer);

        int buttonColor = ContextCompat.getColor(getContext(), R.color.button_color_filter);
        btnGoToParent.setColorFilter(buttonColor);
        btnGoBack.setColorFilter(buttonColor);

        btnVoteUp.setOnClickListener(v -> voteComment(Vote.UP));
        btnVoteDown.setOnClickListener(v -> voteComment(Vote.DOWN));
    }

    public void bindCommentInTree(Comment comment,
                                  Node node,
                                  CommentsTreeInfo commentsTreeInfo,
                                  CommentsTreeActions commentsTreeActions) {
        this.comment = comment;
        this.commentNode = node;
        this.commentsTreeInfo = commentsTreeInfo;
        this.navigationArrowInfo = commentsTreeInfo.getNavigationArrowInfo();
        this.commentsTreeActions = commentsTreeActions;

        showAuthorName();
        showRating();
        showTime();
        showCommentBody();
        setVoteActions();

        btnCommentActions.setOnClickListener(new CommentActionsOnClickListener());

        bindCollapseAction();
        bindGoToParentAction();
        bindBackToChildAction();
    }

    private void bindBackToChildAction() {
        if (comment.getCommentId() == navigationArrowInfo.getParentCommentId()) {
            btnGoBack.setVisibility(View.VISIBLE);
        } else {
            btnGoBack.setVisibility(View.INVISIBLE);
        }
        btnGoBack.setOnClickListener(view -> {
            btnGoBack.setVisibility(View.INVISIBLE);
            commentsTreeActions.goToComment(navigationArrowInfo.getChildCommentId());
            navigationArrowInfo.setChildCommentId(NavigationArrowInfo.NOTHING);
            navigationArrowInfo.setParentCommentId(NavigationArrowInfo.NOTHING);
        });
    }

    private void bindGoToParentAction() {
        if (commentNode.getDataLevel() == 0) {
            btnGoToParent.setVisibility(View.GONE);
            return;
        } else {
            btnGoToParent.setVisibility(View.VISIBLE);
        }
        btnGoToParent.setOnClickListener(view -> {
            commentsTreeActions.goToComment(comment.getParentCommentId());
            navigationArrowInfo.setChildCommentId(comment.getCommentId());
            navigationArrowInfo.setParentCommentId(comment.getParentCommentId());
        });
    }

    private void bindCollapseAction() {
        int childrenCount = commentNode.getChildrenCount();
        if (childrenCount == 0) {
            tvCommentNumber.setVisibility(View.GONE);
            ivCommentsNumberIcon.setVisibility(View.GONE);
            clickableContainer.setOnClickListener(null);
            clickableContainer.setClickable(false);
        } else {
            clickableContainer.setOnClickListener(new CollapseOnClickListener());
            if (commentNode.isCollapsed()) {
                tvCommentNumber.setVisibility(View.VISIBLE);
                ivCommentsNumberIcon.setVisibility(View.VISIBLE);
                tvCommentNumber.setText(String.valueOf(childrenCount));
            } else {
                tvCommentNumber.setVisibility(View.GONE);
                ivCommentsNumberIcon.setVisibility(View.GONE);
            }
        }
    }

    private static final String MAGIC_INVISIBLE_RATING = "99";

    private void showRating() {
        int rating = comment.getRating();
        if (rating == Comment.HIDE_RATING) {
            tvRating.setVisibility(View.INVISIBLE);
            tvRating.setText(MAGIC_INVISIBLE_RATING);
            ivHideRating.setVisibility(View.VISIBLE);
        } else {
            ivHideRating.setVisibility(View.GONE);
            tvRating.setVisibility(View.VISIBLE);
            String ratingStr = String.valueOf(rating);
            if (rating > 0) {
                ratingStr = "+" + ratingStr;
            }
            tvRating.setText(ratingStr);
        }
    }

    private void showAuthorName() {
        String commentAuthor = comment.getAuthor();
        String storyAuthor = commentsTreeInfo.getStoryAuthor();

        tvAuthorName.setText(commentAuthor);
        tvAuthorName.setOnClickListener(new ProfileOnClickListener(getContext(), commentAuthor));

        int backgroundId = R.drawable.selection_drawable_gray_with_corners;
        int textColor = R.color.comment_title_color;
        if (storyAuthor != null && storyAuthor.equals(commentAuthor)) {
            backgroundId = R.drawable.green_rectangle;
            textColor = R.color.white_text;
        }
        if (UserModel.isUser(commentAuthor)) {
             textColor = R.color.steel_blue;
        }
        tvAuthorName.setTextColor(ContextCompat.getColor(getContext(), textColor));
        tvAuthorName.setBackgroundResource(backgroundId);
    }

    private void showTime() {
        long time = comment.getTime();
        tvTime.setText(new FormatTimeHelper(getContext()).formatTime((int) time));
    }

    private void showCommentBody() {
        int storyId = commentsTreeInfo.getStoryId();
        CommentLinkOnClickListener linkOnClickListener = new CommentLinkOnClickListener(storyId, commentsTreeActions);

        ArrayList<CommentContentBlock> blocks = comment.getContentBlocks();
        CommentBindArgs bindArgs = new CommentBindArgs(linkOnClickListener);
        contentBlocksViewHolder.bindContentBlocks(blocks, bindArgs);
    }

    private void setVoteActions() {
        String author = comment.getAuthor();
        if (UserModel.isUser(author)) {
            btnVoteUp.setEnabled(false);
            btnVoteDown.setEnabled(false);
        } else {
            btnVoteUp.setEnabled(true);
            btnVoteDown.setEnabled(true);
        }
    }

    public void showVoteStatus(Vote vote) {
        boolean voteUp = vote == Vote.UP;
        boolean voteDown = vote == Vote.DOWN;

        btnVoteUp.setSelected(voteUp);
        btnVoteUp.setEnabled(!voteUp);

        btnVoteDown.setSelected(voteDown);
        btnVoteDown.setEnabled(!voteDown);
    }

    private Context getContext() {
        return tvAuthorName.getContext();
    }

    private class HideRatingOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(), R.string.hide_comment_rating, Toast.LENGTH_SHORT).show();
        }
    }

    private void voteComment(Vote vote) {
        if (UserModel.getInstance().isLogin()) {
            int storyId = commentsTreeInfo.getStoryId();
            UserActions.getInstance().voteComment(storyId, comment.getCommentId(), vote);
        } else {
            Toast.makeText(getContext(), R.string.must_login_for_action, Toast.LENGTH_SHORT).show();
        }
    }

    private class CommentActionsOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.setOnMenuItemClickListener(new CommentActionsMenuOnClickListener());
            popup.inflate(R.menu.comment_actions_menu);
            popup.show();
        }

        class CommentActionsMenuOnClickListener implements PopupMenu.OnMenuItemClickListener {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.share_comment_link:
                        AndroidUtils.shareText(UrlUtils.getTargetCommentLink(commentsTreeInfo.getStoryId(), comment.getCommentId()), getContext());
                        return true;
                    case R.id.copy_comment_link:
                        copyCommentLink();
                        return true;
                    case R.id.go_to_profile:
                        Intent intent = new Intent(getContext(), ProfileActivity.class);
                        intent.putExtra(ProfileActivity.PROFILE_NAME, comment.getAuthor());
                        getContext().startActivity(intent);
                        return true;
                    case R.id.reply_comment:
                        commentsTreeActions.replyComment(comment.getCommentId());
                        return true;
                    case R.id.copy_text:
                        copyCommentText();
                        return true;
                    default:
                        return false;
                }
            }

            private void copyCommentText() {
                StringBuilder sb = new StringBuilder();
                for (CommentContentBlock block : comment.getContentBlocks()) {
                    if (block.getContentType() == ContentType.TEXT) {
                        String text = ((TextContentBlock) block).getText();
                        sb.append(text);
                    }
                }
                Intent intent = new Intent(getContext(), CopyTextActivity.class);
                intent.putExtra(CopyTextActivity.TEXT, sb.toString());
                getContext().startActivity(intent);
            }

            private void copyCommentLink() {
                String commentLink = UrlUtils.getTargetCommentLink(commentsTreeInfo.getStoryId(), comment.getCommentId());
                if (commentLink != null) {
                    AndroidUtils.copyText(commentLink, getContext());
                } else {
                    Toast.makeText(getContext(), R.string.something_bad, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class CollapseOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            commentsTreeActions.setCollapseComment(commentNode);
        }
    }
}
