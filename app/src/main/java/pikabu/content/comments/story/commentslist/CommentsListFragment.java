package pikabu.content.comments.story.commentslist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

import pikabu.content.comments.story.commentslist.fullstory.FullStoryModel;
import pikabu.content.comments.story.commentslist.fullstory.FullStoryModelContainer;
import pikabu.content.comments.story.commentslist.fullstory.StoryController;
import pikabu.content.comments.story.commentslist.fullstory.StoryView;
import pikabu.content.story.Story;
import pikabu.main.DrawerActivity;


public class CommentsListFragment extends UpArrowRecyclerViewFragmentV2 {
    public static final String STORY_ID = "links_to_story";
    public static final String TARGET_COMMENT_ID = "target_comment_id";
    public final static String SERIAL_STORY = "serial_story";

    public static CommentsListFragment newInstance(int storyId, int targetCommentId) {
        CommentsListFragment fragment = new CommentsListFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(STORY_ID, storyId);
        arguments.putInt(TARGET_COMMENT_ID, targetCommentId);
        fragment.setArguments(arguments);
        return fragment;
    }

    public static CommentsListFragment newInstance(Story story) {
        CommentsListFragment fragment = new CommentsListFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(SERIAL_STORY, story);
        arguments.putInt(STORY_ID, story.getStoryId());
        fragment.setArguments(arguments);
        return fragment;
    }

    private CommentsListModel commentsListModel;
    private CommentsListView commentsListView;
    private CommentsListController commentsListController;

    private RefreshButtonView refreshButtonView;

    private StoryController storyController;
    private StoryView storyView;
    private FullStoryModel fullStoryModel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.comments_list, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("");

        commentsListView = new CommentsListView(view);
        refreshButtonView = new RefreshButtonView();
        View v = View.inflate(getContext(), R.layout.full_story_view, null);
        storyView = new StoryView(v, getActivity());

        commentsListModel = CommentsListModelContainer.getInstance(getStoryId());
        commentsListModel.setTargetCommentId(getTargetCommentId());
        fullStoryModel = FullStoryModelContainer.getInstance(getStoryId(), getStory());

        storyController = new StoryController(fullStoryModel, storyView);
        storyController.bind();
        commentsListController = new CommentsListController(commentsListView, commentsListModel,
                getActivity(), refreshButtonView, fullStoryModel, storyView);
        CommentsTreeAdapter adapter = new CommentsTreeAdapter(commentsListModel.getCommentsTree(),
                commentsListModel.getVoteList(), commentsListController, commentsListController);
        adapter.addHeader(v);
        commentsListController.bind(adapter);
	}

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        storyController.unbind();
        commentsListController.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (!((DrawerActivity) getActivity()).isDrawerOpen()) {
            inflater.inflate(R.menu.comments_list_menu, menu);
            View actionView = menu.findItem(R.id.action_refresh).getActionView();
            actionView.setOnClickListener(view -> commentsListModel.startLoading());
            refreshButtonView.setView(actionView, commentsListModel.getListState() == ListState.PROGRESS);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.reply_comment: {
                commentsListController.replyComment(0);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private int getStoryId() {
        return getArguments().getInt(STORY_ID);
    }

    private int getTargetCommentId() {
        int targetCommentId = getArguments().getInt(TARGET_COMMENT_ID);
        getArguments().putInt(TARGET_COMMENT_ID, 0);
        return targetCommentId;
    }

    private Story getStory() {
        return (Story) getArguments().getSerializable(SERIAL_STORY);
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return commentsListController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return commentsListView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return commentsListModel;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return commentsListModel;
    }
}
