package pikabu.content.comments.story.commentslist.fullstory;

import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.emirinay.pikabuapp.R;

import pikabu.content.story.Story;
import pikabu.content.story.StoryViewBinder;
import pikabu.views.ProgressViewBinder;

/**
 * Created on 22.12.2016.
 */

public class StoryView {

    private View storyView;
    private StoryViewBinder storyViewBinder;
    private ProgressViewBinder progressViewBinder;

    private ViewActions viewActions;

    public StoryView(View view, FragmentActivity fragmentActivity) {
        storyView = view.findViewById(R.id.story_view);
        storyViewBinder = new StoryViewBinder(storyView, fragmentActivity);
        View progressStateView = view.findViewById(R.id.progress_state_view);
        progressViewBinder = new ProgressViewBinder(progressStateView);
        progressViewBinder.setTryAgainButtonOnClickListener(v -> viewActions.onErrorClick());
    }

    public void bind(ViewActions viewActions) {
        this.viewActions = viewActions;
    }

    public void showProgress() {
        progressViewBinder.showProgress();
        storyView.setVisibility(View.GONE);
    }

    public void showError(int messageId) {
        progressViewBinder.showMessage(messageId, true);
        storyView.setVisibility(View.GONE);
    }

    public void showStory(Story story) {
        storyView.setVisibility(View.VISIBLE);
        storyViewBinder.bindStory(story, true, true);
        progressViewBinder.hideAll();
    }

    public void updateSaveStatus(Story story) {
        storyViewBinder.updateSaveStatus(story);
    }

    public void updateVoteStatus(Story story) {
        storyViewBinder.updateVoteStatus(story);
    }

    public void updateCommentsNumber(int commentsNumber) {
        storyViewBinder.updateCommentsNumber(commentsNumber);
    }
}
