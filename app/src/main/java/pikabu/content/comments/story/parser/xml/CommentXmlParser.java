package pikabu.content.comments.story.parser.xml;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import pikabu.content.comments.blocks.parser.CommentBodyParser;
import pikabu.content.comments.story.Comment;
import pikabu.content.comments.blocks.CommentContentBlock;
import rx.Observable;

/**
 * Created on 26.02.2016.
 */
public class CommentXmlParser {

    public static Observable<ArrayList<Comment>> getXmlCommentParserObservable(String xmlDocument) {
        return Observable.create(subscriber -> {
            ArrayList<Comment> comments = parseXmlComments(xmlDocument);
            if (comments == null) {
                subscriber.onError(new ExceptionWithInteger(R.string.comment_parser_error));
            } else {
                subscriber.onNext(comments);
            }
        });
    }

    /**
     * Get comments list from xml document
     * @param xmlDocument document
     * @return comments list, or null if was onError
     */
    public static ArrayList<Comment> parseXmlComments(String xmlDocument) {
        ArrayList<Comment> comments = new ArrayList<>();
        try {
            XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
            xmlPullParser.setInput(new StringReader(xmlDocument));

            while (xmlPullParser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (xmlPullParser.getEventType() == XmlPullParser.START_TAG) {
                    String name = xmlPullParser.getName();
                    if (name.equals("comment")) {
                        Comment comment = readComment(xmlPullParser);
                        if (comment != null) {
                            comments.add(comment);
                        }
                    }
                }
                xmlPullParser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return comments;
    }

    private static Comment readComment(XmlPullParser xmlPullParser) throws Exception {
        CommentXmlParser commentXmlParser = new CommentXmlParser(xmlPullParser);
        int id = commentXmlParser.readId();
        int parentId = commentXmlParser.readParentId();
        int rating = commentXmlParser.readRating();
        String author = xmlPullParser.getAttributeValue(null, "nick");
        long time = commentXmlParser.readDate();

        if (xmlPullParser.next() == XmlPullParser.TEXT) {
            String commentBody = xmlPullParser.getText();
            ArrayList<CommentContentBlock> blocks = new CommentBodyParser().parseCommentBody(commentBody);
            return new Comment(id, parentId, rating, author, time, blocks);
        } else {
            return null;//deleted comment
        }

    }

    private XmlPullParser xmlPullParser;

    public CommentXmlParser(XmlPullParser xmlPullParser) {
        this.xmlPullParser = xmlPullParser;
    }

    private int readId() throws NumberFormatException{
        String id = xmlPullParser.getAttributeValue(null, "id");
        return Integer.parseInt(id);
    }

    private int readParentId() throws NumberFormatException {
        String parentId = xmlPullParser.getAttributeValue(null, "answer");
        return Integer.parseInt(parentId);
    }

    private int readRating() throws NumberFormatException {
        String rating = xmlPullParser.getAttributeValue(null, "rating");
        if (rating.equals("-")) {
            return Comment.HIDE_RATING;
        }
        return Integer.parseInt(rating);
    }

    private long readDate() throws ParseException{
        String input = xmlPullParser.getAttributeValue(null, "date");//2016-02-28 15:22(example)
        Date date = new SimpleDateFormat("yyyy-MM-d H:m", Locale.getDefault()).parse(input);
        return date.getTime()/1000L;
    }
}
