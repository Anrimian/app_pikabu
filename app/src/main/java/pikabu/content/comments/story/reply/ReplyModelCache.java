package pikabu.content.comments.story.reply;

import android.support.v4.util.LruCache;

/**
 * Created on 01.06.2016.
 */
public class ReplyModelCache {

    private static final int MODEL_CACHE_SIZE = 20;

    private static LruCache<String, ReplyModel> cache = new LruCache<>(MODEL_CACHE_SIZE);

    public static ReplyModel getReplyModel(int storyId, int parentCommentId) {
        String key = getKeyForCache(storyId, parentCommentId);
        ReplyModel model = cache.get(key);
        if (model == null) {
            model = new ReplyModel(storyId, parentCommentId);
            cache.put(key, model);
        }
        return model;
    }

    public static void removeReplyModel(int storyId, int parentCommentId) {
        String key = getKeyForCache(storyId, parentCommentId);
        cache.remove(key);
    }

    private static String getKeyForCache(int storyId, int parentCommentId) {
        return "s_" + storyId + "_c_" + parentCommentId;
    }
}
