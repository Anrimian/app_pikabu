package pikabu.content.comments.story.commentslist;

/**
 * Created on 15.02.2016.
 */
public interface CommentsTreeInfo {

    String getStoryAuthor();

    int getStoryId();

    NavigationArrowInfo getNavigationArrowInfo();
}
