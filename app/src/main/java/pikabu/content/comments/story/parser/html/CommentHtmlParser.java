package pikabu.content.comments.story.parser.html;

import android.support.annotation.NonNull;

import com.emirinay.tools.TextUtils;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import pikabu.content.comments.story.Comment;
import pikabu.content.comments.story.parser.CommentsParserException;
import rx.Observable;

public class CommentHtmlParser {

	private final Element htmlComment;

	protected CommentHtmlParser(Element htmlComment) {
		this.htmlComment = htmlComment;
	}

	/**
	 * Extract comments from document
	 *
	 * @param doc , not null
	 * @return commentsList, not null
	 */
	public static ArrayList<Comment> getCommentsList(@NonNull Document doc) {
		ArrayList<Comment> commentsList = new ArrayList<>();
		Elements htmlComments = doc.select(".b-comment");
		if (htmlComments.size() == 0) {
			return commentsList;
		}
		for (Element htmlComment : htmlComments) {
			Comment comment = getCommentInstance(htmlComment);
			if (comment != null) {
				commentsList.add(comment);
			}
		}
		return commentsList;

	}

	public static Observable<ArrayList<Comment>> getCommentsListObservable(@NonNull Document doc) {
		return Observable.create(subscriber -> subscriber.onNext(getCommentsList(doc)));
	}

	/**
	 * Extract comment from html text block
	 *
	 * @return Comment
	 *
	 */
	public static Comment getCommentInstance(Element htmlComment) {
		CommentHtmlParser parser = new CommentHtmlParser(htmlComment);
		/*Comment comment;
		try {
			comment = new Comment(parser.selectCommentId(),
					parser.selectParentCommentId(),
					parser.selectRaiting(),
					parser.selectVoteStatus(),
					parser.selectGender(),
					parser.selectTime(),
					parser.selectAuthor(),
					parser.selectAuthorAvatar(),
					parser.selectContent());
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}*/
		return null;
	}

	private int selectCommentId() throws NumberFormatException, CommentsParserException {
		String htmlCommentId = htmlComment.attr("data-id");
		int commentId = Integer.parseInt(htmlCommentId);
		if (commentId < 0) {
			throw new CommentsParserException("invalid commentId: " + commentId);
		}
		return commentId;
	}

	private int selectParentCommentId() throws NumberFormatException, CommentsParserException {
		String htmlparentCommentId = htmlComment.attr("data-parent-id");
		int parentCommentId = Integer.parseInt(htmlparentCommentId);
		if (parentCommentId < 0) {
			throw new CommentsParserException("invalid parentCommentId: " + parentCommentId);
		}
		return parentCommentId;
	}

	private int selectRaiting() throws NumberFormatException {
        Element elRaiting = htmlComment.select(".b-comment__rating-count").first();
        if (elRaiting.toString().contains("i-sprite--comments__rating-lock i-sprite--inline-block")) {
            return Comment.HIDE_RATING;
        }
        String htmlRaiting = elRaiting.text();
		htmlRaiting = htmlRaiting.replace("+", "");
		return Integer.parseInt(htmlRaiting);
	}

	private int selectVoteStatus() throws NumberFormatException, CommentsParserException {
		String htmlVoteStatus = htmlComment.attr("data-vote");
		int voteStatus = Integer.parseInt(htmlVoteStatus);
		if (voteStatus < -1 || voteStatus > 1) {
			throw new CommentsParserException("invalid voteStatus: " + voteStatus);
		}
		return voteStatus;
	}

	private int selectGender() throws CommentsParserException{
		String htmlGender = htmlComment.select(".b-comment__user ").first().text();
		if (htmlGender.contains("отправлено")){
			return 0;
		}
		if (htmlGender.contains("отправилa")){
			return 2;
		}
		if (htmlGender.contains("отправил")){
			return 1;
		}
		throw new CommentsParserException("invalid htmlGender: " + htmlGender);
	}

	private long selectTime() throws NumberFormatException, CommentsParserException{
		String htmlTime = htmlComment.select(".b-comment__time").first().attr("datetime");
		long unixTime = Long.parseLong(htmlTime);
		if (unixTime < 0) {
			throw new CommentsParserException("invalid unixTime: " + unixTime);
		}
		return unixTime;
	}

	private String selectAuthor() throws CommentsParserException {
		String authorLink = htmlComment.select(".b-comment__user a").first().attr("href");
		if (authorLink == null || authorLink.isEmpty()) {
			throw new CommentsParserException("invalid author name: " + htmlComment);
		}
		return TextUtils.getUrlLastSegment(authorLink);
	}


	private String selectAuthorAvatar() {
		return htmlComment.select(".b-comment__user").first().select("img").attr("src");
	}

	private String selectContent() {
		return htmlComment.select(".b-comment__content").first().toString();
	}

}
