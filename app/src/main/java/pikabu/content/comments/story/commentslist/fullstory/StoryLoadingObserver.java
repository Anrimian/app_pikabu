package pikabu.content.comments.story.commentslist.fullstory;


public interface StoryLoadingObserver {

	void onProgress();

	void onError();

	void onCompleted();

	void updateSaveStatus();

	void updateVoteStatus();
}
