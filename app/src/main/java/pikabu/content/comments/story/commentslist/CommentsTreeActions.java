package pikabu.content.comments.story.commentslist;

import com.emirinay.tools.treelist.Node;

/**
 * Created on 16.02.2016.
 */
public interface CommentsTreeActions {

    void goToComment(int commentId);

    void setCollapseComment(Node node);

    void replyComment(int commentId);
}
