package pikabu.content.comments.story.commentslist;

/**
 * Created on 01.05.2016.
 */
public class NavigationArrowInfo {
    public static final int NOTHING = -1;

    private int parentCommentId;
    private int childCommentId;

    public int getChildCommentId() {
        return childCommentId;
    }

    public void setChildCommentId(int childCommentId) {
        this.childCommentId = childCommentId;
    }

    public int getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(int parentCommentId) {
        this.parentCommentId = parentCommentId;
    }
}
