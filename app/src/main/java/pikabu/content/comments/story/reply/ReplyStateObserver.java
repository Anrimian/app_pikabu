package pikabu.content.comments.story.reply;

/**
 * Created on 02.06.2016.
 */
public interface ReplyStateObserver {

    void onProgress();

    void onCompleted();

    void onError();
}
