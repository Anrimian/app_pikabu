package pikabu.content.comments.story.commentslist;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;

import pikabu.views.ProgressViewBinder;

/**
 * Created on 22.12.2016.
 */

public class CommentsListView implements RecyclerViewInfo {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private View progressStateView;
    private ProgressViewBinder progressViewBinder;

    private ViewActions viewActions;

    public CommentsListView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        progressStateView = View.inflate(getContext(), R.layout.progress_state_view, null);
        progressViewBinder = new ProgressViewBinder(progressStateView);

        progressViewBinder.setTryAgainButtonOnClickListener(new ErrorOnClickListener());

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.ptr_layout);
        swipeRefreshLayout.setOnRefreshListener(new RefreshListener());
        swipeRefreshLayout.setColorSchemeResources(R.color.green);
    }

    public void bind(CommentsTreeAdapter adapter, ViewActions viewActions) {
        this.viewActions = viewActions;
        adapter.addFooter(progressStateView);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public LinearLayoutManager getLinearLayoutManager() {
        return linearLayoutManager;
    }

    public void showProgress(boolean refreshing) {
        progressViewBinder.showProgress();
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(refreshing));
    }

    public void showError(int messageId) {
        enableSwipeRefreshLayout();
        progressViewBinder.showMessage(messageId, true);
    }

    public void showEnd(int messageId) {
        enableSwipeRefreshLayout();
        progressViewBinder.showMessage(messageId, false);
    }

    private void enableSwipeRefreshLayout() {
        swipeRefreshLayout.setEnabled(true);
        swipeRefreshLayout.setRefreshing(false);
    }

    public void scrollToPosition(int index) {
        int firstVisibleIndex = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
        int lastVisibleIndex = linearLayoutManager.findLastCompletelyVisibleItemPosition();
        if (index < firstVisibleIndex || index > lastVisibleIndex) {//not visible
            linearLayoutManager.scrollToPositionWithOffset(index, 0);
        }
    }

    private class ErrorOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            viewActions.onErrorClick();
        }
    }

    private class RefreshListener implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            viewActions.onRefresh();
        }
    }

    private Context getContext() {
        return recyclerView.getContext();
    }
}
