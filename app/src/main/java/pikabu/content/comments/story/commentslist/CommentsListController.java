package pikabu.content.comments.story.commentslist;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.ListPosition;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.treelist.Node;
import com.emirinay.tools.treelist.Tree;

import pikabu.content.comments.story.commentslist.fullstory.FullStoryModel;
import pikabu.content.comments.story.commentslist.fullstory.StoryView;
import pikabu.content.comments.story.reply.ReplyDialog;
import pikabu.content.story.Story;
import pikabu.main.DrawerActivity;

/**
 * Created on 22.12.2016.
 */

public class CommentsListController implements CommentsActionObserver, ViewActions, CommentsTreeInfo, CommentsTreeActions, ArrowControllerInfo {

    private FragmentActivity activity;

    private CommentsListModel commentsListModel;
    private CommentsListView commentsListView;
    private CommentsTreeAdapter commentsTreeAdapter;

    private FullStoryModel fullStoryModel;
    private StoryView storyView;

    @Nullable
    private RefreshButtonView refreshButtonView;

    public CommentsListController(CommentsListView commentsListView,
                                  CommentsListModel commentsListModel,
                                  FragmentActivity activity,
                                  @Nullable RefreshButtonView refreshButtonView,
                                  FullStoryModel fullStoryModel,
                                  StoryView storyView) {
        this.commentsListView = commentsListView;
        this.commentsListModel = commentsListModel;
        this.activity = activity;
        this.refreshButtonView = refreshButtonView;
        this.fullStoryModel = fullStoryModel;
        this.storyView = storyView;
    }

    public void bind(CommentsTreeAdapter commentsTreeAdapter) {
        this.commentsTreeAdapter = commentsTreeAdapter;
        commentsListView.bind(commentsTreeAdapter, this);
        commentsListModel.registerObserver(this);
        updateCommentsNumber();
        goToTargetCommentIfExists();
    }

    public void unbind() {
        commentsListModel.unregisterObserver(this);
    }

    @Override
    public void onComplete() {
        if (refreshButtonView != null) {
            refreshButtonView.stopButtonAnimation();
        }
        int messageId = R.string.no_more_comments;
        if (isListEmpty()) {
            messageId = R.string.no_comments;
        }
        commentsListView.showEnd(messageId);
    }

    @Override
    public void onError() {
        if (refreshButtonView != null) {
            refreshButtonView.stopButtonAnimation();
        }
        int messageId = commentsListModel.getErrorMessageId();
        if (isRefreshing() && !isListEmpty()) {
            Toast.makeText(activity, messageId, Toast.LENGTH_SHORT).show();
            commentsListModel.showState(ListState.COMPLETE);
            return;
        }
        commentsListView.showError(messageId);
    }

    @Override
    public void onProgress() {
        if (refreshButtonView != null) {
            refreshButtonView.startButtonAnimation();
        }
        commentsListView.showProgress(isRefreshing());
    }

    public boolean isRefreshing() {
        return commentsListModel.isRefreshing();
    }

    @Override
    public void updateList() {
        commentsTreeAdapter.notifyDataSetChanged();
        activity.invalidateOptionsMenu();

        updateCommentsNumber();
        goToTargetCommentIfExists();
    }

    private void goToTargetCommentIfExists() {
        if (!isListEmpty()) {
            int targetCommentId = commentsListModel.getTargetCommentId();
            if (targetCommentId != 0) {
                goToComment(targetCommentId);
                commentsListModel.setTargetCommentId(0);
            }
        }
    }

    public boolean isListEmpty() {
        return commentsTreeAdapter.getCount() == 0;
    }

    private void updateCommentsNumber() {
        Tree tree = commentsListModel.getCommentsTree();
        if (!tree.isEmpty()) {
            storyView.updateCommentsNumber(tree.getNodeCount());
        }
    }

    @Override
    public void onRefresh() {
        fullStoryModel.loadStory();
        commentsListModel.setRefreshing(true);
        commentsListModel.startLoading();
    }

    @Override
    public void onErrorClick() {
        commentsListModel.startLoading();
    }

    @Override
    public void goToComment(int commentId) {
        Node targetNode = commentsListModel.findCommentNodeById(commentId);
        if (targetNode != null) {
            if (targetNode.hasCollapsedParent()) {
                commentsListModel.getCommentsTree().expandBranch(targetNode);
            }
            commentsTreeAdapter.setAnimationTargetId(commentId);
            commentsTreeAdapter.notifyDataSetChanged();

            int index = targetNode.getIndex() + 1;
            commentsListView.scrollToPosition(index);

            commentsListModel.saveListPosition(new ListPosition(index, 0));//for restoring list position
        } else {
            Toast.makeText(activity, R.string.comment_not_found, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setCollapseComment(Node node) {
        if (node.getChildrenCount() == 0) {
            return;
        }

        Tree tree = commentsListModel.getCommentsTree();
        if (node.isCollapsed()) {
            tree.expandNodes(node);
        } else {
            tree.collapseNodes(node);
        }
        commentsTreeAdapter.notifyDataSetChanged();
    }

    @Override
    public void replyComment(int commentId) {
        ReplyDialog.start(activity, getStoryId(), commentId);
    }

    @Override
    public String getStoryAuthor() {
        Story story = fullStoryModel.getStory();
        if (story != null) {
            return story.getAuthor();
        }
        return null;
    }

    @Override
    public int getStoryId() {
        return commentsListModel.getStoryId();
    }

    @Override
    public NavigationArrowInfo getNavigationArrowInfo() {
        return commentsListModel.getNavigationArrowInfo();
    }

    @Override
    public boolean showArrow() {
        return !isListEmpty() && !((DrawerActivity) activity).isDrawerOpen();
    }
}
