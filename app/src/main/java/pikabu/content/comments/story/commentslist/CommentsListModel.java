package pikabu.content.comments.story.commentslist;

import android.util.SparseArray;

import com.emirinay.tools.ExceptionWithInteger;
import com.emirinay.tools.fragment.lists.ListPosition;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.treelist.Node;
import com.emirinay.tools.treelist.Tree;

import java.util.ArrayList;

import pikabu.AppExecutors;
import pikabu.content.Vote;
import pikabu.content.comments.story.Comment;
import pikabu.content.comments.story.parser.xml.CommentXmlParser;
import pikabu.user.actions.UserActions;
import pikabu.user.actions.comments.UserCommentActionsObserver;
import pikabu.utils.network.Api;
import rx.android.schedulers.AndroidSchedulers;

public class CommentsListModel implements UserCommentActionsObserver, ArrowPositionModel, ListPositionModel {

    private ListPosition listPosition;
    private ListPosition arrowListPosition;

    private int targetCommentId;

    private SparseArray<Node> nodeHashMap = new SparseArray<>();
    private Tree commentsTree = new Tree();
    private SparseArray<Vote> voteList = new SparseArray<>();

    private NavigationArrowInfo navigationArrowInfo = new NavigationArrowInfo();

	private CommentsActionObservable mObservable;

	private int storyId;

	private int errorMessageId;

    private boolean isRefreshing = false;

    private ListState listState;

	public CommentsListModel(int storyId) {
        this.storyId = storyId;
        mObservable = new CommentsActionObservable();
        startLoading();
        UserActions.getInstance().registerCommentActionsObserver(this);
    }

    public void startLoading() {
        showState(ListState.PROGRESS);
        loadComments(storyId);
    }

    private void loadComments(int storyId) {
        Api.getXmlCommentsObservable(storyId)
                .subscribeOn(AppExecutors.COMMENTS_SCHEDULER)
                .flatMap(CommentXmlParser::getXmlCommentParserObservable)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    addListData(result);
                    updateList();
                    isRefreshing = false;
                    showState(ListState.COMPLETE);
                }, throwable -> {
                    setErrorMessageId(((ExceptionWithInteger) throwable).getMessageId());
                    showState(ListState.ERROR);
                    isRefreshing = false;
                });
    }

	public void registerObserver(final CommentsActionObserver observer) {
		mObservable.registerObserver(observer);
        showState(listState);
	}
	
	public void unregisterObserver(final CommentsActionObserver observer) {
		mObservable.unregisterObserver(observer);		
	}
	
	public void updateList() {
        mObservable.updateList();
    }

    public void showState(ListState listState) {
        this.listState = listState;
        mObservable.showState(listState);
    }

    public void addListData(ArrayList<Comment> listData) {
        for (Comment comment : listData) {
            int commentId = comment.getCommentId();
            Node commentNode = nodeHashMap.get(commentId);
            if (commentNode == null) {
                Node node = commentsTree.createNode(comment, nodeHashMap.get(comment.getParentCommentId()));
                nodeHashMap.put(commentId, node);
            } else {
                commentNode.setNodeData(comment);
            }
        }
    }

    @Override
    public void commentWasVoted(int storyId, int commentId, Vote newVote) {
        if (this.storyId == storyId) {
            Node node = findCommentNodeById(commentId);
            if (node != null) {
                Vote oldVote = voteList.get(commentId);
                Vote vote = Vote.mergeVote(oldVote, newVote);
                voteList.put(commentId, vote);

                Comment comment = (Comment) node.getNodeData();
                comment.setRating(comment.getRating() + newVote.toInt());

                updateList();
            }
        }
    }

    public Tree getCommentsTree() {
        return commentsTree;
    }

    public int getErrorMessageId() {
        return errorMessageId;
    }

    public boolean isRefreshing() {
        return isRefreshing;
    }

    public void setRefreshing(boolean isRefreshing) {
        this.isRefreshing = isRefreshing;
    }

    public void setErrorMessageId(int errorMessageId) {
        this.errorMessageId = errorMessageId;
    }

    public ListPosition getArrowListPosition() {
        return arrowListPosition;
    }

    public void setArrowListPosition(ListPosition arrowListPosition) {
        this.arrowListPosition = arrowListPosition;
    }

    public SparseArray<Vote> getVoteList() {
        return voteList;
    }

    public Node findCommentNodeById(int commentId) {
        return nodeHashMap.get(commentId);
    }

    public NavigationArrowInfo getNavigationArrowInfo() {
        return navigationArrowInfo;
    }

    public ListState getListState() {
        return listState;
    }

    @Override
    public void saveListPosition(ListPosition listPosition) {
        this.listPosition = listPosition;
    }

    @Override
    public ListPosition loadListPosition() {
        return listPosition;
    }

    public int getTargetCommentId() {
        return targetCommentId;
    }

    public void setTargetCommentId(int targetCommentId) {
        this.targetCommentId = targetCommentId;
    }


    public int getStoryId() {
        return storyId;
    }
}
