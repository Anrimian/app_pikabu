package pikabu.content.comments.story.reply;

import android.database.Observable;

/**
 * Created on 02.06.2016.
 */
public class ReplyStateObservable extends Observable<ReplyStateObserver> {

    public void onProgress() {
        for (final ReplyStateObserver observer : mObservers) {
            observer.onProgress();
        }
    }

    public void onCompleted() {
        for (final ReplyStateObserver observer : mObservers) {
            observer.onCompleted();
        }
    }

    public void onError() {
        for (final ReplyStateObserver observer : mObservers) {
            observer.onError();
        }
    }
}
