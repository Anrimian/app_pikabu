package pikabu.content.comments.story.commentslist;


public interface CommentsActionObserver {

	void onComplete();

	void onError();
	
	void onProgress();

	void updateList();
}
