package pikabu.content.comments.story.reply;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;

import pikabu.user.UserModel;

/**
 * Created on 01.06.2016.
 */
public class ReplyActivity extends AppCompatActivity implements ReplyStateObserver {

    private static final String STORY_ID = "story_id";
    private static final String PARENT_COMMENT_ID = "parent_comment_id";

    public static void start(Context ctx, int storyId, int parentCommentId) {
        if (UserModel.getInstance().isLogin()) {
            Intent intent = new Intent(ctx, ReplyActivity.class);
            intent.putExtra(ReplyActivity.STORY_ID, storyId);
            intent.putExtra(ReplyActivity.PARENT_COMMENT_ID, parentCommentId);
            ctx.startActivity(intent);
        } else {
            Toast.makeText(ctx, R.string.must_login_for_action, Toast.LENGTH_SHORT).show();
        }
    }

    private int storyId;
    private int parentCommentId;

    private EditText etReplyComment;
    private TextView tvPreview;
    private ProgressBar progressBar;

    private ReplyModel replyModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reply_activity_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        etReplyComment = (EditText) findViewById(R.id.reply_edit_text);
        etReplyComment.addTextChangedListener(new ReplyTextWatcher());
        tvPreview = (TextView) findViewById(R.id.tv_preview);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        Intent intent = getIntent();
        storyId = intent.getIntExtra(STORY_ID, 0);
        parentCommentId = intent.getIntExtra(PARENT_COMMENT_ID, 0);

        if (storyId == 0) {
            Toast.makeText(this, R.string.can_not_get_story_id, Toast.LENGTH_LONG).show();
            finish();
        }

        replyModel = ReplyModelCache.getReplyModel(storyId, parentCommentId);
        etReplyComment.setText(replyModel.getReplyCommentText());
        etReplyComment.requestFocus();
        replyModel.registerObserver(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reply_activity_options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.send_comment: {
                replyComment();
                return true;
            }
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        replyModel.unregisterObserver(this);
    }

    @Override
    public void onCompleted() {
        showProgress(false);
        ReplyModelCache.removeReplyModel(storyId, parentCommentId);
        Toast.makeText(this, R.string.reply_comment_success, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onProgress() {
        showProgress(true);
    }

    @Override
    public void onError() {
        showProgress(false);
        Toast.makeText(this, replyModel.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    private void replyComment() {
        String text = etReplyComment.getText().toString();
        if (text.isEmpty()) {
            Toast.makeText(this, R.string.empty_comment_error, Toast.LENGTH_SHORT).show();
            return;
        }
        replyModel.replyComment(text);
    }

    private void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    class ReplyTextWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence text, int start, int before, int count) {
            String htmlText = text.toString();
            replyModel.setReplyCommentText(htmlText);
            tvPreview.setText(Html.fromHtml(htmlText));
        }
    }
}
