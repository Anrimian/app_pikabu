package pikabu.content.comments.story.commentslist;

/**
 * Created on 12.08.2016.
 */
public enum ListState {
    PROGRESS {
        @Override
        public void showState(CommentsActionObserver observer) {
            observer.onProgress();
        }
    },
    ERROR {
        @Override
        public void showState(CommentsActionObserver observer) {
            observer.onError();
        }
    },
    COMPLETE {
        @Override
        public void showState(CommentsActionObserver observer) {
            observer.onComplete();
        }
    };

    public void showState(CommentsActionObserver observer) {

    }
}
