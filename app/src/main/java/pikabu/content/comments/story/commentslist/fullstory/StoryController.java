package pikabu.content.comments.story.commentslist.fullstory;

import pikabu.content.story.Story;

/**
 * Created on 22.12.2016.
 */

public class StoryController implements ViewActions, StoryLoadingObserver {
    private StoryView storyView;
    private FullStoryModel fullStoryModel;

    public StoryController(FullStoryModel fullStoryModel, StoryView storyView) {
        this.fullStoryModel = fullStoryModel;
        this.storyView = storyView;
    }

    public void bind() {
        storyView.bind(this);
        fullStoryModel.registerObserver(this);
    }

    public void unbind() {
        fullStoryModel.unregisterObserver(this);
    }

    @Override
    public void onProgress() {
        if (getStory() == null) {
            storyView.showProgress();
        }
    }

    @Override
    public void onError() {
        int messageId = fullStoryModel.getErrorMessageId();
        storyView.showError(messageId);
    }

    @Override
    public void onCompleted() {
        storyView.showStory(getStory());
    }

    @Override
    public void updateSaveStatus() {
        storyView.updateSaveStatus(getStory());
    }

    @Override
    public void updateVoteStatus() {
        storyView.updateVoteStatus(getStory());
    }

    private Story getStory() {
        return fullStoryModel.getStory();
    }

    @Override
    public void onErrorClick() {
        fullStoryModel.loadStory();
    }


}
