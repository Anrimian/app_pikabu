package pikabu.content.comments.profile;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.AndroidUtils;
import com.emirinay.tools.time.FormatTimeHelper;
import com.emirinay.tools.views.FRelativeLayout;

import java.util.ArrayList;

import pikabu.content.Vote;
import pikabu.content.blocks.ContentBlocksViewHolder;
import pikabu.content.comments.blocks.CommentContentBlock;
import pikabu.content.comments.blocks.TextContentBlock;
import pikabu.content.comments.profile.fragment.CommentActivityActions;
import pikabu.content.profile.ProfileOnClickListener;
import pikabu.content.profile.view.ProfileActivity;
import pikabu.content.story.blocks.ContentType;
import pikabu.user.UserModel;
import pikabu.user.actions.UserActions;
import pikabu.utils.network.UrlUtils;
import pikabu.utils.text.CopyTextActivity;

/**
 * Created on 12.11.2016.
 */

public class UserCommentViewHolder {

    private TextView tvAuthorName;
    private TextView tvRating;
    private TextView tvTime;

    private ImageView btnGoToParent;
    private ImageView btnGoBack;

    private View ivHideRating;
    private View btnVoteUp;
    private View btnVoteDown;
    private View btnCommentActions;

    private ContentBlocksViewHolder contentBlocksViewHolder;

    private UserComment comment;
    private int storyId;

    private CommentActivityActions commentActions;

    public UserCommentViewHolder(View view) {
        tvAuthorName = (TextView) view.findViewById(R.id.tv_comment_author_name);
        tvRating = (TextView) view.findViewById(R.id.tv_comment_rating);
        tvTime = (TextView) view.findViewById(R.id.tv_comment_time);
        btnVoteUp = view.findViewById(R.id.btn_comment_vote_up);
        btnVoteDown = view.findViewById(R.id.btn_comment_vote_down);
        btnCommentActions = view.findViewById(R.id.btn_comment_actions_menu);
        ivHideRating = view.findViewById(R.id.iv_clock);
        ivHideRating.setOnClickListener(new HideRatingOnClickListener());
        TextView tvCommentNumber = (TextView) view.findViewById(R.id.tv_comments_number);
        tvCommentNumber.setVisibility(View.GONE);
        View ivCommentsIcon = view.findViewById(R.id.iv_comments_icon);
        ivCommentsIcon.setVisibility(View.GONE);
        btnGoToParent = (ImageView) view.findViewById(R.id.btn_go_to_parent);
        btnGoBack = (ImageView) view.findViewById(R.id.btn_go_back);
        ((FRelativeLayout) view).setForeground(null);

        ViewGroup contentContainer = (ViewGroup) view.findViewById(R.id.comment_content_container);
        contentBlocksViewHolder = new ContentBlocksViewHolder(contentContainer);

        int buttonColor = ContextCompat.getColor(getContext(), R.color.button_color_filter);
        btnGoToParent.setColorFilter(buttonColor);
        btnGoBack.setColorFilter(buttonColor);

        btnVoteUp.setOnClickListener(v -> voteComment(Vote.UP));
        btnVoteDown.setOnClickListener(v -> voteComment(Vote.DOWN));
    }

    public void bindUserComment(UserComment comment, int storyId, CommentActivityActions commentActions) {
        this.comment = comment;
        this.storyId = storyId;
        this.commentActions = commentActions;

        showAuthorName();
        showRating();
        showTime();
        showCommentBody();
        setVoteActions();

        btnCommentActions.setOnClickListener(new CommentActionsOnClickListener());
    }

    public void bindAsTargetComment(boolean showParentComment, TwoCommentsActions twoCommentsActions) {
        btnGoBack.setVisibility(View.GONE);
        if (showParentComment) {
            btnGoToParent.setVisibility(View.GONE);
        } else {
            btnGoToParent.setVisibility(View.VISIBLE);
            btnGoToParent.setOnClickListener(v -> twoCommentsActions.showParent());
        }
    }

    public void bindAsParentComment(TwoCommentsActions twoCommentsActions) {
        btnGoToParent.setVisibility(View.GONE);
        btnGoBack.setOnClickListener(v -> twoCommentsActions.hideParent());
    }

    private void showAuthorName() {
        String commentAuthor = comment.getAuthor();

        tvAuthorName.setText(commentAuthor);
        tvAuthorName.setOnClickListener(new ProfileOnClickListener(getContext(), commentAuthor));

        int backgroundId = R.drawable.selection_drawable_gray_with_corners;
        int textColor = R.color.comment_title_color;
        if (UserModel.isUser(commentAuthor)) {
            textColor = R.color.steel_blue;
        }
        tvAuthorName.setTextColor(ContextCompat.getColor(getContext(), textColor));
        tvAuthorName.setBackgroundResource(backgroundId);
    }

    private static final String MAGIC_INVISIBLE_RATING = "99";

    private void showRating() {
        int rating = comment.getRating();
        if (rating == UserComment.HIDE_RATING) {
            tvRating.setVisibility(View.INVISIBLE);
            tvRating.setText(MAGIC_INVISIBLE_RATING);
            ivHideRating.setVisibility(View.VISIBLE);
        } else {
            ivHideRating.setVisibility(View.GONE);
            tvRating.setVisibility(View.VISIBLE);
            String ratingStr = String.valueOf(rating);
            if (rating > 0) {
                ratingStr = "+" + ratingStr;
            }
            tvRating.setText(ratingStr);
        }
    }

    private void showTime() {
        long time = comment.getTime();
        tvTime.setText(new FormatTimeHelper(getContext()).formatTime((int) time));
    }

    private void showCommentBody() {
        ArrayList<CommentContentBlock> blocks = comment.getContentBlocksList();
        contentBlocksViewHolder.bindContentBlocks(blocks, null);
    }

    private void setVoteActions() {
        String author = comment.getAuthor();
        if (UserModel.isUser(author)) {
            btnVoteUp.setEnabled(false);
            btnVoteDown.setEnabled(false);
        } else {
            Vote vote = comment.getVoteStatus();
            boolean voteUp = vote == Vote.UP;
            boolean voteDown = vote == Vote.DOWN;

            btnVoteUp.setSelected(voteUp);
            btnVoteUp.setEnabled(!voteUp);

            btnVoteDown.setSelected(voteDown);
            btnVoteDown.setEnabled(!voteDown);
        }
    }

    private void voteComment(Vote vote) {
        if (UserModel.getInstance().isLogin()) {
            UserActions.getInstance().voteComment(storyId, comment.getCommentId(), vote);
        } else {
            Toast.makeText(getContext(), R.string.must_login_for_action, Toast.LENGTH_SHORT).show();
        }
    }

    private Context getContext() {
        return tvAuthorName.getContext();
    }

    private class HideRatingOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(), R.string.hide_comment_rating, Toast.LENGTH_SHORT).show();
        }
    }

    private class CommentActionsOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.setOnMenuItemClickListener(new CommentActionsMenuOnClickListener());
            popup.inflate(R.menu.comment_actions_menu);
            popup.show();
        }

        class CommentActionsMenuOnClickListener implements PopupMenu.OnMenuItemClickListener {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.share_comment_link:
                        AndroidUtils.shareText(UrlUtils.getTargetCommentLink(storyId, comment.getCommentId()), getContext());
                        return true;
                    case R.id.copy_comment_link:
                        copyCommentLink();
                        return true;
                    case R.id.go_to_profile:
                        Intent intent = new Intent(getContext(), ProfileActivity.class);
                        intent.putExtra(ProfileActivity.PROFILE_NAME, comment.getAuthor());
                        getContext().startActivity(intent);
                        return true;
                    case R.id.reply_comment:
                        commentActions.replyComment(storyId, comment.getCommentId());
                        return true;
                    case R.id.copy_text:
                        copyCommentText();
                        return true;
                    default:
                        return false;
                }
            }

            private void copyCommentText() {
                StringBuilder sb = new StringBuilder();
                for (CommentContentBlock block : comment.getContentBlocksList()) {
                    if (block.getContentType() == ContentType.TEXT) {
                        String text = ((TextContentBlock) block).getText();
                        sb.append(text);
                    }
                }
                Intent intent = new Intent(getContext(), CopyTextActivity.class);
                intent.putExtra(CopyTextActivity.TEXT, sb.toString());
                getContext().startActivity(intent);
            }

            private void copyCommentLink() {
                String commentLink = UrlUtils.getTargetCommentLink(storyId, comment.getCommentId());
                if (commentLink != null) {
                    AndroidUtils.copyText(commentLink, getContext());
                } else {
                    Toast.makeText(getContext(), R.string.something_bad, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
