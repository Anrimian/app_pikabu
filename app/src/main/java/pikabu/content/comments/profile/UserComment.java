package pikabu.content.comments.profile;

import java.util.ArrayList;

import pikabu.content.Vote;
import pikabu.content.comments.blocks.CommentContentBlock;

/**
 * Created on 11.11.2016.
 */

public class UserComment {

    public static final int HIDE_RATING = Integer.MIN_VALUE;
    private String author;

    private int commentId;
    private int rating;

    private long time;

    private Vote voteStatus;

    private ArrayList<CommentContentBlock> contentBlocksList;

    public UserComment(int commentId, String author, int rating, Vote voteStatus, long time, ArrayList<CommentContentBlock> contentBlocksList) {
        this.commentId = commentId;
        this.author = author;
        this.rating = rating;
        this.voteStatus = voteStatus;
        this.time = time;
        this.contentBlocksList = contentBlocksList;
    }

    public String getAuthor() {
        return author;
    }

    public int getCommentId() {
        return commentId;
    }

    public ArrayList<CommentContentBlock> getContentBlocksList() {
        return contentBlocksList;
    }

    public int getRating() {
        return rating;
    }

    public long getTime() {
        return time;
    }

    public Vote getVoteStatus() {
        return voteStatus;
    }

    public void setVoteStatus(Vote voteStatus) {
        this.voteStatus = voteStatus;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
