package pikabu.content.comments.profile.fragment;

import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;

/**
 * Created on 20.11.2016.
 */

public class ProfileCommentsActivity extends DrawerActivity {

    public static final String LINK = "link";
    public static final String TITLE_ID = "title_id";

    @Override
    protected Fragment initFragment() {
        String link = getIntent().getStringExtra(LINK);
        int titleId = getIntent().getIntExtra(TITLE_ID, 0);
        return ProfileCommentsFragment.newInstance(link, titleId);
    }
}
