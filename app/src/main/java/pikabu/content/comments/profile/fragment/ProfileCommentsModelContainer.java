package pikabu.content.comments.profile.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;

/**
 * Created on 24.08.2015.
 */
public class ProfileCommentsModelContainer extends Fragment {

    private final static int MODEL_CACHE_SIZE = 2;

    private static LruCache<String, ProfileCommentsModel> cache  = new LruCache(MODEL_CACHE_SIZE);

    public static ProfileCommentsModel getInstance(String modelTag) {
        ProfileCommentsModel model = cache.get(modelTag);
        if (model == null) {
            model = new ProfileCommentsModel();
            cache.put(modelTag, model);
        }
        return model;
    }
}
