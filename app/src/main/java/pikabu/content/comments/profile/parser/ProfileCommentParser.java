package pikabu.content.comments.profile.parser;


import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;
import com.emirinay.tools.TextUtils;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import pikabu.content.ParserException;
import pikabu.content.Vote;
import pikabu.content.comments.blocks.CommentContentBlock;
import pikabu.content.comments.blocks.parser.CommentBodyParser;
import pikabu.content.comments.profile.ProfileComment;
import pikabu.content.comments.profile.UserComment;
import rx.Observable;

/**
 * Created on 24.08.2016.
 */
public class ProfileCommentParser {

    public static Observable<List<ProfileComment>> parseDocument(Document doc) {
        return getElements(doc).flatMap(ProfileCommentParser::parseElement).toList();
    }

    private static Observable<Element> getElements(Document doc) {
        return Observable.from(selectElements(doc));
    }

    private static Observable<ProfileComment> parseElement(Element element) {
        return Observable.create(subscriber -> {
            ProfileCommentParser parser = new ProfileCommentParser(element);
            ProfileComment comment = parser.parseProfileComment();
            if (comment != null) {
                subscriber.onNext(comment);
            } else {
                subscriber.onError(new ExceptionWithInteger(R.string.comment_parser_error));
            }
            subscriber.onCompleted();
        });
    }

    private static Elements selectElements(Document doc) {
        return doc.select(".b-comments-profile");
    }

    private Element element;

    private ProfileCommentParser(Element element) {
        this.element = element;
    }

    private ProfileComment parseProfileComment() {
        try {
            return new ProfileComment(selectStoryId(),
                    selectStoryTitle(),
                    selectUserComment(),
                    selectParentUserComment());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String selectStoryTitle() throws ParserException {
        String title = element.select(".b-comments-profile__title a").text();
        if (title.isEmpty()) {
            throw new ParserException("story title is empty " + element);
        }
        return title;
    }

    private int selectStoryId() throws ParserException {
        String storyIdStr = element.attr("data-story-id");
        int storyId = TextUtils.getNumbersFromString(storyIdStr);
        if (storyId < 0) {
            throw new ParserException("storyId <= 0 " + storyId);
        }
        return storyId;
    }

    private UserComment selectUserComment() throws ParserException {
        Element userElement = element.select(".b-comments-profile__main").first();
        UserCommentParser parser = new UserCommentParser(userElement);
        return parser.parseUserComment();
    }

    private UserComment selectParentUserComment() throws ParserException {
        Element userElement = element.select(".b-comments-profile__parent").first();
        if (userElement == null) {
            return null;
        }
        UserCommentParser parser = new UserCommentParser(userElement);
        return parser.parseUserComment();
    }

    private class UserCommentParser {
        private Element element;

        public UserCommentParser(Element element) {
            this.element = element;
        }

        private UserComment parseUserComment() throws ParserException {
            return new UserComment(selectCommentId(),
                    selectAuthor(),
                    selectRating(),
                    selectVoteStatus(),
                    selectTime(),
                    selectContentBlocks());
        }

        private ArrayList<CommentContentBlock> selectContentBlocks() throws ParserException {
            String commentBody = element.select(".b-comment__content").toString();
            CommentBodyParser parser = new HtmlCommentBodyParser();
            return parser.parseCommentBody(commentBody);
        }

        private Vote selectVoteStatus() {
            Element elRating = element.select(".b-comment__rating").first();
            String voteUpStr = elRating.select(".i-sprite--comments__rating-up-active").toString();
            if (!voteUpStr.isEmpty()) {
                return Vote.UP;
            }
            String voteDownStr = elRating.select(".i-sprite--comments__rating-down-active").toString();
            if (!voteDownStr.isEmpty()) {
                return Vote.DOWN;
            }
            return Vote.NOTHING;
        }

        private long selectTime() throws ParserException {
            String timeStr = element.select(".b-comment__time").attr("datetime");
            long time = Long.parseLong(timeStr);
            if (time < 0) {
                throw new ParserException("time <= 0 " + time);
            }
            return time;
        }

        private int selectRating() {
            String rating = element.select(".b-comment__rating-count").text();
            if (rating.isEmpty()) {
                return UserComment.HIDE_RATING;
            }
            return TextUtils.getNumbersFromString(rating);
        }

        private int selectCommentId() throws ParserException {
            String commentIdStr = element.select(".b-comment").attr("data-id");
            int commentId = TextUtils.getNumbersFromString(commentIdStr);
            if (commentId < 0) {
                throw new ParserException("commentId <= 0 " + commentId);
            }
            return commentId;
        }

        private String selectAuthor() throws ParserException {
            String authorLink = element.select(".b-comment__user a").attr("href");
            String author = TextUtils.getUrlLastSegment(authorLink);
            if (TextUtils.isEmpty(author)) {
                throw new ParserException("author name is empty " + element);
            }
            return author;
        }
    }
}
