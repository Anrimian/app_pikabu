package pikabu.content.comments.profile.fragment;

import android.database.Observable;

/**
 * Created on 14.11.2016.
 */

public class ListStateObservable extends Observable<ListStateObserver> {

    public void showState(ListState listState) {
        for(ListStateObserver observer : mObservers) {
            listState.showState(observer);
        }
    }

    public void updateList(boolean refresh) {
        for(ListStateObserver observer : mObservers) {
            observer.updateList(refresh);
        }
    }
}
