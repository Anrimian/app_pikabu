package pikabu.content.comments.profile.fragment;

/**
 * Created on 18.11.2016.
 */

public interface ProfileCommentsListActions {

    void onErrorClick();

    void onRefresh();
}
