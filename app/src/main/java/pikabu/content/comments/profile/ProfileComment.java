package pikabu.content.comments.profile;

import java.io.Serializable;

/**
 * Created on 24.08.2016.
 */
public class ProfileComment implements Serializable {

    private String storyTitle;
    private int storyId;

    private UserComment parentComment;
    private UserComment comment;

    private boolean showParentComment = false;

    public ProfileComment(int storyId, String storyTitle, UserComment comment, UserComment parentComment) {
        this.storyId = storyId;
        this.storyTitle = storyTitle;
        this.comment = comment;
        this.parentComment = parentComment;
    }

    public UserComment getComment() {
        return comment;
    }

    public UserComment getParentComment() {
        return parentComment;
    }

    public int getStoryId() {
        return storyId;
    }

    public String getStoryTitle() {
        return storyTitle;
    }

    public boolean isShowParentComment() {
        return showParentComment;
    }

    public void showParentComment(boolean showParentComment) {
        this.showParentComment = showParentComment;
    }
}
