package pikabu.content.comments.profile.fragment;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.SparseArray;

import com.emirinay.tools.ExceptionWithInteger;
import com.emirinay.tools.fragment.lists.ListPosition;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pikabu.AppExecutors;
import pikabu.content.Vote;
import pikabu.content.comments.profile.ProfileComment;
import pikabu.content.comments.profile.UserComment;
import pikabu.content.comments.profile.parser.ProfileCommentParser;
import pikabu.user.UserModel;
import pikabu.user.actions.UserActions;
import pikabu.user.actions.comments.UserCommentActionsObserver;
import pikabu.utils.network.LoaderHelper;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created on 11.11.2016.
 */

public class ProfileCommentsModel implements UserCommentActionsObserver, ArrowPositionModel, ListPositionModel {

    private static final int DEFAULT_PAGE_NUMBER = 1;

    private ListPosition arrowListPosition;
    private ListPosition listPosition;

    private ListState listState;
    private ListStateObservable listStateObservable = new ListStateObservable();
    private boolean isRefreshing;
    private Subscription subscription;

    private String link;
    private int pageNumber = DEFAULT_PAGE_NUMBER;
    private int errorMessageId;

    private Map<String, ProfileComment> profileCommentsIdMap = new HashMap<>();
    private List<ProfileComment> comments = new ArrayList<>();
    private SparseArray<SparseArray<ArrayList<UserComment>>> storyIdMap = new SparseArray<>();

    public ProfileCommentsModel() {
        UserActions.getInstance().registerCommentActionsObserver(this);
    }

    public void setLink(String link) {
        if (!TextUtils.equals(this.link, link)) {
            deleteListData();
            updateList(false);
            this.link = link;
            startLoading(false);
        }
    }

    public void registerObserver(ListStateObserver observer) {
        listState.showState(observer);
        listStateObservable.registerObserver(observer);
    }

    public void unregisterObserver(ListStateObserver observer) {
        listStateObservable.unregisterObserver(observer);
    }

    public void startLoading(boolean refresh) {
        if (!TextUtils.isEmpty(link)) {
            cancelLoading();
            String pageLink = link;
            if (!refresh) {
                pageLink += pageNumber;
            }
            showState(ListState.PROGRESS);
            loadComments(pageLink, refresh);
        }
    }

    public void cancelLoading() {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    private void loadComments(String link, boolean refresh) {
        subscription = LoaderHelper.getDocumentObservable(link)
                .subscribeOn(AppExecutors.COMMENTS_SCHEDULER)
                .doOnNext(UserModel.getInstance().getUserInfoModel()::updateUserInfo)
                .flatMap(ProfileCommentParser::parseDocument)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(comments -> {
                    setListData(comments, refresh);
                    updateList(refresh);
                    isRefreshing = false;
                    if (comments.isEmpty()) {
                        showState(ListState.END);
                    } else if (pageNumber == DEFAULT_PAGE_NUMBER + 1) {
                        startLoading(false);
                    } else {
                        showState(ListState.WAIT);
                    }
                }, throwable -> {
                    errorMessageId = (((ExceptionWithInteger) throwable).getMessageId());
                    showState(ListState.ERROR);
                    isRefreshing = false;
                });
    }

    public void updateList(boolean refresh) {
        listStateObservable.updateList(refresh);
    }

    private void setListData(List<ProfileComment> comments, boolean refresh) {
        if (refresh && !comments.isEmpty()) {
            deleteListData();
        }
        for (ProfileComment comment : comments) {
            String key = getItemId(comment.getStoryId(), comment.getComment().getCommentId());
            if (!profileCommentsIdMap.containsKey(key)) {
                this.comments.add(comment);
                profileCommentsIdMap.put(key, comment);
                addCommentToVoteMap(comment);
            }
        }
        pageNumber++;
    }

    private void addCommentToVoteMap(ProfileComment profileComment) {
        int storyId = profileComment.getStoryId();
        SparseArray<ArrayList<UserComment>> commentsIdMap = storyIdMap.get(storyId);
        if (commentsIdMap == null) {
            commentsIdMap = new SparseArray<>();
            storyIdMap.put(storyId, commentsIdMap);
        }
        addCommentToVoteMap(profileComment.getComment(), commentsIdMap);
        addCommentToVoteMap(profileComment.getParentComment(), commentsIdMap);
    }

    private void addCommentToVoteMap(@Nullable UserComment comment, SparseArray<ArrayList<UserComment>> commentsIdMap) {
        if (comment != null) {
            int commentId = comment.getCommentId();
            ArrayList<UserComment> commentsList = commentsIdMap.get(commentId);
            if (commentsList == null) {
                commentsList = new ArrayList<>();
                commentsIdMap.put(commentId, commentsList);
            }
            commentsList.add(comment);
        }
    }

    private String getItemId(int storyId, int commentId) {
        StringBuilder sb = new StringBuilder();
        sb.append(storyId);
        sb.append(commentId);
        return sb.toString();
    }

    @Override
    public void commentWasVoted(int storyId, int commentId, Vote newVote) {
        SparseArray<ArrayList<UserComment>> commentsIdMap = storyIdMap.get(storyId);
        if (commentsIdMap != null) {
            ArrayList<UserComment> commentsList = commentsIdMap.get(commentId);
            if (commentsList != null) {
                for (UserComment comment : commentsList) {
                    Vote oldVote = comment.getVoteStatus();
                    Vote vote = Vote.mergeVote(oldVote, newVote);
                    comment.setVoteStatus(vote);
                    comment.setRating(comment.getRating() + newVote.toInt());
                    updateList(false);
                }
            }
        }
    }

    public void deleteListData() {
        comments.clear();
        profileCommentsIdMap.clear();
        storyIdMap.clear();
        pageNumber = DEFAULT_PAGE_NUMBER;
        listPosition = null;
        arrowListPosition = null;
    }

    void showState(ListState listState) {
        this.listState = listState;
        listStateObservable.showState(listState);
    }

    @Override
    public ListPosition getArrowListPosition() {
        return arrowListPosition;
    }

    @Override
    public void setArrowListPosition(ListPosition arrowListPosition) {
        this.arrowListPosition = arrowListPosition;
    }

    public int getErrorMessageId() {
        return errorMessageId;
    }

    public List<ProfileComment> getListData() {
        return comments;
    }

    public ListState getListState() {
        return listState;
    }

    public boolean isRefreshing() {
        return isRefreshing;
    }

    public void setRefreshing(boolean refreshing) {
        isRefreshing = refreshing;
    }

    @Override
    public void saveListPosition(ListPosition listPosition) {
        this.listPosition = listPosition;
    }

    @Override
    public ListPosition loadListPosition() {
        return listPosition;
    }
}
