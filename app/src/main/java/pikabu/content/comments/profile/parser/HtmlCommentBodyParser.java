package pikabu.content.comments.profile.parser;

import org.jsoup.nodes.Element;

import pikabu.content.ParserException;
import pikabu.content.comments.blocks.CommentContentBlock;
import pikabu.content.comments.blocks.GifContentBlock;
import pikabu.content.comments.blocks.parser.CommentBodyParser;

/**
 * Created on 24.11.2016.
 */

public class HtmlCommentBodyParser extends CommentBodyParser {

    @Override
    protected CommentContentBlock selectGifBlock(Element element, String imageLink) throws ParserException{
        String gifLink = selectGifLink(element);
        int width = selectWidth(element);
        int height = selectHeight(element);
        int gifSize = selectGifSize(element);
        return new GifContentBlock(gifLink, height, width, gifSize);
    }

    private String selectGifLink(Element element) throws ParserException {
        String link = element.select(".b-gifx__state").attr("href");
        if (link == null || link.isEmpty()) {
            throw new ParserException("fail parse comment gif link from: " + element);
        }
        return link;
    }

    private int selectWidth(Element element) {
        try {
            String width = element.select(".b-gifx.b-gifx_state_new").attr("data-width");
            return Integer.parseInt(width);
        } catch (NumberFormatException e) {
            return CommentContentBlock.DEFAULT_IMAGE_WIDTH;
        }
    }

    private int selectHeight(Element element) {
        try {
            String height = element.select(".b-gifx.b-gifx_state_new").attr("data-height");
            return Integer.parseInt(height);
        } catch (NumberFormatException e) {
            return CommentContentBlock.DEFAULT_IMAGE_HEIGHT;
        }
    }

    private int selectGifSize(Element element) {
        try {
            String height = element.select(".b-gifx.b-gifx_state_new").attr("data-size-gif");
            return Integer.parseInt(height);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
