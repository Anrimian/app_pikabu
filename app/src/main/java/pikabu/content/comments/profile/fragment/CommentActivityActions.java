package pikabu.content.comments.profile.fragment;

/**
 * Created on 12.11.2016.
 */

public interface CommentActivityActions {

    void replyComment(int storyId, int commentId);

    void updateList();
}
