package pikabu.content.comments.profile.fragment;

import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.endlesslist.EndlessListCallback;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;

import pikabu.content.comments.story.reply.ReplyDialog;
import pikabu.main.DrawerActivity;

/**
 * Created on 14.11.2016.
 */

public class ProfileCommentsListController implements ListStateObserver,
        CommentActivityActions, ProfileCommentsListActions, EndlessListCallback, ArrowControllerInfo {

    private FragmentActivity activity;
    private ProfileCommentsAdapter adapter;
    private ProfileCommentsView profileCommentsView;

    private ProfileCommentsModel model;

    public ProfileCommentsListController(FragmentActivity activity, ProfileCommentsView view) {
        this.activity = activity;
        this.profileCommentsView = view;
    }

    public void bind(ProfileCommentsModel model, String link, ProfileCommentsAdapter adapter) {
        this.model = model;
        this.adapter = adapter;
        model.setLink(link);
        profileCommentsView.bindView(adapter, this, this);
        model.registerObserver(this);
    }

    public void unbind() {
        model.unregisterObserver(this);
    }

    @Override
    public void onProgress() {
        profileCommentsView.setUpProgressStateView(isListEmpty() && !isHeaderVisible());
        boolean blockSwipeRefreshLayout = isListEmpty() || isRefreshing();
        profileCommentsView.showProgress(blockSwipeRefreshLayout, isRefreshing());
    }

    @Override
    public void onWait() {
        profileCommentsView.setUpProgressStateView(isListEmpty() && !isHeaderVisible());
        profileCommentsView.showWait();
    }

    @Override
    public void onError() {
        profileCommentsView.setUpProgressStateView(isListEmpty() && !isHeaderVisible());
        int messageId = model.getErrorMessageId();
        if (isRefreshing() && !isListEmpty()) {
            Toast.makeText(activity, messageId, Toast.LENGTH_SHORT).show();
            model.showState(ListState.WAIT);
            return;
        }
        profileCommentsView.showError(messageId);
    }

    @Override
    public void onEnd() {
        int messageId = getEndMessageId();
        if (isListEmpty()) {
            messageId = getEmptyMessageId();
        }
        profileCommentsView.setUpProgressStateView(isListEmpty() && !isHeaderVisible());
        profileCommentsView.showEnd(messageId);
    }

    @Override
    public void updateList(boolean refresh) {
        adapter.notifyDataSetChanged();
        activity.invalidateOptionsMenu();
    }

    protected boolean isHeaderVisible() {
        return false;
    }

    public boolean isListEmpty() {
        return adapter.getCount() == 0;
    }

    @Override
    public void replyComment(int storyId, int commentId) {
        ReplyDialog.start(activity, storyId, commentId);
    }

    @Override
    public void updateList() {
        updateList(false);
    }

    @Override
    public void onErrorClick() {
        startLoading(false);
    }

    @Override
    public void onRefresh() {
        model.setRefreshing(true);
        startLoading(true);
    }

    @Override
    public boolean canLoad() {
        return model.getListState() == ListState.WAIT;
    }

    @Override
    public void loadNextData() {
        startLoading(false);
    }

    protected int getEmptyMessageId() {
        return R.string.no_comments;
    }

    protected int getEndMessageId() {
        return R.string.no_more_comments;
    }

    @Override
    public boolean showArrow() {
        return !isListEmpty() && !((DrawerActivity) activity).isDrawerOpen();
    }

    public ProfileCommentsAdapter getAdapter() {
        return adapter;
    }

    public void startLoading(boolean refresh) {
        model.startLoading(refresh);
    }

    public boolean isRefreshing() {
        return model.isRefreshing();
    }

    public ProfileCommentsModel getModel() {
        return model;
    }
}
