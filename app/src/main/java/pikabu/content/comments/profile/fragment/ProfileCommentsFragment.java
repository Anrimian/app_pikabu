package pikabu.content.comments.profile.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

/**
 * Created on 14.11.2016.
 */

public final class ProfileCommentsFragment extends UpArrowRecyclerViewFragmentV2 {

    private static final String LINK = "link";
    private static final String TITLE_ID = "title_id";

    public static ProfileCommentsFragment newInstance(String link) {
        return newInstance(link, 0);
    }

    public static ProfileCommentsFragment newInstance(String link, int titleId) {
        ProfileCommentsFragment fragment = new ProfileCommentsFragment();
        Bundle arguments = new Bundle();
        arguments.putString(LINK, link);
        arguments.putInt(TITLE_ID, titleId);
        fragment.setArguments(arguments);
        return fragment;
    }
    private ProfileCommentsView profileCommentsView;

    private ProfileCommentsListController listController;

    private ProfileCommentsModel model;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int titleId = getArguments().getInt(TITLE_ID);
        if (titleId != 0) {
            getActivity().setTitle(titleId);
        }
        profileCommentsView = new ProfileCommentsView(view);
        listController = new ProfileCommentsListController(getActivity(), profileCommentsView);
        model = ProfileCommentsModelContainer.getInstance(getModelTag());
        ProfileCommentsAdapter adapter = new ProfileCommentsAdapter(model.getListData(), listController);
        listController.bind(model, getLink(), adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listController.unbind();
    }

    private String getLink() {
        return getArguments().getString(LINK);
    }

    private String getModelTag() {
        return getLink();
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return listController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return profileCommentsView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return model;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return model;
    }
}
