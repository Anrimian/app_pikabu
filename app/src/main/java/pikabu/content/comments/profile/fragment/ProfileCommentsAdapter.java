package pikabu.content.comments.profile.fragment;

import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.adapter.HeaderFooterRecyclerViewAdapter;

import java.util.List;

import pikabu.content.comments.profile.ProfileComment;
import pikabu.content.comments.profile.ProfileCommentViewHolder;

/**
 * Created on 18.11.2016.
 */

public class ProfileCommentsAdapter extends HeaderFooterRecyclerViewAdapter<ProfileCommentViewHolder> {

    private List<ProfileComment> listData;

    private CommentActivityActions activityActions;

    public ProfileCommentsAdapter(List<ProfileComment> listData, CommentActivityActions activityActions) {
        this.listData = listData;
        this.activityActions = activityActions;
    }

    @Override
    public ProfileCommentViewHolder createVH(ViewGroup viewGroup, int type) {
        View itemView = View.inflate(viewGroup.getContext(), R.layout.two_comments_view, null);
        return new ProfileCommentViewHolder(itemView);
    }

    @Override
    public void bindVH(ProfileCommentViewHolder viewHolder, int position) {
        viewHolder.bindComment(listData.get(position), activityActions);
    }

    @Override
    public int getCount() {
        return listData.size();
    }
}
