package pikabu.content.comments.profile;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;

import pikabu.content.comments.profile.fragment.CommentActivityActions;
import pikabu.content.comments.story.commentslist.fullstory.FullStoryActivity;
import pikabu.utils.network.UrlUtils;

/**
 * Created on 12.11.2016.
 */

public class ProfileCommentViewHolder extends RecyclerView.ViewHolder implements TwoCommentsActions {

    private TextView tvTitle;

    private View commentView;
    private View parentCommentView;
    private View dataLevelColorView;
    private View borderLine;
    private View clickableView;

    private UserCommentViewHolder commentViewHolder;
    private UserCommentViewHolder parentCommentViewHolder;

    private ProfileComment profileComment;
    private CommentActivityActions commentActions;

    public ProfileCommentViewHolder(View view) {
        super(view);
        commentView = view.findViewById(R.id.comment_view);
        parentCommentView = view.findViewById(R.id.parent_comment_view);
        dataLevelColorView = view.findViewById(R.id.data_level_color_view);
        borderLine = view.findViewById(R.id.border_line);
        clickableView = view.findViewById(R.id.clickable_view);
        clickableView.setOnClickListener(v -> goToStory());
        tvTitle = (TextView) view.findViewById(R.id.tv_title);

        commentViewHolder = new UserCommentViewHolder(commentView);
        parentCommentViewHolder = new UserCommentViewHolder(parentCommentView);
    }

    public void bindComment(ProfileComment profileComment, CommentActivityActions commentActions) {
        this.profileComment = profileComment;
        this.commentActions = commentActions;

        bindStoryTitle();
        bindTargetComment();
        bindParentComment();
    }

    private void bindStoryTitle() {
        tvTitle.setText(profileComment.getStoryTitle());
    }

    private void bindTargetComment() {
        commentViewHolder.bindUserComment(profileComment.getComment(), profileComment.getStoryId(), commentActions);
        commentViewHolder.bindAsTargetComment(profileComment.getParentComment() == null || profileComment.isShowParentComment(), this);
    }

    private void bindParentComment() {
        UserComment parentComment = profileComment.getParentComment();
        boolean hasParentComment = parentComment != null;
        if (hasParentComment && profileComment.isShowParentComment()) {
            parentCommentView.setVisibility(View.VISIBLE);
            dataLevelColorView.setVisibility(View.VISIBLE);
            borderLine.setVisibility(View.VISIBLE);
        } else {
            parentCommentView.setVisibility(View.GONE);
            dataLevelColorView.setVisibility(View.GONE);
            borderLine.setVisibility(View.GONE);
        }

        if (hasParentComment) {
            parentCommentViewHolder.bindUserComment(parentComment, profileComment.getStoryId(), commentActions);
            parentCommentViewHolder.bindAsParentComment(this);
        }
    }

    @Override
    public void hideParent() {
        profileComment.showParentComment(false);
        commentActions.updateList();
    }

    @Override
    public void showParent() {
        profileComment.showParentComment(true);
        commentActions.updateList();
    }

    private void goToStory() {
        Intent intent = new Intent(getContext(), FullStoryActivity.class);
        String link = UrlUtils.getTargetCommentLink(profileComment.getStoryId(), profileComment.getComment().getCommentId());
        intent.putExtra(FullStoryActivity.STORY_LINK, link);
        getContext().startActivity(intent);
    }

    private Context getContext() {
        return clickableView.getContext();
    }
}
