package pikabu.content.comments.profile;

/**
 * Created on 12.11.2016.
 */

public interface TwoCommentsActions {
    void showParent();

    void hideParent();
}
