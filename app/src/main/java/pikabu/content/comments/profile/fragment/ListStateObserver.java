package pikabu.content.comments.profile.fragment;

/**
 * Created on 14.11.2016.
 */

public interface ListStateObserver {
    void onProgress();

    void onError();

    void onWait();

    void onEnd();

    void updateList(boolean refresh);


}
