package pikabu.content.comments.profile.fragment;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.endlesslist.EndlessListCallback;
import com.emirinay.tools.fragment.lists.recyclerview.endlesslist.EndlessListScrollListener;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;

import pikabu.views.ProgressViewBinder;

/**
 * Created on 14.11.2016.
 */

public class ProfileCommentsView implements RecyclerViewInfo {

    private View emptyProgressView;
    private View progressStateView;
    private ProgressViewBinder currentProgressViewBinder;
    private ProgressViewBinder progressViewBinder;
    private ProgressViewBinder emptyListProgressViewBinder;

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    private SwipeRefreshLayout swipeRefreshLayout;

    private ProfileCommentsListActions profileCommentsListActions;

    public ProfileCommentsView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        emptyProgressView = view.findViewById(R.id.empty_list_progress_state_view);
        emptyListProgressViewBinder = new ProgressViewBinder(emptyProgressView);
        progressStateView = View.inflate(getContext(), R.layout.progress_state_view, null);
        progressViewBinder = new ProgressViewBinder(progressStateView);
        View.OnClickListener clickListener = new ErrorOnClickListener();
        emptyListProgressViewBinder.setTryAgainButtonOnClickListener(clickListener);
        progressViewBinder.setTryAgainButtonOnClickListener(clickListener);


        swipeRefreshLayout = ((SwipeRefreshLayout) view.findViewById(R.id.ptr_layout));
        swipeRefreshLayout.setOnRefreshListener(new RefreshListener());
        swipeRefreshLayout.setColorSchemeResources(R.color.green);
    }

    public void bindView(ProfileCommentsAdapter adapter,
                         EndlessListCallback endlessListCallback,
                         ProfileCommentsListActions profileCommentsListActions) {
        this.profileCommentsListActions = profileCommentsListActions;
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new EndlessListScrollListener(linearLayoutManager, endlessListCallback));
        adapter.addFooter(progressStateView);
    }

    public void showProgress(boolean blockSwipeRefreshLayout, boolean refreshing) {
        currentProgressViewBinder.showProgress();
        if (blockSwipeRefreshLayout) {
            swipeRefreshLayout.setEnabled(false);
        }
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(refreshing));
    }

    public void showError(int messageId) {
        enableSwipeRefreshLayout();
        currentProgressViewBinder.showMessage(messageId, true);
    }

    public void showEnd(int messageId) {
        enableSwipeRefreshLayout();
        currentProgressViewBinder.showMessage(messageId, false);
    }

    public void showWait() {
        enableSwipeRefreshLayout();
    }

    private void enableSwipeRefreshLayout() {
        swipeRefreshLayout.setEnabled(true);
        swipeRefreshLayout.setRefreshing(false);
    }

    void setUpProgressStateView(boolean showEmptyView) {
        if (showEmptyView) {
            progressStateView.setVisibility(View.INVISIBLE);
            emptyProgressView.setVisibility(View.VISIBLE);
            currentProgressViewBinder = emptyListProgressViewBinder;
        } else {
            emptyProgressView.setVisibility(View.INVISIBLE);
            progressStateView.setVisibility(View.VISIBLE);
            currentProgressViewBinder = progressViewBinder;
        }
    }

    @Override
    public LinearLayoutManager getLinearLayoutManager() {
        return linearLayoutManager;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    private Context getContext() {
        return recyclerView.getContext();
    }

    private class ErrorOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            profileCommentsListActions.onErrorClick();
        }
    }

    private class RefreshListener implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            profileCommentsListActions.onRefresh();
        }
    }

}