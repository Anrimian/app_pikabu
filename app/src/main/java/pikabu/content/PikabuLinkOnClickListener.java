package pikabu.content;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.emirinay.tools.TextUtils;

import pikabu.content.community.view.CommunityActivity;
import pikabu.content.profile.view.ProfileActivity;
import pikabu.content.comments.story.commentslist.fullstory.FullStoryActivity;
import pikabu.data.Links;
import pikabu.image.gif.activity.GifControlActivity;
import pikabu.image.zoom.ZoomImageActivity;
import pikabu.utils.text.HtmlTextView;
import pikabu.utils.network.UrlUtils;

/**
 * Created on 21.09.2015.
 */
public class PikabuLinkOnClickListener implements HtmlTextView.LinkOnClickListener {

    @Override
    public void onLinkClick(View view, String url) {
        Context ctx = view.getContext();
        if (url.contains("pikabu.ru/story")) {
            goToStory(ctx, url);
            return;
        }

        if (url.contains("pikabu.ru/profile")) {
            goToProfile(ctx, url);
            return;
        }

        if (url.contains("pikabu.ru/community")) {
            goToCommunity(ctx, url);
            return;
        }

        if (url.contains("pikabu.ru/images") || url.contains("pikabu.ru/post_img")) {
            openImageViewer(ctx, url);
            return;
        }

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        ctx.startActivity(browserIntent);
    }

    protected void goToCommunity(Context ctx, String url) {
        CommunityActivity.start(ctx, UrlUtils.getCommunityPathName(url));
    }

    protected void goToStory(Context ctx, String url) {
        Intent intent = new Intent(ctx, FullStoryActivity.class);
        String link = Links.LINK_SITE + "/story/" + TextUtils.getUrlLastSegment(url);
        intent.putExtra(FullStoryActivity.STORY_LINK, link);
        ctx.startActivity(intent);
    }

    protected void goToProfile(Context ctx, String url) {
        Intent intent = new Intent(ctx, ProfileActivity.class);
        String name = TextUtils.getUrlLastSegment(url);
        intent.putExtra(ProfileActivity.PROFILE_NAME, name);
        ctx.startActivity(intent);
    }

    protected void openImageViewer(Context ctx, String url) {
        if (url.contains(".gif")) {
            Intent intent = new Intent(ctx, GifControlActivity.class);
            intent.putExtra(GifControlActivity.GIF_LINK, url);
            ctx.startActivity(intent);
        } else {
            Intent intent = new Intent(ctx, ZoomImageActivity.class);
            intent.putExtra(ZoomImageActivity.IMAGE_LINK, url);
            ctx.startActivity(intent);
        }
    }
}
