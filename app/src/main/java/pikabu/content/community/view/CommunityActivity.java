package pikabu.content.community.view;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;
import pikabu.utils.network.UrlUtils;

/**
 * Created on 15.07.2016.
 */
public class CommunityActivity extends DrawerActivity {

    private static final String PATH_NAME = "path_name";

    public static void start(Context ctx, String name) {
        Intent intent = new Intent(ctx, CommunityActivity.class);
        intent.putExtra(PATH_NAME, name);
        ctx.startActivity(intent);
    }

    @Override
    protected Fragment initFragment() {
        Intent intent = getIntent();
        String pathName = intent.getStringExtra(PATH_NAME);
        if (pathName == null) {
            String link = intent.getData().toString();
            pathName = UrlUtils.getCommunityPathName(link);
        }
        return CommunityFragment.newInstance(pathName);
    }
}
