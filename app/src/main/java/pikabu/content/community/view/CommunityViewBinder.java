package pikabu.content.community.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.emirinay.pikabuapp.R;
import com.emirinay.tools.TextUtils;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import pikabu.content.PikabuLinkOnClickListener;
import pikabu.content.community.Community;
import pikabu.content.profile.ProfileOnClickListener;
import pikabu.image.glide.image.MyGlideImageLoader;
import pikabu.image.zoom.ZoomImageOnClickListener;
import pikabu.utils.text.HtmlTextView;

/**
 * Created on 16.07.2016.
 */
public class CommunityViewBinder {
    private Community community;

    private ImageView ivCommunity;

    private TextView tvCommunityName;
    private TextView tvStoryCount;
    private TextView tvSubsCount;
    private TextView tvRulesTitle;
    private HtmlTextView tvDescription;
    private HtmlTextView tvRules;

    private ViewGroup ownerContainer;
    private ViewGroup moderatorsContainer;
    private ViewGroup authorsContainer;

    public CommunityViewBinder(View view) {
        ivCommunity = (ImageView) view.findViewById(R.id.iv_community);
        tvCommunityName = (TextView) view.findViewById(R.id.tv_community_name);
        tvStoryCount = (TextView) view.findViewById(R.id.tv_story_count);
        tvSubsCount = (TextView) view.findViewById(R.id.tv_subs_count);
        tvDescription = (HtmlTextView) view.findViewById(R.id.tv_description);
        tvDescription.setLinkOnClickListener(new PikabuLinkOnClickListener());
        tvRulesTitle = (TextView) view.findViewById(R.id.tv_rules_title);
        tvRules = (HtmlTextView) view.findViewById(R.id.tv_rules);
        tvRules.setLinkOnClickListener(new PikabuLinkOnClickListener());
        ownerContainer = (ViewGroup) view.findViewById(R.id.owner_container);
        moderatorsContainer = (ViewGroup) view.findViewById(R.id.moderators_container);
        authorsContainer = (ViewGroup) view.findViewById(R.id.authors_container);
    }

    public void bindView(Community community) {
        this.community = community;
        bindCommunityImage();
        bindCommunityName();
        bindStoryCount();
        bindSubsCount();
        bindDescription();
        bindRulesDescription();
        bindOwner();
        bindModerators();
        bindAuthors();
    }

    private void bindCommunityImage() {
        String imageLink = community.getBackgroundImageLink();
        ivCommunity.setOnClickListener(new ZoomImageOnClickListener(getContext(), imageLink));
        MyGlideImageLoader.displayImage(imageLink, ivCommunity);
    }

    private void bindCommunityName() {
        tvCommunityName.setText(community.getName());
    }

    private void bindStoryCount() {
        int storyNumber = community.getStoryNumber();
        StringBuilder sb = new StringBuilder(String.valueOf(storyNumber));
        sb.append(" ");
        sb.append(TextUtils.getNumEnding(storyNumber, getContext().getResources().getStringArray(R.array.posts)));
        tvStoryCount.setText(sb.toString());
    }

    private void bindSubsCount() {
        int subsNumber = community.getSubsNumber();
        StringBuilder sb = new StringBuilder(String.valueOf(subsNumber));
        sb.append(" ");
        sb.append(TextUtils.getNumEnding(subsNumber, getContext().getResources().getStringArray(R.array.subs)));
        tvSubsCount.setText(sb.toString());
    }

    private void bindDescription() {
        String description = community.getDescription();
        if (TextUtils.isEmpty(description)) {
            tvDescription.setVisibility(View.GONE);
        } else {
            tvDescription.setHtmlText(description);
            tvDescription.setVisibility(View.VISIBLE);
        }
    }

    private void bindRulesDescription() {
        String rulesDescription = community.getRulesDescription();
        if (rulesDescription.isEmpty()) {
            tvRulesTitle.setVisibility(View.GONE);
            tvRules.setVisibility(View.GONE);
        } else {
            tvRulesTitle.setVisibility(View.VISIBLE);
            tvRules.setVisibility(View.VISIBLE);
            tvRules.setHtmlText(rulesDescription);
        }
    }

    private void bindOwner() {
        ownerContainer.removeViews(1, ownerContainer.getChildCount() - 1);
        View ownerView = newProfileView(community.getCommunityOwner());
        ownerContainer.addView(ownerView);
    }

    private void bindModerators() {
        ArrayList<Community.ProfileInfo> moderators = community.getModerators();
        if (moderators.isEmpty()) {
            moderatorsContainer.setVisibility(View.GONE);
        } else {
            moderatorsContainer.setVisibility(View.VISIBLE);
            moderatorsContainer.removeViews(1, moderatorsContainer.getChildCount() - 1);
            for (Community.ProfileInfo moderator : moderators) {
                View view = newProfileView(moderator);
                moderatorsContainer.addView(view);
            }
        }
    }

    private void bindAuthors() {
        ArrayList<Community.ProfileInfo> authors = community.getPopularAuthors();
        if (authors.isEmpty()) {
            authorsContainer.setVisibility(View.GONE);
        } else {
            authorsContainer.setVisibility(View.VISIBLE);
            authorsContainer.removeViews(1, authorsContainer.getChildCount() - 1);
            for (Community.ProfileInfo author : authors) {
                View view = newProfileView(author);
                authorsContainer.addView(view);
            }
        }
    }

    private View newProfileView(Community.ProfileInfo profileInfo) {
        View view = View.inflate(getContext(), R.layout.text_with_image, null);
        new ProfileInfoViewBinder(view).bindProfileInfo(profileInfo);
        return view;
    }

    private class ProfileInfoViewBinder {
        private View view;
        private ImageView imageView;
        private TextView textView;

        private Community.ProfileInfo profileInfo;

        private ProfileInfoViewBinder(View profileInfoView) {
            view = profileInfoView;
            imageView = (ImageView) profileInfoView.findViewById(R.id.image_view);
            textView = (TextView) profileInfoView.findViewById(R.id.text_view);
        }

        private void bindProfileInfo(Community.ProfileInfo profileInfo) {
            this.profileInfo = profileInfo;
            bindName();
            bindPicture();
            view.setOnClickListener(new ProfileOnClickListener(getContext(), profileInfo.getName()));
        }

        private void bindName() {
            textView.setText(profileInfo.getName());
        }

        private void bindPicture() {
            String imageLink = profileInfo.getPicture();
            Glide.with(getContext())
                    .load(imageLink)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .bitmapTransform(new RoundedCornersTransformation(getContext(), 4, 0))
                    .into(imageView);
        }
    }

    private Context getContext() {
        return tvCommunityName.getContext();
    }
}
