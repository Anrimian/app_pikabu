package pikabu.content.community.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created on 16.07.2016.
 */
public class CommunityModelContainer extends Fragment {

    private static final String COMMUNITY_MODEL_TAG = "community_model_tag";

    private CommunityModel model;

    public static CommunityModel getModel(Context ctx, String pathName) {
        FragmentManager fm = ((FragmentActivity) ctx).getSupportFragmentManager();
        CommunityModelContainer container = (CommunityModelContainer) fm
                .findFragmentByTag(COMMUNITY_MODEL_TAG);
        if (container == null) {
            container = new CommunityModelContainer();
            fm.beginTransaction().add(container, COMMUNITY_MODEL_TAG).commit();
        }
        if (container.model == null) {
            container.model = new CommunityModel(pathName);
        }
        return container.model;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
