package pikabu.content.community.view;

import pikabu.data.Links;

/**
 * Created on 16.07.2016.
 */
public class CommunityModel  {
    private int tabPosition;

    private final String pathName;

    public CommunityModel(String pathName) {
        this.pathName = pathName;
    }

    public int getTabPosition() {
        return tabPosition;
    }

    public void setTabPosition(int tabPosition) {
        this.tabPosition = tabPosition;
    }

    private String[] communityTabsArgs = {"", "/best", "/new"};

    public String getLink() {
        StringBuilder sb = new StringBuilder(Links.LINK_COMMUNITY);
        sb.append(pathName);
        sb.append(communityTabsArgs[tabPosition]);
        sb.append("?page=");
        return sb.toString();
    }
}
