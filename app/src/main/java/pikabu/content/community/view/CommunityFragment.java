package pikabu.content.community.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.AndroidUtils;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

import pikabu.content.community.parser.CommunityParser;
import pikabu.content.story.storylist.StoryListAdapter;
import pikabu.content.story.storylist.StoryListModel;
import pikabu.content.story.storylist.StoryListModelContainer;
import pikabu.content.story.storylist.StoryListView;
import pikabu.data.Links;
import pikabu.main.DrawerActivity;

/**
 * Created on 15.07.2016.
 */
public class CommunityFragment extends UpArrowRecyclerViewFragmentV2 {

    private static final String PATH_NAME = "path_name";

    public static CommunityFragment newInstance(String name) {
        CommunityFragment communityFragment = new CommunityFragment();
        Bundle arguments = new Bundle();
        arguments.putString(PATH_NAME, name);
        communityFragment.setArguments(arguments);
        return communityFragment;
    }

    private StoryListModel listModel;
    private StoryListView storyListView;
    private CommunityListController storyListController;

    private CommunityView communityView;
    private CommunityModel communityModel;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.community);

        storyListView = new StoryListView(view);
        View header = View.inflate(getContext(), R.layout.community_view, null);
        communityView = new CommunityView(header);

        communityModel = CommunityModelContainer.getModel(getContext(), getPathName());
        listModel = StoryListModelContainer.getInstance(null, getClearCommunityLink());
        listModel.setAdditionalDataParser(CommunityParser::getCommunityObservable);
        StoryListAdapter adapter = new StoryListAdapter(listModel.getStoryList(), getActivity());
        adapter.addHeader(header);
        storyListController = new CommunityListController(getActivity(), storyListView);
        storyListController.bindCommunity(communityView, communityModel);
        storyListController.bindList(communityModel.getLink(), listModel, adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        storyListController.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (!((DrawerActivity) getActivity()).isDrawerOpen()) {
            inflater.inflate(R.menu.community_view_actions_menu, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share_community_link: {
                AndroidUtils.shareText(getClearCommunityLink(), getContext());
                return true;
            }
            case R.id.copy_community_link: {
                AndroidUtils.copyText(getClearCommunityLink(), getContext());
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private String getClearCommunityLink() {
        StringBuilder sbLink = new StringBuilder(Links.LINK_COMMUNITY);
        sbLink.append(getPathName());
        return sbLink.toString();
    }

    private String getPathName() {
        return getArguments().getString(PATH_NAME);
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return storyListController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return storyListView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return listModel;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return listModel;
    }
}
