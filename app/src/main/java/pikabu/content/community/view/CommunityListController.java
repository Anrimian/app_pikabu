package pikabu.content.community.view;

import android.support.v4.app.FragmentActivity;

import pikabu.content.community.Community;
import pikabu.content.story.storylist.StoryListController;
import pikabu.content.story.storylist.StoryListView;
import rx.Observable;

/**
 * Created on 03.12.2016.
 */

public class CommunityListController extends StoryListController implements CommunityActions {

    private CommunityView communityView;
    private CommunityModel communityModel;

    public CommunityListController(FragmentActivity activity, StoryListView storyListView) {
        super(activity, storyListView);
    }

    public void bindCommunity(CommunityView communityView, CommunityModel communityModel) {
        this.communityView = communityView;
        this.communityModel = communityModel;
        communityView.bind(communityModel.getTabPosition(), this);
    }

    @Override
    public void showEndState() {
        showCommunity();
        super.showEndState();
    }

    @Override
    public void showErrorState() {
        showCommunity();
        super.showErrorState();
    }

    @Override
    public void showProgressState() {
        showCommunity();
        super.showProgressState();
    }

    @Override
    public void showWaitState() {
        showCommunity();
        super.showWaitState();
    }

    private void showCommunity() {
        Observable<Community> observable = getModel().getAdditionalData();
        if (observable != null) {
            observable.subscribe(communityView::showCommunity, throwable -> communityView.showError());
        } else {
            communityView.hideAll();
        }
    }

    @Override
    protected boolean isHeaderVisible() {
        return communityView.isCommunityViewVisible();
    }

    @Override
    public void onTabSelected(int position) {
        communityModel.setTabPosition(position);
        getModel().setLink(communityModel.getLink());
    }
}
