package pikabu.content.community.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;

import pikabu.content.community.Community;

/**
 * Created on 02.12.2016.
 */

public class CommunityView {

    private CommunityViewBinder communityViewBinder;

    private View communityView;
    private View errorView;
    private TabLayout tabLayout;

    private CommunityActions communityActions;

    public CommunityView(View view) {
        errorView = view.findViewById(R.id.error_view);
        communityView = view.findViewById(R.id.rl_community_view);
        communityViewBinder = new CommunityViewBinder(communityView);

        tabLayout = (TabLayout) communityView.findViewById(R.id.tab_layout);
        setUpTabLayout();
    }

    public void bind(int startPosition, CommunityActions communityActions) {
        this.communityActions = communityActions;
        tabLayout.getTabAt(startPosition).select();
    }

    public void showCommunity(Community community) {
        communityViewBinder.bindView(community);
        communityView.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
    }

    public void showError() {
        communityView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    public void hideAll() {
        communityView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
    }

    public boolean isCommunityViewVisible() {
        return communityView.getVisibility() == View.VISIBLE || errorView.getVisibility() == View.VISIBLE;
    }

    private Context getContext() {
        return errorView.getContext();
    }

    private void setUpTabLayout() {
        String[] tabTitles = getContext().getResources().getStringArray(R.array.community_tabs);
        for(String title : tabTitles) {
            TabLayout.Tab tab = tabLayout.newTab().setText(title);
            tabLayout.addTab(tab);
        }
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                TextView tabTextView = (TextView) tab.getCustomView();
                if (tabTextView == null) {
                    tabTextView = (TextView) View.inflate(getContext(), R.layout.tab_text_view, null);
                    tab.setCustomView(tabTextView);
                }
                tabTextView.setText(tab.getText());
                if (tab.isSelected()) {
                    tabTextView.setTypeface(null, Typeface.BOLD);
                }
            }
        }
        tabLayout.setOnTabSelectedListener(new OnCommunityTabSelectedListener());
    }

    private class OnCommunityTabSelectedListener implements TabLayout.OnTabSelectedListener {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            TextView text = (TextView) tab.getCustomView();
            text.setTypeface(null, Typeface.BOLD);
            communityActions.onTabSelected(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            TextView text = (TextView) tab.getCustomView();
            text.setTypeface(null, Typeface.NORMAL);
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    }
}
