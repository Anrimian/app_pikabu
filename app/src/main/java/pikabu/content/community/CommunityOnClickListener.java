package pikabu.content.community;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.View;

import com.emirinay.pikabuapp.R;

import pikabu.content.community.view.CommunityActivity;

/**
 * Created on 11.07.2016.
 */
public class CommunityOnClickListener implements View.OnClickListener {

    private String pathName;

    public CommunityOnClickListener(String pathName) {
        this.pathName = pathName;
    }

    @Override
    public void onClick(View view) {
        Context ctx = view.getContext();
        PopupMenu popup = new PopupMenu(ctx, view);
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.go_to_community:
                    CommunityActivity.start(ctx, pathName);
                    return true;
                default: return false;
            }
        });
        popup.inflate(R.menu.community_actions_menu);
        popup.show();
    }
}
