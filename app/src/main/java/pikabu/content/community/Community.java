package pikabu.content.community;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created on 24.06.2016.
 */
public class Community implements Serializable{

    private String pathName;
    private String name;
    private String backgroundImageLink;
    private String headerImageLink;
    private String description;
    private String rulesDescription;

    private ProfileInfo communityOwner;

    private int storyNumber;
    private int subsNumber;

    private ArrayList<ProfileInfo> moderators;
    private ArrayList<ProfileInfo> popularAuthors;

    public Community(String pathName,
                     String name,
                     String backgroundImageLink,
                     String headerImageLink,
                     int storyNumber,
                     int subsNumber,
                     String description,
                     String rulesDescription,
                     ProfileInfo communityOwner,
                     ArrayList<ProfileInfo> moderators,
                     ArrayList<ProfileInfo> popularAuthors) {
        this.pathName = pathName;
        this.name = name;
        this.backgroundImageLink = backgroundImageLink;
        this.headerImageLink = headerImageLink;
        this.storyNumber = storyNumber;
        this.subsNumber = subsNumber;
        this.description = description;
        this.rulesDescription = rulesDescription;
        this.communityOwner = communityOwner;
        this.moderators = moderators;
        this.popularAuthors = popularAuthors;
    }

    public String getPathName() {
        return pathName;
    }

    public String getName() {
        return name;
    }

    public String getBackgroundImageLink() {
        return backgroundImageLink;
    }

    public String getHeaderImageLink() {
        return headerImageLink;
    }

    public int getStoryNumber() {
        return storyNumber;
    }

    public int getSubsNumber() {
        return subsNumber;
    }

    public String getDescription() {
        return description;
    }

    public String getRulesDescription() {
        return rulesDescription;
    }

    public ProfileInfo getCommunityOwner() {
        return communityOwner;
    }

    public ArrayList<ProfileInfo> getModerators() {
        return moderators;
    }

    public ArrayList<ProfileInfo> getPopularAuthors() {
        return popularAuthors;
    }

    public static class ProfileInfo implements Serializable{
        private String name;
        private String picture;

        public ProfileInfo(String name, String picture) {
            this.name = name;
            this.picture = picture;
        }

        public String getName() {
            return name;
        }

        public String getPicture() {
            return picture;
        }

        @Override
        public String toString() {
            return "ProfileInfo{" +
                    "name='" + name + '\'' +
                    ", picture='" + picture + '\'' +
                    '}';
        }
    }
}
