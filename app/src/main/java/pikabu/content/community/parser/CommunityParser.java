package pikabu.content.community.parser;

import com.emirinay.tools.TextUtils;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import pikabu.content.ParserException;
import pikabu.content.community.Community;
import rx.Observable;

/**
 * Created on 24.06.2016.
 */
public class CommunityParser {

    public static Observable<Community> getCommunityObservable(Document doc) {
        Community community = getCommunityInstance(doc);
        return Observable.create(subscriber -> {
            if (community == null) {
                subscriber.onError(null);
            } else {
                subscriber.onNext(community);
            }
        });
    }

    public static Community getCommunityInstance(Document document) {
        return new CommunityParser(document).parse();
    }

    private Document document;

    private CommunityParser(Document document) {
        this.document = document;
    }

    private Community parse(){
        Community community;
        try {
            community = new Community(selectPathName(),
                    selectName(),
                    selectBackgroundImageLink(),
                    selectHeaderImageLink(),
                    selectStoryNumber(),
                    selectSubsNumber(),
                    selectDescription(),
                    selectRulesDescription(),
                    selectCommunityOwner(),
                    selectModerators(),
                    selectPopularAuthors());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return community;
    }

    private ArrayList<Community.ProfileInfo> selectPopularAuthors() {
        ArrayList<Community.ProfileInfo> authors = new ArrayList<>();
        Elements elements = document.select(".community-owner")
                .last()
                .select(".community-user.community-moderator");
        for (Element element : elements) {
            String link = element.attr("href");
            String name = TextUtils.getUrlLastSegment(link);
            String picture = element.select("img").attr("src");
            authors.add(new Community.ProfileInfo(name, picture));
        }
        return authors;
    }

    private ArrayList<Community.ProfileInfo> selectModerators() {
        ArrayList<Community.ProfileInfo> moderators = new ArrayList<>();
        Elements elements = document.select(".community-owner")
                .first()
                .select(".community-user.community-moderator");
        for (Element element : elements) {
            String link = element.attr("href");
            String name = TextUtils.getUrlLastSegment(link);
            String picture = element.select("img").attr("src");
            moderators.add(new Community.ProfileInfo(name, picture));
        }
        return moderators;
    }

    private Community.ProfileInfo selectCommunityOwner() {
        Element owner = document.select(".community-owner")
                .first()
                .select(".community-user").first();
        String link = owner.attr("href");
        String name = TextUtils.getUrlLastSegment(link);
        String picture = owner.select("img").attr("src");
        return new Community.ProfileInfo(name, picture);
    }

    private String selectRulesDescription() {
        Elements elements = document.select(".community-rules");
        Elements trash = elements.select("section");
        trash.remove();
        return elements.toString();
    }

    private String selectDescription() {
        return document.select(".community-desc").toString();
    }

    private int selectSubsNumber() {
        String subsNumber = document.getElementsByAttributeValue("data-role", "subs_cnt").attr("data-value");
        return Integer.parseInt(subsNumber);
    }

    private int selectStoryNumber() {
        String subsNumber = document.getElementsByAttributeValue("data-role", "stories_cnt").attr("data-value");
        return Integer.parseInt(subsNumber);
    }

    private String selectHeaderImageLink() {
        String style = document.select(".community-header-bg").attr("style");
        return TextUtils.getStringBetween(style, "URL('", "')");
    }

    private String selectBackgroundImageLink() {
        return document.select(".community-bg img").attr("src");
    }

    private String selectName() throws ParserException {
        String name = document.select(".community-info div h1").text();
        if (name.isEmpty()) {
            throw new ParserException("community name is empty");
        }
        return name;
    }

    private String selectPathName() throws ParserException {
        String link = document.getElementsByAttributeValue("data-role", "link").attr("href");
        String pathName = TextUtils.getUrlLastSegment(link);
        if (pathName.isEmpty()) {
            throw new ParserException("pathName is empty");
        }
        return pathName;
    }
}
