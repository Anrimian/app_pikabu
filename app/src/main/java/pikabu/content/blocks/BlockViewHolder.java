package pikabu.content.blocks;

import android.content.Context;
import android.view.View;

/**
 * Created on 19.10.2016.
 */

public abstract class BlockViewHolder<T extends ContentBlock> {

    public BlockViewHolder(Context ctx) {
    }

    public void clearView() {

    }

    public abstract void bindView(T block, BindArgs bindArgs);

    public abstract View getView();


}
