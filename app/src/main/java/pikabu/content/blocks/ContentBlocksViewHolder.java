package pikabu.content.blocks;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Map;

import pikabu.content.story.blocks.ContentType;

/**
 * Created on 23.07.2016.
 */
public class ContentBlocksViewHolder {

    private Map<ContentType, SparseArray<BlockViewHolder>> viewListMap = new EnumMap<>(ContentType.class);

    private ViewGroup container;

    public ContentBlocksViewHolder(ViewGroup container) {
        this.container = container;
    }

    public void bindContentBlocks(ArrayList<? extends ContentBlock> contentBlocks, BindArgs bindArgs) {
        clearAllBlocks();

        Map<ContentType, Integer> typeBlockCount = new EnumMap<>(ContentType.class);
        for (ContentBlock block : contentBlocks) {
            ContentType contentType = block.getContentType();
            Integer typeBlockNumberObj = typeBlockCount.get(contentType);
            int typeBlockNumber = 0;
            if (typeBlockNumberObj != null) {
                typeBlockNumber = typeBlockNumberObj;
            }

            BlockViewHolder blockViewHolder = getContentBlockViewHolder(block, typeBlockNumber, container.getContext());
            blockViewHolder.bindView(block, bindArgs);
            container.addView(blockViewHolder.getView());

            typeBlockNumber++;
            typeBlockCount.put(contentType, typeBlockNumber);
        }
    }

    private BlockViewHolder getContentBlockViewHolder(ContentBlock block, int blockNumber, Context ctx) {
        SparseArray<BlockViewHolder> viewList = getViewList(block.getContentType());
        BlockViewHolder blockViewHolder = viewList.get(blockNumber);
        if (blockViewHolder == null) {
            blockViewHolder = block.createBlockViewHolder(ctx);
            viewList.put(blockNumber, blockViewHolder);
        }
        return blockViewHolder;
    }

    private SparseArray<BlockViewHolder> getViewList(ContentType contentType) {
        SparseArray<BlockViewHolder> viewList = viewListMap.get(contentType);
        if (viewList == null) {
            viewList = new SparseArray();
            viewListMap.put(contentType, viewList);
        }
        return viewList;
    }

    private void clearAllBlocks() {
        container.removeAllViews();
        for (SparseArray<BlockViewHolder> blocks : viewListMap.values()) {
            for (int i = 0; i < blocks.size(); i++) {
                int key = blocks.keyAt(i);
                BlockViewHolder block = blocks.get(key);
                block.clearView();
            }
        }
    }
}
