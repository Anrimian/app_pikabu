package pikabu.content.blocks;

import android.content.ContentValues;
import android.content.Context;

import java.io.Serializable;

import pikabu.content.story.blocks.ContentType;

/**
 * Created on 22.07.2016.
 */
public abstract class ContentBlock implements Serializable {

    public ContentBlock() {
    }

    public abstract BlockViewHolder createBlockViewHolder(Context ctx);

    public abstract ContentType getContentType();

    public ContentValues toContentValues() {
        return null;
    }
}
