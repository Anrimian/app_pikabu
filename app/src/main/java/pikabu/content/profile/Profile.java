package pikabu.content.profile;

import java.io.Serializable;
import java.util.ArrayList;

public class Profile implements Serializable {

	public static final int NO_RATING = Integer.MIN_VALUE;
    
	private String name;
	private Gender gender;
	private String approvedInfo;
	private String pictureLink;
	private String registerTime;
    private String userNote;

	private int rating;
	private int commentsCount;
	private int storyCount;
	private int hotStoryCount;
	private int plusesNumber;
	private int minusesNumber;

    private ArrayList<ProfileCommunityInfo> communityInfoList;
	private ArrayList<Award> awardsList;
	
	public Profile(String name,
				   Gender gender,
                   String approvedInfo,
			       String pictureLink,
			       String registerTime,
                   String userNote,
			       int rating,
			       int commentsCount,
			       int storyCount,
			       int hotStoryCount,
			       int plusesNumber,
			       int minusesNumber,
			       ArrayList<Award> awardsList,
                   ArrayList<ProfileCommunityInfo> communityInfoList) {
		this.name = name;
		this.gender = gender;
        this.approvedInfo = approvedInfo;
		this.pictureLink = pictureLink;
		this.registerTime = registerTime;
        this.userNote = userNote;
		this.rating = rating;
		this.commentsCount = commentsCount;
		this.storyCount = storyCount;
		this.hotStoryCount = hotStoryCount;
		this.plusesNumber = plusesNumber;
		this.minusesNumber = minusesNumber;
		this.awardsList = awardsList;
        this.communityInfoList = communityInfoList;
	}

	public String getName() {
		return name;
	}

	public String getPictureLink() {
		return pictureLink;
	}

	public String getRegisterTime() {
		return registerTime;
	}

	public int getRating() {
		return rating;
	}

	public int getCommentsCount() {
		return commentsCount;
	}

	public int getStoryCount() {
		return storyCount;
	}

	public int getHotStoryCount() {
		return hotStoryCount;
	}

	public int getPlusesNumber() {
		return plusesNumber;
	}

	public int getMinusesNumber() {
		return minusesNumber;
	}

	public ArrayList<Award> getAwardsList() {
		return awardsList;
	}	
	
	public Gender getGender() {
		return gender;
	}

    public String getApprovedInfo() {
        return approvedInfo;
    }

    public ArrayList<ProfileCommunityInfo> getCommunityInfoList() {
        return communityInfoList;
    }

    public String getUserNote() {
        return userNote;
    }

    public enum Gender {
		MALE,
		FEMALE,
		UNKNOWN
	}
	
}
