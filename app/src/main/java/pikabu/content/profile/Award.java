package pikabu.content.profile;

import java.io.Serializable;

public class Award implements Serializable{
	
	private String imageLink;
	private String title;
	private String storyLink;
	
	public Award(String imageLink, String title, String storyLink) {
		this.imageLink = imageLink;
		this.title = title;
		this.storyLink = storyLink;
	}

	public String getImageLink() {
		return imageLink;
	}

	public String getTitle() {
		return title;
	}

	public String getStoryLink() {
		return storyLink;
	}

	@Override
	public String toString() {		
		return " \nstoryLink: " + storyLink + "\nimageLink: " + imageLink + " \ntitle: " + title;
	}
}
