package pikabu.content.profile.parser;

public class ProfileParserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6204883504450170747L;

	public ProfileParserException() {
		super();		
	}

	public ProfileParserException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);		
	}

	public ProfileParserException(String detailMessage) {
		super(detailMessage);		
	}

	public ProfileParserException(Throwable throwable) {
		super(throwable);		
	}

	
	
}
