package pikabu.content.profile.parser;

import com.emirinay.tools.TextUtils;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import pikabu.content.profile.Award;
import pikabu.content.profile.Profile;
import pikabu.content.profile.ProfileCommunityInfo;
import rx.Observable;


public class ProfileParser {

	public static Observable<Profile> getProfileObservable(Document doc) {
        Profile profile = getProfileInstance(doc);
		return Observable.create(subscriber -> {
			if (profile == null) {
				subscriber.onError(null);
			} else {
				subscriber.onNext(profile);
			}
		});
	}

	/**
	 * Extract profile from html text
	 * @param doc, not null
	 * @return profile, can be null when nothing to extract or parser has errors
	 */
	public static Profile getProfileInstance(Document doc) {
		return new ProfileParser(doc).getProfile();
		
	}

	private Element htmlProfileInfo;

	private Element userProfileInfo;

	private String strProfileInfo;

	private Document document;

	protected ProfileParser(Document document) {
		this.document = document;
	}

    private Profile getProfile() {
        try {
			htmlProfileInfo = document.select(".b-user-profile").first();
			userProfileInfo = htmlProfileInfo.select("td").get(1).select("div").first();
			strProfileInfo = userProfileInfo.toString();
            return new Profile(selectName(),
                    selectGender(),
                    selectApprovedInfo(),
                    selectPictureLink(),
                    selectRegisterTime(),
                    selectUserNote(),
                    selectRating(),
                    selectComments(),
                    selectStoryCount(),
                    selectStoryHotCount(),
                    selectPlusesNumber(),
                    selectMinusesNumber(),
                    selectAwardsList(),
                    selectCommunityList());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private ArrayList<ProfileCommunityInfo> selectCommunityList() {
        ArrayList<ProfileCommunityInfo> communityList = new ArrayList<>();
        Elements elements = htmlProfileInfo.select(".b-user-profile__community");
        for(Element element : elements) {
            String name = element.select("span").text();
            String link = element.attr("href");
            String icon = element.select("img").attr("src");
            if (name.isEmpty() || link.isEmpty()) {
                continue;
            }
            link = TextUtils.getUrlLastSegment(link);
            ProfileCommunityInfo communityInfo = new ProfileCommunityInfo(name, link, icon);
            communityList.add(communityInfo);
        }
        return communityList;
    }

    private String selectUserNote() {
        return document.select("#usr-note-text").attr("value");
    }

    private String selectApprovedInfo() {
		String approvedInfo = userProfileInfo.select(".b-user-profile__approved").text();
		return TextUtils.getStringBetween(approvedInfo, "[ ", "]");
	}

	private String selectName() throws ProfileParserException {
		String authorLink = userProfileInfo.select("a").attr("href");		
		if (authorLink.isEmpty()) {
			throw new ProfileParserException("selected author link is empty");
		}
		String name = TextUtils.getUrlLastSegment(authorLink);
		if (name.isEmpty()) {
			throw new ProfileParserException("selected name is empty: " + authorLink);
		}		
		return name;
	}
	
	private Profile.Gender selectGender() {
		String genderImageLink = userProfileInfo.select("img").attr("src");
		if (genderImageLink.equals("http://cs.pikabu.ru/images/gender_new-female.png")) {			
			return Profile.Gender.FEMALE;
		}
		if (genderImageLink.equals("http://cs.pikabu.ru/images/gender_new.png")) {			
			return Profile.Gender.MALE;
		}
		return Profile.Gender.UNKNOWN;
	}
	
	private String selectPictureLink() {
		return htmlProfileInfo.select("td").get(0).select("div img").attr("src");
	}
	
	private String selectRegisterTime() throws ProfileParserException {			
		String time = htmlProfileInfo.select(".b-user-profile__value").get(0).text();
		if (time.isEmpty()) {
			throw new ProfileParserException("time is empty: " + strProfileInfo);
		}
		return time;
	}
	
	private int selectRating() throws ProfileParserException {
		String textRating = htmlProfileInfo.select(".b-user-profile__value").get(1).text();
		if (textRating.isEmpty()) {
			throw new ProfileParserException("textRating is empty");
		}
		int rating;
		try {
			rating = TextUtils.getNumbersFromString(textRating);
		} catch (NumberFormatException e) {
			rating = Profile.NO_RATING;
		}
		return rating;
	}
	
	private int selectComments() throws ProfileParserException, NumberFormatException {
        String textComments = htmlProfileInfo.select(".b-user-profile__value").get(2).text();
		if (textComments.isEmpty()) {
			throw new ProfileParserException("textComments is empty");
		}
		return TextUtils.getNumbersFromString(textComments);
	}
	
	private int selectStoryCount() throws ProfileParserException, NumberFormatException {
		String storyCount = htmlProfileInfo.select(".b-user-profile__value").get(3).text();
		if (storyCount.isEmpty()) {
			throw new ProfileParserException("storyCount is empty");
		}
		return TextUtils.getNumbersFromString(storyCount);
	}
	
	private int selectStoryHotCount() throws ProfileParserException, NumberFormatException {
		String storyHotCount = htmlProfileInfo.select(".b-user-profile__value").get(4).text();
		if (storyHotCount.isEmpty()) {
			throw new ProfileParserException("storyHotCount is empty");
		}
		return TextUtils.getNumbersFromString(storyHotCount);
	}
	
	private int selectMinusesNumber() throws ProfileParserException, NumberFormatException {
		String minusesNumberStr = TextUtils.getStringBetween(strProfileInfo, "down_profile.png\" class=\"vmiddle\" style=\"padding-bottom: 1px;\"> &nbsp;", "минус");
		if (minusesNumberStr.isEmpty()) {
			throw new ProfileParserException("minusesNumberStr is empty");
		}
		return TextUtils.getNumbersFromString(minusesNumberStr);
	}

	private int selectPlusesNumber() throws ProfileParserException, NumberFormatException {		
		String plusesNumberStr = TextUtils.getStringBetween(strProfileInfo, "rating_t_up_profile.png\" class=\"vmiddle\" style=\"padding-bottom: 1px;\"> &nbsp;", "плюс");
		if (plusesNumberStr.isEmpty()) {
			throw new ProfileParserException("plusesNumberStr is empty");
		}
		return TextUtils.getNumbersFromString(plusesNumberStr);
	}	
	
	private ArrayList<Award> selectAwardsList() {
		ArrayList<Award> awardsList = new ArrayList<>();
		Elements elAwards = htmlProfileInfo.select(".awards_wrap a");
        elAwards.remove();
		elAwards.addAll(htmlProfileInfo.select(".award_img"));
		for (Element elAward : elAwards) {
			String awardStoryLink = elAward.attr("href");
		    String awardImageLink = elAward.select("img").attr("src");
			String awardTitle = elAward.select("img").attr("title");
			
			if (awardImageLink.isEmpty() || awardTitle.isEmpty()) {
				continue;
			}
			Award award = new Award(awardImageLink, awardTitle+"", awardStoryLink);
            awardsList.add(award);
		}
		return awardsList;
	}
}
