package pikabu.content.profile;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;

import com.emirinay.pikabuapp.R;

import pikabu.content.profile.view.ProfileActivity;

/**
 * Created on 16.09.2015.
 */
public class ProfileOnClickListener implements View.OnClickListener {
    private Context ctx;

    private String name;

    public ProfileOnClickListener(Context ctx, String name) {
        this.ctx = ctx;
        this.name = name;
    }

    @Override
    public void onClick(View v) {
        PopupMenu popup = new PopupMenu(ctx, v);
        popup.setOnMenuItemClickListener(new ProfileMenuClickListener());
        popup.inflate(R.menu.profile_actions_menu);
        popup.show();
    }

    class ProfileMenuClickListener implements PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.go_to_profile:
                    Intent intent = new Intent(ctx, ProfileActivity.class);
                    intent.putExtra(ProfileActivity.PROFILE_NAME, name);
                    ctx.startActivity(intent);
                    return true;
                default:
                    return false;
            }
        }
    }
}
