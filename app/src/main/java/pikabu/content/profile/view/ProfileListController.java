package pikabu.content.profile.view;

import android.support.v4.app.FragmentActivity;

import pikabu.content.profile.Profile;
import pikabu.content.story.storylist.StoryListController;
import pikabu.content.story.storylist.StoryListView;
import rx.Observable;

/**
 * Created on 03.12.2016.
 */

public class ProfileListController extends StoryListController implements ProfileActions {

    private ProfileView profileView;
    private ProfileModel profileModel;

    public ProfileListController(FragmentActivity activity, StoryListView storyListView) {
        super(activity, storyListView);
    }

    public void bindProfile(ProfileView profileView, ProfileModel profileModel) {
        this.profileView = profileView;
        this.profileModel = profileModel;
        profileView.bind(profileModel.getTabPosition(), this);
    }

    @Override
    public void showEndState() {
        showProfile();//start before super method for correctly displaying empty view
        super.showEndState();
    }

    @Override
    public void showErrorState() {
        showProfile();
        super.showErrorState();
    }

    @Override
    public void showProgressState() {
        showProfile();
        super.showProgressState();
    }

    @Override
    public void showWaitState() {
        showProfile();
        super.showWaitState();
    }

    private void showProfile() {
        Observable<Profile> observable = getModel().getAdditionalData();
        if (observable != null) {
            observable.subscribe(profileView::showProfile, throwable -> profileView.showError());
        } else {
            profileView.hideAll();
        }
    }

    @Override
    protected boolean isHeaderVisible() {
        return profileView.isProfileViewVisible();
    }

    @Override
    public void onTabSelected(int position) {
        profileModel.setTabPosition(position);
        getModel().setLink(profileModel.getLink());
    }
}
