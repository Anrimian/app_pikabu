package pikabu.content.profile.view;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;

/**
 * Created on 15.09.2015.
 */
public class ProfileActivity extends DrawerActivity {

    public static final String PROFILE_NAME = "profile_name";

    public static void start(Context ctx, String userName) {
        Intent intent = new Intent(ctx, ProfileActivity.class);
        intent.putExtra(PROFILE_NAME, userName);
        ctx.startActivity(intent);
    }

    @Override
    protected Fragment initFragment() {
        Intent intent = getIntent();
        String name = intent.getStringExtra(PROFILE_NAME);
        if (name == null) {
            name = intent.getData().getLastPathSegment();
            if (name.contains("?")) {
                name = name.substring(0, name.indexOf("?"));
            }
        }
        return ProfileFragment.newInstance(name);
    }
}
