package pikabu.content.profile.view;

import pikabu.data.Links;

/**
 * Created on 16.07.2016.
 */
public class ProfileModel {
    private int tabPosition;

    private final String name;

    public ProfileModel(String name) {
        this.name = name;
    }

    public int getTabPosition() {
        return tabPosition;
    }

    public void setTabPosition(int tabPosition) {
        this.tabPosition = tabPosition;
    }

    private String[] profileListOrder = {"time", "rating", "my"};

    public String getLink() {
        StringBuilder sbLink = new StringBuilder(Links.LINK_PROFILE);
        sbLink.append("/");
        sbLink.append(name);
        sbLink.append("?f=");
        sbLink.append(profileListOrder[tabPosition]);
        sbLink.append("&page=");
        return sbLink.toString();
    }
}
