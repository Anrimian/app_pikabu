package pikabu.content.profile.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.emirinay.pikabuapp.R;
import com.emirinay.tools.TextUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import pikabu.content.community.CommunityOnClickListener;
import pikabu.content.profile.Award;
import pikabu.content.profile.Profile;
import pikabu.content.profile.ProfileCommunityInfo;
import pikabu.content.comments.story.commentslist.fullstory.FullStoryActivity;
import pikabu.image.glide.image.MyGlideImageLoader;
import pikabu.image.zoom.ZoomImageOnClickListener;

/**
 * Created on 26.06.2016.
 */
public class ProfileViewBinder {

    private ImageView profileImage;
    private ImageView ivGender;

    private TextView tvName;
    private TextView tvApprovedInfo;
    private TextView tvRegisterTime;
    private TextView tvRating;
    private TextView tvCommentsNumber;
    private TextView tvStoriesCount;
    private TextView tvVoteInfo;
    private TextView tvUserNote;

    private ViewGroup communitiesContainer;
    private ViewGroup vgAwards;

    private Profile profile;

    public ProfileViewBinder(View profileView) {
        profileImage = (ImageView) profileView.findViewById(R.id.iv_profile_picture);
        tvName = (TextView) profileView.findViewById(R.id.tv_profile_name);
        tvApprovedInfo = (TextView) profileView.findViewById(R.id.tv_user_approved_info);
        ivGender = (ImageView) profileView.findViewById(R.id.iv_profile_gender);
        tvRegisterTime = (TextView) profileView.findViewById(R.id.tv_profile_register);
        tvRating = (TextView) profileView.findViewById(R.id.tv_profile_rating);
        tvCommentsNumber = (TextView) profileView.findViewById(R.id.tv_profile_comments);
        tvStoriesCount = (TextView) profileView.findViewById(R.id.tv_story_count);
        tvVoteInfo = (TextView) profileView.findViewById(R.id.tv_vote_info);
        communitiesContainer = (ViewGroup) profileView.findViewById(R.id.communities_container);
        vgAwards = (ViewGroup) profileView.findViewById(R.id.fl_profile_awards_container);
        tvUserNote = (TextView) profileView.findViewById(R.id.tv_user_note);
    }

    public void bindProfile(@NotNull Profile profile) {
        this.profile = profile;
        showProfilePicture();
        showName();
        showApprovedInfo();
        showAuthorGender();
        showRegisterTime();
        showRating();
        showCommentsNumber();
        showStoriesCount();
        showVoteInfo();
        showCommunities();
        showAwards();
        showUserNote();
    }

    private Context getContext() {
        return tvName.getContext();
    }

    private void showProfilePicture() {
        String pictureLink = profile.getPictureLink();
        profileImage.setOnClickListener(new ZoomImageOnClickListener(getContext(), pictureLink));
        MyGlideImageLoader.displayImage(pictureLink, profileImage);
    }

    private void showName() {
        tvName.setText(profile.getName());
    }

    private void showAuthorGender() {
        switch (profile.getGender()) {
            case MALE: {
                ivGender.setImageResource(R.drawable.gender_male);
                ivGender.setContentDescription(getContext().getString(R.string.gender_male));
                break;
            }
            case FEMALE: {
                ivGender.setImageResource(R.drawable.gender_female);
                ivGender.setContentDescription(getContext().getString(R.string.gender_female));
                break;
            }
            default: {
                ivGender.setImageResource(0);
            }
        }
    }

    private void showApprovedInfo() {
        String approvedInfo = profile.getApprovedInfo();
        if (approvedInfo != null) {
            approvedInfo = getContext().getString(R.string.user_approved_info, approvedInfo);
            tvApprovedInfo.setText(approvedInfo);
            tvApprovedInfo.setVisibility(View.VISIBLE);
        } else {
            tvApprovedInfo.setVisibility(View.GONE);
        }

    }

    private void showRegisterTime() {
        int pikabuUser = R.string.pikabu_user_male;
        if (profile.getGender() == Profile.Gender.FEMALE) {
            pikabuUser = R.string.pikabu_user_female;
        }
        tvRegisterTime.setText(getContext().getString(pikabuUser, profile.getRegisterTime()));
    }

    private void showRating() {
        int rating = profile.getRating();
        String ratingStr = String.valueOf(rating);
        if (rating == Profile.NO_RATING) {
            ratingStr = "--";
        }
        tvRating.setText(getContext().getString(
                R.string.rating, ratingStr));
    }

    private void showCommentsNumber() {
        tvCommentsNumber.setText(getContext().getString(R.string.comments_number, profile.getCommentsCount()));
    }

    private void showStoriesCount() {
        Profile.Gender gender = profile.getGender();
        int addId = R.string.add_male;
        if (gender == Profile.Gender.FEMALE) {
            addId = R.string.add_female;
        }
        String add = getContext().getString(addId);
        int storiesCount = profile.getStoryCount();
        int hotStoriesCount = profile.getHotStoryCount();
        tvStoriesCount.setText(getContext().getString(R.string.story_info, add, storiesCount, hotStoriesCount));
    }

    private void showVoteInfo() {
        Profile.Gender gender = profile.getGender();
        int putId = R.string.put;
        if (gender == Profile.Gender.FEMALE) {
            putId = R.string.put_female;
        }
        String put = getContext().getString(putId);
        int plusesNumber = profile.getPlusesNumber();
        int minusesNumber = profile.getMinusesNumber();
        String[] plusesArray = getContext().getResources().getStringArray(R.array.pluses);
        String[] minusesArray = getContext().getResources().getStringArray(R.array.minuses);

        tvVoteInfo.setText(getContext().getString(R.string.vote_info,
                put,
                plusesNumber,
                TextUtils.getNumEnding(plusesNumber, plusesArray),
                minusesNumber,
                TextUtils.getNumEnding(minusesNumber, minusesArray)));
    }

    private void showCommunities() {
        ArrayList<ProfileCommunityInfo> communities = profile.getCommunityInfoList();
        if (communities.isEmpty()) {
            communitiesContainer.setVisibility(View.GONE);
        } else {
            communitiesContainer.setVisibility(View.VISIBLE);
            communitiesContainer.removeViews(1, communitiesContainer.getChildCount() - 1);
            for (ProfileCommunityInfo communityInfo : communities) {
                View view = newCommunityInfoView(communityInfo);
                communitiesContainer.addView(view);
            }
        }
    }

    private View newCommunityInfoView(ProfileCommunityInfo communityInfo) {
        View view = View.inflate(getContext(), R.layout.text_with_image, null);
        new ProfileCommunityViewBinder(view).bindCommunityInfo(communityInfo);
        return view;
    }

    private void showAwards() {
        vgAwards.removeAllViews();
        ArrayList<Award> awards = profile.getAwardsList();
        for (Award award : awards) {
            vgAwards.addView(newAwardView(award));
        }
    }

    private View newAwardView(Award award) {
        ImageView view = (ImageView) View.inflate(getContext(), R.layout.award_view, null);
        view.setOnClickListener(new AwardOnClickListener(award));
        view.setContentDescription(award.getTitle());
        MyGlideImageLoader.displayImage(award.getImageLink(), view);
        return view;
    }

    private class ProfileCommunityViewBinder {
        private View view;
        private ImageView imageView;
        private TextView textView;

        private ProfileCommunityInfo communityInfo;

        private ProfileCommunityViewBinder(View communityInfoView) {
            view = communityInfoView;
            imageView = (ImageView) communityInfoView.findViewById(R.id.image_view);
            textView = (TextView) communityInfoView.findViewById(R.id.text_view);
        }

        private void bindCommunityInfo(ProfileCommunityInfo communityInfo) {
            this.communityInfo = communityInfo;
            bindCommunityName();
            bindCommunityImage();
            view.setOnClickListener(new CommunityOnClickListener(communityInfo.getPathName()));
        }

        private void bindCommunityName() {
            textView.setText(communityInfo.getName());
        }

        private void bindCommunityImage() {
            String imageLink = communityInfo.getIcon();
            Glide.with(getContext())
                    .load(imageLink)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .bitmapTransform(new RoundedCornersTransformation(getContext(), 4, 0))
                    .into(imageView);
        }
    }

    private void showUserNote() {
        String userNote = profile.getUserNote();
        if (!userNote.isEmpty()) {
            tvUserNote.setVisibility(View.VISIBLE);
            userNote = getContext().getString(R.string.user_note, userNote);
            tvUserNote.setText(userNote);
        } else {
            tvUserNote.setVisibility(View.GONE);
        }

    }

    class AwardOnClickListener implements View.OnClickListener {
        private Award award;

        public AwardOnClickListener(Award award) {
            this.award = award;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.getMenu().add(award.getTitle()).setEnabled(false);
            popup.setOnMenuItemClickListener(new AwardMenuClickListener());
            String storyLink = award.getStoryLink();
            if (storyLink != null && !storyLink.isEmpty()) {
                popup.inflate(R.menu.award_actions_menu);
            }
            popup.show();
        }

        class AwardMenuClickListener implements PopupMenu.OnMenuItemClickListener {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.go_to_story:
                        Intent intent = new Intent(getContext(), FullStoryActivity.class);
                        intent.putExtra(FullStoryActivity.STORY_LINK, award.getStoryLink());
                        getContext().startActivity(intent);
                        return true;
                    default:
                        return false;
                }
            }
        }
    }


}
