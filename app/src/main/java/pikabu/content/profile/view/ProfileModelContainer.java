package pikabu.content.profile.view;

import android.support.v4.util.LruCache;

/**
 * Created on 03.07.2016.
 */
public class ProfileModelContainer {

    private static final int PROFILE_CACHE_SIZE = 5;

    private static LruCache<String, ProfileModel> profileCache = new LruCache<>(PROFILE_CACHE_SIZE);

    public static ProfileModel getInstance(String name) {
        ProfileModel model = profileCache.get(name);
        if (model == null) {
            model = new ProfileModel(name);
            profileCache.put(name, model);
        }
        return model;
    }
}
