package pikabu.content.profile.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.AndroidUtils;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

import pikabu.content.profile.parser.ProfileParser;
import pikabu.content.story.storylist.StoryListAdapter;
import pikabu.content.story.storylist.StoryListModel;
import pikabu.content.story.storylist.StoryListModelContainer;
import pikabu.content.story.storylist.StoryListView;
import pikabu.data.Links;
import pikabu.main.DrawerActivity;

import static com.emirinay.pikabuapp.R.string.profile;

/**
 * Created on 15.09.2015.
 */
public class ProfileFragment extends UpArrowRecyclerViewFragmentV2 {
    private static final String NAME = "name";

    public static ProfileFragment newInstance(String name) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle arguments = new Bundle();
        arguments.putString(NAME, name);
        fragment.setArguments(arguments);
        return fragment;
    }

    private StoryListModel listModel;
    private StoryListView storyListView;
    private ProfileListController storyListController;

    private ProfileView profileView;
    private ProfileModel profileModel;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(profile);

        storyListView = new StoryListView(view);
        View header = View.inflate(getContext(), R.layout.profile_view, null);
        profileView = new ProfileView(header);

        profileModel = ProfileModelContainer.getInstance(getName());
        listModel = StoryListModelContainer.getInstance(null, getClearProfileLink());
        listModel.setAdditionalDataParser(ProfileParser::getProfileObservable);
        StoryListAdapter adapter = new StoryListAdapter(listModel.getStoryList(), getActivity());
        adapter.addHeader(header);
        storyListController = new ProfileListController(getActivity(), storyListView);
        storyListController.bindProfile(profileView, profileModel);
        storyListController.bindList(profileModel.getLink(), listModel, adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        storyListController.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (!((DrawerActivity) getActivity()).isDrawerOpen()) {
            inflater.inflate(R.menu.profile_view_actions_menu, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share_profile_link: {
                AndroidUtils.shareText(getClearProfileLink(), getContext());
                return true;
            }
            case R.id.copy_profile_link: {
                AndroidUtils.copyText(getClearProfileLink(), getContext());
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private String getClearProfileLink() {
        StringBuilder sbLink = new StringBuilder(Links.LINK_PROFILE);
        sbLink.append("/");
        sbLink.append(getName());
        return sbLink.toString();
    }

    private String getName() {
        return getArguments().getString(NAME);
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return storyListController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return storyListView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return listModel;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return listModel;
    }
}
