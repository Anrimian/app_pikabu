package pikabu.content.profile;

import java.io.Serializable;

/**
 * Created on 08.06.2016.
 */
public class ProfileCommunityInfo implements Serializable{
    private String name;
    private String pathName;
    private String icon;

    public ProfileCommunityInfo(String name, String pathName, String icon) {
        this.name = name;
        this.pathName = pathName;
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public String getPathName() {
        return pathName;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "\n name: " + name + "\n pathName: " + pathName + " \n icon: " + icon;
    }
}
