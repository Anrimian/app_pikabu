package pikabu.content;

import org.jsoup.nodes.Document;

import rx.Observable;

/**
 * Created on 30.08.2015.
 */
public interface AdditionalDataParser<T> {

    Observable<T> parseData(Document doc);
}
