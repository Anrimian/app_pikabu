package pikabu.main;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;

/**
 * Created on 05.08.2016.
 */
public class ToolbarTitleBinder {
    private TextView tvTitle;
    private TextView tvSubtitle;

    private ImageView ivMenuDown;

    private ViewGroup titleContainer;

    private CharSequence title;
    private CharSequence subtitle;

    public ToolbarTitleBinder(ViewGroup toolbar) {
        tvTitle = (TextView) toolbar.findViewById(R.id.tv_title);
        tvSubtitle = (TextView) toolbar.findViewById(R.id.tv_subtitle);
        titleContainer = (ViewGroup) toolbar.findViewById(R.id.title_container);
        ivMenuDown = (ImageView) toolbar.findViewById(R.id.iv_menu_down);
        bind(null, null, null);
    }

    public void bindTitle(CharSequence title) {
        bind(title, null, null);
    }

    public void bind(CharSequence title, CharSequence subtitle, View.OnClickListener clickListener) {
        setTitle(title);
        setSubtitle(subtitle);
        setOnClickListener(clickListener);
    }

    public void setTitle(int resId) {
        String title = tvSubtitle.getContext().getString(resId);
        setTitle(title);
    }

    public void setTitle(CharSequence title) {
        this.title = title;
        if (!TextUtils.isEmpty(title)) {
            titleContainer.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
        } else {
            titleContainer.setVisibility(View.GONE);
        }
    }

    public void setSubtitle(int resId) {
        String subtitle = tvSubtitle.getContext().getString(resId);
        setSubtitle(subtitle);
    }

    public void setSubtitle(CharSequence subtitle) {
        this.subtitle = subtitle;
        if (TextUtils.isEmpty(title)) {
            return;
        }
        if (!TextUtils.isEmpty(subtitle)) {
            tvSubtitle.setText(subtitle);
            tvSubtitle.setVisibility(View.VISIBLE);
        } else {
            tvSubtitle.setVisibility(View.GONE);
        }
    }

    public void setOnClickListener(View.OnClickListener clickListener) {
        titleContainer.setOnClickListener(clickListener);
        if (clickListener != null) {
            titleContainer.setClickable(true);
            ivMenuDown.setVisibility(View.VISIBLE);
        } else {
            titleContainer.setClickable(false);
            ivMenuDown.setVisibility(View.GONE);
        }

    }
}
