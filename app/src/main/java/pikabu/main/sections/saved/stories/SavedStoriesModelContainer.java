package pikabu.main.sections.saved.stories;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created on 08.12.2016.
 */

public class SavedStoriesModelContainer extends Fragment {

    private static final String SAVED_STORIES_MODEL_TAG = "saved_stories_model_tag";

    private SavedStoriesModel model;

    public static SavedStoriesModel getSavedStoriesModel(FragmentActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        SavedStoriesModelContainer container = (SavedStoriesModelContainer) fm.findFragmentByTag(SAVED_STORIES_MODEL_TAG);
        if (container == null) {
            container = new SavedStoriesModelContainer();
            fm.beginTransaction().add(container, SAVED_STORIES_MODEL_TAG).commit();
        }
        if (container.model == null) {
            container.model = new SavedStoriesModel();
        }
        return container.model;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}