package pikabu.main.sections.saved.stories;

import android.database.Observable;

import java.util.List;

/**
 * Created on 08.12.2016.
 */

public class SavedStoriesModelObservable extends Observable<SavedStoriesModelObserver> {

    public void showSaveCategories(List<SaveCategory> categories, int position) {
        for (SavedStoriesModelObserver observer : mObservers) {
            observer.showSaveCategories(categories, position);
        }
    }
}
