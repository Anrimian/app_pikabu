package pikabu.main.sections.saved.stories;

import java.util.List;

import pikabu.data.Links;
import pikabu.user.UserModel;
import pikabu.user.savedcategories.SavedCategoriesModel;
import pikabu.user.savedcategories.SavedCategoriesObserver;

/**
 * Created on 08.12.2016.
 */

public class SavedStoriesModel implements SavedCategoriesObserver {

    private List<SaveCategory> categories;
    private SaveCategory selectedCategory;

    private SavedCategoriesModel savedCategoriesModel;

    private SavedStoriesModelObservable observable = new SavedStoriesModelObservable();

    public SavedStoriesModel() {
        savedCategoriesModel = UserModel.getInstance().getSavedCategoriesModel();
        categories = savedCategoriesModel.getCategories();
        selectedCategory = categories.get(0);
        savedCategoriesModel.registerObserver(this);
    }

    public void registerModelObserver(SavedStoriesModelObserver observer) {
        observable.registerObserver(observer);
        observer.showSaveCategories(categories, getPosition());
    }

    public void unregisterModelObserver(SavedStoriesModelObserver observer) {
        observable.unregisterObserver(observer);
    }

    @Override
    public void onUpdateSavedCategories() {
        categories = savedCategoriesModel.getCategories();
        observable.showSaveCategories(categories, getPosition());
    }

    public void setSaveCategories(List<SaveCategory> newCategories) {
        savedCategoriesModel.setCategories(newCategories);
    }

    public String getLink() {//http://pikabu.ru/index.php?cmd=saved&cid=169165&page=
        StringBuilder sb = new StringBuilder(Links.LINK_SAVED_STORIES);
        sb.append("&cid=");
        sb.append(selectedCategory.getId());
        sb.append("&page=");
        return sb.toString();
    }

    public int getPosition() {
        int position = categories.indexOf(selectedCategory);
        if (position == -1) {
            position = 0;
        }
        return position;
    }

    public void setPosition(int position) {
        selectedCategory = categories.get(position);
    }
}
