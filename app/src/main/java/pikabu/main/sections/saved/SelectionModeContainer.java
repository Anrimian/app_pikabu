package pikabu.main.sections.saved;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created on 21.11.2016.
 */

public class SelectionModeContainer extends Fragment {

    private static final String SAVE_MODEL_TAG = "save_model_tag";

    private SaveFragmentSelectionMode selectionMode;

    public static SaveFragmentSelectionMode getSelectionMode(FragmentActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        SelectionModeContainer container = (SelectionModeContainer) fm.findFragmentByTag(SAVE_MODEL_TAG);
        if (container == null) {
            container = new SelectionModeContainer();
            fm.beginTransaction().add(container, SAVE_MODEL_TAG).commit();
        }
        if (container.selectionMode == null) {
            container.selectionMode = new SaveFragmentSelectionMode();
        }
        return container.selectionMode;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
