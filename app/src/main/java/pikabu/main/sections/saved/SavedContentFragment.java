package pikabu.main.sections.saved;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;

import pikabu.main.DrawerActivity;
import pikabu.main.ToolbarTitleBinder;

/**
 * Created on 21.11.2016.
 */

public class SavedContentFragment extends Fragment {

    private SaveFragmentSelectionMode selectionMode;

    private ToolbarTitleBinder toolbarTitleBinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return View.inflate(getContext(), R.layout.container, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectionMode = SelectionModeContainer.getSelectionMode(getActivity());
        toolbarTitleBinder = ((DrawerActivity) getActivity()).getToolbarTitleBinder();
        toolbarTitleBinder.setTitle(R.string.saved);
        toolbarTitleBinder.setSubtitle(selectionMode.getDescriptionId());
        toolbarTitleBinder.setOnClickListener(new SaveFragmentSelectionModeOnClickListener());
        setSaveFragment(selectionMode.getFragment());
    }

    private void setSaveFragment(Fragment fragment) {
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private class SaveFragmentSelectionModeOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.inflate(R.menu.save_fragment_selection_menu);
            popup.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.posts: {
                        selectionMode.setSelection(SaveFragmentSelectionMode.SaveSelection.POSTS);
                        break;
                    }
                    case R.id.comments: {
                        selectionMode.setSelection(SaveFragmentSelectionMode.SaveSelection.COMMENTS);
                        break;
                    }
                }
                toolbarTitleBinder.setSubtitle(selectionMode.getDescriptionId());
                setSaveFragment(selectionMode.getFragment());
                return true;
            });
            popup.show();
        }
    }
}
