package pikabu.main.sections.saved.stories;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

import pikabu.content.story.storylist.StoryListAdapter;
import pikabu.content.story.storylist.StoryListModel;
import pikabu.content.story.storylist.StoryListModelContainer;
import pikabu.content.story.storylist.StoryListView;
import pikabu.main.sections.saved.stories.parser.SaveStoriesParser;

/**
 * Created on 07.12.2016.
 */

public class SavedStoriesFragment extends UpArrowRecyclerViewFragmentV2 {

    private static final String SAVED_STORIES_MODEL_TAG = "saved_stories_model";

    private StoryListModel model;
    private StoryListView storyListView;
    private SavedStoriesController storyListController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.saved_stories_view, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        storyListView = new StoryListView(view);
        View headerView = view.findViewById(R.id.category_container);
        SavedStoriesCategoryView categoryView = new SavedStoriesCategoryView(headerView);

        model = StoryListModelContainer.getInstance(null, SAVED_STORIES_MODEL_TAG);
        model.setAdditionalDataParser(SaveStoriesParser::getObservable);
        SavedStoriesModel savedStoriesModel = SavedStoriesModelContainer.getSavedStoriesModel(getActivity());

        StoryListAdapter adapter = new StoryListAdapter(model.getStoryList(), getActivity());
        storyListController = new SavedStoriesController(getActivity(), storyListView);
        storyListController.bind(categoryView, savedStoriesModel);
        storyListController.bindList(savedStoriesModel.getLink(), model, adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        storyListController.unbind();
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return storyListController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return storyListView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return model;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return model;
    }

}