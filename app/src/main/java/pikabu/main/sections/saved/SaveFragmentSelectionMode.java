package pikabu.main.sections.saved;

import android.support.v4.app.Fragment;

import com.emirinay.pikabuapp.R;

import pikabu.content.comments.profile.fragment.ProfileCommentsFragment;
import pikabu.data.Links;
import pikabu.main.sections.saved.stories.SavedStoriesFragment;

/**
 * Created on 21.11.2016.
 */
public class SaveFragmentSelectionMode {

    private SaveSelection selection = SaveSelection.POSTS;

    public void setSelection(SaveSelection selection) {
        this.selection = selection;
    }

    public Fragment getFragment() {
        return selection.getFragment();
    }

    public int getDescriptionId() {
        return selection.getDescriptionId();
    }

    public enum SaveSelection {
        POSTS {
            @Override
            public int getDescriptionId() {
                return R.string.posts;
            }

            @Override
            public Fragment getFragment() {
                return new SavedStoriesFragment();
            }
        },
        COMMENTS {
            @Override
            public int getDescriptionId() {
                return R.string.comments;
            }

            @Override
            public Fragment getFragment() {
                return ProfileCommentsFragment.newInstance(Links.LINK_TO_SAVED_COMMENTS);
            }
        };

        public Fragment getFragment() {
            return null;
        }

        public int getDescriptionId() {
            return 0;
        }
    }
}
