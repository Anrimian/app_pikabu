package pikabu.main.sections.saved.stories;

import android.support.v4.app.FragmentActivity;

import java.util.List;

import pikabu.content.story.storylist.StoryListController;
import pikabu.content.story.storylist.StoryListView;
import rx.Observable;

/**
 * Created on 08.12.2016.
 */

public class SavedStoriesController extends StoryListController implements SavedStoriesActions, SavedStoriesModelObserver {

    private SavedStoriesCategoryView savedStoriesCategoryView;
    private SavedStoriesModel storiesModel;

    public SavedStoriesController(FragmentActivity activity, StoryListView storyListView) {
        super(activity, storyListView);
    }

    public void bind(SavedStoriesCategoryView categoryView, SavedStoriesModel storiesModel) {
        this.savedStoriesCategoryView = categoryView;
        this.storiesModel = storiesModel;
        categoryView.bind(this);
        storiesModel.registerModelObserver(this);
    }

    @Override
    public void unbind() {
        super.unbind();
        storiesModel.unregisterModelObserver(this);
    }

    @Override
    public void showEndState() {
        setSaveCategories();
        super.showEndState();
    }

    @Override
    public void showErrorState() {
        setSaveCategories();
        super.showErrorState();
    }

    @Override
    public void showProgressState() {
        setSaveCategories();
        super.showProgressState();
    }

    @Override
    public void showWaitState() {
        setSaveCategories();
        super.showWaitState();
    }

    private void setSaveCategories() {
        Observable<List<SaveCategory>> observable = getModel().getAdditionalData();
        if (observable != null) {
            observable.subscribe(categories -> storiesModel.setSaveCategories(categories),
                    throwable -> savedStoriesCategoryView.showError());
        }
    }

    @Override
    public void onCategorySelected(int position) {
        if (storiesModel.getPosition() != position) {
            storiesModel.setPosition(position);
            getModel().setLink(storiesModel.getLink());
        }
    }

    @Override
    public void showSaveCategories(List<SaveCategory> categories, int position) {
        savedStoriesCategoryView.showSaveCategories(categories, position);
    }
}
