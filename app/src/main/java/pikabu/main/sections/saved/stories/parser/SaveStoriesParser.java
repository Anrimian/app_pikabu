package pikabu.main.sections.saved.stories.parser;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;

import pikabu.content.ParserException;
import pikabu.main.sections.saved.stories.SaveCategory;
import rx.Observable;

/**
 * Created on 08.12.2016.
 */

public class SaveStoriesParser {

    public static Observable<List<SaveCategory>> getObservable(Document document) {
        return getElements(document).flatMap(SaveStoriesParser::parseElement).toList();
    }

    private static Observable<Element> getElements(Document doc) {
        return Observable.create(subscriber -> {
            Element element = doc.select(".ss-cats-list").first();
            if (element != null) {
                Elements categories = element.getElementsByAttribute("data-id");
                for (Element category : categories) {
                    subscriber.onNext(category);
                }
            } else {
                subscriber.onError(new ParserException("can not select categories"));
            }
            subscriber.onCompleted();
        });
    }

    private static Observable<SaveCategory> parseElement(Element element) {
        return Observable.create(subscriber -> {
            SaveStoriesParser parser = new SaveStoriesParser(element);
            SaveCategory category = parser.parseElement();
            if (category != null) {
                subscriber.onNext(category);
            } else {
                subscriber.onError(new ParserException());
            }
            subscriber.onCompleted();
        });
    }

    private Element element;

    private SaveStoriesParser(Element element) {
        this.element = element;
    }

    private SaveCategory parseElement() {
        try {
            return new SaveCategory(selectId(), selectName(), selectStoriesCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private int selectStoriesCount() {
        String storiesCount = element.select("b").text();
        return Integer.parseInt(storiesCount);
    }

    private String selectName() throws ParserException {
        String name = element.select("span").text();
        if (name.isEmpty()) {
            throw new ParserException("save category name is empty, element: " + element);
        }
        return name;
    }

    private int selectId() {
        String id = element.attr("data-id");
        return Integer.parseInt(id);
    }
}
