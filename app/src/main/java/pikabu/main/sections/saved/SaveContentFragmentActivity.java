package pikabu.main.sections.saved;

import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;

/**
 * Created on 21.11.2016.
 */

public class SaveContentFragmentActivity extends DrawerActivity {

    @Override
    protected Fragment initFragment() {
        return new SavedContentFragment();
    }
}
