package pikabu.main.sections.saved.stories;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.view.View;

import com.emirinay.pikabuapp.R;

import java.util.List;

/**
 * Created on 08.12.2016.
 */

public class SavedStoriesCategoryView {

    private SavedStoriesActions storiesActions;

    private View view;
    private TabLayout tabLayout;
    private View errorView;

    public SavedStoriesCategoryView(View view) {
        this.view = view;
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setOnTabSelectedListener(new OnTabSelectedListener());
        errorView = view.findViewById(R.id.error_view);
    }

    public void bind(SavedStoriesActions storiesActions) {
        this.storiesActions = storiesActions;
    }

    public void showSaveCategories(List<SaveCategory> categories, int position) {
        view.setVisibility(View.VISIBLE);
        tabLayout.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        tabLayout.removeAllTabs();
        for (int i = 0; i < categories.size(); i++ ) {
            SaveCategory category = categories.get(i);
            TabLayout.Tab tab = tabLayout.newTab().setText(category.getName());
            tabLayout.addTab(tab, i == position);
        }
    }

    public void hideAll() {
        view.setVisibility(View.GONE);
    }

    public void showError() {
        view.setVisibility(View.VISIBLE);
        tabLayout.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    private class OnTabSelectedListener implements TabLayout.OnTabSelectedListener {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            storiesActions.onCategorySelected(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    }

    private Context getContext() {
        return tabLayout.getContext();
    }

}
