package pikabu.main.sections.saved.stories;

import java.util.List;

/**
 * Created on 08.12.2016.
 */

public interface SavedStoriesModelObserver {
    void showSaveCategories(List<SaveCategory> categories, int position);
}
