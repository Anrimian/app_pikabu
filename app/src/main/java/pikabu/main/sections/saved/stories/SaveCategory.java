package pikabu.main.sections.saved.stories;

import android.support.annotation.NonNull;

/**
 * Created on 08.12.2016.
 */

public class SaveCategory {
    private int id;
    private int storiesCount;
    @NonNull private String name;

    public SaveCategory(int id, @NonNull String name, int storiesCount) {
        this.id = id;
        this.name = name;
        this.storiesCount = storiesCount;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public int getStoriesCount() {
        return storiesCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SaveCategory category = (SaveCategory) o;

        return name.equals(category.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
