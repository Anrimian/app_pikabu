package pikabu.main.sections.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

import pikabu.content.story.storylist.StoryListAdapter;
import pikabu.content.story.storylist.StoryListModel;
import pikabu.content.story.storylist.StoryListModelContainer;
import pikabu.content.story.storylist.StoryListView;

public class SearchStoryFragment extends UpArrowRecyclerViewFragmentV2 {
	public static final String SEARCH_FRAGMENT_TAG = "search_fragment";
    private static final String START_TAG = "start_tag";

    public static SearchStoryFragment newInstance() {
        return newInstance(null);
    }

	public static SearchStoryFragment newInstance(String startTag) {
		SearchStoryFragment fragment = new SearchStoryFragment();
        Bundle arguments = new Bundle();
        arguments.putString(START_TAG, startTag);
        fragment.setArguments(arguments);
		return fragment;
	}

    private StoryListModel listModel;
    private StoryListView storyListView;
    private SearchController storyListController;

    private SearchView searchView;
    private SearchModel searchModel;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.search);

        storyListView = new StoryListView(view);
        View headerView = View.inflate(getContext(), R.layout.search_story_view, null);
        searchView = new SearchView(headerView, getActivity());

        searchModel = getSearchModel();
        listModel = StoryListModelContainer.getInstance(null, getModelTag());
        StoryListAdapter adapter = new StoryListAdapter(listModel.getStoryList(), getActivity());
        adapter.addHeader(headerView);
        storyListController = new SearchController(getActivity(), storyListView);
        storyListController.bind(searchView, searchModel);
        storyListController.bindList(searchModel.getSearchQuery(), listModel, adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        storyListController.unbind();
    }

    private SearchModel getSearchModel() {
        String startTag = getStartTag();
        SearchModel searchModel;
        if (startTag == null) {
            searchModel = MainSearchModelContainer.getInstance();
        } else {
            searchModel = SearchModelContainer.getInstance(getActivity());
            searchModel.addTag(startTag);
            searchModel.createSearchQuery();
        }
        return searchModel;
    }

    private String getModelTag() {
        return "search: " + getStartTag();
    }

    private String getStartTag() {
        return getArguments().getString(START_TAG);
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return storyListController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return storyListView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return listModel;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return listModel;
    }
}
