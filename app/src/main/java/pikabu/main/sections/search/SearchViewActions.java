package pikabu.main.sections.search;

/**
 * Created on 01.10.2016.
 */

interface SearchViewActions {

    void startSearch();

    void resetSearchData();

    void setSearchText(String searchText);

    void addTag(String tag);

    void removeTag(String tag);
}
