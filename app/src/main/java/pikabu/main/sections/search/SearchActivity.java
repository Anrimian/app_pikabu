package pikabu.main.sections.search;

import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;

/**
 * Created on 27.08.2015.
 */
public class SearchActivity extends DrawerActivity {
    public static final String START_TAG = "start_tag";

    @Override
    protected Fragment initFragment() {
        String startTag = getIntent().getStringExtra(START_TAG);
        return SearchStoryFragment.newInstance(startTag);
    }
}
