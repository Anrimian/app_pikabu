package pikabu.main.sections.search;

import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import pikabu.content.story.storylist.StoryListController;
import pikabu.content.story.storylist.StoryListView;

/**
 * Created on 04.12.2016.
 */

public class SearchController extends StoryListController implements SearchModelObserver, SearchViewActions {

    private SearchView searchView;
    private SearchModel searchModel;

    public SearchController(FragmentActivity activity, StoryListView storyListView) {
        super(activity, storyListView);
    }

    public void bind(SearchView searchView, SearchModel searchModel) {
        this.searchView = searchView;
        this.searchModel = searchModel;
        searchView.bind(this);
        searchModel.registerSearchModelObserver(this);
    }

    @Override
    public void unbind() {
        super.unbind();
        searchModel.unregisterSearchModelObserver(this);
    }

    @Override
    public void showEndState() {
        super.showEndState();
        searchView.setEnabled(true);
    }

    @Override
    public void showErrorState() {
        super.showErrorState();
        searchView.setEnabled(true);
    }

    @Override
    public void showProgressState() {
        super.showProgressState();
        boolean blockSwipeRefreshLayout = isListEmpty() || isRefreshing();
        searchView.setEnabled(!blockSwipeRefreshLayout);
    }

    @Override
    public void showWaitState() {
        super.showWaitState();
        searchView.setEnabled(true);
    }

    @Override
    public void onSearchTextChanged() {
        searchView.setSearchText(searchModel.getSearchText());
    }

    @Override
    public void onTagAdded(String tag) {
        searchView.addTag(tag);
    }

    @Override
    public void onTagRemoved(String tag) {
        searchView.removeTag(tag);
    }

    @Override
    public void startSearch() {
        getModel().deleteListData();
        updateList(false);
        startLoading(true);
    }

    @Override
    public void resetSearchData() {
        getModel().deleteListData();
        updateList(false);
        searchModel.clearData();
    }

    @Override
    public void setSearchText(String searchText) {
        searchModel.setSearchText(searchText);
    }

    @Override
    public void addTag(String tag) {
        searchModel.addTag(tag);
    }

    @Override
    public void removeTag(String tag) {
        searchModel.removeTag(tag);
    }

    @Override
    public void startLoading(boolean refresh) {
        String previewLink = searchModel.getSearchQuery();
        searchModel.createSearchQuery();
        String newLink = searchModel.getSearchQuery();
        if (TextUtils.equals(previewLink, newLink)) {
            super.startLoading(refresh);
        } else {
            getModel().setLink(newLink);
        }
    }

    @Override
    protected boolean isHeaderVisible() {
        return true;
    }
}
