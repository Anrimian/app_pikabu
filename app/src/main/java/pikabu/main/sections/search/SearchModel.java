package pikabu.main.sections.search;


import java.util.ArrayList;

class SearchModel {

    private SearchModelObservable observable = new SearchModelObservable();

    private String searchText;
    private ArrayList<String> tags = new ArrayList<>();

    private String searchQuery;

    void registerSearchModelObserver(SearchModelObserver observer) {
        observable.registerObserver(observer);
        observer.onSearchTextChanged();
        for (String tag : tags)  {
            observer.onTagAdded(tag);
        }
    }

    void unregisterSearchModelObserver(SearchModelObserver observer) {
        observable.unregisterObserver(observer);
    }

    void clearData() {
        searchText = null;
        observable.onSearchTextChanged();

        for (String tag : tags)  {
            observable.onTagRemoved(tag);
        }
        tags.clear();

        createSearchQuery();
    }

    void addTag(String tag) {
        if (!tags.contains(tag)) {
            tags.add(tag);
            observable.onTagAdded(tag);
        }
    }

    void removeTag(String tag) {
        tags.remove(tag);
        observable.onTagRemoved(tag);
    }

    String getSearchText() {
        return searchText;
    }

    void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    void createSearchQuery() {
        SearchRequestBuilder builder = new SearchRequestBuilder();
        builder.setSearchText(searchText);
        builder.setTags(tags);
        searchQuery = builder.build();
    }

    String getSearchQuery() {
        return searchQuery;
    }
}
