package pikabu.main.sections.search;

import android.database.Observable;

/**
 * Created on 01.10.2016.
 */

class SearchModelObservable extends Observable<SearchModelObserver> {

    public void onSearchTextChanged() {
        for (final SearchModelObserver observer : mObservers) {
            observer.onSearchTextChanged();
        }
    }

    public void onTagAdded(String tag) {
        for (final SearchModelObserver observer : mObservers) {
            observer.onTagAdded(tag);
        }
    }

    public void onTagRemoved(String tag) {
        for (final SearchModelObserver observer : mObservers) {
            observer.onTagRemoved(tag);
        }
    }
}
