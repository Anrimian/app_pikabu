package pikabu.main.sections.search;

import android.text.TextUtils;

import java.util.ArrayList;

import pikabu.data.Links;


class SearchRequestBuilder {
	private String searchText = "";
	private String tags = "";
	
	public String build() {
		StringBuilder request = new StringBuilder(Links.LINK_SEARCH);
		if (!tags.isEmpty()) {
			request.append(tags);
			if (!searchText.isEmpty()) {
				request.append("&");
			}
		}
		request.append(searchText);
        request.append("&page=");
		return request.toString();
	}
	
	void setSearchText(String searchText){
		if (!TextUtils.isEmpty(searchText)){
			StringBuilder sb = new StringBuilder("q=");
			searchText = replaceSpaces(searchText);
	        sb.append(searchText);
	        this.searchText = sb.toString();
		}
	}

	void setTags(ArrayList<String> tagList) {
		if (!tagList.isEmpty()){
			StringBuilder sb = new StringBuilder("t=");
            for (int i = 0; i < tagList.size(); i++) {
                if (i > 0) {
                    sb.append("%2C");
                }
				String tag = tagList.get(i);
				tag = replaceSpaces(tag);
                sb.append(tag);
            }
            this.tags = sb.toString();
		}		
	}
	
	private String replaceSpaces(String text){		
		return text.replace(" ", "%20");
	}
}
