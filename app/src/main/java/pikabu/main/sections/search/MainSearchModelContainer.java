package pikabu.main.sections.search;

/**
 * Created on 01.10.2016.
 */
class MainSearchModelContainer {

    private SearchModel model;

    private volatile static MainSearchModelContainer container;

    private MainSearchModelContainer() {
        model = new SearchModel();
    }

    public static SearchModel getInstance() {
        if (container == null) {
            synchronized (MainSearchModelContainer.class) {
                if (container == null) {
                    container = new MainSearchModelContainer();
                }
            }
        }
        return container.model;
    }
}
