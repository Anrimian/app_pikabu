package pikabu.main.sections.search;

/**
 * Created on 01.10.2016.
 */

interface SearchModelObserver {

    void onSearchTextChanged();

    void onTagAdded(String tag);

    void onTagRemoved(String tag);
}
