package pikabu.main.sections.search;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created on 24.08.2015.
 */
public class SearchModelContainer extends Fragment {

    private static final String SEARCH_MODEL_TAG = "search_model_tag";

    private SearchModel model;

    public static SearchModel getInstance(FragmentActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        SearchModelContainer container = (SearchModelContainer) fm.findFragmentByTag(SEARCH_MODEL_TAG);
        if (container == null) {
            container = new SearchModelContainer();
            fm.beginTransaction().add(container, SEARCH_MODEL_TAG).commit();
        }
        if (container.model == null) {
            container.model = new SearchModel();
        }
        return container.model;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
