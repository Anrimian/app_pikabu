package pikabu.main.sections.search.tagdialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.emirinay.pikabuapp.R;

/**
 * Created on 17.04.2016.
 */
public class AddTagDialogFragment extends DialogFragment {
    private AddTagAction addTagAction;

    private EditText etTagText;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(R.string.add_tag);
        View view = inflater.inflate(R.layout.add_tag_dialog, container);
        etTagText = (EditText) view.findViewById(R.id.et_new_tag);

        Button btnAdd = (Button) view.findViewById(R.id.btn_dialog_add_tag);
        btnAdd.setOnClickListener(new PositiveOnClickListener());

        Button btnCancel = (Button) view.findViewById(R.id.btn_dialog_cancel_add_tag);
        btnCancel.setOnClickListener(new NegativeOnClickListener());
        return view;
    }

    public void setAddTagAction(AddTagAction addTagAction) {
        this.addTagAction = addTagAction;
    }

    class PositiveOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String tagText = etTagText.getText().toString();
            if (!tagText.isEmpty()) {
                addTagAction.addTag(tagText);
                dismiss();
            }
            else {
                etTagText.setError(getString(R.string.empty_field_error));
            }
        }
    }

    class NegativeOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            dismiss();
        }
    }

}
