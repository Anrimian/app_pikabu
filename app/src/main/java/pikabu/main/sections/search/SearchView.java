package pikabu.main.sections.search;

import android.app.Activity;
import android.app.FragmentManager;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;

import org.apmem.tools.layouts.FlowLayout;

import java.util.HashMap;
import java.util.Map;

import pikabu.main.sections.search.tagdialog.AddTagDialogFragment;

/**
 * Created on 01.10.2016.
 */

class SearchView {

    private Button btnSearchStory;
    private Button btnResetForm;
    private Button btnAddNewTag;

    private FlowLayout tagsContainerLayout;

    private EditText etSearchStory;

    private SearchViewActions actions;

    private Activity activity;

    SearchView(View searchView, Activity activity) {
        this.activity = activity;

        btnSearchStory = (Button) searchView.findViewById(R.id.btn_find_story);
        btnSearchStory.setOnClickListener(new StartSearchOnClickListener());

        btnResetForm = (Button) searchView.findViewById(R.id.btn_reset_search_form);
        btnResetForm.setOnClickListener(new ResetSearchFormOnClickListener());

        btnAddNewTag = (Button) searchView.findViewById(R.id.btn_add_new_tag);
        btnAddNewTag.setOnClickListener(new AddNewTagOnClickListener());

        tagsContainerLayout = (FlowLayout) searchView.findViewById(R.id.fl_search_tags_container);

        etSearchStory = (EditText) searchView.findViewById(R.id.et_search_story);
        etSearchStory.addTextChangedListener(new SearchFieldWatcher());
    }

    void bind(SearchViewActions actions) {
        this.actions = actions;
    }

    void setEnabled(boolean isEnabled) {
        btnSearchStory.setEnabled(isEnabled);
        btnResetForm.setEnabled(isEnabled);
        btnAddNewTag.setEnabled(isEnabled);
    }

    void setSearchText(String searchText) {
        etSearchStory.setText(searchText);
    }

    private Map<String, View> tagViewMap = new HashMap<>();

    void addTag(String tag) {
        View view = newTagView(tag);
        tagViewMap.put(tag, view);
        tagsContainerLayout.addView(view);
    }

    private View newTagView(String tagText) {
        View tagView = View.inflate(activity, R.layout.tag_view, null);
        TextView btnTag = (TextView) tagView.findViewById(R.id.tv_tag);
        btnTag.setText(tagText);
        tagView.setOnClickListener(new TagOnClickListener(tagText));
        return tagView;
    }

    void removeTag(String tag) {
        View view = tagViewMap.get(tag);
        tagsContainerLayout.removeView(view);
    }

    private class StartSearchOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            actions.startSearch();
        }
    }

    private class ResetSearchFormOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            actions.resetSearchData();
        }
    }

    private class SearchFieldWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            actions.setSearchText(s.toString());

        }
    }

    private class AddNewTagOnClickListener implements View.OnClickListener {

        private static final String ADD_TAG_DIALOG = "add_tag_dialog";

        private AddTagDialogFragment addTagDialog;

        AddNewTagOnClickListener() {
            FragmentManager fm = activity.getFragmentManager();
            addTagDialog = (AddTagDialogFragment) fm.findFragmentByTag(ADD_TAG_DIALOG);
            if (addTagDialog != null) {
                addTagDialog.setAddTagAction(tag -> actions.addTag(tag));
            }
        }

        @Override
        public void onClick(View v) {
            addTagDialog = new AddTagDialogFragment();
            addTagDialog.setAddTagAction(tag -> actions.addTag(tag));
            addTagDialog.show(activity.getFragmentManager(), ADD_TAG_DIALOG);
        }
    }

    private class TagOnClickListener implements View.OnClickListener {
        private String tagText;

        private TagOnClickListener(String tagText) {
            this.tagText = tagText;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(activity, v);
            popup.setOnMenuItemClickListener(new TagMenuItemClickListener());
            popup.inflate(R.menu.tag_search_actions_menu);
            popup.show();
        }

        class TagMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.remove_tag:
                        actions.removeTag(tagText);
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}
