package pikabu.main.sections.search.tagdialog;

/**
 * Created on 01.10.2016.
 */

public interface AddTagAction {

    void addTag(String text);
}
