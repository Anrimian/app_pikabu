package pikabu.main.sections;

/**
 * Created on 29.11.2016.
 */

public class SelectionMode<T extends Selection> {

    private T selection;

    public SelectionMode(T startSelection) {
        this.selection = startSelection;
    }

    public void setSelection(T selection) {
        this.selection = selection;
    }

    public String getPath() {
        return selection.getPath();
    }

    public int getDescriptionId() {
        return selection.getDescriptionId();
    }
}
