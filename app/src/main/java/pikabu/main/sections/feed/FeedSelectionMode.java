package pikabu.main.sections.feed;

import android.content.SharedPreferences;

import com.emirinay.pikabuapp.R;

import pikabu.Preferences;

/**
 * Created on 09.08.2016.
 */
public class FeedSelectionMode {

    private static final String USERS_SELECTION = "feed_users_selection";
    private static final String TAGS_SELECTION = "feed_tags_selection";
    private static final String COMMUNITY_SELECTION = "feed_community_selection";
    private static final String ANY_RATING_SELECTION = "feed_rating_selection";

    private boolean usersSelection;
    private boolean tagsSelection;
    private boolean communitySelection;
    private boolean anyRatingSelection;


    public FeedSelectionMode() {
        SharedPreferences pref = Preferences.get();
        usersSelection = pref.getBoolean(USERS_SELECTION, true);
        tagsSelection = pref.getBoolean(TAGS_SELECTION, true);
        communitySelection = pref.getBoolean(COMMUNITY_SELECTION, true);
        anyRatingSelection = pref.getBoolean(ANY_RATING_SELECTION, true);
    }

    public void save() {
        SharedPreferences pref = Preferences.get();
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(USERS_SELECTION, usersSelection);
        editor.putBoolean(TAGS_SELECTION, tagsSelection);
        editor.putBoolean(COMMUNITY_SELECTION, communitySelection);
        editor.putBoolean(ANY_RATING_SELECTION, anyRatingSelection);
        editor.apply();
    }

    public void setUsersSelection(boolean usersSelection) {
        this.usersSelection = usersSelection;
    }

    public void setTagsSelection(boolean tagsSelection) {
        this.tagsSelection = tagsSelection;
    }

    public void setCommunitySelection(boolean communitySelection) {
        this.communitySelection = communitySelection;
    }

    public boolean isUsersSelection() {
        return usersSelection;
    }

    public boolean isTagsSelection() {
        return tagsSelection;
    }

    public boolean isCommunitySelection() {
        return communitySelection;
    }

    public boolean isAnyRatingSelection() {
        return anyRatingSelection;
    }

    public void setAnyRatingSelection(boolean anyRatingSelection) {
        this.anyRatingSelection = anyRatingSelection;
    }

    public int getState() {
        int state = 0;
        if (usersSelection) {
            state += 1;
        }
        if (tagsSelection) {
            state += 2;
        }
        if (communitySelection) {
            state += 4;
        }
        if (state == 7) {
            state = 0;
        }
        return state;
    }

    public int getDescriptionId() {
        switch (getState()) {
            case 0: return R.string.all_subs;
            case 1: return R.string.subscription_on_users;
            case 2: return R.string.subscription_on_tags;
            case 3: return R.string.subscription_on_users_and_tags;
            case 4: return R.string.subscription_on_community;
            case 5: return R.string.subscription_on_users_and_community;
            case 6: return R.string.subscription_on_tags_and_community;
            default: return R.string.error;
        }
    }

}
