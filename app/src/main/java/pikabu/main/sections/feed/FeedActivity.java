package pikabu.main.sections.feed;

import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;

/**
 * Created on 03.08.2016.
 */
public class FeedActivity extends DrawerActivity {

    @Override
    protected Fragment initFragment() {
        return new FeedFragment();
    }
}
