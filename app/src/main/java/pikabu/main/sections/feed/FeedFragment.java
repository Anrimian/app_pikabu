package pikabu.main.sections.feed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

import pikabu.content.story.storylist.StoryListAdapter;
import pikabu.content.story.storylist.StoryListController;
import pikabu.content.story.storylist.StoryListModel;
import pikabu.content.story.storylist.StoryListModelContainer;
import pikabu.content.story.storylist.StoryListView;
import pikabu.data.Links;
import pikabu.main.DrawerActivity;
import pikabu.main.ToolbarTitleBinder;

/**
 * Created on 03.08.2016.
 */
public class FeedFragment extends UpArrowRecyclerViewFragmentV2 {

    private static final String FEED_STORIES_MODEL_TAG = "feed_stories_model_tag";

    private FeedSelectionMode feedSelectionMode;

    private ToolbarTitleBinder toolbarTitleBinder;

    private StoryListModel listModel;
    private StoryListView storyListView;
    private StoryListController storyListController;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        feedSelectionMode = new FeedSelectionMode();
        toolbarTitleBinder = ((DrawerActivity) getActivity()).getToolbarTitleBinder();
        toolbarTitleBinder.setTitle(R.string.feed);
        toolbarTitleBinder.setSubtitle(feedSelectionMode.getDescriptionId());
        toolbarTitleBinder.setOnClickListener(new FeedSelectionModeOnClickListener());

        storyListView = new StoryListView(view);
        listModel = StoryListModelContainer.getInstance(null, FEED_STORIES_MODEL_TAG);
        StoryListAdapter adapter = new StoryListAdapter(listModel.getStoryList(), getActivity());
        storyListController = new StoryListController(getActivity(), storyListView);
        storyListController.bindList(getStoryListLink(), listModel, adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        storyListController.unbind();
    }

    public String getStoryListLink() {
        StringBuilder sb = new StringBuilder(Links.LINK_FEED);
        sb.append("?st=");
        sb.append(feedSelectionMode.getState());
        sb.append("&r=");
        sb.append(feedSelectionMode.isAnyRatingSelection() ? 0 : 1);
        sb.append("&page=");
        return sb.toString();
    }

    private class FeedSelectionModeOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.inflate(R.menu.feed_selection_menu);
            setUpMenu(popup);
            popup.setOnMenuItemClickListener(item -> {
                item.setChecked(!item.isChecked());
                switch (item.getItemId()) {
                    case R.id.users: {
                        feedSelectionMode.setUsersSelection(item.isChecked());
                        break;
                    }
                    case R.id.tags:  {
                        feedSelectionMode.setTagsSelection(item.isChecked());
                        break;
                    }
                    case R.id.communities: {
                        feedSelectionMode.setCommunitySelection(item.isChecked());
                        break;
                    }
                    case R.id.any_rating: {
                        feedSelectionMode.setAnyRatingSelection(true);
                        break;
                    }
                    case R.id.positive_rating: {
                        feedSelectionMode.setAnyRatingSelection(false);
                        break;
                    }
                }
                toolbarTitleBinder.setSubtitle(feedSelectionMode.getDescriptionId());
                item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
                item.setActionView(new View(getContext()));
                return false;
            });
            popup.setOnDismissListener((menu) -> {
                feedSelectionMode.save();
                listModel.setLink(getStoryListLink());
            });
            popup.show();
        }

        private void setUpMenu(PopupMenu popupMenu){
            Menu menu = popupMenu.getMenu();
            menu.findItem(R.id.users).setChecked(feedSelectionMode.isUsersSelection());
            menu.findItem(R.id.tags).setChecked(feedSelectionMode.isTagsSelection());
            menu.findItem(R.id.communities).setChecked(feedSelectionMode.isCommunitySelection());
            if (feedSelectionMode.isAnyRatingSelection()) {
                menu.findItem(R.id.any_rating).setChecked(true);
            } else {
                menu.findItem(R.id.positive_rating).setChecked(true);
            }
        }

    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return storyListController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return storyListView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return listModel;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return listModel;
    }

}
