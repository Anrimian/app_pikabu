package pikabu.main.sections.commented;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

import pikabu.content.comments.profile.fragment.ProfileCommentsAdapter;
import pikabu.content.comments.profile.fragment.ProfileCommentsModel;
import pikabu.content.comments.profile.fragment.ProfileCommentsModelContainer;
import pikabu.content.comments.profile.fragment.ProfileCommentsView;
import pikabu.main.DrawerActivity;
import pikabu.main.ToolbarTitleBinder;
import pikabu.main.sections.SelectionMode;

/**
 * Created on 26.11.2016.
 */

public class CommentedFragment extends UpArrowRecyclerViewFragmentV2 {

    private static final String COMMENTED_MODEL_TAG = "commented_model";


    private ProfileCommentsView profileCommentsView;
    private CommentedListController listController;
    private ProfileCommentsModel listModel;
    private CommentedModel commentedModel;
    private SelectionMode<CommentedSelection> selectionMode;

    private ToolbarTitleBinder toolbarTitleBinder;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        profileCommentsView = new ProfileCommentsView(view);
        View searchView = View.inflate(getContext(), R.layout.simple_search_view, null);
        SimpleSearchView simpleSearchView = new SimpleSearchView(searchView);

        listModel = ProfileCommentsModelContainer.getInstance(COMMENTED_MODEL_TAG);
        commentedModel = CommentedModelContainer.getCommentedModel(getActivity());
        selectionMode = commentedModel.getSelectionMode();
        toolbarTitleBinder = ((DrawerActivity) getActivity()).getToolbarTitleBinder();
        toolbarTitleBinder.setTitle(R.string.comments);
        toolbarTitleBinder.setSubtitle(selectionMode.getDescriptionId());
        toolbarTitleBinder.setOnClickListener(new CommentedFragmentSelectionModeOnClickListener());

        listController = new CommentedListController(getActivity(), profileCommentsView);
        listController.bindSearchView(simpleSearchView, commentedModel);
        ProfileCommentsAdapter adapter = new ProfileCommentsAdapter(listModel.getListData(), listController);
        adapter.addHeader(searchView);
        listController.bind(listModel, commentedModel.getSearchQuery(), adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listController.unbind();
    }

    private class CommentedFragmentSelectionModeOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.inflate(R.menu.commented_fragment_selection_menu);
            popup.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.by_time: {
                        selectionMode.setSelection(CommentedSelection.BY_TIME);
                        break;
                    }
                    case R.id.by_rating: {
                        selectionMode.setSelection(CommentedSelection.BY_RATING);
                        break;
                    }
                }
                toolbarTitleBinder.setSubtitle(selectionMode.getDescriptionId());
                commentedModel.createSearchQuery();
                listModel.setLink(commentedModel.getSearchQuery());
                return true;
            });
            popup.show();
        }
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return listController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return profileCommentsView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return listModel;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return listModel;
    }


}
