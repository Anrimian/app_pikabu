package pikabu.main.sections.commented;

import android.text.TextUtils;

import pikabu.data.Links;
import pikabu.main.sections.SelectionMode;

/**
 * Created on 27.11.2016.
 */

public class CommentedModel {

    private SelectionMode<CommentedSelection> selectionMode = new SelectionMode<>(CommentedSelection.BY_TIME);
    private String searchText;

    private String searchQuery;

    public CommentedModel() {
        createSearchQuery();
    }

    public SelectionMode<CommentedSelection> getSelectionMode() {
        return selectionMode;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    private String getSearchPath() {
        if (TextUtils.isEmpty(searchText)) {
            return "";
        } else {
            return "&q=" + searchText;
        }
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void createSearchQuery() {
        StringBuilder sb = new StringBuilder(Links.COMMENTS);
        sb.append("?");
        sb.append(selectionMode.getPath());
        sb.append(getSearchPath());
        sb.append("&page=");
        searchQuery = sb.toString();
    }
}
