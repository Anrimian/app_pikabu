package pikabu.main.sections.commented;

/**
 * Created on 28.11.2016.
 */

public interface SearchActions {

    void startSearch();

    void onSearchTextChanged(String text);
}
