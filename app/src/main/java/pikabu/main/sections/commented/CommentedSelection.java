package pikabu.main.sections.commented;

import com.emirinay.pikabuapp.R;

import pikabu.main.sections.Selection;

/**
 * Created on 29.11.2016.
 */

public enum CommentedSelection implements Selection {
    BY_TIME {
        @Override
        public int getDescriptionId() {
            return R.string.by_time;
        }

        @Override
        public String getPath() {
            return "f=0";
        }
    },
    BY_RATING {
        @Override
        public int getDescriptionId() {
            return R.string.by_rating;
        }

        @Override
        public String getPath() {
            return "f=2";
        }
    };

    @Override
    public String getPath() {
        return null;
    }

    @Override
    public int getDescriptionId() {
        return 0;
    }
}
