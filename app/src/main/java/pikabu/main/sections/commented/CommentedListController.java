package pikabu.main.sections.commented;

import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import pikabu.content.comments.profile.fragment.ProfileCommentsListController;
import pikabu.content.comments.profile.fragment.ProfileCommentsView;

/**
 * Created on 28.11.2016.
 */

public class CommentedListController extends ProfileCommentsListController implements SearchActions {

    private SimpleSearchView simpleSearchView;
    private CommentedModel commentedModel;

    public CommentedListController(FragmentActivity activity, ProfileCommentsView view) {
        super(activity, view);
    }

    public void bindSearchView(SimpleSearchView simpleSearchView, CommentedModel commentedModel) {
        this.simpleSearchView = simpleSearchView;
        this.commentedModel = commentedModel;
        simpleSearchView.bind(this, commentedModel.getSearchText());
    }

    @Override
    public void onSearchTextChanged(String text) {
        commentedModel.setSearchText(text);
    }

    @Override
    public void startSearch() {
        getModel().deleteListData();
        updateList();
        startLoading(true);
    }

    @Override
    protected boolean isHeaderVisible() {
        return true;
    }

    @Override
    public void onEnd() {
        super.onEnd();
        simpleSearchView.setEnabled(true);
    }

    @Override
    public void onError() {
        super.onError();
        simpleSearchView.setEnabled(true);
    }

    @Override
    public void onProgress() {
        super.onProgress();
        boolean blockSwipeRefreshLayout = isListEmpty() || isRefreshing();
        simpleSearchView.setEnabled(!blockSwipeRefreshLayout);
    }

    @Override
    public void onWait() {
        super.onWait();
        simpleSearchView.setEnabled(true);
    }

    @Override
    public void startLoading(boolean refresh) {
        String previewLink = commentedModel.getSearchQuery();
        commentedModel.createSearchQuery();
        String newLink = commentedModel.getSearchQuery();
        if (TextUtils.equals(previewLink, newLink)) {
            super.startLoading(refresh);
        } else {
            getModel().setLink(newLink);
        }
    }
}
