package pikabu.main.sections.commented;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created on 21.11.2016.
 */

public class CommentedModelContainer extends Fragment {

    private static final String COMMENTED_MODEL_TAG = "commented_model_tag";

    private CommentedModel model;

    public static CommentedModel getCommentedModel(FragmentActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        CommentedModelContainer container = (CommentedModelContainer) fm.findFragmentByTag(COMMENTED_MODEL_TAG);
        if (container == null) {
            container = new CommentedModelContainer();
            fm.beginTransaction().add(container, COMMENTED_MODEL_TAG).commit();
        }
        if (container.model == null) {
            container.model = new CommentedModel();
        }
        return container.model;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
