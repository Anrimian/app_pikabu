package pikabu.main.sections.commented;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.emirinay.pikabuapp.R;

/**
 * Created on 28.11.2016.
 */

public class SimpleSearchView {

    private View btnSearch;
    private EditText etSearchField;

    private SearchActions searchActions;

    public SimpleSearchView(View searchView) {
        btnSearch = searchView.findViewById(R.id.btn_search);
        btnSearch.setOnClickListener((v -> searchActions.startSearch()));
        etSearchField = (EditText) searchView.findViewById(R.id.et_search_field);
        etSearchField.addTextChangedListener(new SearchTextWatcher());
        EditText etAuthorName = (EditText) searchView.findViewById(R.id.et_author_name);
        etAuthorName.setVisibility(View.INVISIBLE);
    }

    public void bind(SearchActions searchActions, String text) {
        this.searchActions = searchActions;
        etSearchField.setText(text);
    }

    public void setEnabled(boolean enabled) {
        btnSearch.setEnabled(enabled);
        etSearchField.setEnabled(enabled);
    }

    private class SearchTextWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchActions.onSearchTextChanged(s.toString());
        }
    }
}
