package pikabu.main.sections.commented;

import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;

/**
 * Created on 26.11.2016.
 */

public class CommentedActivity extends DrawerActivity {
    @Override
    protected Fragment initFragment() {
        return new CommentedFragment();
    }
}
