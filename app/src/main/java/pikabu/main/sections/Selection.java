package pikabu.main.sections;

/**
 * Created on 29.11.2016.
 */

public interface Selection {
    String getPath();

    int getDescriptionId();
}
