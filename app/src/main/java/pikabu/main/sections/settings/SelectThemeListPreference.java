package pikabu.main.sections.settings;

import android.app.Activity;
import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;

import pikabu.utils.android.ActivityUtils;

/**
 * Created on 24.01.2017.
 */

public class SelectThemeListPreference extends ListPreference {

    public SelectThemeListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            Activity activity = (Activity) getContext();
            ActivityUtils.setTheme(activity);
            activity.recreate();
        }
    }

    @Override
    public CharSequence getSummary() {
        return getEntry();
    }
}
