package pikabu.main.sections.settings;

import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;

/**
 * Created on 02.09.2015.
 */
public class SettingsActivity extends DrawerActivity {

    @Override
    protected Fragment initFragment() {
        return new SettingsFragmentContainer();
    }
}
