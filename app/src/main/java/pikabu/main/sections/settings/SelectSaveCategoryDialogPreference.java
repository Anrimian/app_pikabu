package pikabu.main.sections.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;

import com.emirinay.pikabuapp.R;

import pikabu.content.story.savedialog.DialogResultActions;
import pikabu.content.story.savedialog.SaveStoryController;
import pikabu.content.story.savedialog.SaveStoryModel;
import pikabu.content.story.savedialog.SaveStoryModelContainer;
import pikabu.content.story.savedialog.SaveStoryView;
import pikabu.main.sections.saved.stories.SaveCategory;
import pikabu.user.UserModel;
import pikabu.user.savedcategories.SavedCategoriesModel;

/**
 * Created on 20.12.2016.
 */

public class SelectSaveCategoryDialogPreference extends DialogPreference implements DialogResultActions {

    public static final String DEFAULT_SAVE_CATEGORY = "default_save_category";

    private SaveStoryModel saveStoryModel;
    private SaveStoryView saveStoryView;
    private SaveStoryController saveStoryController;

    public SelectSaveCategoryDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        super.onPrepareDialogBuilder(builder);
        builder.setNegativeButton(null, null);
        builder.setPositiveButton(null, null);
        builder.setTitle(null);
    }

    @Override
    protected View onCreateDialogView() {
        return View.inflate(getContext(), R.layout.select_save_category_dialog, null);
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        saveStoryView = new SaveStoryView(view);
        saveStoryModel = SaveStoryModelContainer.getModel(getContext());
        saveStoryController = new SaveStoryController(saveStoryView, saveStoryModel, (Activity) getContext());
        saveStoryController.bind(this);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        saveStoryController.unbind();
    }

    @Override
    public void complete() {
        SaveCategory saveCategory = saveStoryModel.getSelectedCategory();
        setSummary(saveCategory.getName());
        getEditor().putInt(DEFAULT_SAVE_CATEGORY, saveCategory.getId()).apply();
        getDialog().dismiss();
    }

    @Override
    public void cancel() {
        getDialog().dismiss();
    }

    @Override
    public CharSequence getSummary() {
        int id = getSharedPreferences().getInt(DEFAULT_SAVE_CATEGORY, 0);
        SaveCategory category = UserModel.getInstance().getSavedCategoriesModel().getCategoryById(id);
        if (category == null) {
            category = SavedCategoriesModel.DEFAULT_SAVE_CATEGORY;
        }
        return category.getName();
    }
}
