package pikabu.main.sections.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.emirinay.pikabuapp.R;


public class SettingsFragment extends PreferenceFragment {
	
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    addPreferencesFromResource(R.xml.settings);
	  }
}
