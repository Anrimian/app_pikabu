package pikabu.main.sections.settings;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;

public class SettingsFragmentContainer extends Fragment {
	public static final String SETTINGS_FRAGMENT_TAG = "settings_fragment";
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getActivity().setTitle(R.string.settings);
		return inflater.inflate(R.layout.settings_fragment_container, null, true);
	}

}
