package pikabu.main.sections.messages;

import android.support.v4.app.Fragment;

import pikabu.main.DrawerActivity;

/**
 * Created on 29.11.2016.
 */

public class MessagesActivity extends DrawerActivity {
    @Override
    protected Fragment initFragment() {
        return new MessagesFragment();
    }
}
