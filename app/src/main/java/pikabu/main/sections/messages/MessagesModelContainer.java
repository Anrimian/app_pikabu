package pikabu.main.sections.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created on 29.11.2016.
 */

public class MessagesModelContainer extends Fragment {

    private static final String COMMENTED_MODEL_TAG = "messages_model_tag";

    private MessagesModel model;

    public static MessagesModel getMessagesModel(FragmentActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        MessagesModelContainer container = (MessagesModelContainer) fm.findFragmentByTag(COMMENTED_MODEL_TAG);
        if (container == null) {
            container = new MessagesModelContainer();
            fm.beginTransaction().add(container, COMMENTED_MODEL_TAG).commit();
        }
        if (container.model == null) {
            container.model = new MessagesModel();
        }
        return container.model;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
