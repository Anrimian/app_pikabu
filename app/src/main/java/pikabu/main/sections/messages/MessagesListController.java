package pikabu.main.sections.messages;

import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import pikabu.content.comments.profile.fragment.ProfileCommentsListController;
import pikabu.content.comments.profile.fragment.ProfileCommentsView;

/**
 * Created on 29.11.2016.
 */

public class MessagesListController extends ProfileCommentsListController implements SearchActions{

    private MessagesSearchView messagesSearchView;
    private MessagesModel messagesModel;

    public MessagesListController(FragmentActivity activity, ProfileCommentsView view) {
        super(activity, view);
    }

    public void bindSearchView(MessagesSearchView messagesSearchView, MessagesModel messagesModel) {
        this.messagesSearchView = messagesSearchView;
        this.messagesModel = messagesModel;
        messagesSearchView.bind(this, messagesModel.getSearchText(), messagesModel.getUserText());
    }

    @Override
    public void onSearchTextChanged(String text) {
        messagesModel.setSearchText(text);
    }

    @Override
    public void startSearch() {
        getModel().deleteListData();
        updateList();
        startLoading(true);
    }

    @Override
    public void onUserTextChanged(String text) {
        messagesModel.setUserText(text);
    }

    @Override
    protected boolean isHeaderVisible() {
        return true;
    }

    @Override
    public void onEnd() {
        super.onEnd();
        messagesSearchView.setEnabled(true);
    }

    @Override
    public void onError() {
        super.onError();
        messagesSearchView.setEnabled(true);
    }

    @Override
    public void onProgress() {
        super.onProgress();
        boolean blockSwipeRefreshLayout = isListEmpty() || isRefreshing();
        messagesSearchView.setEnabled(!blockSwipeRefreshLayout);
    }

    @Override
    public void onWait() {
        super.onWait();
        messagesSearchView.setEnabled(true);
    }

    @Override
    public void startLoading(boolean refresh) {
        String previewLink = messagesModel.getSearchQuery();
        messagesModel.createSearchQuery();
        String newLink = messagesModel.getSearchQuery();
        if (TextUtils.equals(previewLink, newLink)) {
            super.startLoading(refresh);
        } else {
            getModel().setLink(newLink);
        }
    }
}
