package pikabu.main.sections.messages;

/**
 * Created on 28.11.2016.
 */

public interface SearchActions {

    void startSearch();

    void onSearchTextChanged(String text);

    void onUserTextChanged(String text);
}
