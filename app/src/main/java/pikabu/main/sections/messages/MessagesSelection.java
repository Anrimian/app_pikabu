package pikabu.main.sections.messages;

import com.emirinay.pikabuapp.R;

import pikabu.main.sections.Selection;

/**
 * Created on 29.11.2016.
 */

public enum MessagesSelection implements Selection {
    ALL {
        @Override
        public int getDescriptionId() {
            return R.string.all;
        }

        @Override
        public String getPath() {
            return "t=0";
        }
    },
    POSTS_ANSWERS {
        @Override
        public int getDescriptionId() {
            return R.string.posts_answers;
        }

        @Override
        public String getPath() {
            return "t=1";
        }
    },
    COMMENTS_ANSWERS {
        @Override
        public int getDescriptionId() {
            return R.string.comments_answers;
        }

        @Override
        public String getPath() {
            return "t=2";
        }
    },
    MENTIONS_OF_USER {
        @Override
        public int getDescriptionId() {
            return R.string.mentions_of_user;
        }

        @Override
        public String getPath() {
            return "t=3";
        }
    };

    @Override
    public String getPath() {
        return null;
    }

    @Override
    public int getDescriptionId() {
        return 0;
    }
}
