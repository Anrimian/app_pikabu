package pikabu.main.sections.messages;

import android.text.TextUtils;

import pikabu.data.Links;
import pikabu.main.sections.SelectionMode;

/**
 * Created on 29.11.2016.
 */

public class MessagesModel {

    private SelectionMode<MessagesSelection> selectionMode = new SelectionMode<>(MessagesSelection.ALL);

    private String searchText;
    private String userText;

    private String searchQuery;

    public MessagesModel() {
        createSearchQuery();
    }

    public SelectionMode<MessagesSelection> getSelectionMode() {
        return selectionMode;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getUserText() {
        return userText;
    }

    public void setUserText(String userText) {
        this.userText = userText;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void createSearchQuery() {
        StringBuilder sb = new StringBuilder(Links.MESSAGES);
        sb.append("?");
        sb.append(selectionMode.getPath());
        sb.append(getSearchPath());
        sb.append(getUserPath());
        sb.append("&page=");
        searchQuery = sb.toString();
    }

    private String getSearchPath() {
        if (TextUtils.isEmpty(searchText)) {
            return "";
        } else {
            return "&q=" + searchText;
        }
    }

    private String getUserPath() {
        if (TextUtils.isEmpty(userText)) {
            return "";
        } else {
            return "&auth=" + userText;
        }
    }
}
