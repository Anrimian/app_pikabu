package pikabu.main.sections.messages;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.emirinay.pikabuapp.R;


/**
 * Created on 29.11.2016.
 */

public class MessagesSearchView {
    private View btnSearch;
    private EditText etSearchField;
    private EditText etAuthorName;

    private SearchActions searchActions;

    public MessagesSearchView(View searchView) {
        btnSearch = searchView.findViewById(R.id.btn_search);
        btnSearch.setOnClickListener((v -> searchActions.startSearch()));
        etSearchField = (EditText) searchView.findViewById(R.id.et_search_field);
        etSearchField.addTextChangedListener(new SearchTextWatcher());
        etAuthorName = (EditText) searchView.findViewById(R.id.et_author_name);
        etAuthorName.addTextChangedListener(new UserTextWatcher());
    }

    public void bind(SearchActions searchActions, String text, String user) {
        this.searchActions = searchActions;
        etSearchField.setText(text);
        etAuthorName.setText(user);
    }

    public void setEnabled(boolean enabled) {
        btnSearch.setEnabled(enabled);
        etSearchField.setEnabled(enabled);
        etAuthorName.setEnabled(enabled);
    }

    private class SearchTextWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchActions.onSearchTextChanged(s.toString());
        }
    }

    private class UserTextWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchActions.onUserTextChanged(s.toString());
        }
    }
}
