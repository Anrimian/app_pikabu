package pikabu.main.sections.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

import pikabu.content.comments.profile.fragment.ProfileCommentsAdapter;
import pikabu.content.comments.profile.fragment.ProfileCommentsModel;
import pikabu.content.comments.profile.fragment.ProfileCommentsModelContainer;
import pikabu.content.comments.profile.fragment.ProfileCommentsView;
import pikabu.main.DrawerActivity;
import pikabu.main.ToolbarTitleBinder;
import pikabu.main.sections.SelectionMode;

/**
 * Created on 29.11.2016.
 */

public class MessagesFragment extends UpArrowRecyclerViewFragmentV2 {

    private static final String MESSAGES_MODEL_TAG = "messages_model";

    private ProfileCommentsView profileCommentsView;
    private ProfileCommentsModel listModel;
    private MessagesModel messagesModel;
    private SelectionMode<MessagesSelection> selectionMode;

    private MessagesListController listController;

    private ToolbarTitleBinder toolbarTitleBinder;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        profileCommentsView = new ProfileCommentsView(view);
        View searchView = View.inflate(getContext(), R.layout.simple_search_view, null);
        MessagesSearchView messagesSearchView = new MessagesSearchView(searchView);

        listModel = ProfileCommentsModelContainer.getInstance(MESSAGES_MODEL_TAG);
        messagesModel = MessagesModelContainer.getMessagesModel(getActivity());
        selectionMode = messagesModel.getSelectionMode();

        toolbarTitleBinder = ((DrawerActivity) getActivity()).getToolbarTitleBinder();
        toolbarTitleBinder.setTitle(R.string.messages);
        toolbarTitleBinder.setSubtitle(selectionMode.getDescriptionId());
        toolbarTitleBinder.setOnClickListener(new MessagesFragmentSelectionModeOnClickListener());

        listController = new MessagesListController(getActivity(), profileCommentsView);
        listController.bindSearchView(messagesSearchView, messagesModel);
        ProfileCommentsAdapter adapter = new ProfileCommentsAdapter(listModel.getListData(), listController);
        adapter.addHeader(searchView);
        listController.bind(listModel, messagesModel.getSearchQuery(), adapter);
    }

    private class MessagesFragmentSelectionModeOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.inflate(R.menu.messages_fragment_selection_menu);
            popup.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.all: {
                        selectionMode.setSelection(MessagesSelection.ALL);
                        break;
                    }
                    case R.id.posts_answers: {
                        selectionMode.setSelection(MessagesSelection.POSTS_ANSWERS);
                        break;
                    }
                    case R.id.comments_answers: {
                        selectionMode.setSelection(MessagesSelection.COMMENTS_ANSWERS);
                        break;
                    }
                    case R.id.mentions_of_user: {
                        selectionMode.setSelection(MessagesSelection.MENTIONS_OF_USER);
                        break;
                    }
                }
                toolbarTitleBinder.setSubtitle(selectionMode.getDescriptionId());
                messagesModel.createSearchQuery();
                listModel.setLink(messagesModel.getSearchQuery());
                return true;
            });
            popup.show();
        }
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return listController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return profileCommentsView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return listModel;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return listModel;
    }
}
