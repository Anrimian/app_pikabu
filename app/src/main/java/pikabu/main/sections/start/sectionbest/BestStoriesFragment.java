package pikabu.main.sections.start.sectionbest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.dialogs.daterangepicker.DateRange;
import com.emirinay.tools.dialogs.daterangepicker.DateRangeDialogListener;
import com.emirinay.tools.dialogs.daterangepicker.DateRangePickerDialog;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;

import pikabu.content.story.storylist.StoryListAdapter;
import pikabu.content.story.storylist.StoryListController;
import pikabu.content.story.storylist.StoryListModel;
import pikabu.content.story.storylist.StoryListModelContainer;
import pikabu.content.story.storylist.StoryListView;
import pikabu.data.DbCategories;
import pikabu.data.Links;
import pikabu.main.DrawerActivity;
import pikabu.main.ToolbarTitleBinder;
import pikabu.main.sections.start.MainStoryListFragment;
import pikabu.main.sections.start.StartStoryListActivity;
import pikabu.main.sections.start.selections.CustomSelection;

/**
 * Created on 09.09.2016.
 */
public class BestStoriesFragment extends MainStoryListFragment {

    private BestStoriesSelectionMode selectionMode;

    private ToolbarTitleBinder toolbarTitleBinder;

    private StoryListModel listModel;
    private StoryListView storyListView;
    private StoryListController storyListController;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectionMode = new BestStoriesSelectionMode();
        toolbarTitleBinder = ((DrawerActivity) getActivity()).getToolbarTitleBinder();
        toolbarTitleBinder.setTitle(R.string.best_category);
        toolbarTitleBinder.setSubtitle(selectionMode.getDescription(getContext()));
        toolbarTitleBinder.setOnClickListener(new BestSelectionModeOnClickListener());

        storyListView = new StoryListView(view);
        listModel = StoryListModelContainer.getInstance(DbCategories.CATEGORY_BEST, getModelTag());
        StoryListAdapter adapter = new StoryListAdapter(listModel.getStoryList(), getActivity());
        storyListController = new StoryListController(getActivity(), storyListView);
        storyListController.bindList(getStoryListLink(), listModel, adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        DateRangePickerDialog.subscribeOnListener(getActivity().getSupportFragmentManager(), new MyDateRangeDialogListener());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        storyListController.unbind();
    }

    public String getStoryListLink() {
        StringBuilder sb = new StringBuilder(Links.LINK_BEST);
        sb.append(selectionMode.getPath());
        sb.append("?page=");
        return sb.toString();
    }

    public String getModelTag() {
        return getName();
    }

    @Override
    public String getName() {
        return StartStoryListActivity.BEST_STORIES;
    }

    private class BestSelectionModeOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.inflate(R.menu.best_stories_selection_menu);
            popup.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.selection_today: {
                        selectionMode.setSelectionMode(selectionMode.getTodaySelection());
                        break;
                    }
                    case R.id.selection_week: {
                        selectionMode.setSelectionMode(selectionMode.getWeekSelection());
                        break;
                    }
                    case R.id.selection_month: {
                        selectionMode.setSelectionMode(selectionMode.getMonthSelection());
                        break;
                    }
                    case R.id.selection_all: {
                        selectionMode.setSelectionMode(selectionMode.getAllSelection());
                        break;
                    }
                    case R.id.select_time: {
                        DateRangePickerDialog.start(getActivity().getSupportFragmentManager(),
                                new MyDateRangeDialogListener());
                        return false;
                    }
                }
                toolbarTitleBinder.setSubtitle(selectionMode.getDescription(getContext()));
                selectionMode.save();
                listModel.setLink(getStoryListLink());
                return false;
            });
            popup.show();
        }
    }

    private class MyDateRangeDialogListener implements DateRangeDialogListener {

        @Override
        public void onComplete(DateRange dateRange) {
            CustomSelection customSelection = selectionMode.getCustomSelection();
            customSelection.setDateRange(dateRange.getFrom(), dateRange.getTo());

            selectionMode.setSelectionMode(customSelection);
            selectionMode.save();
            toolbarTitleBinder.setSubtitle(selectionMode.getDescription(getContext()));
            listModel.setLink(getStoryListLink());
        }
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return storyListController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return storyListView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return listModel;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return listModel;
    }
}
