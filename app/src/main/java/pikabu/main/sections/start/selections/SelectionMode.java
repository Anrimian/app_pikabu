package pikabu.main.sections.start.selections;

import android.content.Context;

/**
 * Created on 27.09.2016.
 */
public interface SelectionMode {

    String getPath();

    String getDescription(Context ctx);
}