package pikabu.main.sections.start.sectionhot;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ListPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowControllerInfo;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.ArrowPositionModel;
import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.RecyclerViewInfo;

import pikabu.content.story.storylist.StoryListAdapter;
import pikabu.content.story.storylist.StoryListController;
import pikabu.content.story.storylist.StoryListModel;
import pikabu.content.story.storylist.StoryListModelContainer;
import pikabu.content.story.storylist.StoryListView;
import pikabu.data.DbCategories;
import pikabu.data.Links;
import pikabu.main.DrawerActivity;
import pikabu.main.ToolbarTitleBinder;
import pikabu.main.sections.start.MainStoryListFragment;
import pikabu.main.sections.start.StartStoryListActivity;

/**
 * Created on 09.09.2016.
 */
public class HotStoriesFragment extends MainStoryListFragment {

    private HotStoriesSelectionMode selectionMode;

    private ToolbarTitleBinder toolbarTitleBinder;

    private StoryListModel listModel;
    private StoryListView storyListView;
    private StoryListController storyListController;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectionMode = new HotStoriesSelectionMode();
        toolbarTitleBinder = ((DrawerActivity) getActivity()).getToolbarTitleBinder();
        toolbarTitleBinder.setTitle(R.string.hot_category);
        toolbarTitleBinder.setSubtitle(selectionMode.getDescriptionId());
        toolbarTitleBinder.setOnClickListener(new FeedSelectionModeOnClickListener());

        storyListView = new StoryListView(view);
        listModel = StoryListModelContainer.getInstance(DbCategories.CATEGORY_HOT, getModelTag());
        StoryListAdapter adapter = new StoryListAdapter(listModel.getStoryList(), getActivity());
        storyListController = new StoryListController(getActivity(), storyListView);
        storyListController.bindList(getStoryListLink(), listModel, adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        storyListController.unbind();
    }

    public String getStoryListLink() {
        StringBuilder sb = new StringBuilder(Links.LINK_HOT);
        sb.append("?f=");
        sb.append(selectionMode.getState());
        sb.append("&page=");
        return sb.toString();
    }

    public String getModelTag() {
        return getName();
    }

    @Override
    public String getName() {
        return StartStoryListActivity.HOT_STORIES;
    }

    private class FeedSelectionModeOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(getContext(), v);
            popup.inflate(R.menu.hot_stories_selection_menu);
            popup.setOnMenuItemClickListener(item -> {
                item.setChecked(!item.isChecked());
                switch (item.getItemId()) {
                    case R.id.selection_act: {
                        selectionMode.setSelection(HotStoriesSelectionMode.SELECTION_ACT);
                        break;
                    }
                    case R.id.selection_time: {
                        selectionMode.setSelection(HotStoriesSelectionMode.SELECTION_TIME);
                        break;
                    }
                }
                toolbarTitleBinder.setSubtitle(selectionMode.getDescriptionId());
                selectionMode.save();
                listModel.setLink(getStoryListLink());
                return false;
            });
            popup.show();
        }
    }

    @Override
    public ArrowControllerInfo getArrowControllerInfo() {
        return storyListController;
    }

    @Override
    public RecyclerViewInfo getRecyclerViewInfo() {
        return storyListView;
    }

    @Override
    public ArrowPositionModel getArrowPositionModel() {
        return listModel;
    }

    @Override
    public ListPositionModel getListPositionModel() {
        return listModel;
    }
}
