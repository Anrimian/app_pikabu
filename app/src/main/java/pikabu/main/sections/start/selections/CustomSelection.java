package pikabu.main.sections.start.selections;

import android.content.Context;
import android.text.format.DateFormat;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created on 27.09.2016.
 */
public class CustomSelection implements SelectionMode {

    private String path;
    private String description;

    private static final String PATH_FORMAT = "dd-MM-yyyy";
    private static final String DESCRIPTION_FORMAT = "dd MMMM yyyy";
    private static final String DESCRIPTION_FORMAT_SHORT = "dd MMM yyyy";

    public void setDate(Date date) {
        setDateRange(date, date);
    }

    public void setDateRange(Date from, Date to) {
        StringBuilder sbPath = new StringBuilder("/");
        sbPath.append(DateFormat.format(PATH_FORMAT, from));

        if (DateUtils.isSameDay(from, to)) {
            description = DateFormat.format(DESCRIPTION_FORMAT, from).toString();
        } else {
            sbPath.append("_");
            sbPath.append(DateFormat.format(PATH_FORMAT, to));

            description = formatDescription(from, to);
        }
        path = sbPath.toString();
    }

    public void createFromSavedPath(String path) {
        try {
            String[] dates = path.split("_");
            if (dates.length < 2) {
                Date date = new SimpleDateFormat("/" + PATH_FORMAT, Locale.getDefault()).parse(path);
                description = DateFormat.format(DESCRIPTION_FORMAT, date).toString();
            } else {
                Date from = new SimpleDateFormat("/" + PATH_FORMAT, Locale.getDefault()).parse(dates[0]);
                Date to = new SimpleDateFormat(PATH_FORMAT, Locale.getDefault()).parse(dates[1]);
                description = formatDescription(from, to);
            }
        } catch (ParseException e) {
            description = "";
            e.printStackTrace();
        }
    }

    private String formatDescription(Date from, Date to) {
        StringBuilder sbDescription = new StringBuilder();
        sbDescription.append(DateFormat.format(DESCRIPTION_FORMAT_SHORT, from));
        sbDescription.append(" - ");
        sbDescription.append(DateFormat.format(DESCRIPTION_FORMAT_SHORT, to));
        return sbDescription.toString();
    }

    @Override
    public String getDescription(Context ctx) {
        return ctx.getString(R.string.for_date, description);
    }

    @Override
    public String getPath() {
        return path;
    }
}