package pikabu.main.sections.start.sectionnew;

import android.content.Context;
import android.content.SharedPreferences;

import com.emirinay.pikabuapp.R;

import pikabu.Preferences;
import pikabu.main.sections.start.selections.CustomSelection;
import pikabu.main.sections.start.selections.SelectionMode;
import pikabu.main.sections.start.selections.StaticSelection;

/**
 * Created on 27.09.2016.
 */
public class NewStoriesSelectionMode {
    public static final String SELECTION_PATH = "new_stories_selection_path";

    private static final String TODAY_SELECTION = "";

    private SelectionMode selectionMode;

    public TodaySelection todaySelection = new TodaySelection();
    public CustomSelection customSelection = new CustomSelection();

    public NewStoriesSelectionMode() {
        SharedPreferences pref = Preferences.get();
        String path = pref.getString(SELECTION_PATH, TODAY_SELECTION);
        selectionMode = createFromPath(path);
    }

    private SelectionMode createFromPath(String path) {
        switch (path) {
            case TODAY_SELECTION: return todaySelection;
            default: {
                customSelection.createFromSavedPath(path);
                return customSelection;
            }
        }
    }

    public void save() {
        SharedPreferences pref = Preferences.get();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SELECTION_PATH, getPath());
        editor.apply();
    }

    public void setSelectionMode(SelectionMode selectionMode) {
        this.selectionMode = selectionMode;
    }

    public String getPath() {
        return selectionMode.getPath();
    }

    public String getDescription(Context ctx) {
        return selectionMode.getDescription(ctx);
    }

    public CustomSelection getCustomSelection() {
        return customSelection;
    }

    public TodaySelection getTodaySelection() {
        return todaySelection;
    }

    public class TodaySelection extends StaticSelection {

        @Override
        public int getDescriptionId() {
            return R.string.selection_today;
        }

        @Override
        public String getPath() {
            return TODAY_SELECTION;
        }
    }
}
