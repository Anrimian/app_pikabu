package pikabu.main.sections.start;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;

import pikabu.Preferences;
import pikabu.main.DrawerActivity;
import pikabu.main.sections.start.sectionbest.BestStoriesFragment;
import pikabu.main.sections.start.sectionhot.HotStoriesFragment;
import pikabu.main.sections.start.sectionnew.NewStoriesFragment;

/**
 * Created on 09.09.2016.
 */
public class StartStoryListActivity extends DrawerActivity {

    public static final String START_FRAGMENT = "start_fragment";

    public static final String HOT_STORIES = "hot_stories";
    public static final String BEST_STORIES = "best_stories";
    public static final String NEW_STORIES = "new_stories";

    @Override
    protected Fragment initFragment() {
        Intent intent = getIntent();
        String startFragmentName = intent.getStringExtra(START_FRAGMENT);
        if (startFragmentName == null) {
            SharedPreferences preferences = Preferences.get();
            startFragmentName = preferences.getString(START_FRAGMENT, HOT_STORIES);
        }
        return getStartFragment(startFragmentName);
    }

    private Fragment getStartFragment(String startFragmentName) {
        switch (startFragmentName) {
            case HOT_STORIES: {
                return new HotStoriesFragment();
            }
            case BEST_STORIES: {
                return new BestStoriesFragment();
            }
            case NEW_STORIES: {
                return new NewStoriesFragment();
            }
            default: {
                return new HotStoriesFragment();
            }
        }
    }
}
