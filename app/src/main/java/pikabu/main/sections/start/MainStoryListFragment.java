package pikabu.main.sections.start;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow.UpArrowRecyclerViewFragmentV2;

import pikabu.Preferences;

/**
 * Created on 09.09.2016.
 */
public abstract class MainStoryListFragment extends UpArrowRecyclerViewFragmentV2 {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = Preferences.get();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(StartStoryListActivity.START_FRAGMENT, getName());
        editor.apply();
    }

    public abstract String getName();
}
