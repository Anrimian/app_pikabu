package pikabu.main.sections.start.selections;

import android.content.Context;

/**
 * Created on 27.09.2016.
 */
public abstract class StaticSelection implements SelectionMode {

    @Override
    public String getDescription(Context ctx) {
        return ctx.getString(getDescriptionId());
    }

    public abstract int getDescriptionId();
}