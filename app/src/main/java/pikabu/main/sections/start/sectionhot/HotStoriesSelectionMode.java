package pikabu.main.sections.start.sectionhot;

import android.content.SharedPreferences;

import com.emirinay.pikabuapp.R;

import pikabu.Preferences;

/**
 * Created on 10.09.2016.
 */
public class HotStoriesSelectionMode {
    public static final String SELECTION = "hot_stories_selection";
    public static final String SELECTION_ACT = "act";
    public static final String SELECTION_TIME = "time";

    private String selection;

    public HotStoriesSelectionMode() {
        SharedPreferences pref = Preferences.get();
        selection = pref.getString(SELECTION, SELECTION_TIME);
    }

    public void save() {
        SharedPreferences pref = Preferences.get();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SELECTION, selection);
        editor.apply();
    }

    public int getDescriptionId() {
        switch (selection) {
            case SELECTION_ACT: return R.string.by_act;
            case SELECTION_TIME: return R.string.by_time;
            default: return R.string.error;
        }
    }

    public String getState() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }
}
