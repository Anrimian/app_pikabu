package pikabu.main.sections.start.sectionbest;

import android.content.Context;
import android.content.SharedPreferences;

import com.emirinay.pikabuapp.R;

import pikabu.Preferences;
import pikabu.main.sections.start.selections.CustomSelection;
import pikabu.main.sections.start.selections.SelectionMode;
import pikabu.main.sections.start.selections.StaticSelection;

/**
 * Created on 13.09.2016.
 */
public class BestStoriesSelectionMode {
    public static final String SELECTION_PATH = "best_stories_selection_path";

    private static final String TODAY_SELECTION = "";
    private static final String WEEK_SELECTION = "/week";
    private static final String MONTH_SELECTION = "/month";
    private static final String ALL_SELECTION = "/all";

    private SelectionMode selectionMode;

    public TodaySelection todaySelection = new TodaySelection();
    public WeekSelection weekSelection = new WeekSelection();
    public MonthSelection monthSelection = new MonthSelection();
    public AllSelection allSelection = new AllSelection();
    public CustomSelection customSelection = new CustomSelection();

    public BestStoriesSelectionMode() {
        SharedPreferences pref = Preferences.get();
        String path = pref.getString(SELECTION_PATH, TODAY_SELECTION);
        selectionMode = createFromPath(path);
    }

    private SelectionMode createFromPath(String path) {
        switch (path) {
            case TODAY_SELECTION: return todaySelection;
            case WEEK_SELECTION: return weekSelection;
            case MONTH_SELECTION: return monthSelection;
            case ALL_SELECTION: return allSelection;
            default: {
                customSelection.createFromSavedPath(path);
                return customSelection;
            }
        }
    }

    public void save() {
        SharedPreferences pref = Preferences.get();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SELECTION_PATH, getPath());
        editor.apply();
    }

    public void setSelectionMode(SelectionMode selectionMode) {
        this.selectionMode = selectionMode;
    }

    public String getPath() {
        return selectionMode.getPath();
    }

    public String getDescription(Context ctx) {
        return selectionMode.getDescription(ctx);
    }


    public AllSelection getAllSelection() {
        return allSelection;
    }

    public MonthSelection getMonthSelection() {
        return monthSelection;
    }

    public TodaySelection getTodaySelection() {
        return todaySelection;
    }

    public WeekSelection getWeekSelection() {
        return weekSelection;
    }

    public CustomSelection getCustomSelection() {
        return customSelection;
    }

    public class TodaySelection extends StaticSelection {

        @Override
        public int getDescriptionId() {
            return R.string.selection_today;
        }

        @Override
        public String getPath() {
            return TODAY_SELECTION;
        }
    }

    public class WeekSelection extends StaticSelection {

        @Override
        public int getDescriptionId() {
            return R.string.selection_week;
        }

        @Override
        public String getPath() {
            return WEEK_SELECTION;
        }
    }


    public class MonthSelection extends StaticSelection {

        @Override
        public int getDescriptionId() {
            return R.string.selection_month;
        }

        @Override
        public String getPath() {
            return MONTH_SELECTION;
        }
    }

    public class AllSelection extends StaticSelection {

        @Override
        public int getDescriptionId() {
            return R.string.selection_all_time;
        }

        @Override
        public String getPath() {
            return ALL_SELECTION;
        }
    }
}
