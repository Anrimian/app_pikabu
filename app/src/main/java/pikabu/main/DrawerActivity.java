package pikabu.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.emirinay.pikabuapp.R;

import pikabu.main.navigation.leftdrawer.LeftNavigationDrawerFragment;
import pikabu.utils.android.ActivityUtils;

/**
 * Created on 26.08.2015.
 */
public abstract class DrawerActivity extends AppCompatActivity {

    private LeftNavigationDrawerFragment leftNavigationDrawerFragment;

    private ToolbarTitleBinder toolbarTitleBinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUtils.setTheme(this);
        setContentView(R.layout.drawer_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitleBinder = new ToolbarTitleBinder(toolbar);

        if (savedInstanceState == null) {
            Fragment fragment = initFragment();
            if (fragment != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_container, fragment)
                        .commit();
            }
        }
        initDrawer();
    }

    private void initDrawer() {
        leftNavigationDrawerFragment = (LeftNavigationDrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.left_drawer);
        leftNavigationDrawerFragment.setUp(R.id.left_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    protected void onResume() {
        super.onResume();
        int intentRootFlags = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
        boolean isRootActivity = isTaskRoot() || getIntent().getFlags() == intentRootFlags;
        leftNavigationDrawerFragment.setDrawerIndicatorEnabled(isRootActivity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isTaskRoot()) {
                    onBackPressed();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        if(isDrawerOpen()){
            leftNavigationDrawerFragment.closeDrawer();
        }
        else if (isTaskRoot()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.exit);
            builder.setMessage(R.string.confirm_exit);
            builder.setPositiveButton(R.string.yes, (dialog, which) -> finish());
            builder.setNegativeButton(R.string.no, null);
            builder.create().show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        toolbarTitleBinder.bindTitle(title);
    }

    public boolean isDrawerOpen() {
        return leftNavigationDrawerFragment.isDrawerOpen();
    }

    public ToolbarTitleBinder getToolbarTitleBinder() {
        return toolbarTitleBinder;
    }

    protected abstract Fragment initFragment();
}