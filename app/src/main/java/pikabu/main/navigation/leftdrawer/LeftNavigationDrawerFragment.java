package pikabu.main.navigation.leftdrawer;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;

import java.util.ArrayList;

import pikabu.content.story.storylist.StoryListActivity;
import pikabu.content.story.storylist.StoryListFragment;
import pikabu.content.story.storylist.tabs.StoryListTabInfo;
import pikabu.content.story.storylist.tabs.StoryTabPagerActivity;
import pikabu.data.Links;
import pikabu.main.sections.search.SearchActivity;
import pikabu.main.sections.search.SearchStoryFragment;
import pikabu.main.sections.commented.CommentedActivity;
import pikabu.main.sections.commented.CommentedFragment;
import pikabu.main.sections.feed.FeedActivity;
import pikabu.main.sections.feed.FeedFragment;
import pikabu.main.sections.messages.MessagesActivity;
import pikabu.main.sections.messages.MessagesFragment;
import pikabu.main.sections.saved.SaveContentFragmentActivity;
import pikabu.main.sections.saved.SavedContentFragment;
import pikabu.main.sections.start.StartStoryListActivity;
import pikabu.main.sections.start.sectionbest.BestStoriesFragment;
import pikabu.main.sections.start.sectionhot.HotStoriesFragment;
import pikabu.main.sections.start.sectionnew.NewStoriesFragment;
import pikabu.main.sections.settings.SettingsActivity;
import pikabu.main.sections.settings.SettingsFragmentContainer;
import pikabu.user.UserModel;
import pikabu.user.info.UserInfoObserver;

/**
 * Created on 20.10.2015.
 */
public class LeftNavigationDrawerFragment extends Fragment implements UserInfoObserver {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;
    private NavigationView navigationView;

    private UserModel user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.left_drawer, container, false);
        navigationView = (NavigationView) view.findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new LeftDrawerOnNavigationItemSelectedListener());
        return view;
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        if (drawerLayout == null) {
            return;
        }
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        mDrawerToggle = new MyActionBarDrawerToggle(getActivity(),
                drawerLayout,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        drawerLayout.post(mDrawerToggle::syncState);
        drawerLayout.addDrawerListener(mDrawerToggle);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        user = UserModel.getInstance();
        user.getUserInfoModel().registerObserver(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        user.getUserInfoModel().unregisterObserver(this);
    }
    @Override
    public void onUpdateUserInfo() {//TODO use rx java observable
        getActivity().runOnUiThread(() -> {
            Menu menu = navigationView.getMenu();
            menu.clear();
            navigationView.inflateMenu(R.menu.left_drawer_actions_menu);
            if (user.isLogin()) {
                navigationView.inflateMenu(R.menu.left_drawer_user_actions_menu);
            }
        });
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isDrawerOpen() && user.isLogin()) {
            inflater.inflate(R.menu.left_drawer_action_bar_menu, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawerToggle != null)
            mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                user.logout();
                return true;
            default: return mDrawerToggle != null
                    && mDrawerToggle.onOptionsItemSelected(item)
                    || super.onOptionsItemSelected(item);
        }

    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null
                && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void closeDrawer() {
        if (mDrawerLayout != null)
            mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    public void setDrawerIndicatorEnabled(Boolean enabled) {
        if (mDrawerToggle != null)
            mDrawerToggle.setDrawerIndicatorEnabled(enabled);
    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    class LeftDrawerOnNavigationItemSelectedListener implements NavigationView.OnNavigationItemSelectedListener {

        @Override
        public boolean onNavigationItemSelected(final MenuItem menuItem) {
            if (mDrawerLayout != null) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
            navigate(menuItem.getItemId());
            return true;
        }

        private void navigate(final int itemId) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            int intentFlags = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
            boolean isRoot = getActivity().isTaskRoot();
            switch (itemId) {
                case R.id.category_hot: {
                   /* Intent intent = new Intent(getActivity(), StartStoryListActivity.class);
                    intent.putExtra(StartStoryListActivity.START_FRAGMENT, StartStoryListActivity.HOT_STORIES);
                    intent.setFlags(intentFlags);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);*/
                    if (isRoot) {
                        Fragment fragment = new HotStoriesFragment();
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), StartStoryListActivity.class);
                        intent.putExtra(StartStoryListActivity.START_FRAGMENT, StartStoryListActivity.HOT_STORIES);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.category_best: {
                    if (isRoot) {
                        Fragment fragment = new BestStoriesFragment();
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), StartStoryListActivity.class);
                        intent.putExtra(StartStoryListActivity.START_FRAGMENT, StartStoryListActivity.BEST_STORIES);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.category_new: {
                    if (isRoot) {
                        Fragment fragment = new NewStoriesFragment();
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), StartStoryListActivity.class);
                        intent.putExtra(StartStoryListActivity.START_FRAGMENT, StartStoryListActivity.NEW_STORIES);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.disputed: {
                    if (isRoot) {
                        Fragment fragment = StoryListFragment.newInstance(null, Links.LINK_DISPUTED, R.string.disputed);
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), StoryListActivity.class);
                        intent.putExtra(StoryListActivity.STORY_LIST_LINK, Links.LINK_DISPUTED);
                        intent.putExtra(StoryListActivity.STORY_LIST_TITLE_ID, R.string.disputed);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.search: {
                    if (isRoot) {
                        SearchStoryFragment fragment = SearchStoryFragment.newInstance();
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, fragment, SearchStoryFragment.SEARCH_FRAGMENT_TAG)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), SearchActivity.class);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.settings: {
                    if (isRoot) {
                        Fragment settingsFragment = new SettingsFragmentContainer();
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, settingsFragment,	SettingsFragmentContainer.SETTINGS_FRAGMENT_TAG)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), SettingsActivity.class);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.feed: {
                    if (isRoot) {
                        Fragment fragment = new FeedFragment();
                        fm.beginTransaction()
                                .replace(R.id.main_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), FeedActivity.class);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.messages: {
                    if (isRoot) {
                        Fragment fragment = new MessagesFragment();
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), MessagesActivity.class);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.comments: {
                    if (isRoot) {
                        Fragment fragment = new CommentedFragment();
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), CommentedActivity.class);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.voted_stories: {
                    ArrayList<StoryListTabInfo> tabInfoList = new ArrayList<>();
                    tabInfoList.add(new StoryListTabInfo(null, Links.LINK_VOTED_STORY_UP, R.string.like));
                    tabInfoList.add(new StoryListTabInfo(null, Links.LINK_VOTED_STORY_DOWN, R.string.dont_like));
                    if (isRoot) {
                        Fragment fragment = pikabu.content.story.storylist.tabs.StoryTabPagerFragment
                                .newInstance(tabInfoList, R.string.votes);
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), StoryTabPagerActivity.class);
                        intent.putExtra(StoryListTabInfo.TAB_INFO_LIST, tabInfoList);
                        intent.putExtra(pikabu.content.story.storylist.tabs.StoryTabPagerFragment.TAB_PAGER_TITLE, R.string.votes);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                case R.id.saved_stories: {
                    if (isRoot) {
                        Fragment fragment = new SavedContentFragment();
                        fm.beginTransaction()
                                .setCustomAnimations(R.anim.show_fragment, 0)
                                .replace(R.id.main_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(getActivity(), SaveContentFragmentActivity.class);
                        intent.setFlags(intentFlags);
                        startActivity(intent);
                    }
                    break;
                }
                default:
                    Toast.makeText(getActivity(), R.string.unknown_menu_item, Toast.LENGTH_SHORT).show();
            }
        }
    }

    class MyActionBarDrawerToggle extends ActionBarDrawerToggle {

        public MyActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            if (!isAdded()) {
                return;
            }
            getActivity().supportInvalidateOptionsMenu();
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            if (!isAdded()) {
                return;
            }
            getActivity().supportInvalidateOptionsMenu();
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }
}