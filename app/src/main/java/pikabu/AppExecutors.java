package pikabu;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created on 18.05.2016.
 */
public class AppExecutors {

    public static ExecutorService STORY_LOADING_EXECUTOR = Executors.newSingleThreadExecutor();
    public static ExecutorService DB_EXECUTOR = Executors.newSingleThreadExecutor();
    public static ExecutorService COMMENTS_EXECUTOR = Executors.newFixedThreadPool(4);
    public static ExecutorService GIF_EXECUTOR = Executors.newFixedThreadPool(1);

    public static Scheduler STORY_LOADING_SCHEDULER = Schedulers.from(STORY_LOADING_EXECUTOR);
    public static Scheduler DB_SCHEDULER = Schedulers.from(DB_EXECUTOR);
    public static Scheduler COMMENTS_SCHEDULER = Schedulers.from(COMMENTS_EXECUTOR);
    public static Scheduler GIF_SCHEDULER = Schedulers.from(GIF_EXECUTOR);
}
