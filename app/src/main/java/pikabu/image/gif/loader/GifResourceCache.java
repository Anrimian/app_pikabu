package pikabu.image.gif.loader;

import android.support.v4.util.LruCache;

import pikabu.image.gif.GifModelCache;

/**
 * Created on 02.11.2016.
 */

public class GifResourceCache {

    private volatile static GifResourceCache instance;

    private LruCache<String, GifResource> gifDrawableCache;

    private GifResourceCache() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory());
        final int cacheSize = maxMemory / 5;
        gifDrawableCache = new LruCache<String, GifResource>(cacheSize){
            @Override
            protected int sizeOf(String key, GifResource gifResource) {
                return (int) gifResource.getSize();
            }

            @Override
            protected void entryRemoved(boolean evicted, String key, GifResource oldValue, GifResource newValue) {
                GifModelCache.getGifStateModel(key).stopPlayingGif();
            }
        };
    }

    public static GifResourceCache getInstance() {
        if (instance == null) {
            synchronized (GifResourceCache.class) {
                if (instance == null) {
                    instance = new GifResourceCache();
                }
            }
        }
        return instance;
    }

    public GifResource getGifResource(String link) {
        return gifDrawableCache.get(link);
    }

    public void putGifResource(String link, GifResource gifResource) {
        gifDrawableCache.put(link, gifResource);
        GifModelCache.getGifStateModel(link).onResourceReady(gifResource.getGifDrawable());
    }

}
