package pikabu.image.gif.loader;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created on 08.11.2016.
 */

public class GifResource {

    private GifDrawable gifDrawable;
    private byte[] gifBytes;

    public GifResource(byte[] gifBytes, GifDrawable gifDrawable) {
        this.gifBytes = gifBytes;
        this.gifDrawable = gifDrawable;
    }

    public byte[] getGifBytes() {
        return gifBytes;
    }

    public GifDrawable getGifDrawable() {
        return gifDrawable;
    }

    public long getSize() {
        return gifBytes.length + gifDrawable.getInputSourceByteCount();
    }
}
