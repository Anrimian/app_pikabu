package pikabu.image.gif.loader;

import com.emirinay.tools.IoUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pikabu.AppExecutors;
import pikabu.utils.network.MyHttpClient;
import pl.droidsonroids.gif.GifDrawable;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created on 08.11.2016.
 */

public class GifLoader {

    private Map<String, Observable> observableMap = new ConcurrentHashMap<>();

    private volatile static GifLoader instance;

    public static GifLoader getInstance() {
        if (instance == null) {
            synchronized (GifLoader.class) {
                if (instance == null) {
                    instance = new GifLoader();
                }
            }
        }
        return instance;
    }

    public Observable<GifResource> getGifResourceObservable(String link) {
        Observable<GifResource> observable;
        GifResource gifResource = GifResourceCache.getInstance().getGifResource(link);
        if (gifResource != null) {
            observable = Observable.just(gifResource);
        } else {
            observable = observableMap.get(link);
            if (observable == null) {
                observable = prepareGifResourceObservable(link);
                observableMap.put(link, observable);
            }
        }
        return observable;
    }

    private Observable<GifResource> prepareGifResourceObservable(String link) {
        return createGifResourceObservable(link)
                .subscribeOn(AppExecutors.GIF_SCHEDULER)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext((gifResource -> GifResourceCache.getInstance().putGifResource(link, gifResource)))
                .doOnCompleted(() -> observableMap.remove(link))
                .share();
    }

    private Observable<GifResource> createGifResourceObservable(String link) {
        return Observable.create(subscriber -> {
            if (subscriber.isUnsubscribed()) {
                subscriber.onCompleted();
                return;
            }
            InputStream is = null;
            ByteArrayOutputStream os = null;
            try {
                OkHttpClient client = MyHttpClient.getInstance().getOkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                is = response.body().byteStream();
                os = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;
                while ((count = is.read(buffer)) != -1) {
                    os.write(buffer, 0, count);
                    if (subscriber.isUnsubscribed()) {
                        return;
                    }
                }
                byte[] gifBytes = os.toByteArray();
                GifDrawable gifDrawable = new GifDrawable(gifBytes);
                subscriber.onNext(new GifResource(gifBytes, gifDrawable));
            } catch (IOException e) {
                subscriber.onError(e);
            } finally {
                IoUtils.closeSilently(is);
                IoUtils.closeSilently(os);
                subscriber.onCompleted();
            }
        });
    }
}
