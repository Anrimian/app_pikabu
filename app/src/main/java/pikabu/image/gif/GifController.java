package pikabu.image.gif;

import pikabu.image.ImageSize;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.MultiCallback;

/**
 * Created on 30.10.2016.
 */

public class GifController implements GifStateObserver, GifActions {

    private GifViewHolder gifViewHolder;
    private GifModel model;

    public GifController(GifViewHolder gifViewHolder) {
        this.gifViewHolder = gifViewHolder;
    }

    public void bindGif(String gifLink, ImageSize previewImageSize) {
        gifViewHolder.bindGif(gifLink, previewImageSize, this);
        model = GifModelCache.getGifStateModel(gifLink);
        model.registerObserver(this);
    }

    public void clear() {
        model.unregisterObserver(this);
        gifViewHolder.clear();
        model.clear();
    }

    public void start() {
        if (model.getGifState() != GifState.LOADING) {
            model.playGif();
        }
    }

    @Override
    public void pause() {
        model.pauseGif();
    }

    @Override
    public void stop() {
        model.stopPlayingGif();
    }

    @Override
    public void changeGifState(boolean pause) {
        switch (model.getGifState()) {
            case LOADING: {
                cancelLoadingGif();
                break;
            }
            case PLAY: {
                if (pause) {
                    pause();
                } else {
                    stop();
                }
                break;
            }
            case PAUSE:
            case STOP: {
                start();
                break;
            }
            case ERROR: {
                start();
                break;
            }
        }
    }

    public void cancelLoadingGif() {
        model.cancelLoadingGif();
    }

    @Override
    public void useTrickToUnfreezeGif() {
        model.restartFreezeGif();
    }

    @Override
    public void onLoading() {
        gifViewHolder.showLoadingState();
    }

    @Override
    public void onPlay() {
        gifViewHolder.showPlayState();
    }

    @Override
    public void onPause() {
        gifViewHolder.showPauseState();
    }

    @Override
    public void onStop() {
        gifViewHolder.showStopState();
    }

    @Override
    public void onError() {
        gifViewHolder.showErrorState();
    }

    @Override
    public void onLoaded(int progress) {
        gifViewHolder.onLoaded(progress);
    }

    @Override
    public void setGifDrawable(GifDrawable gifDrawable, MultiCallback multiCallback) {
        gifViewHolder.setGifDrawable(gifDrawable, multiCallback);
    }

    public GifState getGifState() {
        return model.getGifState();
    }
}
