package pikabu.image.gif;

import android.util.LruCache;

/**
 * Created on 27.10.2016.
 */

public class GifModelCache {

    private static LruCache<String, GifModel> gifStateCache = new LruCache<String, GifModel>(30) {
        @Override
        protected void entryRemoved(boolean evicted, String key, GifModel oldValue, GifModel newValue) {
            super.entryRemoved(evicted, key, oldValue, newValue);
            oldValue.cancelLoadingGif();
        }
    };

    public static GifModel getGifStateModel(String gifLink) {
        GifModel model = gifStateCache.get(gifLink);
        if (model == null) {
            model = new GifModel(gifLink);
            gifStateCache.put(gifLink, model);
        }
        return model;
    }
}
