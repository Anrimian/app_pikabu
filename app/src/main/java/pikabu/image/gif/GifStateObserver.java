package pikabu.image.gif;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.MultiCallback;

/**
 * Created on 30.10.2016.
 */

public interface GifStateObserver {

    void onLoading();

    void onPlay();

    void onPause();

    void onStop();

    void onError();

    void onLoaded(int progress);

    void setGifDrawable(GifDrawable gifDrawable, MultiCallback multiCallback);
}
