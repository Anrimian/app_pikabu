package pikabu.image.gif;

/**
 * Created on 30.10.2016.
 */

public interface GifActions {

    void start();

    void pause();

    void stop();

    void changeGifState(boolean pause);

    void useTrickToUnfreezeGif();
}
