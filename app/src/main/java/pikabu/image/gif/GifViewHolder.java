package pikabu.image.gif;

import pikabu.image.ImageSize;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.MultiCallback;

/**
 * Created on 06.11.2016.
 */

public interface GifViewHolder {

    void bindGif(String gifLink, ImageSize previewImageSize, GifActions gifController);

    void clear();

    void showLoadingState();

    void showPlayState();

    void showPauseState();

    void showStopState();

    void showErrorState();

    void onLoaded(int progress);

    void setGifDrawable(GifDrawable gifDrawable, MultiCallback multiCallback);


}
