package pikabu.image.gif.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.AndroidUtils;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrListener;
import com.r0adkll.slidr.model.SlidrPosition;

import pikabu.image.gif.GifController;
import pikabu.image.gif.GifState;
import pikabu.utils.FileUtils;

/**
 * Created on 06.11.2016.
 */

public class GifControlActivity extends AppCompatActivity {

    public static final String GIF_LINK = "gif_link";

    private String gifLink;

    private GifControlViewHolder gifControlViewHolder;
    private GifController gifController;

    private View backgroundView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(this, R.layout.gif_control_view, null);
        gifControlViewHolder = new GifControlViewHolder(view);
        setContentView(view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        SlidrConfig config = new SlidrConfig.Builder()
                .primaryColor(ContextCompat.getColor(this, R.color.status_bar_color))
                .secondaryColor(ContextCompat.getColor(this, R.color.zoom_image_status_bar_color))
                .position(SlidrPosition.VERTICAL)
                .listener(new MySlidrListener())
                .build();
        Slidr.attach(this, config);
        backgroundView = findViewById(R.id.background_view);

        gifLink = getIntent().getStringExtra(GIF_LINK);
        gifController = new GifController(gifControlViewHolder);
        gifController.bindGif(gifLink, null);
        gifController.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gifController.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.image_actions_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.save_image: {
                saveGif();
                return true;
            }
            case R.id.share_link: {
                AndroidUtils.shareText(gifLink, this);
                return true;
            }
            case R.id.copy_link: {
                AndroidUtils.copyText(gifLink, this);
                return true;
            }
            case R.id.share_image: {
                AndroidUtils.loadAndShareGif(gifLink, this);
                return true;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private void saveGif() {
        Toast.makeText(this, R.string.saving, Toast.LENGTH_SHORT).show();
        FileUtils.loadAndStoreGif(gifLink, this);
    }

    private class MySlidrListener implements SlidrListener {

        @Override
        public void onSlideChange(float percent) {
            backgroundView.setAlpha(percent);
        }

        @Override
        public void onSlideStateChanged(int state) {
            GifState gifState = gifController.getGifState();
            if (state == ViewDragHelper.STATE_DRAGGING) {
                gifControlViewHolder.lock();
            } else if (gifState != GifState.LOADING && gifState != GifState.ERROR){
                gifControlViewHolder.unlock();
            }
        }

        @Override
        public void onSlideOpened() {
        }

        @Override
        public void onSlideClosed() {
        }
    }
}
