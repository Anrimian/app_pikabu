package pikabu.image.gif.activity;

import android.view.View;
import android.widget.ImageView;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ViewUtils;

import pikabu.image.ImageSize;
import pikabu.image.gif.GifActions;
import pikabu.image.gif.GifViewHolder;
import pikabu.image.zoom.ImageProgressView;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import pl.droidsonroids.gif.MultiCallback;

/**
 * Created on 06.11.2016.
 */

public class GifControlViewHolder implements GifViewHolder{

    private GifMediaController mediaController;
    private GifImageView gifImageView;
    private ImageProgressView progressViewBinder;
    private ImageView ivGifPlayIcon;

    private GifActions gifActions;

    public GifControlViewHolder(View view) {
        gifImageView = (GifImageView) view.findViewById(R.id.gif_image_view);
        gifImageView.addOnAttachStateChangeListener(new GifAttachStateListener());
        gifImageView.setOnClickListener(new GifOnClickListener());
        View progressStateView = view.findViewById(R.id.progress_state_view);
        progressViewBinder = new ImageProgressView(progressStateView);
        progressViewBinder.setTryAgainButtonOnClickListener((v) -> gifActions.start());
        ivGifPlayIcon = (ImageView) view.findViewById(R.id.iv_icon_play);
        mediaController = (GifMediaController) view.findViewById(R.id.media_controller);
    }

    @Override
    public void bindGif(String gifLink, ImageSize previewImageSize, GifActions gifActions) {
        this.gifActions = gifActions;
        mediaController.setGifActions(gifActions);
    }

    @Override
    public void clear() {
        mediaController.stopTrackingGif();
    }

    @Override
    public void showLoadingState() {
        gifImageView.setVisibility(View.GONE);
        mediaController.setVisibility(View.GONE);
        progressViewBinder.showProgress();
    }

    @Override
    public void showPlayState() {
        gifImageView.setVisibility(View.VISIBLE);
        mediaController.setVisibility(View.VISIBLE);
        mediaController.showPauseButton();
        progressViewBinder.hideAll();
        mediaController.startTrackingGif();
        ivGifPlayIcon.setImageResource(R.drawable.play);
    }

    @Override
    public void showPauseState() {
        gifImageView.setVisibility(View.VISIBLE);
        mediaController.setVisibility(View.VISIBLE);
        mediaController.showPlayButton();
        progressViewBinder.hideAll();
        ivGifPlayIcon.setImageResource(R.drawable.pause);
    }

    @Override
    public void showStopState() {
        gifImageView.setVisibility(View.VISIBLE);
        mediaController.setVisibility(View.VISIBLE);
        mediaController.showPlayButton();
        progressViewBinder.hideAll();
    }

    @Override
    public void showErrorState() {
        gifImageView.setVisibility(View.GONE);
        mediaController.setVisibility(View.GONE);
        progressViewBinder.showMessage(R.string.error, true);
    }

    @Override
    public void onLoaded(int progress) {
        progressViewBinder.onLoaded(progress);
    }

    @Override
    public void setGifDrawable(GifDrawable gifDrawable, MultiCallback multiCallback) {
        gifImageView.setImageDrawable(gifDrawable);
        multiCallback.addView(gifImageView);
        gifDrawable.setCallback(multiCallback);
        mediaController.setMediaPlayer(gifDrawable);
    }

    public void lock() {
        mediaController.setVisibility(View.GONE);
    }

    public void unlock() {
        mediaController.setVisibility(View.VISIBLE);
    }

    private class GifAttachStateListener implements View.OnAttachStateChangeListener {
        @Override
        public void onViewAttachedToWindow(View v) {
        }

        @Override
        public void onViewDetachedFromWindow(View v) {
            if (gifActions != null) {
                gifActions.useTrickToUnfreezeGif();
            }
        }
    }

    private class GifOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            ViewUtils.animateFadeOutImageView(ivGifPlayIcon);
            gifActions.changeGifState(true);
        }
    }
}
