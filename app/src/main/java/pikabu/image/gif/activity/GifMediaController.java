package pikabu.image.gif.activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ViewUtils;

import java.util.Formatter;
import java.util.Locale;

import pikabu.image.gif.GifActions;

/**
 * Created on 07.11.2016.
 */

public class GifMediaController extends FrameLayout {

    private static final int SHOW_PROGRESS = 2;

    private MediaController.MediaPlayerControl mediaPlayerControl;

    private SeekBar seekBar;
    private TextView tvCurrentTime;
    private TextView tvEndTime;
    private ImageView ivPlay;

    private StringBuilder mFormatBuilder;
    private Formatter mFormatter;
    private GifActions gifActions;

    public GifMediaController(Context context) {
        super(context);
        initView();
    }

    public GifMediaController(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public GifMediaController(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        View view = View.inflate(getContext(), R.layout.simple_media_controller, this);
        seekBar = (SeekBar) view.findViewById(R.id.seek_bar);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
        seekBar.setMax(1000);
        tvCurrentTime = (TextView) view.findViewById(R.id.tv_current_time);
        tvEndTime = (TextView) view.findViewById(R.id.tv_end_time);

        ivPlay = (ImageView) view.findViewById(R.id.iv_play);

        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
    }

    public void setMediaPlayer(MediaController.MediaPlayerControl mediaPlayerControl) {
        this.mediaPlayerControl = mediaPlayerControl;
        startTrackingGif();
    }

    public void setGifActions(GifActions gifActions) {
        this.gifActions = gifActions;
        ivPlay.setOnClickListener((view) -> gifActions.changeGifState(true));
    }

    public void startTrackingGif() {
        mHandler.sendEmptyMessage(SHOW_PROGRESS);
    }

    public void stopTrackingGif() {
        mHandler.removeMessages(SHOW_PROGRESS);
    }

    public void showPauseButton() {
        ViewUtils.imageViewAnimatedChange(getContext(), ivPlay, R.drawable.pause);
    }

    public void showPlayButton() {
        ViewUtils.imageViewAnimatedChange(getContext(), ivPlay, R.drawable.play);
    }

    private String stringForTime(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours   = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    private int setProgress() {
        if (mediaPlayerControl == null || mDragging) {
            return 0;
        }
        int position = mediaPlayerControl.getCurrentPosition();
        int duration = mediaPlayerControl.getDuration();
        if (duration > 0) {
            // use long to avoid overflow
            long pos = 1000L * position / duration;
            seekBar.setProgress( (int) pos);
        }
        int percent = mediaPlayerControl.getBufferPercentage();
        seekBar.setSecondaryProgress(percent * 10);

        tvEndTime.setText(stringForTime(duration));
        tvCurrentTime.setText(stringForTime(position));

        return position;
    }

    private boolean mDragging;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SHOW_PROGRESS:
                    if (!mDragging && mediaPlayerControl.isPlaying()) {
                        setProgress();
                        msg = obtainMessage(SHOW_PROGRESS);
                        sendMessageDelayed(msg, 100);
                    }
                    break;
            }
        }
    };

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (!fromUser) {
                return;
            }
            long duration = mediaPlayerControl.getDuration();
            int position = (int) ((duration * progress) / 1000L);
            mediaPlayerControl.seekTo(position);
            tvCurrentTime.setText(stringForTime(position));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            mDragging = true;
            mHandler.removeMessages(SHOW_PROGRESS);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mDragging = false;
            setProgress();
            mHandler.sendEmptyMessage(SHOW_PROGRESS);
        }
    };
}
