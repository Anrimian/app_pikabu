package pikabu.image.gif;

import android.support.annotation.Nullable;

import pikabu.Preferences;
import pikabu.image.gif.loader.GifLoader;
import pikabu.image.gif.loader.GifResource;
import pikabu.image.gif.loader.GifResourceCache;
import pikabu.utils.network.progress.LoadingObserver;
import pikabu.utils.network.progress.LoadingProgressObservable;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.MultiCallback;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created on 30.10.2016.
 */

public class GifModel {
    private Subscription subscription;

    private GifStateObservable observable;
    private GifState gifState;
    private GifLoadingObserver loadingObserver;

    private MultiCallback multiCallback;

    private String link;

    public GifModel(String link) {
        this.link = link;
        observable = new GifStateObservable();
        multiCallback = new MultiCallback();
        loadingObserver = new GifLoadingObserver();
        LoadingProgressObservable.getInstance().registerLoadingObserver(loadingObserver);
        if (getGifDrawable() != null && autoLoading()) {
            playGif();
        } else {
            gifState = GifState.STOP;
        }
    }

    public void registerObserver(GifStateObserver observer) {
        GifDrawable gifDrawable = getGifDrawable();
        if (gifDrawable != null) {
            observer.setGifDrawable(gifDrawable, multiCallback);
        } else if (gifState == GifState.PLAY) {
            showState(GifState.STOP);
        } else if (gifState == GifState.STOP && autoLoading()) {
            playGif();
        }

        gifState.showState(observer);
        observer.onLoaded(loadingObserver.progress);
        observable.registerObserver(observer);
    }

    public void unregisterObserver(GifStateObserver observer) {
        observable.unregisterObserver(observer);
    }

    public void playGif() {
        GifDrawable gifDrawable = getGifDrawable();
        if (gifDrawable == null) {
            showState(GifState.LOADING);
            loadGif();
        } else {
            showState(GifState.PLAY);
            gifDrawable.start();
        }
    }

    public void stopPlayingGif() {
        showState(GifState.STOP);
        GifDrawable gifDrawable = getGifDrawable();
        if (gifDrawable != null) {
            gifDrawable.stop();
            gifDrawable.seekTo(0);
        }
    }

    public void pauseGif() {
        showState(GifState.PAUSE);
        GifDrawable gifDrawable = getGifDrawable();
        if (gifDrawable != null) {
            gifDrawable.stop();
        }
    }

    public void cancelLoadingGif() {
        showState(GifState.STOP);
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    public void showState(GifState gifState) {
        this.gifState = gifState;
        observable.showState(gifState);
    }

    public GifState getGifState() {
        return gifState;
    }

    public void clear() {
        if (autoLoading() && gifState == GifState.LOADING) {
            cancelLoadingGif();
        }
    }

    private boolean autoLoading() {
        return Preferences.get().getBoolean("auto_loading_gif", false);
    }

    @Nullable
    private GifDrawable getGifDrawable() {
        GifResource gifResource = GifResourceCache.getInstance().getGifResource(link);
        if (gifResource != null) {
            return gifResource.getGifDrawable();
        }
        return null;
    }

    private void loadGif() {
        subscription = GifLoader.getInstance().getGifResourceObservable(link)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(gifResource -> {
                    showState(GifState.PLAY);
                }, e -> {
                    e.printStackTrace();
                    showState(GifState.ERROR);
                });
    }

    public void onResourceReady(GifDrawable gifDrawable) {
        observable.setGifDrawable(gifDrawable, multiCallback);
    }

    public void restartFreezeGif() {
        if (gifState == GifState.PLAY) {
            showState(gifState);
        }
    }

    private class GifLoadingObserver implements LoadingObserver {

        private int progress;

        @Override
        public void onLoaded(int progress) {
            this.progress = progress;
            observable.onLoaded(progress);
        }

        @Override
        public void onCompleted() {
            this.progress = 0;
            observable.onLoaded(progress);
        }

        @Override
        public int getObserverId() {
            return hashCode();
        }

        @Override
        public String getLink() {
            return link;
        }
    }


}
