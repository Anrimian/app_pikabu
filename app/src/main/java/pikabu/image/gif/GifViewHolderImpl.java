package pikabu.image.gif;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.emirinay.pikabuapp.R;

import pikabu.image.ImageSize;
import pikabu.image.gif.activity.GifControlActivity;
import pikabu.image.glide.image.MyGlideImageLoader;
import pikabu.image.glide.image.MyImageViewTarget;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import pl.droidsonroids.gif.MultiCallback;

/**
 * Created on 27.10.2016.
 */

public class GifViewHolderImpl implements GifViewHolder {

    private View gifIcon;
    private GifImageView gifImageView;
    private ImageView previewImageView;
    private TextView tvStatus;
    private ProgressBar progressBar;

    private MyImageViewTarget previewImageViewTarget;

    private String gifLink;
    private GifActions gifActions;

    public GifViewHolderImpl(View view) {
        gifIcon = view.findViewById(R.id.gif_icon);
        gifImageView = (GifImageView) view.findViewById(R.id.gif_image_view);
        gifImageView.addOnAttachStateChangeListener(new GifAttachStateListener());
        previewImageView = (ImageView) view.findViewById(R.id.iv_preview_gif_image);
        tvStatus = (TextView) view.findViewById(R.id.tv_gif_status);
        progressBar = (ProgressBar) view.findViewById(R.id.pb_gif);

        View clickableView = view.findViewById(R.id.gif_container);
        clickableView.setOnClickListener(new GifOnClickListener());
        clickableView.setOnLongClickListener(new GifLongClickListener());

    }

    @Override
    public void bindGif(String gifLink, ImageSize imageSize, GifActions gifActions) {
        this.gifActions = gifActions;
        this.gifLink = gifLink;
        displayPreviewImage(imageSize);
    }

    @Override
    public void clear() {
        gifImageView.setImageDrawable(null);
        Glide.clear(previewImageViewTarget);
    }

    @Override
    public void showLoadingState() {
        gifIcon.setVisibility(View.VISIBLE);
        gifImageView.setVisibility(View.GONE);
        previewImageView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        tvStatus.setVisibility(View.VISIBLE);
        tvStatus.setText(R.string.loading);
    }

    @Override
    public void showPlayState() {
        gifIcon.setVisibility(View.GONE);
        gifImageView.setVisibility(View.VISIBLE);
        previewImageView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        tvStatus.setVisibility(View.GONE);
    }

    @Override
    public void showPauseState() {
        gifIcon.setVisibility(View.VISIBLE);
        gifImageView.setVisibility(View.VISIBLE);
        previewImageView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        tvStatus.setVisibility(View.GONE);
    }

    @Override
    public void showStopState() {
        gifIcon.setVisibility(View.VISIBLE);
        gifImageView.setVisibility(View.GONE);
        previewImageView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        tvStatus.setVisibility(View.GONE);
    }

    @Override
    public void showErrorState() {
        gifIcon.setVisibility(View.GONE);
        gifImageView.setVisibility(View.GONE);
        previewImageView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        tvStatus.setVisibility(View.VISIBLE);
        tvStatus.setText(R.string.error);
    }

    @Override
    public void setGifDrawable(GifDrawable gifDrawable, MultiCallback multiCallback) {
        gifImageView.setImageDrawable(gifDrawable);
        multiCallback.addView(gifImageView);
        gifDrawable.setCallback(multiCallback);
    }

    @Override
    public void onLoaded(int progress) {
        progressBar.setProgress(progress);
    }

    private void displayPreviewImage(ImageSize imageSize) {
        previewImageViewTarget = MyGlideImageLoader.displayImage(gifLink, previewImageView, imageSize);
    }

    private class GifAttachStateListener implements View.OnAttachStateChangeListener {
        @Override
        public void onViewAttachedToWindow(View v) {
        }

        @Override
        public void onViewDetachedFromWindow(View v) {
            if (gifActions != null) {
                gifActions.useTrickToUnfreezeGif();
            }
        }
    }

    private class GifOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            gifActions.changeGifState(false);
        }
    }

    private class GifLongClickListener implements View.OnLongClickListener {
        @Override
        public boolean onLongClick(View v) {
            Context ctx = gifImageView.getContext();
            Intent intent = new Intent(ctx, GifControlActivity.class);
            intent.putExtra(GifControlActivity.GIF_LINK, gifLink);
            ctx.startActivity(intent);
            return true;
        }
    }
}
