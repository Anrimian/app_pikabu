package pikabu.image.gif;

import com.emirinay.tools.observer.WeakObservable;

import java.lang.ref.WeakReference;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.MultiCallback;

/**
 * Created on 30.10.2016.
 */

public class GifStateObservable extends WeakObservable<GifStateObserver> {

    public void showState(GifState gifState) {
        for (WeakReference<GifStateObserver> observerRef : mObservers) {
            GifStateObserver observer = observerRef.get();
            if (observer != null) {
                gifState.showState(observer);
            } else {
                mObservers.remove(observerRef);
            }
        }
    }

    public void setGifDrawable(GifDrawable gifDrawable, MultiCallback multiCallback) {
        for (WeakReference<GifStateObserver> observerRef : mObservers) {
            GifStateObserver observer = observerRef.get();
            if (observer != null) {
                observer.setGifDrawable(gifDrawable, multiCallback);
            } else {
                mObservers.remove(observerRef);
            }
        }
    }

    public void onLoaded(int progress) {
        for (WeakReference<GifStateObserver> observerRef : mObservers) {
            GifStateObserver observer = observerRef.get();
            if (observer != null) {
                observer.onLoaded(progress);
            } else {
                mObservers.remove(observerRef);
            }
        }
    }
}
