package pikabu.image.gif;

/**
 * Created on 30.10.2016.
 */

public enum GifState {
    LOADING {
        @Override
        public void showState(GifStateObserver observer) {
            observer.onLoading();
        }
    },
    PLAY {
        @Override
        public void showState(GifStateObserver observer) {
            observer.onPlay();
        }
    },
    STOP {
        @Override
        public void showState(GifStateObserver observer) {
            observer.onStop();
        }
    },
    ERROR {
        @Override
        public void showState(GifStateObserver observer) {
            observer.onError();
        }
    },
    PAUSE {
        @Override
        public void showState(GifStateObserver observer) {
            observer.onPause();
        }
    };

    public void showState(GifStateObserver observer) {

    }
}
