package pikabu.image.glide;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.GlideModule;

import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import pikabu.image.glide.progress.GlideLoadingObservableManager;
import pikabu.utils.network.progress.ProgressNetworkInterceptor;

/**
 * Created on 06.10.2016.
 */

public class MyGlideModule implements GlideModule {

    private static final long CONNECT_TIMEOUT_TIME = 15;
    private static final long READ_TIMEOUT_TIME = 60;

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory());
        final int cacheSize = maxMemory / 5;
        builder.setMemoryCache(new LruResourceCache(cacheSize));
    }

    @Override
    public void registerComponents(Context context, Glide glide) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_TIME, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT_TIME, TimeUnit.SECONDS)
                .addNetworkInterceptor(new ProgressNetworkInterceptor(GlideLoadingObservableManager.getInstance()))
                .build();

        MyOkHttpUrlLoader.Factory factory = new MyOkHttpUrlLoader.Factory(client);
        glide.register(GlideUrl.class, InputStream.class, factory);
    }
}
