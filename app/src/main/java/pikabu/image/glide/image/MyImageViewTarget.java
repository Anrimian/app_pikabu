package pikabu.image.glide.image;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.ImageViewTarget;

import pikabu.image.ImageSize;
import pikabu.image.LongStoryCallback;
import pikabu.image.glide.image.resources.target.GlideTileBitmapDrawable;

/**
 * Created on 19.10.2016.
 */


public class MyImageViewTarget extends ImageViewTarget<GlideDrawable> {

    private boolean showLongImage;

    private LongStoryCallback longStoryCallback;

    private ImageSize imageSize;

    public MyImageViewTarget(ImageView view) {
        super(view);
    }

    public MyImageViewTarget(ImageView view, ImageSize imageSize, boolean showLongImage, LongStoryCallback longStoryCallback) {
        super(view);
        this.imageSize = imageSize;
        this.showLongImage = showLongImage;
        this.longStoryCallback = longStoryCallback;
    }

    public void setDisplayInfo(ImageSize imageSize, boolean showLongImage, LongStoryCallback longStoryCallback) {
        this.showLongImage = showLongImage;
        this.longStoryCallback = longStoryCallback;
        this.imageSize = imageSize;
    }

    @Override
    public void onLoadFailed(Exception e, Drawable errorDrawable) {
        e.printStackTrace();
        super.onLoadFailed(e, errorDrawable);
    }

    @Override
    protected void setResource(GlideDrawable resource) {
        if (resource instanceof GlideTileBitmapDrawable && !showLongImage) {
            view.setClickable(false);
            if (longStoryCallback != null) {
                longStoryCallback.showLongStoryPicture();
            }
            resource = ((GlideTileBitmapDrawable) resource).getPreviewDrawable();
        }
        view.setImageDrawable(resource);
        imageSize.setPreviewWidth(resource.getIntrinsicWidth());
        imageSize.setPreviewHeight(resource.getIntrinsicHeight());
        view.setBackgroundColor(Color.TRANSPARENT);
    }
}
