package pikabu.image.glide.image.resources.target;

import android.graphics.Bitmap;

import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.util.Util;

/**
 * Created on 03.10.2016.
 */

public class GlideDrawableResource implements Resource<GlideDrawable> {

    private BitmapPool bitmapPool;

    private GlideDrawable glideDrawable;

    private Bitmap bitmap;

    public GlideDrawableResource(Bitmap bitmap, BitmapPool bitmapPool) {
        this.bitmapPool = bitmapPool;
        this.bitmap = bitmap;
        glideDrawable = new GlideBitmapDrawable(null, bitmap);
    }

    @Override
    public GlideDrawable get() {
        return glideDrawable;
    }

    @Override
    public int getSize() {
        return Util.getBitmapByteSize(bitmap);
    }

    @Override
    public void recycle() {
        bitmapPool.put(bitmap);
    }
}
