package pikabu.image.glide.image.resources.target;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.Gravity;

import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.util.Util;

import java.util.ArrayList;

/**
 * Created on 04.10.2016.
 */

public class GlideTileBitmapDrawable extends GlideDrawable {

    private final Rect destRect = new Rect();

    private int width;
    private int height;

    private boolean applyGravity;
    private boolean mutated;

    private int size;

    private GlideTileBitmapDrawable.BitmapState state;

    public GlideTileBitmapDrawable(ArrayList<Bitmap> bitmapList, Bitmap previewBitmap) {
        this(new GlideTileBitmapDrawable.BitmapState(bitmapList, new GlideBitmapDrawable(null, previewBitmap)));
    }

    GlideTileBitmapDrawable(GlideTileBitmapDrawable.BitmapState state) {
        if (state == null) {
            throw new NullPointerException("BitmapState must not be null");
        }

        this.state = state;
        final int targetDensity = state.targetDensity;

        width = state.bitmapList.get(0).getScaledWidth(targetDensity);
        for (Bitmap bitmap : state.bitmapList) {
            size += Util.getBitmapByteSize(bitmap);
            height += bitmap.getScaledHeight(targetDensity);
        }
    }

    @Override
    public int getIntrinsicWidth() {
        return width;
    }

    @Override
    public int getIntrinsicHeight() {
        return height;
    }

    @Override
    public boolean isAnimated() {
        return false;
    }

    @Override
    public void setLoopCount(int loopCount) {
        // Do nothing.
    }

    @Override
    public void start() {
        // Do nothing.
    }

    @Override
    public void stop() {
        // Do nothing.
    }

    @Override
    public boolean isRunning() {
        return false;
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        applyGravity = true;
    }

    @Override
    public ConstantState getConstantState() {
        return state;
    }

    @Override
    public void draw(Canvas canvas) {
        if (applyGravity) {
            Gravity.apply(GlideTileBitmapDrawable.BitmapState.GRAVITY, width, height, getBounds(), destRect);
            applyGravity = false;
        }
        int height = 0;
        for (Bitmap bitmap : state.bitmapList) {
            canvas.drawBitmap(bitmap, 0, height, state.paint);
            height += bitmap.getHeight();
        }
    }

    @Override
    public void setAlpha(int alpha) {
        int currentAlpha = state.paint.getAlpha();
        if (currentAlpha != alpha) {
            state.setAlpha(alpha);
            invalidateSelf();
        }
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        state.setColorFilter(colorFilter);
        invalidateSelf();
    }

    @Override
    public int getOpacity() {
        Bitmap bm = state.bitmapList.get(0);
        return bm == null || bm.hasAlpha() || state.paint.getAlpha() < 255
                ? PixelFormat.TRANSLUCENT : PixelFormat.OPAQUE;
    }

    @Override
    public Drawable mutate() {
        if (!mutated && super.mutate() == this) {
            state = new GlideTileBitmapDrawable.BitmapState(state);
            mutated = true;
        }
        return this;
    }

    public ArrayList<Bitmap> getBitmapList() {
        return state.bitmapList;
    }

    public GlideDrawable getPreviewDrawable() {
        return state.previewDrawable;
    }

    public int getSize() {
        return size;
    }

    public Bitmap getEntireBitmap() {
        int finalWidth = getIntrinsicWidth();
        int finalHeight = getIntrinsicHeight();
        Bitmap finalBitmap = Bitmap.createBitmap(finalWidth, finalHeight, Bitmap.Config.ARGB_8888);

        int height = 0;
        for (Bitmap tileBitmap : getBitmapList()) {
            int x = tileBitmap.getWidth();
            int y = tileBitmap.getHeight();
            int[] tilePixels = new int[x * y];
            tileBitmap.getPixels(tilePixels, 0, x, 0, 0, x, y);
            finalBitmap.setPixels(tilePixels, 0, x, 0, height, x, y);
            height += tileBitmap.getHeight();
        }
        return finalBitmap;
    }

    static class BitmapState extends ConstantState {
        private static final int DEFAULT_PAINT_FLAGS = Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG;
        private static final Paint DEFAULT_PAINT = new Paint(DEFAULT_PAINT_FLAGS);
        private static final int GRAVITY = Gravity.FILL;

        final ArrayList<Bitmap> bitmapList;

        final GlideDrawable previewDrawable;

        int targetDensity;
        Paint paint = DEFAULT_PAINT;

        public BitmapState(ArrayList<Bitmap> bitmapList, GlideDrawable previewDrawable) {
            this.bitmapList = bitmapList;
            this.previewDrawable = previewDrawable;
        }

        BitmapState(GlideTileBitmapDrawable.BitmapState other) {
            this(other.bitmapList, other.previewDrawable);
            targetDensity = other.targetDensity;
        }

        void setColorFilter(ColorFilter colorFilter) {
            mutatePaint();
            paint.setColorFilter(colorFilter);
        }

        void setAlpha(int alpha) {
            mutatePaint();
            paint.setAlpha(alpha);
        }

        // We want to create a new Paint object so we can mutate it safely.
        @SuppressWarnings("PMD.CompareObjectsWithEquals")
        void mutatePaint() {
            if (DEFAULT_PAINT == paint) {
                paint = new Paint(DEFAULT_PAINT_FLAGS);
            }
        }

        @Override
        public Drawable newDrawable() {
            return new GlideTileBitmapDrawable(this);
        }

        @Override
        public Drawable newDrawable(Resources res) {
            return new GlideTileBitmapDrawable(this);
        }

        @Override
        public int getChangingConfigurations() {
            return 0;
        }
    }
}
