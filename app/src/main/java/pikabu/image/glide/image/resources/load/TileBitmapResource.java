package pikabu.image.glide.image.resources.load;

import android.graphics.Bitmap;

import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.Util;

import java.util.ArrayList;

/**
 * Created on 04.10.2016.
 */

public class TileBitmapResource implements Resource<Bitmap> {

    private ArrayList<Bitmap> tileBitmapList;

    private Bitmap previewBitmap;

    private int size;

    public TileBitmapResource(ArrayList<Bitmap> tileBitmapList, Bitmap previewBitmap) {
        this.tileBitmapList = tileBitmapList;
        this.previewBitmap = previewBitmap;

        size += Util.getBitmapByteSize(previewBitmap);
        for (Bitmap tileBitmap : tileBitmapList) {
            size += Util.getBitmapByteSize(tileBitmap);
        }
    }

    public ArrayList<Bitmap> getTileBitmapList() {
        return tileBitmapList;
    }

    @Override
    public Bitmap get() {
        return previewBitmap;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void recycle() {
        previewBitmap.recycle();
        for (Bitmap tileBitmap : tileBitmapList) {
            tileBitmap.recycle();
        }
    }
}
