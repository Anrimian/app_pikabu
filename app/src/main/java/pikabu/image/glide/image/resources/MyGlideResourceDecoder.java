package pikabu.image.glide.image.resources;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.util.Log;

import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;
import com.bumptech.glide.util.ByteArrayPool;
import com.bumptech.glide.util.ExceptionCatchingInputStream;
import com.bumptech.glide.util.MarkEnforcingInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import pikabu.image.glide.image.resources.load.BitmapResourceWrapper;
import pikabu.image.glide.image.resources.load.TileBitmapResource;
import pikabu.image.glide.image.resources.load.TileBitmapResourceWrapper;

/**
 * Created on 03.10.2016.
 */
public class MyGlideResourceDecoder implements ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> {

    private static final int IMAGE_MAX_HEIGHT = 2000;
    private static final int TRIMMED_IMAGE_HEIGHT = 900;
    private static final int TILE_DEFAULT_HEIGHT = 2000;

    private static final int MARK_POSITION = 5 * 1024 * 1024;

    private static final String TAG = "MyGlideResourceDecoder";

    @Override
    public Resource<GifBitmapWrapper> decode(ImageVideoWrapper source, int w, int h) throws IOException {
        InputStream is = source.getStream();

        final ByteArrayPool byteArrayPool = ByteArrayPool.get();
        final byte[] bytesForStream = byteArrayPool.getBytes();

        RecyclableBufferedInputStream bufferedStream = new RecyclableBufferedInputStream(is, bytesForStream);
        ExceptionCatchingInputStream exceptionStream = ExceptionCatchingInputStream.obtain(bufferedStream);
        MarkEnforcingInputStream invalidatingStream = new MarkEnforcingInputStream(exceptionStream);
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            decodeStream(invalidatingStream, bufferedStream, options);

            int height = options.outHeight;
            if (height > IMAGE_MAX_HEIGHT) {
                TileBitmapResource tileBitmapResource = decodeLargeBitmap(bufferedStream);
                return new TileBitmapResourceWrapper(tileBitmapResource);
            } else {
                Bitmap bitmap = BitmapFactory.decodeStream(invalidatingStream);
                return new BitmapResourceWrapper(bitmap);
            }
        } finally {
            byteArrayPool.releaseBytes(bytesForStream);
            exceptionStream.release();
        }
    }

    private static Bitmap decodeStream(MarkEnforcingInputStream is, RecyclableBufferedInputStream bufferedStream,
                                       BitmapFactory.Options options) {
        if (options.inJustDecodeBounds) {
            is.mark(MARK_POSITION);
        } else {
            bufferedStream.fixMarkLimit();
        }

        final Bitmap result = BitmapFactory.decodeStream(is, null, options);

        try {
            if (options.inJustDecodeBounds) {
                is.reset();
            }
        } catch (IOException e) {
            Log.e(TAG, "Exception loading inDecodeBounds=" + options.inJustDecodeBounds
                    + " sample=" + options.inSampleSize, e);
        }

        return result;
    }

    private TileBitmapResource decodeLargeBitmap(InputStream inputStream) throws IOException {
        BitmapRegionDecoder regionDecoder = null;
        try {
            regionDecoder = BitmapRegionDecoder.newInstance(inputStream, false);
            int width = regionDecoder.getWidth();
            int height = regionDecoder.getHeight();
            ArrayList<Bitmap> bitmapList = new ArrayList<>();

            int decodedHeight = 0;
            while(decodedHeight < height) {
                int tileHeightToDecode;
                int remainingHeight = height - decodedHeight;
                if (remainingHeight >= TILE_DEFAULT_HEIGHT) {
                    tileHeightToDecode = TILE_DEFAULT_HEIGHT;
                } else {
                    tileHeightToDecode = remainingHeight;
                }
                Rect rect = new Rect(0, decodedHeight, width, decodedHeight + tileHeightToDecode);
                Bitmap smallBitmap = regionDecoder.decodeRegion(rect, null);
                if (smallBitmap == null) {
                    throw new IOException("error read tile bitmap from stream");
                }
                bitmapList.add(smallBitmap);

                decodedHeight += tileHeightToDecode;
            }
            Bitmap previewBitmap = regionDecoder.decodeRegion(new Rect(0, 0, width, TRIMMED_IMAGE_HEIGHT), null);
            return new TileBitmapResource(bitmapList, previewBitmap);
        } finally {
            if (regionDecoder != null) {
                regionDecoder.recycle();
            }
        }
    }

    @Override
    public String getId() {
        return MyGlideResourceDecoder.class.getName();
    }
}
