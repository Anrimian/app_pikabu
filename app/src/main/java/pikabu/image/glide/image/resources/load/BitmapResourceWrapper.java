package pikabu.image.glide.image.resources.load;

import android.graphics.Bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;

import pikabu.MyApplication;

/**
 * Created on 03.10.2016.
 */
public class BitmapResourceWrapper implements Resource<GifBitmapWrapper> {

    private BitmapResource bitmapResource;

    private GifBitmapWrapper bitmapWrapper;

    public BitmapResourceWrapper(Bitmap bitmap) {
        bitmapResource = new BitmapResource(bitmap, Glide.get(MyApplication.getAppContext()).getBitmapPool());
        bitmapWrapper = new GifBitmapWrapper(bitmapResource, null);
    }

    @Override
    public GifBitmapWrapper get() {
        return bitmapWrapper;
    }

    @Override
    public int getSize() {
        return bitmapResource.getSize();
    }

    @Override
    public void recycle() {
        bitmapResource.recycle();
    }
}
