package pikabu.image.glide.image.resources.target;

import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;

/**
 * Created on 04.10.2016.
 */

public class TileBitmapDrawableResource implements Resource<GlideDrawable> {

    private GlideTileBitmapDrawable tileBitmapDrawable;

    public TileBitmapDrawableResource(GlideTileBitmapDrawable tileBitmapDrawable) {
        this.tileBitmapDrawable = tileBitmapDrawable;
    }

    @Override
    public GlideDrawable get() {
        return tileBitmapDrawable;
    }

    @Override
    public int getSize() {
        return tileBitmapDrawable.getSize();
    }

    @Override
    public void recycle() {
    }
}
