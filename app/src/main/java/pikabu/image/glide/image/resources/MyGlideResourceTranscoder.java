package pikabu.image.glide.image.resources;

import android.graphics.Bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;

import java.util.ArrayList;

import pikabu.MyApplication;
import pikabu.image.glide.image.resources.load.TileBitmapResource;
import pikabu.image.glide.image.resources.target.GlideDrawableResource;
import pikabu.image.glide.image.resources.target.GlideTileBitmapDrawable;
import pikabu.image.glide.image.resources.target.TileBitmapDrawableResource;

/**
 * Created on 03.10.2016.
 */

public class MyGlideResourceTranscoder implements ResourceTranscoder<GifBitmapWrapper, GlideDrawable> {

    @Override
    public Resource<GlideDrawable> transcode(Resource<GifBitmapWrapper> toTranscode) {
        Resource<Bitmap> bitmapResource = toTranscode.get().getBitmapResource();
        if (bitmapResource instanceof TileBitmapResource) {
            TileBitmapResource tileBitmapResource = (TileBitmapResource) bitmapResource;
            ArrayList<Bitmap> tileBitmapList = tileBitmapResource.getTileBitmapList();
            Bitmap previewBitmap = tileBitmapResource.get();
            GlideTileBitmapDrawable tileBitmapDrawable = new GlideTileBitmapDrawable(tileBitmapList, previewBitmap);
            return new TileBitmapDrawableResource(tileBitmapDrawable);
        } else {
            Bitmap bitmap = bitmapResource.get();
            return new GlideDrawableResource(bitmap, Glide.get(MyApplication.getAppContext()).getBitmapPool());
        }
    }

    @Override
    public String getId() {
        return MyGlideResourceTranscoder.class.getName();
    }
}
