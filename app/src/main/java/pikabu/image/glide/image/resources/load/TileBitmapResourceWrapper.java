package pikabu.image.glide.image.resources.load;

import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;

/**
 * Created on 03.10.2016.
 */

public class TileBitmapResourceWrapper implements Resource<GifBitmapWrapper> {

    private GifBitmapWrapper gifBitmapWrapper;

    private TileBitmapResource tileBitmapResource;

    public TileBitmapResourceWrapper(TileBitmapResource tileBitmapResource) {
        this.tileBitmapResource = tileBitmapResource;
        gifBitmapWrapper = new GifBitmapWrapper(tileBitmapResource, null);
    }

    public TileBitmapResource getTileBitmapResource() {
        return tileBitmapResource;
    }

    @Override
    public GifBitmapWrapper get() {
        return gifBitmapWrapper;
    }

    @Override
    public int getSize() {
        return tileBitmapResource.getSize();
    }

    @Override
    public void recycle() {
        tileBitmapResource.recycle();
    }
}
