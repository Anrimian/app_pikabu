package pikabu.image.glide.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;

import pikabu.content.story.blocks.StoryContentBlock;
import pikabu.image.ImageSize;
import pikabu.image.LongStoryCallback;
import pikabu.image.glide.image.resources.MyGlideResourceDecoder;
import pikabu.image.glide.image.resources.MyGlideResourceTranscoder;
import pikabu.image.glide.image.resources.target.GlideTileBitmapDrawable;
import rx.Observable;

/**
 * Created on 02.10.2016.
 */

public class MyGlideImageLoader {

    public static DrawableRequestBuilder load(String link, Context ctx) {
        return Glide.with(ctx)
                .load(link)
                .decoder(new MyGlideResourceDecoder())
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .transcoder(new MyGlideResourceTranscoder())
                .diskCacheStrategy(DiskCacheStrategy.NONE);
    }

    public static void displayImage(String link, ImageView imageView) {
        load(link, imageView.getContext()).into(imageView);
    }

    public static MyImageViewTarget displayImage(final String link,
                                    final ImageView imageView,
                                    final ImageSize previewImageSize) {
        return displayImage(link, imageView, previewImageSize, false, null);
    }

    /**
     * for using in lists(support cancellation)
     */

    public static MyImageViewTarget displayImage(final String link,
                                    final ImageView imageView,
                                    final MyImageViewTarget imageViewTarget,
                                    final ImageSize previewImageSize) {
        return displayImage(link, imageView, imageViewTarget, previewImageSize, false);
    }

    public static MyImageViewTarget displayImage(final String link,
                                    final ImageView imageView,
                                    final ImageSize previewImageSize,
                                    final boolean showLongImage,
                                    final LongStoryCallback longStoryCallback) {
        MyImageViewTarget imageViewTarget = new MyImageViewTarget(imageView, previewImageSize, showLongImage, longStoryCallback);
        return displayImage(link, imageView, imageViewTarget, previewImageSize, showLongImage);
    }


    /**
     * for using in lists(support cancellation)
     */
    public static MyImageViewTarget displayImage(final String link,
                                    final ImageView imageView,
                                    final MyImageViewTarget imageViewTarget,
                                    final ImageSize previewImageSize,
                                    final boolean showLongImage) {
        final Context ctx = imageView.getContext();

        int previewHeight = previewImageSize.getPreviewHeight();
        if (previewHeight > StoryContentBlock.BLOCK_MAX_HEIGHT && !showLongImage) {
            previewHeight = StoryContentBlock.TRIMMED_BLOCK_HEIGHT;
            imageView.setClickable(false);
        }
        imageView.setMinimumHeight(previewHeight);
        imageView.setMinimumWidth(previewImageSize.getPreviewWidth());

        if (TextUtils.isEmpty(link)) {
            imageView.setBackgroundResource(R.drawable.empty_image_background);
            return imageViewTarget;
        }

        imageView.setBackgroundColor(ContextCompat.getColor(ctx, R.color.loading_image_color));
        load(link, imageView.getContext()).crossFade().into(imageViewTarget);
        return imageViewTarget;
    }

    public static Observable<Bitmap> getBitmapObservable(String link, Context ctx) {
        return Observable.create(subscriber -> load(link, ctx)
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        subscriber.onError(new ExceptionWithInteger(R.string.error_loading_image));
                    }

                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        Bitmap bitmap;
                        if (resource instanceof GlideTileBitmapDrawable) {
                            bitmap = ((GlideTileBitmapDrawable) resource).getEntireBitmap();
                        } else {
                            bitmap = ((GlideBitmapDrawable) resource).getBitmap();
                        }
                        subscriber.onNext(bitmap);
                    }
                }));
    }
}
