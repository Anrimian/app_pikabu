package pikabu.image.glide.progress;

import android.content.Context;
import android.util.AttributeSet;

import pikabu.utils.network.progress.LoadingObservableManager;
import pikabu.utils.network.progress.observers.ProgressBarLoadingObserver;

/**
 * Created on 13.10.2016.
 */

public class GlideProgressBarLoadingObserver extends ProgressBarLoadingObserver {

    public GlideProgressBarLoadingObserver(Context context) {
        super(context);
    }

    public GlideProgressBarLoadingObserver(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GlideProgressBarLoadingObserver(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void registerProgressBar(String link) {
        super.registerProgressBar(link, GlideLoadingObservableManager.getInstance());
    }
}
