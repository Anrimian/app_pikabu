package pikabu.image.glide.progress;

import pikabu.utils.network.progress.LoadingObservableManager;

/**
 * Created on 04.11.2016.
 */

public class GlideLoadingObservableManager extends LoadingObservableManager {
    private static LoadingObservableManager instance = new GlideLoadingObservableManager();

    private GlideLoadingObservableManager() {
    }

    public static LoadingObservableManager getInstance() {
        return instance;
    }
}
