package pikabu.image.zoom;

import android.content.Context;
import android.content.Intent;
import android.view.View;

/**
 * Created on 10.01.2016.
 */
public class ZoomImageOnClickListener implements View.OnClickListener {
    private Context ctx;
    private String previewImageLink;
    private String bigImageLink;

    public ZoomImageOnClickListener(Context ctx, String previewImageLink) {
        this.ctx = ctx;
        this.previewImageLink = previewImageLink;
    }

    public ZoomImageOnClickListener(Context ctx, String previewImageLink, String bigImageLink) {
        this.bigImageLink = bigImageLink;
        this.ctx = ctx;
        this.previewImageLink = previewImageLink;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(ctx, ZoomImageActivity.class);
        intent.putExtra(ZoomImageActivity.IMAGE_LINK, previewImageLink);
        intent.putExtra(ZoomImageActivity.BIG_IMAGE_LINK, bigImageLink);

        ctx.startActivity(intent);
    }
}
