package pikabu.image.zoom;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.target.ViewTarget;
import com.emirinay.pikabuapp.R;
import com.emirinay.tools.AndroidUtils;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrInterface;
import com.r0adkll.slidr.model.SlidrListener;
import com.r0adkll.slidr.model.SlidrPosition;

import pikabu.image.glide.image.MyGlideImageLoader;
import pikabu.image.glide.progress.GlideLoadingObservableManager;
import pikabu.utils.FileUtils;
import uk.co.senab.photoview.PhotoView;

/**
 * Created on 10.01.2016.
 */
public class ZoomImageActivity extends AppCompatActivity {
    public static final String IMAGE_LINK = "image_link";
    public static final String BIG_IMAGE_LINK = "big_image_link";
    private String imageLink;
    private String bigImageLink;

    private View backgroundView;
    private PhotoView imageView;
    private ImageProgressView progressViewBinder;

    private SlidrInterface slidrInterface;

    private boolean isSliderLocked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoom_view);

        SlidrConfig config = new SlidrConfig.Builder()
                .primaryColor(ContextCompat.getColor(this, R.color.status_bar_color))
                .secondaryColor(ContextCompat.getColor(this, R.color.zoom_image_status_bar_color))
                .position(SlidrPosition.VERTICAL)
                .listener(new MySlidrListener())
                .build();
        slidrInterface = Slidr.attach(this, config);

        backgroundView = findViewById(R.id.background_view);
        imageView = (PhotoView) findViewById(R.id.photo_view);
        imageView.getIPhotoViewImplementation().setMaximumScale(10.0f);
        imageView.setOnMatrixChangeListener(rect -> {
            if (imageView.getScale() > 1.0f) {
                if (!isSliderLocked) {
                    isSliderLocked = true;
                    slidrInterface.lock();
                }
            } else {
                if (isSliderLocked) {
                    isSliderLocked = false;
                    slidrInterface.unlock();
                }
            }
        });

        View progressStateView = findViewById(R.id.progress_state_view);
        progressViewBinder = new ImageProgressView(progressStateView);
        progressViewBinder.setTryAgainButtonOnClickListener(new LoadImageOnClickListener());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        imageLink = getIntent().getStringExtra(IMAGE_LINK);
        bigImageLink = getIntent().getStringExtra(BIG_IMAGE_LINK);
        loadImage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.image_actions_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.save_image: {
                saveImage();
                return true;
            }
            case R.id.share_link: {
                AndroidUtils.shareText(getHighestQualityImageLink(), this);
                return true;
            }
            case R.id.copy_link: {
                AndroidUtils.copyText(getHighestQualityImageLink(), this);
                return true;
            }
            case R.id.share_image: {
                AndroidUtils.loadAndShareImage(getHighestQualityImageLink(), this);
                return true;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private void saveImage() {
        Toast.makeText(this, R.string.saving, Toast.LENGTH_SHORT).show();
        FileUtils.loadAndStoreBitmap(getHighestQualityImageLink(), this);
    }

    private String getHighestQualityImageLink() {
        if (bigImageLink != null && !bigImageLink.isEmpty()) {
            return bigImageLink;
        }
        return imageLink;
    }

    private void loadImage() {
        if (imageLink != null && !imageLink.isEmpty()) {
            progressViewBinder.showProgress();
            progressViewBinder.registerLoadingObserver(imageLink, GlideLoadingObservableManager.getInstance());
            MyGlideImageLoader.load(imageLink, this)
                    .dontAnimate()
                    .listener(new ImageLoadingListener())
                    .into(new MyImageViewTarget(imageView));
        }
    }

    private class MyImageViewTarget extends ViewTarget<ImageView, GlideDrawable> {

        MyImageViewTarget(ImageView view) {
            super(view);
        }

        @Override
        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
            view.setImageDrawable(resource);
            if (bigImageLink != null && !bigImageLink.isEmpty()) {
                progressViewBinder.showProgress();
                progressViewBinder.registerLoadingObserver(bigImageLink, GlideLoadingObservableManager.getInstance());
                MyGlideImageLoader.load(bigImageLink, ZoomImageActivity.this)
                        .placeholder(resource)
                        .listener(new ImageLoadingListener())
                        .dontAnimate()
                        .into(imageView);
            }
        }
    }

    private class ImageLoadingListener implements RequestListener<String, GlideDrawable> {

        @Override
        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
            int messageId = R.string.connection_error;
            if (e instanceof NullPointerException) {
                messageId = R.string.decoder_error;
            }
            progressViewBinder.showMessage(messageId, true);
            return false;
        }

        @Override
        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
            progressViewBinder.hideAll();
            return false;
        }
    }

    private class LoadImageOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            loadImage();
        }
    }

    private class MySlidrListener implements SlidrListener {

        @Override
        public void onSlideChange(float percent) {
            backgroundView.setAlpha(percent);
        }

        @Override
        public void onSlideStateChanged(int state) {
        }

        @Override
        public void onSlideOpened() {

        }

        @Override
        public void onSlideClosed() {

        }
    }
}
