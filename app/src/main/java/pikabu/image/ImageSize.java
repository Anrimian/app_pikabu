package pikabu.image;

import java.io.Serializable;

/**
 * Created on 16.12.2015.
 */
public class ImageSize implements Serializable {
    private static transient final int DEFAULT_PICTURE_WIDTH = 600;
    private static transient final int DEFAULT_PICTURE_HEIGHT = 600;

    protected int previewWidth = DEFAULT_PICTURE_WIDTH;
    protected int previewHeight = DEFAULT_PICTURE_HEIGHT;

    public ImageSize() {
    }

    public ImageSize(int previewWidth, int previewHeight) {
        this.previewWidth = previewWidth;
        this.previewHeight = previewHeight;
    }

    public int getPreviewHeight() {
        return previewHeight;
    }

    public void setPreviewHeight(int previewHeight) {
        this.previewHeight = previewHeight;
    }

    public int getPreviewWidth() {
        return previewWidth;
    }

    public void setPreviewWidth(int previewWidth) {
        this.previewWidth = previewWidth;
    }
}
