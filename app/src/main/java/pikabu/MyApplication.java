package pikabu;

import android.app.Application;
import android.content.Context;

import com.emirinay.pikabuapp.R;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import pikabu.data.DB;

/**
 * Created on 13.07.2015.
 */

@ReportsCrashes(mailTo = "emirinay@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.error)
public class MyApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
        ACRA.init(this);
        DB.getInstance().init(this);
        Preferences.init(this);
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }
}
