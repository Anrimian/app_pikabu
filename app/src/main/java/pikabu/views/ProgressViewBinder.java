package pikabu.views;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;

import pikabu.utils.network.progress.LoadingObservableManager;
import pikabu.utils.network.progress.LoadingObserver;

/**
 * Created on 21.02.2016.
 */
public class ProgressViewBinder implements LoadingObserver {

    private ProgressBar progressBar;
    private TextView tvMessage;
    private View btnTryAgain;

    private String link;

    public ProgressViewBinder(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        tvMessage = (TextView) view.findViewById(R.id.tv_message);
        btnTryAgain = view.findViewById(R.id.btn_action);
    }

    public ProgressViewBinder(ProgressBar progressBar, TextView tvMessage, View btnTryAgain) {
        this.progressBar = progressBar;
        this.tvMessage = tvMessage;
        this.btnTryAgain = btnTryAgain;
    }

    public void setTryAgainButtonOnClickListener(View.OnClickListener listener) {
        btnTryAgain.setOnClickListener(listener);
    }

    public void hideAll() {
        progressBar.setVisibility(View.INVISIBLE);
        tvMessage.setVisibility(View.INVISIBLE);
        btnTryAgain.setVisibility(View.INVISIBLE);
    }

    public void showMessage(int messageId, boolean showTryAgainButton) {
        progressBar.setVisibility(View.INVISIBLE);
        tvMessage.setVisibility(View.VISIBLE);
        if (messageId != 0) {
            tvMessage.setText(messageId);
        }
        if (showTryAgainButton) {
            btnTryAgain.setVisibility(View.VISIBLE);
        } else {
            btnTryAgain.setVisibility(View.INVISIBLE);
        }
    }

    public void showProgress() {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        tvMessage.setVisibility(View.INVISIBLE);
        btnTryAgain.setVisibility(View.INVISIBLE);
    }

    public void registerLoadingObserver(String link, LoadingObservableManager observableManager) {
        this.link = link;
        observableManager.registerLoadingObserver(this);
    }

    @Override
    public void onLoaded(int progress) {
        progressBar.setIndeterminate(false);
        progressBar.setProgress(progress);
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public String getLink() {
        return link;
    }

    @Override
    public int getObserverId() {
        return hashCode();
    }
}
