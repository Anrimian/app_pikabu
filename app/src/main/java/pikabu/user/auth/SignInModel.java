package pikabu.user.auth;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;

import org.jsoup.nodes.Document;

import pikabu.AppExecutors;
import pikabu.user.UserModel;
import pikabu.user.savedcategories.parser.SavedCategoriesParser;
import pikabu.utils.network.Api;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class SignInModel {

    private State state;
	
	private SignInObservable mObservable;

    private int errorMessageId;

    public SignInModel() {
		mObservable = new SignInObservable();
	}

	public void registerObserver(final SignInObserver observer) {
		mObservable.registerObserver(observer);
		showState(state);
	}
	
	public void unregisterObserver(final SignInObserver observer) {
		mObservable.unregisterObserver(observer);		
	}

	public void showState(State newState) {
		state = (newState == null ? State.WAIT : newState);
		state.showState(mObservable);
	}

    private Subscription subscription;

	public void signIn(String userName, String password) {
		if (state == State.PROGRESS) {
			return;
		}
        showState(State.PROGRESS);
        subscription = Api.getLoginObservable(userName, password)
                .subscribeOn(AppExecutors.COMMENTS_SCHEDULER)
                .doOnNext(this::updateUserInfo)
                .doOnNext(this::updateSavedCategoriesInfo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(document -> {
                    showState(State.WAIT);
                    mObservable.onCompleted();
                }, throwable -> {
                    if (throwable instanceof ExceptionWithInteger) {
                        errorMessageId = ((ExceptionWithInteger) throwable).getMessageId();
                    } else {
                        errorMessageId = R.string.error;
                    }
                    showState(State.ERROR);
                });
	}

    private void updateUserInfo(Document doc) {
        UserModel.getInstance().getUserInfoModel().updateUserInfo(doc);
    }

    private void updateSavedCategoriesInfo(Document document) {
        SavedCategoriesParser.parseSavedCategories(document)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(categories -> {
                    UserModel.getInstance().getSavedCategoriesModel().setCategories(categories);
                });
    }

	public void stopSignIn() {
		if (state == State.PROGRESS && subscription != null) {
            subscription.unsubscribe();
		}
	}

    public int getErrorMessageId() {
        return errorMessageId;
    }

    public enum State {
        WAIT {
            @Override
            public void showState(SignInObservable observable) {
                observable.onWait();
            }
        },
        PROGRESS {
            @Override
            public void showState(SignInObservable observable) {
                observable.onProgress();
            }
        },
        ERROR {
            @Override
            public void showState(SignInObservable observable) {
                observable.onError();
            }
        };

        public void showState(SignInObservable observable) {
        }

    }
}
