package pikabu.user.auth;

public interface SignInObserver {
	void onWait();
	
	void onProgress();

	void onCompleted();

	void onError();
}
