package pikabu.user.auth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.emirinay.pikabuapp.R;

import pikabu.data.Links;

public class SignInActivity extends AppCompatActivity implements SignInObserver {
    private EditText etUserEmail;
    private EditText etUserPassword;
    private View btnSubmit;
    private ProgressBar pbSignInProgress;
    private TextView tvErrorMessage;
    
    private SignInModel model;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_in_view);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		setTitle(R.string.authorize);

		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		etUserEmail = (EditText) findViewById(R.id.et_user_email);
		etUserEmail.requestFocus();
        etUserEmail.setText("Emirinay@gmail.com");
		etUserEmail.setOnEditorActionListener(new EmailEditorListener());

		etUserPassword = (EditText) findViewById(R.id.et_user_password);
        etUserPassword.setText("pi14215");
		etUserPassword.setOnEditorActionListener(new PasswordEditorListener());
		
		pbSignInProgress = (ProgressBar) findViewById(R.id.pb_auth_progress);
		tvErrorMessage = (TextView) findViewById(R.id.tv_error_message);
		
		View btnReg = findViewById(R.id.btn_reg);
		btnReg.setOnClickListener(new UserRegOnClick());
		
		btnSubmit = findViewById(R.id.btn_auth);
		btnSubmit.setOnClickListener(new UserAuthOnClick());
		
		model = SignInModelContainer.getInstance(this);
		model.registerObserver(this);
	}	
	
	@Override
	protected void onDestroy() {		
		super.onDestroy();
		model.unregisterObserver(this);
		if (isFinishing()) {
		    model.stopSignIn();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home: {
			onBackPressed();
		    }
		}
		return super.onOptionsItemSelected(item);
	}

	public void signIn() {
		final String userName = etUserEmail.getText().toString();
		if (userName.isEmpty()) {				
			etUserEmail.setError(getString(R.string.empty_email));
			return;
		}
		final String password = etUserPassword.getText().toString();
		if (password.isEmpty()) {				
			etUserPassword.setError(getString(R.string.empty_password));
			return;
		}
		model.signIn(userName, password);	
	}
		
	private class UserRegOnClick implements OnClickListener {
		@Override
		public void onClick(View v) {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse(Links.LINK_REG));
			startActivity(browserIntent);	
		}		
	}
	
	@Override
	public void onWait() {
		showProgress(false);		
	}
	
	@Override
	public void onProgress() {
		showProgress(true);		
	}

	@Override
	public void onCompleted() {
		finish();		
	}

	@Override
	public void onError() {
		int messageId = model.getErrorMessageId();
		showProgress(false);
		tvErrorMessage.setText(messageId);
	}
	
	private void showProgress(final boolean show) {
		etUserEmail.setEnabled(!show);
		etUserPassword.setEnabled(!show);
		btnSubmit.setEnabled(!show);
		pbSignInProgress.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
		tvErrorMessage.setText(null);
	}

	private class UserAuthOnClick implements OnClickListener {

		@Override
		public void onClick(View v) {
			signIn();	
		}		
	}

    private class EmailEditorListener implements OnEditorActionListener {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            etUserPassword.requestFocus();
            return true;
        }
    }

    private class PasswordEditorListener implements OnEditorActionListener {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            signIn();
            return true;
        }
    }
}
