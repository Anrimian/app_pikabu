package pikabu.user.auth;

import android.database.Observable;



class SignInObservable extends Observable<SignInObserver> {
	
	public void onWait() {
		for (final SignInObserver observer : mObservers) {
			observer.onWait();
		}
	}
	
	public void onProgress() {
		for (final SignInObserver observer : mObservers) {
			observer.onProgress();
		}
	}

	public void onCompleted() {
		for (final SignInObserver observer : mObservers) {
			observer.onCompleted();
		}
	}

	public void onError() {
		for (final SignInObserver observer : mObservers) {
			observer.onError();
		}
	}
}
