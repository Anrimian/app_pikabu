package pikabu.user.auth;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created by Vlad on 14.07.2015.
 */
public class SignInModelContainer extends Fragment {

    private static final String AUTH_MODEL_TAG = "auth_model_tag";

    private SignInModel model;

    public static SignInModel getInstance(Context ctx) {
        FragmentManager fm = ((FragmentActivity) ctx).getSupportFragmentManager();
        SignInModelContainer container = (SignInModelContainer) fm
                .findFragmentByTag(AUTH_MODEL_TAG);
        if (container == null) {
            container = new SignInModelContainer();
            fm.beginTransaction().add(container, AUTH_MODEL_TAG).commit();
        }
        if (container.model == null) {
            container.model = new SignInModel();
        }
        return container.model;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}

