package pikabu.user.actions.comments;

import pikabu.content.Vote;

/**
 * Created on 18.11.2015.
 */
public interface UserCommentActionsObserver {

    void commentWasVoted(int storyId, int commentId, Vote vote);
}
