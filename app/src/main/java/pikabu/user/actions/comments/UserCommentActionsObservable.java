package pikabu.user.actions.comments;

import com.emirinay.tools.observer.WeakObservable;

import java.lang.ref.WeakReference;

import pikabu.content.Vote;

/**
 * Created on 18.11.2015.
 */
public class UserCommentActionsObservable extends WeakObservable<UserCommentActionsObserver> {

    public void commentWasVoted(int storyId, int commentId, Vote vote) {
        for (final WeakReference<UserCommentActionsObserver> refObserver : mObservers) {
            UserCommentActionsObserver observer = refObserver.get();
            if (observer != null) {
                observer.commentWasVoted(storyId, commentId, vote);
            } else {
                mObservers.remove(refObserver);
            }
        }
    }
}
