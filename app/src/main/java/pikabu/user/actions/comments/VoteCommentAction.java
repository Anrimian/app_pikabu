package pikabu.user.actions.comments;

import android.util.Log;

import java.io.IOException;

import pikabu.content.Vote;
import pikabu.data.DB;
import pikabu.utils.network.Api;
import pikabu.user.actions.Action;

/**
 * Created on 22.11.2015.
 */
public class VoteCommentAction extends Action {

    private int storyId;
    private int commentId;

    private Vote vote;

    private UserCommentActionsObservable observable;

    public VoteCommentAction(int storyId, int commentId, Vote vote, UserCommentActionsObservable observable) {
        this.vote = vote;
        this.storyId = storyId;
        this.commentId = commentId;
        this.observable = observable;
    }

    @Override
    public void notifyObservers() {
        observable.commentWasVoted(storyId, commentId, vote);
    }

    @Override
    public void prepareAction() throws IOException {
        Api.voteComment(storyId, commentId, vote);
    }

    @Override
    public void saveAction() {
        Log.d(getClass().getSimpleName(), "saveVoteAction");
        DB.getInstance().saveDeferredVoteCommentAction(this);
    }

    @Override
    public void removeAction() {
        Log.d(getClass().getSimpleName(), "saveVoteAction");
        DB.getInstance().removeDeferredVoteCommentAction(this);
    }

    public Vote getVote() {
        return vote;
    }

    public int getStoryId() {
        return storyId;
    }

    public int getCommentId() {
        return commentId;
    }
}
