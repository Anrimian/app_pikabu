package pikabu.user.actions.story;

import android.util.Log;

import java.io.IOException;

import pikabu.data.DB;
import pikabu.utils.network.Api;
import pikabu.user.actions.Action;

/**
 * Created on 23.11.2015.
 */
public class SaveStoryAction extends Action {

    private int storyId;
    private int category;
    private boolean save;
    private UserStoryActionsObservable observable;

    /**
     * save story
     * @param storyId
     * @param category
     * @param observable
     */
    public SaveStoryAction(int storyId, int category, UserStoryActionsObservable observable) {
        this.storyId = storyId;
        this.category = category;
        this.observable = observable;
        save = true;
    }

    /**
     * delete story
     * @param storyId
     * @param observable
     */
    public SaveStoryAction(int storyId, UserStoryActionsObservable observable) {
        this.storyId = storyId;
        this.observable = observable;
        save = false;
    }

    @Override
    public void notifyObservers() {
        observable.storyWasSaved(storyId, save);
    }

    @Override
    public void prepareAction() throws IOException {
        if (save) {
            Api.saveStory(storyId, category);
        } else {
            Api.deleteSavedStory(storyId);
        }
    }

    @Override
    public void saveAction() {
        Log.d(getClass().getSimpleName(), "saveSaveAction");
        DB.getInstance().saveDeferredSaveStoryAction(this);
    }

    @Override
    public void removeAction() {
        Log.d(getClass().getSimpleName(), "removeSaveAction");
        DB.getInstance().removeDeferredSaveStoryAction(this);
    }

    public boolean isSave() {
        return save;
    }

    public int getStoryId() {
        return storyId;
    }

    public int getCategory() {
        return category;
    }
}
