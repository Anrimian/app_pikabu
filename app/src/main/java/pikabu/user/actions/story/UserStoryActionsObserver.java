package pikabu.user.actions.story;

import pikabu.content.Vote;

/**
 * Created on 18.11.2015.
 */
public interface UserStoryActionsObserver {
    void storyWasVoted(int storyId, Vote vote);

    void storyWasSaved(int storyId, boolean saved);
}
