package pikabu.user.actions.story;

import com.emirinay.tools.observer.WeakObservable;

import java.lang.ref.WeakReference;

import pikabu.content.Vote;

/**
 * Created on 18.11.2015.
 */
public class UserStoryActionsObservable extends WeakObservable<UserStoryActionsObserver> {

    public void storyWasVoted(int storyId, Vote vote) {
        for (final WeakReference<UserStoryActionsObserver> refObserver : mObservers) {
            UserStoryActionsObserver observer = refObserver.get();
            if (observer != null) {
                observer.storyWasVoted(storyId, vote);
            } else {
                mObservers.remove(refObserver);
            }
        }
    }

    public void storyWasSaved(int storyId, boolean saved) {
        for (final WeakReference<UserStoryActionsObserver> refObserver : mObservers) {
            UserStoryActionsObserver observer = refObserver.get();
            if (observer != null) {
                observer.storyWasSaved(storyId, saved);
            } else {
                mObservers.remove(refObserver);
            }
        }
    }
}
