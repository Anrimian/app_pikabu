package pikabu.user.actions.story;

import android.util.Log;

import java.io.IOException;

import pikabu.data.DB;
import pikabu.content.Vote;
import pikabu.utils.network.Api;
import pikabu.user.actions.Action;

/**
 * Created on 22.11.2015.
 */
public class VoteStoryAction extends Action {
    private int voteStoryId;
    private Vote vote;
    private UserStoryActionsObservable observable;

    public VoteStoryAction(int voteStoryId, Vote vote, UserStoryActionsObservable observable) {
        this.vote = vote;
        this.voteStoryId = voteStoryId;
        this.observable = observable;
    }

    @Override
    public void notifyObservers() {
        observable.storyWasVoted(voteStoryId, vote);
    }

    @Override
    public void prepareAction() throws IOException {
        Api.voteStory(voteStoryId, vote);
    }

    @Override
    public void saveAction() {
        Log.d(getClass().getSimpleName(), "saveVoteAction");
        DB.getInstance().saveDeferredVoteStoryAction(this);
    }

    @Override
    public void removeAction() {
        Log.d(getClass().getSimpleName(), "saveVoteAction");
        DB.getInstance().removeDeferredVoteStoryAction(this);
    }

    public Vote getVote() {
        return vote;
    }

    public int getVoteStoryId() {
        return voteStoryId;
    }
}
