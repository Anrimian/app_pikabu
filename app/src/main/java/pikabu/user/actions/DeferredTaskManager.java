package pikabu.user.actions;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import pikabu.data.DB;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created on 20.11.2015.
 */
public class DeferredTaskManager {
    private volatile static DeferredTaskManager taskManager;

    private boolean isRun = false;

    private DeferredTaskManager() {
    }

    public static DeferredTaskManager getInstance() {
        if (taskManager == null) {
            synchronized (DeferredTaskManager.class) {
                if (taskManager == null) {
                    taskManager = new DeferredTaskManager();
                }
            }
        }
        return taskManager;
    }
    public void runDeferredTasks() {
        if (!isRun) {
            Log.d(getClass().getSimpleName(), "runDeferredTasks");
            Observable.create(subscriber -> {
                isRun = true;
                tryReleaseTasks(DB.getInstance().getDeferredSaveStoryActionList());
                tryReleaseTasks(DB.getInstance().getDeferredVoteStoryActionList());
                tryReleaseTasks(DB.getInstance().getDeferredVoteCommentActionList());
                isRun = false;
            }).subscribeOn(Schedulers.from(UserActions.getInstance().getUserActionExec()))
              .subscribe();
        }
    }

    private void tryReleaseTasks(ArrayList<Action> actionList) {
        for (Action action : actionList) {
            try {
                action.prepareAction();
                action.removeAction();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
