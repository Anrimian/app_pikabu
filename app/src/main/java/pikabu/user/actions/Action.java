package pikabu.user.actions;

import java.io.IOException;

/**
 * Created on 22.11.2015.
 */
public abstract class Action {

    public abstract void notifyObservers();

    public abstract void prepareAction() throws IOException;

    public abstract void saveAction();

    public abstract void removeAction();
}
