package pikabu.user.actions;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pikabu.content.Vote;
import pikabu.user.actions.comments.UserCommentActionsObservable;
import pikabu.user.actions.comments.UserCommentActionsObserver;
import pikabu.user.actions.comments.VoteCommentAction;
import pikabu.user.actions.story.SaveStoryAction;
import pikabu.user.actions.story.UserStoryActionsObservable;
import pikabu.user.actions.story.UserStoryActionsObserver;
import pikabu.user.actions.story.VoteStoryAction;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created on 17.11.2015.
 */
public class UserActions {
    private volatile static UserActions userActions;

    private static final int USER_ACTION_THREAD_NUMBER = 1;

    private ExecutorService userActionExec;

    private UserStoryActionsObservable storyActionsObservable;
    private UserCommentActionsObservable commentActionsObservable;

    private UserActions() {
        userActionExec = Executors.newFixedThreadPool(USER_ACTION_THREAD_NUMBER);
        storyActionsObservable = new UserStoryActionsObservable();
        commentActionsObservable = new UserCommentActionsObservable();
    }

    public static UserActions getInstance() {
        if (userActions == null) {
            synchronized (UserActions.class) {
                if (userActions == null) {
                    userActions = new UserActions();
                }
            }
        }
        return userActions;
    }

    public void registerStoryActionsObserver(UserStoryActionsObserver observer) {
        storyActionsObservable.registerObserver(observer);
    }

    public void registerCommentActionsObserver(UserCommentActionsObserver observer) {
        commentActionsObservable.registerObserver(observer);
    }

    public ExecutorService getUserActionExec() {
        return userActionExec;
    }

    public void voteStory(int storyId, Vote vote) {
        Action action = new VoteStoryAction(storyId, vote, storyActionsObservable);
        useAction(action);
    }

    public void saveStory(int storyId, int category) {
        Action action = new SaveStoryAction(storyId, category, storyActionsObservable);
        useAction(action);
    }

    public void deleteStory(int storyId) {
        Action action = new SaveStoryAction(storyId, storyActionsObservable);
        useAction(action);
    }

    public void voteComment(int storyId, int commentId, Vote vote) {
        Action action = new VoteCommentAction(storyId, commentId, vote, commentActionsObservable);
        useAction(action);
    }

    private void useAction(Action action) {
        action.notifyObservers();
        Observable.create(subscriber -> {
            DeferredTaskManager.getInstance().runDeferredTasks();
            try {
                action.prepareAction();
            } catch (IOException e) {
                e.printStackTrace();
                action.saveAction();
            }
        }).subscribeOn(Schedulers.from(userActionExec))
          .subscribe();
    }
}
