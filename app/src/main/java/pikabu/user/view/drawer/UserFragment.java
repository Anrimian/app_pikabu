package pikabu.user.view.drawer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emirinay.pikabuapp.R;

import pikabu.user.UserModel;
import pikabu.user.info.UserInfoObserver;

public class UserFragment extends Fragment implements UserInfoObserver {

    private UserModel userModel;
    private UserViewBinder userViewBinder;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View userInfoView = inflater.inflate(R.layout.left_drawer_user_info, container, false);
        userViewBinder = new UserViewBinder(userInfoView);
		return userInfoView;
	}

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userModel = UserModel.getInstance();
        userModel.getUserInfoModel().registerObserver(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        userModel.getUserInfoModel().unregisterObserver(this);
    }

    @Override
    public void onUpdateUserInfo() {//TODO use rx java observables
        getActivity().runOnUiThread(() -> {
            if (userModel.isLogin()) {
                userViewBinder.bindUser(userModel.getUserInfoModel().getUserInfo());
            } else {
                userViewBinder.showSignInView();
            }
        });
    }




}
