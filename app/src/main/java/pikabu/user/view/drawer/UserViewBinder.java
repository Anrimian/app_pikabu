package pikabu.user.view.drawer;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;

import java.util.Locale;

import pikabu.content.profile.view.ProfileActivity;
import pikabu.image.glide.image.MyGlideImageLoader;
import pikabu.user.info.UserInfo;
import pikabu.user.auth.SignInActivity;

/**
 * Created on 16.08.2016.
 */
public class UserViewBinder {

    private TextView tvUserName;
    private TextView tvUserRating;
    private TextView tvUserSubs;
    private ImageView ivUserPicture;
    private TextView tvError;

    private View btnAuth;
    private View selectionView;

    private UserInfo userInfo;

    public UserViewBinder(View userInfoView) {
        tvUserName = (TextView) userInfoView.findViewById(R.id.tvUserName);
        tvUserRating = (TextView) userInfoView.findViewById(R.id.tvUserRating);
        tvUserSubs = (TextView) userInfoView.findViewById(R.id.tvUserSubs);
        ivUserPicture = (ImageView) userInfoView.findViewById(R.id.ivUserPicture);
        tvError = (TextView) userInfoView.findViewById(R.id.tv_error_parsing_user_data);
        selectionView = userInfoView.findViewById(R.id.selection);

        btnAuth = userInfoView.findViewById(R.id.btn_left_drawer_sign_in);
        btnAuth.setOnClickListener(new UserAuthOnClick());
    }

    public void bindUser(UserInfo userInfo) {
        this.userInfo = userInfo;
        if (userInfo.isEmptyUserInfo()) {
            showErrorView();
        } else {
            showUserInfo();
        }
    }

    public void showSignInView() {
        showUserInfo(false);
    }

    private void showErrorView() {
        selectionView.setOnClickListener(null);
        selectionView.setClickable(false);
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(R.string.error_parsing_user_data);
        showLoginUser(false);
        showLogoutUser(false);
    }

    private void showUserInfo() {
        showUserInfo(true);
        tvUserName.setText(userInfo.getUserName());
        tvUserRating.setText(getContext().getResources().getString(
                    R.string.rating, String.format(Locale.getDefault(), "%.0f", userInfo.getCarma())));

        int subsNum = userInfo.getSubsNum();
        tvUserSubs.setText(getContext().getResources().getString(
                    R.string.subscribes, subsNum == 0 ? getContext().getString(R.string.not_yet) : subsNum));
        MyGlideImageLoader.displayImage(userInfo.getPicture(), ivUserPicture);

        selectionView.setOnClickListener(v -> ProfileActivity.start(getContext(), userInfo.getUserName()));
        selectionView.setClickable(true);
    }

    private void showUserInfo(boolean show) {
        tvError.setVisibility(View.GONE);
        showLoginUser(show);
        showLogoutUser(!show);
    }

    private void showLoginUser(boolean show) {
        int visibility = show ? View.VISIBLE : View.GONE;
        tvUserName.setVisibility(visibility);
        tvUserRating.setVisibility(visibility);
        tvUserSubs.setVisibility(visibility);
        ivUserPicture.setVisibility(visibility);
    }

    private void showLogoutUser(boolean show) {
        int visibility = show ? View.VISIBLE : View.GONE;
        btnAuth.setVisibility(visibility);
        selectionView.setOnClickListener(null);
        selectionView.setClickable(false);
    }

    private Context getContext() {
        return tvUserName.getContext();
    }

    private class UserAuthOnClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            getContext().startActivity(new Intent(getContext(), SignInActivity.class));
        }
    }
}
