package pikabu.user.savedcategories;

import android.support.annotation.Nullable;

import java.util.List;

import pikabu.data.DB;
import pikabu.main.sections.saved.stories.SaveCategory;

/**
 * Created on 15.12.2016.
 */

public class SavedCategoriesModel {

    public static final SaveCategory DEFAULT_SAVE_CATEGORY = new SaveCategory(0, "Общее", 0);

    private SavedCategoriesObservable observable;
    private List<SaveCategory> categories;

    public SavedCategoriesModel() {
        observable = new SavedCategoriesObservable();
        loadCategories();
    }

    public void registerObserver(SavedCategoriesObserver observer) {
        observable.registerObserver(observer);
        observer.onUpdateSavedCategories();
    }

    public void unregisterObserver(SavedCategoriesObserver observer) {
        observable.unregisterObserver(observer);
    }

    public void setCategories(List<SaveCategory> newCategories) {
        if (newCategories.containsAll(categories) && categories.containsAll(newCategories)) {
            return;
        }
        categories = newCategories;
        saveCategories();
        observable.onUpdateSavedCategories();
    }

    public List<SaveCategory> getCategories() {
        return categories;
    }

    public void clear() {
        categories.clear();
        saveCategories();
        categories.add(DEFAULT_SAVE_CATEGORY);
    }

    private void loadCategories() {
        categories = DB.getInstance().getStorySaveCategories();
        if (categories.isEmpty()) {
            categories.add(DEFAULT_SAVE_CATEGORY);
        }
    }

    private void saveCategories() {
        DB.getInstance().saveStorySaveCategories(categories);
    }

    @Nullable
    public SaveCategory getCategoryById(int id) {
        for (SaveCategory saveCategory : categories) {
            if (saveCategory.getId() == id) {
                return saveCategory;
            }
        }
        return null;
    }
}
