package pikabu.user.savedcategories;

import com.emirinay.tools.observer.WeakObservable;

import java.lang.ref.WeakReference;

/**
 * Created on 29.10.2015.
 */
public class SavedCategoriesObservable extends WeakObservable<SavedCategoriesObserver> {

    public void onUpdateSavedCategories() {
        for (WeakReference<SavedCategoriesObserver> ref : mObservers) {
            SavedCategoriesObserver observer = ref.get();
            if (observer != null) {
                observer.onUpdateSavedCategories();
            } else {
                mObservers.remove(ref);
            }
        }
    }
}
