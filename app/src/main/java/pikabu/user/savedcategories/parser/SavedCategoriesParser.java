package pikabu.user.savedcategories.parser;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Document;

import java.util.List;

import pikabu.main.sections.saved.stories.SaveCategory;
import rx.Observable;

/**
 * Created on 17.12.2016.
 */

public class SavedCategoriesParser {

    public static Observable<List<SaveCategory>> parseSavedCategories(Document document) {
        return getJSONObjects(document).flatMap(SavedCategoriesParser::parseJsonObject).toList();
    }

    private static Observable<JSONObject> getJSONObjects(Document document) {
        return Observable.create(subscriber -> {
            try {
                String json = document.select(".ss-cat-list").attr("data-cats");
                JSONArray jsonArray = new JSONArray(json);
                for (int i = 0; i < jsonArray.length(); i++) {
                    subscriber.onNext(jsonArray.getJSONObject(i));
                }
            } catch (Exception e) {
                subscriber.onError(e);
            }
            subscriber.onCompleted();
        });
    }

    private static Observable<SaveCategory> parseJsonObject(JSONObject jsonObject) {
        return Observable.create(subscriber -> {
            try {
                int id = jsonObject.getInt("id");
                String name = jsonObject.getString("name");
                int count = jsonObject.getInt("count");
                subscriber.onNext(new SaveCategory(id, name, count));
            } catch (JSONException e) {
                e.printStackTrace();
                subscriber.onError(new ExceptionWithInteger(R.string.parser_error));
            }
            subscriber.onCompleted();
        });
    }
}
