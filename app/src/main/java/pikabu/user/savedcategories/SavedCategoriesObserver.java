package pikabu.user.savedcategories;

/**
 * Created on 29.10.2015.
 */
public interface SavedCategoriesObserver {
    void onUpdateSavedCategories();
}
