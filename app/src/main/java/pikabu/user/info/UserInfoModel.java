package pikabu.user.info;

import android.content.SharedPreferences;

import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Document;

import pikabu.Preferences;
import pikabu.user.UserModel;
import pikabu.user.info.parser.UserInfoParser;

/**
 * Created on 15.12.2016.
 */

public class UserInfoModel {

    private UserInfoObservable observable;
    private UserInfo userInfo;

    public UserInfoModel() {
        observable = new UserInfoObservable();
        userInfo = loadUserInfo();
    }

    public void registerObserver(UserInfoObserver observer) {
        observable.registerObserver(observer);
        observer.onUpdateUserInfo();
    }

    public void unregisterObserver(UserInfoObserver observer) {
        observable.unregisterObserver(observer);
    }

    public void updateUserInfo(@NotNull Document doc) {
        if (!UserModel.getInstance().isLogin()) {
            observable.onUpdateUserInfo();
        }
        UserInfo userInfo = UserInfoParser.parseUserData(doc);
        if (!this.userInfo.equals(userInfo)) {
            this.userInfo = userInfo;
            saveUserInfo();
            observable.onUpdateUserInfo();
        }
    }

    public void clear() {
        userInfo.clear();
        saveUserInfo();
        observable.onUpdateUserInfo();
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    private static final String SAVED_USER_NAME = "saved_user_name";
    private static final String SAVED_USER_PICTURE = "saved_user_picture";
    private static final String SAVED_USER_CARMA = "saved_user_carma";
    private static final String SAVED_USER_SUBS_NUM = "saved_user_subs_num";

    private void saveUserInfo() {
        SharedPreferences pref = Preferences.get();
        pref.edit()
                .putString(SAVED_USER_NAME, userInfo.getUserName())
                .putString(SAVED_USER_PICTURE, userInfo.getPicture())
                .putFloat(SAVED_USER_CARMA, userInfo.getCarma())
                .putInt(SAVED_USER_SUBS_NUM, userInfo.getSubsNum())
                .apply();
    }

    private UserInfo loadUserInfo() {
        SharedPreferences pref = Preferences.get();
        String userName = pref.getString(SAVED_USER_NAME, UserInfo.USER_NO_NAME);
        String picture = pref.getString(SAVED_USER_PICTURE, UserInfo.USER_NO_NAME);
        int subsNum = pref.getInt(SAVED_USER_SUBS_NUM, 0);
        float carma = pref.getFloat(SAVED_USER_CARMA, 0);
        return new UserInfo(userName, picture, subsNum, carma);
    }


}
