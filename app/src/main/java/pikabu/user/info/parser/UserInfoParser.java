package pikabu.user.info.parser;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.ExceptionWithInteger;
import com.emirinay.tools.TextUtils;

import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import pikabu.content.ParserException;
import pikabu.user.info.UserInfo;
import rx.Observable;

/**
 * Created on 27.10.2015.
 */
public class UserInfoParser {

    public static Observable<UserInfo> getUserDataObservable(@NotNull Document doc) {
        return Observable.create(subscriber -> {
            if (!isUserLogin(doc)) {
                subscriber.onError(null);
                return;
            }
            UserInfo userInfo = parseUserData(doc);
            if (userInfo != null) {
                subscriber.onNext(userInfo);
            } else {
                subscriber.onError(new ExceptionWithInteger(R.string.error_parsing_user_data));
            }
        });
    }

    private static boolean isUserLogin(Document doc) {
        return doc.select(".b-sign").toString().isEmpty();
    }

    public static UserInfo parseUserData(@NotNull Document doc) {
        try {
            UserInfoParser userInfoParser = new UserInfoParser(doc);
            return new UserInfo(userInfoParser.selectUserName(),
                    userInfoParser.selectUserPicture(),
                    userInfoParser.selectUserSubscribers(),
                    userInfoParser.selectUserCarma());
        } catch (Exception e) {
            e.printStackTrace();
            return new UserInfo();
        }
    }

    private Elements userInfo;
    private Document doc;

    private UserInfoParser(Document doc) {
        this.doc = doc;
        userInfo = doc.select(".b-user-menu__main");
    }

    private String selectUserName() throws ParserException {
        String userLink = userInfo.select(".b-user-menu__header a").attr("href");
        String userName = TextUtils.getUrlLastSegment(userLink);
        if (userName == null || userName.isEmpty()) {
            throw new ParserException("userName is empty: " + userInfo);
        }
        return userName;
    }

    private float selectUserCarma() throws ParserException, NumberFormatException {
        String carma = userInfo.select(".b-user-menu-list li").get(0).text();
        if (carma == null || carma.isEmpty()) {
            throw new ParserException("user carma is empty: " + userInfo);
        }
        return TextUtils.getFloatNumbersFromString(carma);
    }

    private int selectUserSubscribers() throws ParserException {
        String subsNum = userInfo.select(".b-user-menu-list li").get(1).text();
        if (subsNum == null || subsNum.isEmpty()) {
            throw new ParserException("user subsNum is empty: " + userInfo);
        }
        try {
            return TextUtils.getNumbersFromString(subsNum);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private String selectUserPicture() throws ParserException {
        String userPictureLink = doc.select(".b-user-menu__avatar img").attr("src");
        if (userPictureLink == null || userPictureLink.isEmpty()) {
            throw new ParserException("user picture link is empty: " + userInfo);
        }
        return userPictureLink;
    }
}
