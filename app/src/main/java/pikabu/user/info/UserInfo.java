package pikabu.user.info;

import org.jetbrains.annotations.NotNull;

/**
 * Created on 27.10.2015.
 */
public class UserInfo {

    public static final String USER_NO_NAME = "";
    public static final String USER_NO_PICTURE = "";

    private String userName = USER_NO_NAME;
    private String picture = USER_NO_PICTURE;
    private float carma;
    private int subsNum;

    public UserInfo(@NotNull String userName, @NotNull String picture, int subsNum, float carma) {
        this.userName = userName;
        this.picture = picture;
        this.subsNum = subsNum;
        this.carma = carma;
    }

    public UserInfo() {
    }

    public String getUserName() {
        return userName;
    }

    public String getPicture() {
        return picture;
    }

    public int getSubsNum() {
        return subsNum;
    }

    public float getCarma() {
        return carma;
    }

    public boolean isEmptyUserInfo() {
        return userName.equals(USER_NO_NAME);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!UserInfo.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final UserInfo other = (UserInfo) obj;
        if ((this.userName == null) ? (other.userName != null) : !this.userName.equals(other.userName)) {
            return false;
        }
        if ((this.picture == null) ? (other.picture != null) : !this.picture.equals(other.picture)) {
            return false;
        }
        if (this.subsNum != other.subsNum) {
            return false;
        }
        if (this.carma != other.carma) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = userName.hashCode();
        result = 31 * result + picture.hashCode();
        result = 31 * result + (carma != +0.0f ? Float.floatToIntBits(carma) : 0);
        result = 31 * result + subsNum;
        return result;
    }

    public void clear() {
        userName = USER_NO_NAME;
        picture = USER_NO_PICTURE;
        carma = Integer.MIN_VALUE;
        subsNum = Integer.MIN_VALUE;
    }
}
