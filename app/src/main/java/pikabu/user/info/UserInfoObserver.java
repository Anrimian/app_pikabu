package pikabu.user.info;

/**
 * Created on 29.10.2015.
 */
public interface UserInfoObserver {
    void onUpdateUserInfo();
}
