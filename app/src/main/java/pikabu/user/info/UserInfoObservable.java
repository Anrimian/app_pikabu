package pikabu.user.info;

import android.database.Observable;

/**
 * Created on 29.10.2015.
 */
public class UserInfoObservable extends Observable<UserInfoObserver> {

    public void onUpdateUserInfo() {
        for (UserInfoObserver observer : mObservers) {
            observer.onUpdateUserInfo();
        }
    }
}
