package pikabu.user;

import pikabu.user.info.UserInfoModel;
import pikabu.user.savedcategories.SavedCategoriesModel;
import pikabu.utils.network.MyHttpClient;

/**
 * Created on 27.10.2015.
 */
public class UserModel {

    private volatile static UserModel model;

    public static UserModel getInstance() {
        if (model == null) {
            synchronized (UserModel.class) {
                if (model == null) {
                    model = new UserModel();
                }
            }
        }
        return model;
    }

    private UserInfoModel userInfoModel;
    private SavedCategoriesModel savedCategoriesModel;

    private UserModel() {
        userInfoModel = new UserInfoModel();
        savedCategoriesModel = new SavedCategoriesModel();
    }

    public static boolean isUser(String name) {
        UserModel user = UserModel.getInstance();
        return user.isLogin() && user.userInfoModel.getUserInfo().getUserName().equals(name);
    }

    public UserInfoModel getUserInfoModel() {
        return userInfoModel;
    }

    public SavedCategoriesModel getSavedCategoriesModel() {
        return savedCategoriesModel;
    }

    public void logout() {
        MyHttpClient.getInstance().clearCookies();
        userInfoModel.clear();
        savedCategoriesModel.clear();
    }

    public boolean isLogin() {
        return MyHttpClient.getInstance().getCookie("phpDug2") != null;
    }

}
