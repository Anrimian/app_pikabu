package com.emirinay.tools;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.widget.Toast;

import com.emirinay.pikabuapp.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import pikabu.image.gif.loader.GifLoader;
import pikabu.image.gif.loader.GifResource;
import pikabu.image.glide.image.MyGlideImageLoader;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 07.05.2016.
 */
public class AndroidUtils {

    public static void copyText(@NonNull String text,@NonNull Context ctx) {
        ClipboardManager clipboard = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(text, text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(ctx, R.string.copy_success, Toast.LENGTH_SHORT).show();
    }

    public static void shareText(@NonNull String text,@NonNull Context ctx) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.setType("text/html");
        ctx.startActivity(Intent.createChooser(shareIntent, ctx.getString(R.string.share)));
    }

    public static void loadAndShareImage(String link, Context ctx) {
        MyGlideImageLoader.getBitmapObservable(link, ctx)
                .subscribeOn(AndroidSchedulers.mainThread())
                .flatMap(shareBitmap -> AndroidUtils.saveBitmapInShareCache(shareBitmap, ctx))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uri -> {
                    shareImage(uri, ctx);
                }, throwable -> {
                    Toast.makeText(ctx, ((ExceptionWithInteger) throwable).getMessageId(), Toast.LENGTH_SHORT).show();
                });
    }

    private static Observable<Uri> saveBitmapInShareCache(Bitmap bitmap, Context ctx) {
        return saveInShareCache((stream -> bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)),
                "share_image.png", ctx);
    }

    public static void loadAndShareGif(String link, Context ctx) {
        GifLoader.getInstance().getGifResourceObservable(link)
                .map(GifResource::getGifBytes)
                .flatMap(gifBytes -> saveGifInShareCache(gifBytes, ctx))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uri -> {
                    shareImage(uri, ctx);
                }, throwable -> {
                    Toast.makeText(ctx, ((ExceptionWithInteger) throwable).getMessageId(), Toast.LENGTH_SHORT).show();
                });
    }

    private static Observable<Uri> saveGifInShareCache(byte[] bytes, Context ctx) {
        return saveInShareCache((stream -> {
            try {
                stream.write(bytes);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }),"share_image.gif" , ctx);
    }

    private static Observable<Uri> saveInShareCache(FileCompressor fileCompressor, String fileName, Context ctx) {
        return Observable.create(subscriber -> {
            FileOutputStream stream = null;
            try {
                File cachePath = new File(ctx.getCacheDir(), "images");
                if (!cachePath.exists()){
                    if (!cachePath.mkdirs()) {
                        subscriber.onError(new ExceptionWithInteger(R.string.error_creating_cache));
                        return;
                    }
                }

                File shareFile = new File(cachePath, fileName);
                stream = new FileOutputStream(shareFile); // overwrites this image every time
                boolean compressedResult = fileCompressor.compress(stream);
                if (!compressedResult) {
                    subscriber.onError(new Exception(new ExceptionWithInteger(R.string.error_write_image)));
                }

                Uri uri = FileProvider.getUriForFile(ctx, "com.emirinay.pikabuapp.fileprovider", shareFile);
                if (uri == null) {
                    subscriber.onError(new Exception(new ExceptionWithInteger(R.string.error_accessing_cache)));
                    return;
                }
                subscriber.onNext(uri);
            } catch (FileNotFoundException e) {
                subscriber.onError(new ExceptionWithInteger(R.string.error_write_cache));
            } finally {
                IoUtils.closeSilently(stream);
            }
        });
    }

    interface FileCompressor {
        boolean compress(FileOutputStream fos);
    }

    public static void shareImage(Uri uri, Context ctx) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setDataAndType(uri, ctx.getContentResolver().getType(uri));
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        ctx.startActivity(Intent.createChooser(shareIntent, ctx.getString(R.string.share_image)));
    }
}
