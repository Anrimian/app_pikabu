package com.emirinay.tools.modelcache;

/**
 * Created on 05.08.2015.
 */
public class ModelCache extends android.support.v4.util.LruCache {

    public ModelCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected void entryRemoved(boolean evicted, Object key, Object oldValue, Object newValue) {
        if (oldValue instanceof AbstractModel) {
            ((AbstractModel) oldValue).onDestroy();
        }
        super.entryRemoved(evicted, key, oldValue, newValue);
    }
}
