package com.emirinay.tools.modelcache;

/**
 * Created on 05.08.2015.
 */
public interface AbstractModel {
    /**     *
     * Called when model is no longer in use;     *
     */
    void onDestroy();
}
