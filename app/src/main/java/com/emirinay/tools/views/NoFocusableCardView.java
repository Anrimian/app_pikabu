package com.emirinay.tools.views;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

/**
 * Created on 26.09.2015.
 */
public class NoFocusableCardView extends CardView {
    public NoFocusableCardView(Context context) {
        super(context);
    }

    public NoFocusableCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoFocusableCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean hasFocusable() {
        return false;
    }
}
