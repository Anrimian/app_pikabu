package com.emirinay.tools.views;

import com.emirinay.pikabuapp.R;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewStrokeText extends TextView {
	private int strokeColor = Color.TRANSPARENT;
	private int strokeWidth = 2;

	public TextViewStrokeText(Context context) {
		super(context);
	}

	public TextViewStrokeText(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.TextViewStrokeText);
		strokeColor = a.getColor(
				R.styleable.TextViewStrokeText_strokeColor, strokeColor);
		strokeWidth = a.getDimensionPixelSize(
				R.styleable.TextViewStrokeText_strokeWidth, strokeWidth);
		a.recycle();
	}

	@Override
	public void onDraw(Canvas canvas) {
		final ColorStateList textColor = getTextColors();

		TextPaint paint = this.getPaint();

		paint.setStyle(Style.STROKE);
		paint.setStrokeJoin(Join.ROUND);
		paint.setStrokeMiter(10);
		this.setTextColor(strokeColor);
		paint.setStrokeWidth(strokeWidth);

		super.onDraw(canvas);
		paint.setStyle(Style.FILL);

		setTextColor(textColor);
		super.onDraw(canvas);
	}
}