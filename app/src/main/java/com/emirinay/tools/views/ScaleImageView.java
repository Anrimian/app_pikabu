package com.emirinay.tools.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.ImageView;

public class ScaleImageView extends ImageView {
	Context context;

	public ScaleImageView(Context context) {
		super(context);
		this.context = context;
	}

	public ScaleImageView(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		this.context = context;
	}

	public ScaleImageView(Context context, AttributeSet attributeSet,
			int defStyle) {
		super(context, attributeSet, defStyle);
		this.context = context;
	}

	@Override
	public void setImageBitmap(Bitmap bitmap) {
		super.setImageBitmap(bitmap);
		if (bitmap != null) {
			DisplayMetrics displaymetrics = new DisplayMetrics();
			((Activity) context).getWindowManager().getDefaultDisplay()
					.getMetrics(displaymetrics);
			int width = displaymetrics.widthPixels;
			if (bitmap.getWidth() < width)
				setAdjustViewBounds(false);
		}

	}

}
