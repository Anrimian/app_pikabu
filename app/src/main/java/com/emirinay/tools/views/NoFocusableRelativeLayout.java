package com.emirinay.tools.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created on 22.09.2015.
 */
public class NoFocusableRelativeLayout extends RelativeLayout {

    public NoFocusableRelativeLayout(Context context) {
        super(context);
    }

    public NoFocusableRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoFocusableRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean hasFocusable() {
        return false;
    }
}
