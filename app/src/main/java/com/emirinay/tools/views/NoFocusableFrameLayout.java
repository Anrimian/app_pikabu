package com.emirinay.tools.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created on 21.09.2015.
 */
public class NoFocusableFrameLayout extends FrameLayout{

    public NoFocusableFrameLayout(Context context) {
        super(context);
    }

    public NoFocusableFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoFocusableFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean hasFocusable() {
        return false;
    }
}
