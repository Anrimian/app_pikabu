package com.emirinay.tools;

import android.support.annotation.Nullable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 25.07.2015.
 */
public class TextUtils {

    /**
     * Returns true if the string is null or 0-length.
     * @param str the string to be examined
     * @return true if str is null or zero length
     */
    public static boolean isEmpty(@Nullable CharSequence str) {
        if (str == null || str.length() == 0)
            return true;
        else
            return false;
    }

    public static CharSequence trimTrailingWhitespace(CharSequence source) {
        if (source == null)
            return "";
        int i = source.length();

        while (--i >= 0 && Character.isWhitespace(source.charAt(i))) {
        }
        return source.subSequence(0, i + 1);
    }

    public static String getUrlLastSegment(String src) {
        String[] segments = src.split("/");
        return segments[segments.length - 1];

    }

    public static int getNumbersFromString(String text) throws NumberFormatException{
        String textOnlyDigits = text.replaceAll("[^0-9,-]", "");
        return Integer.parseInt(textOnlyDigits);
    }

    public static float getFloatNumbersFromString(String text) throws NumberFormatException{
        String textOnlyDigits = text.replaceAll("[^0-9,-.]", "");
        return Float.parseFloat(textOnlyDigits);
    }

    public static String createLink(String text) {
        return "<a href='" + text + "'>" + text + " </a>";
    }

    public static String getStringBetween(String source, String startString, String endString) {
        int beginIndex = source.indexOf(startString) + startString.length();
        int endIndex = source.indexOf(endString, beginIndex);
        if (beginIndex >=0 && endIndex >= 0) {
            return source.substring(beginIndex, endIndex);
        } else {
            return null;
        }
    }

    public static String getNumEnding(int number, String[] endingArray) {
        String ending;
        number = number % 100;
        if (number >= 11 && number <= 19) {
            ending = endingArray[2];
        } else {
            switch (number % 10) {
                case (1):
                    ending = endingArray[0];
                    break;
                case (2):
                case (3):
                case (4):
                    ending = endingArray[1];
                    break;
                default:
                    ending = endingArray[2];
            }
        }
        return ending;
    }

    public static String getYoutubeVideoImage(String videoLink) {
        if (videoLink.contains("youtu")) {
            String videoId = extractYTId(videoLink);
            if (videoId != null) {
                return "http://img.youtube.com/vi/<id>/hqdefault.jpg".replace("<id>", videoId);
            }
        }
        return null;
    }

    public static String extractYTId(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
                "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()){
            vId = matcher.group(1);
        }
        return vId;
    }

}
