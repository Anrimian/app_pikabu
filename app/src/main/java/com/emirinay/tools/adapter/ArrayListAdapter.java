package com.emirinay.tools.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created on 11.08.2015.
 */
public abstract class ArrayListAdapter extends BaseAdapter{

    private ArrayList dataList;

    private Context ctx;

    public ArrayListAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public ArrayListAdapter(Context ctx, ArrayList dataList) {
        this.ctx = ctx;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        if (dataList != null) {
            return dataList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (dataList != null) {
            return dataList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Object data = dataList.get(position);
        View v;
        if (convertView == null) {
            v = newView(ctx, data, parent);
        } else {
            v = convertView;
        }
        bindView(v, ctx, data);
        return v;
    }

    public void setData(ArrayList dataList) {
        this.dataList = dataList;
    }

    protected abstract View newView(Context ctx, Object data, ViewGroup parent);

    protected abstract void bindView(View view, Context ctx, Object data);

}
