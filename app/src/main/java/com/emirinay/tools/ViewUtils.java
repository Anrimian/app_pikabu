package com.emirinay.tools;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;

/**
 * Created on 01.04.2016.
 */
public class ViewUtils {

    public static void imageViewAnimatedChange(Context ctx, final ImageView imageView, final int resourceId) {
        final Animation outAnim = AnimationUtils.loadAnimation(ctx, android.R.anim.fade_out);
        outAnim.setDuration(100);
        final Animation inAnim  = AnimationUtils.loadAnimation(ctx, android.R.anim.fade_in);
        inAnim.setDuration(100);
        outAnim.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {
                imageView.setImageResource(resourceId);
                inAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                imageView.startAnimation(inAnim);
            }
        });
        imageView.startAnimation(outAnim);
    }

    public static void animateFadeOutImageView(final ImageView imageView) {
        ObjectAnimator targetAnimator = ObjectAnimator.ofPropertyValuesHolder(imageView,
                PropertyValuesHolder.ofFloat("alpha", 0.5f, 0f),
                PropertyValuesHolder.ofFloat("scaleX", 1f, 1.7f),
                PropertyValuesHolder.ofFloat("scaleY", 1f, 1.7f));
        targetAnimator.setDuration(300);
        targetAnimator.start();
    }

    public static View getListItemByIndex(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }
}
