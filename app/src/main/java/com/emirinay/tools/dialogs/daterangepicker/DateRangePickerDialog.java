package com.emirinay.tools.dialogs.daterangepicker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.adapter.ViewPagerAdapter;

import java.util.Date;

/**
 * Created on 16.09.2016.
 */
public class DateRangePickerDialog extends DialogFragment implements DateChangeObserver {

    public static final String DATE_RANGE_FRAGMENT_TAG = "date_range_picker_dialog";

    public static void start(FragmentManager fragmentManager, DateRangeDialogListener listener) {
        DateRangePickerDialog dialog = new DateRangePickerDialog();
        dialog.setDialogListener(listener);
        dialog.show(fragmentManager, DATE_RANGE_FRAGMENT_TAG);
    }

    public static void subscribeOnListener(FragmentManager fragmentManager, DateRangeDialogListener listener) {
        DateRangePickerDialog dialog = (DateRangePickerDialog) fragmentManager.findFragmentByTag(DATE_RANGE_FRAGMENT_TAG);
        if (dialog != null) {
            dialog.setDialogListener(listener);
        }
    }

    private DateViewBinder dateViewBinderFrom;
    private DateViewBinder dateViewBinderTo;

    private CalendarFragment calendarFrom;
    private CalendarFragment calendarTo;

    private Button btnOk;
    private Button btnCancel;

    private DateRangeModel dateRangeModel;

    private DateRangeDialogListener dialogListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateRangeModel = DateRangeModelContainer.getInstance(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.date_range_picker_dialog_view, container);

        TextView tvMonthFrom = (TextView) view.findViewById(R.id.tv_date_month_from);
        TextView tvDayFrom = (TextView) view.findViewById(R.id.tv_date_day_from);
        TextView tvYearFrom = (TextView) view.findViewById(R.id.tv_date_year_from);
        dateViewBinderFrom = new DateViewBinder(tvDayFrom, tvMonthFrom, tvYearFrom);

        TextView tvMonthTo = (TextView) view.findViewById(R.id.tv_date_month_to);
        TextView tvDayTo = (TextView) view.findViewById(R.id.tv_date_day_to);
        TextView tvYearTo = (TextView) view.findViewById(R.id.tv_date_year_to);
        dateViewBinderTo = new DateViewBinder(tvDayTo, tvMonthTo, tvYearTo);

        dateRangeModel.registerObserver(this);

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        setupViewPager(viewPager);

        btnOk = (Button) view.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(btn -> {
            dialogListener.onComplete(new DateRange(
                    dateRangeModel.getDate(DateRangeModel.Tag.FROM),
                    dateRangeModel.getDate(DateRangeModel.Tag.TO)));
            dismiss();
        });

        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(btn -> dismiss());
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        calendarFrom = CalendarFragment.newInstance(DateRangeModel.Tag.FROM);
        calendarTo = CalendarFragment.newInstance(DateRangeModel.Tag.TO);
        adapter.addFragment(calendarFrom);
        adapter.addFragment(calendarTo);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dateRangeModel.unregisterObserver(this);
    }

    @Override
    public void onDateChanged(Date from, Date to) {
        dateViewBinderFrom.bindDate(from);
        dateViewBinderTo.bindDate(to);
    }

    public void setDialogListener(DateRangeDialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }
}
