package com.emirinay.tools.dialogs.daterangepicker;

import android.database.Observable;

import java.util.Date;

/**
 * Created on 19.09.2016.
 */
public class DateChangeObservable extends Observable<DateChangeObserver> {

    public void onDateChanged(Date from, Date to) {
        for (final DateChangeObserver observer : mObservers) {
            observer.onDateChanged(from, to);
        }
    }
}
