package com.emirinay.tools.dialogs.daterangepicker;

import java.util.Date;

/**
 * Created on 26.09.2016.
 */
public class DateRange {

    private Date from;
    private Date to;

    public DateRange(Date from, Date to) {
        if (from.getTime() < to.getTime()) {
            this.from = from;
            this.to = to;
        } else {
            this.from = to;
            this.to = from;
        }
    }

    public Date getFrom() {
        return from;
    }

    public Date getTo() {
        return to;
    }
}
