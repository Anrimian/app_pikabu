package com.emirinay.tools.dialogs.daterangepicker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import com.emirinay.pikabuapp.R;

import java.util.Calendar;

/**
 * Created on 18.09.2016.
 */
public class CalendarFragment extends Fragment {

    private static final String TAG = "tag";

    private DateRangeModel.Tag tag;

    private CalendarView calendarView;

    private DateRangeModel dateRangeModel;

    public static CalendarFragment newInstance(DateRangeModel.Tag tag) {
        CalendarFragment calendarFragment = new CalendarFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(TAG, tag);
        calendarFragment.setArguments(arguments);
        return calendarFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tag = (DateRangeModel.Tag) getArguments().getSerializable(TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(getContext(), R.layout.calendar_view, null);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        dateRangeModel = DateRangeModelContainer.getInstance(getContext());
        calendarView = (CalendarView) v.findViewById(R.id.calendar_view);
        calendarView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth, 0, 0);
            dateRangeModel.setNewDate(calendar.getTime(), tag);
        });
        calendarView.setDate(dateRangeModel.getDate(tag).getTime());
    }
}
