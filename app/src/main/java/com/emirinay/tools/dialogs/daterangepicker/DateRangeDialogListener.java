package com.emirinay.tools.dialogs.daterangepicker;

/**
 * Created on 26.09.2016.
 */
public interface DateRangeDialogListener {

    void onComplete(DateRange dateRange);
}
