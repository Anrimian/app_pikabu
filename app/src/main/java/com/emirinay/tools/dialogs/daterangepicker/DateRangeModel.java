package com.emirinay.tools.dialogs.daterangepicker;

import java.util.Calendar;
import java.util.Date;

/**
 * Created on 19.09.2016.
 */
public class DateRangeModel {

    private Date dateFrom;
    private Date dateTo;

    private DateChangeObservable dateChangeObservable;

    public DateRangeModel() {
        Date currentTime = Calendar.getInstance().getTime();
        dateFrom = currentTime;
        dateTo = currentTime;
        dateChangeObservable = new DateChangeObservable();
    }

    public void registerObserver(DateChangeObserver observer) {
        dateChangeObservable.registerObserver(observer);
        onDateChanged();
    }

    public void unregisterObserver(DateChangeObserver observer) {
        dateChangeObservable.unregisterObserver(observer);
    }

    public void setNewDate(Date date, Tag tag) {
        if (tag == Tag.FROM) {
            dateFrom = date;
        } else {
            dateTo = date;
        }
        onDateChanged();
    }

    public void setNewDate(Date from, Date to) {
        dateFrom = from;
        dateTo = to;
        onDateChanged();
    }

    public void onDateChanged() {
        dateChangeObservable.onDateChanged(dateFrom, dateTo);
    }

    public Date getDate(Tag tag) {
        if (tag == Tag.FROM) {
            return dateFrom;
        } else {
            return dateTo;
        }
    }

    public enum Tag {
        FROM,
        TO
    }
}
