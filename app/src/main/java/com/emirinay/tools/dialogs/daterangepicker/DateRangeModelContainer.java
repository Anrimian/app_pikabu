package com.emirinay.tools.dialogs.daterangepicker;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;


/**
 * Created on 19.09.2016.
 */
public class DateRangeModelContainer extends Fragment {

    private static final String AUTH_MODEL_TAG = "date_range_model_tag";

    private DateRangeModel model;

    public static DateRangeModel getInstance(Context ctx) {
        FragmentManager fm = ((FragmentActivity) ctx).getSupportFragmentManager();
        DateRangeModelContainer container = (DateRangeModelContainer) fm
                .findFragmentByTag(AUTH_MODEL_TAG);
        if (container == null) {
            container = new DateRangeModelContainer();
            fm.beginTransaction().add(container, AUTH_MODEL_TAG).commit();
        }
        if (container.model == null) {
            container.model = new DateRangeModel();
        }
        return container.model;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
