package com.emirinay.tools.dialogs.daterangepicker;

import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created on 22.09.2016.
 */
public class DateViewBinder {

    private TextView tvDay;
    private TextView tvMonth;
    private TextView tvYear;

    public DateViewBinder(TextView tvDay, TextView tvMonth, TextView tvYear) {
        this.tvDay = tvDay;
        this.tvMonth = tvMonth;
        this.tvYear = tvYear;
    }

    public void bindDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int year = calendar.get(Calendar.YEAR);
        tvYear.setText(String.valueOf(year));

        tvMonth.setText(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        tvDay.setText(String.valueOf(day));
    }
}
