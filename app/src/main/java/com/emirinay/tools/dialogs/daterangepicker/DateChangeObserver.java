package com.emirinay.tools.dialogs.daterangepicker;

import java.util.Date;

/**
 * Created on 18.09.2016.
 */
public interface DateChangeObserver {

    void onDateChanged(Date from, Date to);
}
