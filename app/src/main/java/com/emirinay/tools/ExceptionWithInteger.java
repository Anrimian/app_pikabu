package com.emirinay.tools;

/**
 * Created on 23.02.2016.
 */
public class ExceptionWithInteger extends Exception {

    private int messageId;

    public ExceptionWithInteger(int messageId) {
        this.messageId = messageId;
    }

    public int getMessageId() {
        return messageId;
    }
}
