package com.emirinay.tools.closeactivity;

import android.database.Observable;

/**
 * Created on 22.08.2015.
 */
public class CloseActivityObservable extends Observable<CloseActivityObserver> {

    void closeApp() {
        for(final CloseActivityObserver observer : mObservers) {
            observer.onCloseApp();
            unregisterObserver(observer);
        }
    }
}
