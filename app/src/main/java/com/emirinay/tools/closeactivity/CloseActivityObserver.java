package com.emirinay.tools.closeactivity;

/**
 * Created on 22.08.2015.
 */
interface CloseActivityObserver {

    void onCloseApp();
}
