package com.emirinay.tools.closeactivity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

/**
 * Created on 22.08.2015.
 */

public class CloseActivity {

    private final String RETAIN_FRAGMENT_TAG = "close_app_fragment";

    private volatile static CloseActivity closeActivity;

    private static CloseActivityObservable observable;

    private RetainFragment fragment;

    private boolean isInit = false;

    private CloseActivity() {
        observable = new CloseActivityObservable();
    }

    public static CloseActivity getInstance() {
        if (closeActivity == null) {
            synchronized (CloseActivity.class) {
                if (closeActivity == null) {
                    closeActivity = new CloseActivity();
                }
            }
        }
        return closeActivity;
    }

    public void init(Context ctx){
        if (!isInit) {
            fragment = new RetainFragment();
            ((FragmentActivity) ctx)
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .add(fragment, RETAIN_FRAGMENT_TAG)
                    .commit();
            isInit = true;
        }
    }

    public void registerObserver(CloseActivityObserver observer) {
        if (isInit) {
            observable.registerObserver(observer);
        }
    }

    public static class RetainFragment extends Fragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            observable.closeApp();
        }
    }
}
