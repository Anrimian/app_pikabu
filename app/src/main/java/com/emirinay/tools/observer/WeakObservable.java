package com.emirinay.tools.observer;

import java.lang.ref.WeakReference;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created on 17.10.2015.
 */
public class WeakObservable<T> {

    protected final CopyOnWriteArrayList<WeakReference<T>> mObservers = new CopyOnWriteArrayList<>();

    public void registerObserver(T observer) {
        if (observer == null) {
            throw new IllegalArgumentException("The observer is null.");
        }
        for (WeakReference refObserver : mObservers) {
            Object existObserver = refObserver.get();
            if (existObserver == null) {
                mObservers.remove(refObserver);
            }
        }
        mObservers.addIfAbsent(new WeakReference<>(observer));
    }

    public void unregisterObserver(T observer) {
        for (WeakReference refObserver : mObservers) {
            Object existObserver = refObserver.get();
            if (existObserver == observer || existObserver == null) {
                mObservers.remove(refObserver);
            }
        }
    }

    public boolean hasObservers() {
        return !mObservers.isEmpty();
    }
}
