package com.emirinay.tools.fragment.lists.recyclerview.savedposition;

import android.support.v7.widget.LinearLayoutManager;

/**
 * Created on 26.11.2016.
 */

public interface ViewInfo {

    LinearLayoutManager getLinearLayoutManager();
}
