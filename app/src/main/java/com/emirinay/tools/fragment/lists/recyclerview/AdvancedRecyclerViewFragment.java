package com.emirinay.tools.fragment.lists.recyclerview;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.emirinay.tools.fragment.lists.ListPosition;

/**
 * Created on 10.05.2016.
 */
public abstract class AdvancedRecyclerViewFragment extends Fragment {

    @Override
    public void onResume() {
        super.onResume();
        if (!isListEmpty()) {
            restoreListPosition();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        saveListPosition(getListPosition());
    }

    protected void restoreListPosition() {
        ListPosition listPosition = loadListPosition();
        if (listPosition != null) {
            moveToPosition(listPosition);
        }
    }

    protected void moveToPosition(@NonNull ListPosition listPosition) {
        int index = listPosition.getIndex();
        int top = listPosition.getTop();
        getLinearLayoutManager().scrollToPositionWithOffset(index, top);
    }

    protected ListPosition getListPosition() {
        int index = getLinearLayoutManager().findFirstVisibleItemPosition();
        View v = getLinearLayoutManager().getChildAt(0);
        int top = (v == null) ? 0 : v.getTop();
        return new ListPosition(index, top);
    }

    public abstract LinearLayoutManager getLinearLayoutManager();

    public abstract boolean isListEmpty();

    public abstract void saveListPosition(ListPosition listPosition);

    public abstract ListPosition loadListPosition();

}
