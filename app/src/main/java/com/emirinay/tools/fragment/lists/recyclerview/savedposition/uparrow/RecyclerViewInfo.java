package com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow;

import android.support.v7.widget.RecyclerView;

import com.emirinay.tools.fragment.lists.recyclerview.savedposition.ViewInfo;

/**
 * Created on 26.11.2016.
 */

public interface RecyclerViewInfo extends ViewInfo {

    RecyclerView getRecyclerView();
}
