package com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow;

/**
 * Created on 26.11.2016.
 */

public interface ArrowControllerInfo {

    boolean showArrow();
}
