package com.emirinay.tools.fragment.lists.recyclerview.endlesslist;

/**
 * Created on 18.11.2016.
 */

public interface EndlessListCallback {
    boolean canLoad();

    void loadNextData();
}
