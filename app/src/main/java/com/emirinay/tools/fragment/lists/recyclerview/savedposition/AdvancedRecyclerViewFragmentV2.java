package com.emirinay.tools.fragment.lists.recyclerview.savedposition;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.emirinay.tools.fragment.lists.ListPosition;

/**
 * Created on 10.05.2016.
 */
public abstract class AdvancedRecyclerViewFragmentV2 extends Fragment {

    @Override
    public void onResume() {
        super.onResume();
        if (!isListEmpty()) {
            restoreListPosition();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ListPosition listPosition = getListPosition();
        if (listPosition != null) {
            getListPositionModel().saveListPosition(listPosition);
        }
    }

    protected void restoreListPosition() {
        ListPosition listPosition = getListPositionModel().loadListPosition();
        if (listPosition != null) {
            moveToPosition(listPosition);
        }
    }

    protected void moveToPosition(@NonNull ListPosition listPosition) {
        int index = listPosition.getIndex();
        int top = listPosition.getTop();
        getLinearLayoutManager().scrollToPositionWithOffset(index, top);
    }

    protected @Nullable ListPosition getListPosition() {
        int index = getLinearLayoutManager().findFirstVisibleItemPosition();
        if (index == RecyclerView.NO_POSITION) {
            return null;
        } else {
            View v = getLinearLayoutManager().getChildAt(0);
            int top = (v == null) ? 0 : v.getTop();
            return new ListPosition(index, top);
        }
    }

    protected boolean isListEmpty() {
        return getLinearLayoutManager().getItemCount() == 0;
    }

    protected LinearLayoutManager getLinearLayoutManager() {
        return getViewInfo().getLinearLayoutManager();
    }

    public abstract ViewInfo getViewInfo();

    public abstract ListPositionModel getListPositionModel();


}
