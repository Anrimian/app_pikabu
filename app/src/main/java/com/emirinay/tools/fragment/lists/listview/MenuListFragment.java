package com.emirinay.tools.fragment.lists.listview;

import android.support.v4.app.ListFragment;

public class MenuListFragment extends ListFragment {

	@Override
	public void onResume() {
		super.onResume();
		setHasOptionsMenu(true);		
	}

	@Override
	public void onPause() {
		super.onPause();
		setHasOptionsMenu(false);

	}
}
