package com.emirinay.tools.fragment.lists.recyclerview;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.fragment.lists.ListPosition;

/**
 * Created on 10.05.2016.
 */
public abstract class UpArrowRecyclerViewFragment extends AdvancedRecyclerViewFragment {

    private ImageView ivArrow;

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        setHasOptionsMenu(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getRecyclerView().addOnScrollListener(new OnScrollListener());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (showArrow()) {
            inflater.inflate(R.menu.story_list_menu, menu);
            View actionView = menu.findItem(R.id.button_up).getActionView();
            ivArrow = (ImageView) actionView.findViewById(R.id.arrow);
            ListPosition oldPosition = getArrowListPosition();
            if (oldPosition != null) {
                ivArrow.setRotation(180);
                ivArrow.setContentDescription(getString(R.string.down));
            }
            actionView.setOnClickListener(view -> {
                int rotation = 180;
                int contentDescriptionId = R.string.up;
                ListPosition arrowPosition = getArrowListPosition();
                if (arrowPosition == null) {
                    arrowPosition = getListPosition();
                    setArrowListPosition(arrowPosition);
                    getLinearLayoutManager().scrollToPosition(0);
                } else {
                    rotation = 360;
                    moveToPosition(arrowPosition);
                    setArrowListPosition(null);
                    contentDescriptionId = R.string.down;
                }
                ivArrow.setContentDescription(getString(contentDescriptionId));
                ivArrow.animate().rotation(rotation);

            });
        }
    }

    private class OnScrollListener extends RecyclerView.OnScrollListener {

        private int scrollState = 0;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            this.scrollState = newState;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (scrollState == RecyclerView.SCROLL_STATE_SETTLING) {
                return;
            }
            ListPosition arrowListPosition = getArrowListPosition();
            if (arrowListPosition != null && ivArrow != null) {
                int index = arrowListPosition.getIndex();
                int firstVisibleItem = getLinearLayoutManager().findFirstVisibleItemPosition();
                if (firstVisibleItem > index) {
                    setArrowListPosition(null);
                    ivArrow.animate().rotation(360);
                    ivArrow.setContentDescription(getString(R.string.up));
                }
            }
        }
    }

    public abstract RecyclerView getRecyclerView();

    public abstract boolean showArrow();

    public abstract ListPosition getArrowListPosition();

    public abstract void setArrowListPosition(ListPosition listPosition);

}
