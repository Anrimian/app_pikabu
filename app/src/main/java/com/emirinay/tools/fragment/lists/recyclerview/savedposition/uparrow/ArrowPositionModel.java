package com.emirinay.tools.fragment.lists.recyclerview.savedposition.uparrow;

import com.emirinay.tools.fragment.lists.ListPosition;

/**
 * Created on 26.11.2016.
 */

public interface ArrowPositionModel {

    void setArrowListPosition(ListPosition listPosition);

    ListPosition getArrowListPosition();
}
