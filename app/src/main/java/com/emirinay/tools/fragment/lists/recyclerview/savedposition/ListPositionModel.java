package com.emirinay.tools.fragment.lists.recyclerview.savedposition;

import com.emirinay.tools.fragment.lists.ListPosition;

/**
 * Created on 26.11.2016.
 */

public interface ListPositionModel {

    void saveListPosition(ListPosition listPosition);

    ListPosition loadListPosition();
}
