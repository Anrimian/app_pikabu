package com.emirinay.tools.treelist;

/**
 * Created on 01.02.2016.
 */
public interface NodeData {
    Object getNodeId();
}