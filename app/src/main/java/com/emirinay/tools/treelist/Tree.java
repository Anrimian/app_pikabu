package com.emirinay.tools.treelist;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created on 29.01.2016.
 */
public class Tree {

    private int maxVisibleDataLevel;

    private Node root = new Node(null, null);

    private ArrayList<Node> visibleNodes = new ArrayList<>();

    private int nodeCount;

    public Tree() {
        this(Integer.MAX_VALUE);
    }

    public Tree(int maxVisibleDataLevel) {
        if (maxVisibleDataLevel < 0) {
            maxVisibleDataLevel = 0;
        }
        this.maxVisibleDataLevel = maxVisibleDataLevel;
    }

    /**
     *
     * @param nodeData node data
     * @param parent parent of node
     * @return created node
     */
    public Node createNode(Object nodeData, Node parent) {
        nodeCount++;
        if (parent == null) {
            parent = root;
        }

        Node node = new Node(nodeData, parent);

        boolean isCollapsed = node.getDataLevel() == maxVisibleDataLevel;
        int index = parent.addChild(node, isCollapsed);

        if (!node.hasCollapsedParent()) {
            visibleNodes.add(node.getIndex(), node);

            for (int i = index + 1; i < getVisibleNodeCount(); i++) {
                visibleNodes.get(i).setIndex(i);
            }
        }
        return node;
    }

    public void collapseNodes(Node parent) {
        if (parent == null || parent.isCollapsed() || parent.hasCollapsedParent()) {
            return;
        }
        int parentIndex = parent.getIndex();
        int visibleChildrenCount = parent.getVisibleChildrenCount();

        parent.collapseNodes();

        for (int i = parentIndex + 1; i < getVisibleNodeCount(); i++) {
            int replacedIndex = i + visibleChildrenCount;
            if (replacedIndex >= visibleNodes.size()) {
                break;
            }
            Node replacedElement = visibleNodes.get(i + visibleChildrenCount);
            if (replacedElement != null) {
                visibleNodes.set(i, replacedElement);
                replacedElement.setIndex(i);
            }
        }
    }

    public void expandNodes(Node parent) {
        if (parent == null || !parent.isCollapsed() || parent.hasCollapsedParent()) {
            return;
        }
        ArrayList<Node> nodes = parent.expandNodes();
        
        int parentIndex = parent.getIndex() + 1;
        visibleNodes.addAll(parentIndex, nodes);
        for (int i = parentIndex; i < getVisibleNodeCount(); i++) {
            visibleNodes.get(i).setIndex(i);
        }
    }

    public void expandBranch(Node nodeInBranch) {
        Node currentNode = nodeInBranch.getParent();
        Stack<Node> collapsedNodes = new Stack<>();
        while(currentNode != null) {
            if (currentNode.isCollapsed()) {
                collapsedNodes.push(currentNode);
            }
            currentNode = currentNode.getParent();
        }
        while(!collapsedNodes.isEmpty()) {
            Node collapsedNode = collapsedNodes.pop();
            expandNodes(collapsedNode);
        }
    }

    public int getVisibleNodeCount() {
        return root.getVisibleChildrenCount();
    }

    public Node getVisibleNodeOnPosition(int position) {
        if (position > getVisibleNodeCount() || position < 0) {
            throw new IndexOutOfBoundsException("Invalid position " + position + ", size: " + getVisibleNodeCount());
        }
        return visibleNodes.get(position);
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public boolean isEmpty() {
        return nodeCount == 0;
    }
}
