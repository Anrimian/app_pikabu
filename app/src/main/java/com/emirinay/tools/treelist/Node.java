package com.emirinay.tools.treelist;

import java.util.ArrayList;

/**
 * Created on 29.01.2016.
 */
public class Node<T> {

    private static final int COLLAPSED_NODE_INDEX = -1;
    private Node parent;

    private T data;

    private ArrayList<Node> childList = new ArrayList<>();

    private int childrenCount;
    private int visibleChildrenCount;

    private boolean hasCollapsedParent = false;
    private boolean isCollapsed = false;

    private int index = -1;

    private int dataLevel = -1;

    Node(T data, Node parent) {
        this.data = data;
        this.parent = parent;
        if (parent != null) {
            this.dataLevel = parent.getDataLevel() + 1;
        }
    }

    /**
     *
     * @param node node to add
     * @return index of added node
     */
    int addChild(Node node, boolean isCollapsed) {
        childList.add(node);
        incrementChildNumber();
        node.isCollapsed = isCollapsed;

        boolean hasCollapsedParent = isCollapsed() || hasCollapsedParent();
        node.hasCollapsedParent = hasCollapsedParent;
        node.setChildrenHasCollapsedParent(hasCollapsedParent);
        changeVisibleNodesCountBy(1);

        node.index = calculateIndexForNode(node);
        return node.index;
    }


    void collapseNodes() {
        changeVisibleNodesCountBy(-visibleChildrenCount);
        isCollapsed = true;
        setChildrenHasCollapsedParent(true);
    }

    ArrayList<Node> expandNodes() {
        isCollapsed = false;
        setChildrenHasCollapsedParent(false);
        ArrayList<Node> nodes = getExpandedChildList(new ArrayList<>());
        changeVisibleNodesCountBy(nodes.size());
        return nodes;
    }

    private void incrementChildNumber(){
        childrenCount++;
        if (parent != null) {
            parent.incrementChildNumber();
        }
    }

    private void changeVisibleNodesCountBy(int number) {
        if (!isCollapsed()) {
            visibleChildrenCount += number;
            if (parent != null) {
                parent.changeVisibleNodesCountBy(number);
            }
        }
    }

    private int calculateIndexForNode(Node node) {
        Node parent = node.getParent();
        if (node.hasCollapsedParent()) {
            return COLLAPSED_NODE_INDEX;
        }
        int parentChildNumber = parent.getVisibleChildrenCount();
        int parentIndex = parent.getIndex();
        return parentChildNumber + parentIndex;
    }

    private void setChildrenHasCollapsedParent(boolean hasCollapsedParent) {
        for (Node node : childList) {
            node.hasCollapsedParent = hasCollapsedParent;
            if (!node.isCollapsed()) {
                node.setChildrenHasCollapsedParent(hasCollapsedParent);
            }
        }
    }

    private ArrayList<Node> getExpandedChildList(ArrayList childList) {
        for (Node node : this.childList) {
            if (node.hasCollapsedParent()) {
                break;
            }
            childList.add(node);
            node.getExpandedChildList(childList);
        }
        return childList;
    }

    public boolean hasCollapsedParent() {
        return hasCollapsedParent;
    }

    public int getDataLevel() {
        return dataLevel;
    }

    public T getNodeData() {
        return data;
    }

    public Node getParent() {
        return parent;
    }

    public int getIndex() {
        return index;
    }

    void setIndex(int index) {
        this.index = index;
    }

    public int getChildrenCount() {
        return childrenCount;
    }

    public int getVisibleChildrenCount() {
        return visibleChildrenCount;
    }

    public boolean isCollapsed() {
        return isCollapsed;
    }

    public void setNodeData(T nodeData) {
        this.data = nodeData;
    }
}
