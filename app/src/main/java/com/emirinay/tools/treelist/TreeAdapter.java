package com.emirinay.tools.treelist;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Created on 29.01.2016.
 */
public abstract class TreeAdapter extends BaseAdapter {

    private Tree tree;

    private Context ctx;

    public TreeAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public TreeAdapter(Context ctx, Tree tree) {
        this.ctx = ctx;
        this.tree = tree;
    }

    @Override
    public int getCount() {
        if (tree != null) {
            return tree.getVisibleNodeCount();
        } else {
            return 0;
        }
    }

    @Override
    public Node getItem(int position) {
        if (tree != null) {
            return tree.getVisibleNodeOnPosition(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Node node = getItem(position);
        View v;
        if (convertView == null) {
            v = newView(ctx, node, parent);
        } else {
            v = convertView;
        }
        bindView(v, ctx, node);
        return v;
    }

    public void setData(Tree dataList) {
        this.tree = dataList;
    }

    protected abstract View newView(Context ctx, Node node, ViewGroup parent);

    protected abstract void bindView(View view, Context ctx, Node node);

}
