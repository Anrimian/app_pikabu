package com.emirinay.tools.time;

import android.content.Context;

import com.emirinay.pikabuapp.R;
import com.emirinay.tools.TextUtils;

public class FormatTimeHelper {
	private Context ctx;

	public FormatTimeHelper(Context ctx) {
		this.ctx = ctx;
	}

	/**
	 * format seconds to readable time
	 * 
	 * @param unixTime time in unix seconds
	 * @return time + min/hour/day + back
	 */
	
	public String formatTime(long unixTime) {
		long currentTime = System.currentTimeMillis();
		long seconds = (currentTime - unixTime * 1000L) / 1000;
		if (seconds <= 0) {
			return ctx.getResources().getString(R.string.just_now);
		}
		String[] endingArray = getEndingArray(seconds);
		int timeNumber = (int) getTimeNumber(seconds);//int
		StringBuilder formattedTime = new StringBuilder();
		formattedTime.append(timeNumber);
		formattedTime.append(" ");
		formattedTime.append(TextUtils.getNumEnding(timeNumber, endingArray));
		formattedTime.append(" ");
		formattedTime.append(ctx.getResources().getString(R.string.back));
		return formattedTime.toString();
	}

	private long getTimeNumber(long seconds) {
        long timeNumber;
		if (seconds <= 60) {
			timeNumber = seconds;
		} else if (seconds <= 3600) {
			timeNumber = seconds / 60;
		} else if (seconds <= 84600) {
			timeNumber = seconds / 3600;
		} else {
			timeNumber = seconds / 84600;
		}
		return timeNumber;
	}

	private String[] getEndingArray(long seconds) {
		String[] endingArray;
		if (seconds <= 60) {
			endingArray = ctx.getResources().getStringArray(R.array.seconds);
		} else if (seconds <= 3600) {
			endingArray = ctx.getResources().getStringArray(R.array.minutes);
		} else if (seconds <= 84600) {
			endingArray = ctx.getResources().getStringArray(R.array.hours);
		} else {
			endingArray = ctx.getResources().getStringArray(R.array.days);
		}
		return endingArray;
	}



}
